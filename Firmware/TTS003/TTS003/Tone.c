//------------------------------------------------------------------------------
//
// File Name: Tone.c
//
//	Date:  2 Aug 2018
//
// Content:	TriTone Transmitter TTS-003	Tone Service.
//
//
// Copyright 2018 Allied AnaLogic, Inc
// All rights reserved
//
//=============================================================================
// $Log:$
//=============================================================================
//-----------------------------------------------------------------------------
// Header file Includes
//-----------------------------------------------------------------------------
#include	<stdio.h>
#include	<avr/io.h>
#include "TTS003.h"
#include "Meter.h"
#include "lcd.h"
#include "math.h"
#include "string.h"
#include "Sound.h"
#include <avr/wdt.h>
#include <stdlib.h>

//==============================================================================
//	The below table contain Base Freq, screen string positions (x, y, w)
// and display tag. 
//
const struct LocateData AxData[AERIAL_FREQS]=
{
//	{1004, 4, 12, 36, "1 KHZ"},
	{987,  4, 20, 36, "987 HZ"},
	{577,  4, 28, 36, "577 HZ"},
};
//
//==============================================================================
//	The below table contain Tone Level Percentage of maximum output, screen
//	string positions (x, y, w) and display tag. 
//
const struct LocateData LvlData[LEVELS]=
{
	{100, 4, 12, 36, "MAX"},
	{50,  4, 20, 36, "HI"},
	{25,  4, 28, 36, "NORM"},
};
//
//==============================================================================
//

void Tone(void)
//------------------------------------------------------------------------------
// F1, F2, or F3 has been pressed while in the idle screen.
//
//		F1:			SMPLX-1T
//		F2:			SMPLX-3T
//		F3:			TriTone
//
//	K_FREQ, K_LEVEL, K_CFG will service function without returning.
//	K_F1, K_F2, K_F3 will change modes without returning.
//
//	Returns:
//				FKey == K_NONE		// If K_CLR was pressed. 
//------------------------------------------------------------------------------
{
	uint8_t Video;

	Video = STD;

	NewModeMessage("Tone");
	AllowedKeys = K_F1 | K_F2 | K_F3;
	SetSineFreq(SYSdata.Frequency);
	while(FKey != K_CLR)
	{
		switch(FKey)
		{
			case K_F1:
			{
				TestInfo.Terminal = SMPLX_1T;
				WriteFunctionWindow(pSMPLX_1T, WF1, REV);
				ToneTypeSimplex();				// Services K_FREQ, K_LEVEL, K_CFG
				break;
			}
			case K_F2:	
			{
				TestInfo.Terminal = SMPLX_3T;
				ToneTypeSimplex();				// Services K_FREQ, K_LEVEL, K_CFG
				break;
			}
			case K_F3:
			{
				TestInfo.Terminal = TRIPLEX;		// Services K_FREQ, K_LEVEL, K_CFG
				ToneTypeTriplex();
				if(FKey == K_F3)
				{
					FKey = K_F2;					// Simplex 3T.
				}
				break;
			}
		}
	}
}


void ToneTypeSimplex(void)
//------------------------------------------------------------------------------
//	Send Pulsed Simplex tone.
//
//	Read Vdc TR during idle time, and Vac T/Vac R in during active time.
//------------------------------------------------------------------------------
{
	NewWindow(DATA_W, SBORDER);
	//
	//	Setup tone routing to the outside world, and measurement path for TR DC.
	//
	AllowedKeys = K_F1 | K_F3;							// SMPLX_1T/3T and TriPlex.
	while(!(FKey & (HOTKEYS | K_F3)))				//	Return if TriPlex Requested.	
	{
		LoadXmitLevel();
		SetupSimplex();
		switch(FKey)
		{
			case K_F2:
			{
				FKey = 0;
				TestInfo.Terminal = SMPLX_3T;
				NewModeMessage("Simplex Tone");
				SetupDCMeterWindows();					// 3 Terminal Screen.
				DisplayFreqLevel();
				WriteFunctionWindow(pSMPLX_1T, WF1, STD);
				WriteFunctionWindow(pTRIPLEX, WF3, STD);
				TonePulser();
				break;	
			}
			case K_F1:
			{
				FKey = 0;
				TestInfo.Terminal = SMPLX_1T;
				NewWindow(FULL_W, SBORDER);
				LinearAnalogFace(&Mtr60);
				NewWindow(TALKBAT_W, NOBORDER);
				LCD_CtrStrg("DC TR",FONTVWX5, C_MIDDLE, STD);
				DisplayFreqLevel();
				WriteFunctionWindow(pSMPLX_3T, WF1, STD);
				WriteFunctionWindow(pTRIPLEX, WF3, STD);
				ToneTRBuzz();
				if(FKey == K_F1)
				{
					FKey = K_F2;
				}
				break;
			}
			case K_FREQ:
			case K_LEVEL:
			case K_CFG:
			{
				ServiceSetup();
			}
		}
		//	If Tone loop exit was due to HOTKEY, and not K_CLR, service.
		if(FKey & (K_FREQ | K_LEVEL | K_CFG))
		{
			ServiceSetup();
			//Continue where left off unless the CLR key was pressed.
			if(FKey != K_CLR)
			{
				if(TestInfo.Terminal == SMPLX_1T)	FKey = K_F1;
				else                                FKey = K_F2;
			}
		}		
	}
}



void ToneTypeTriplex(void)
//------------------------------------------------------------------------------
//	Send TriPlex tone on the default pair.
//
// Keypress options:
//		F1		TriPlex
//		F2		Metallic
//		F3		OLD/NEW Select			// FTS if system SPL XFER Enabled.
//------------------------------------------------------------------------------
{
	//	Initialize short detection and route tone & meter.
	//
	SetupDeltaVAC();	
	rSMPLX;									// TriPlex Mode.

	LoadXmitLevel();

	FKey = 0;
	AllowedKeys = HOTKEYS | K_F1 | K_F3;
	while(!(FKey & (AllowedKeys)))
	{
		NewModeMessage("TriPlex Tone");
		SetupDCMeterWindows();					// 3 Terminal Screen.
		DisplayFreqLevel();
		WriteFunctionWindow(pSMPLX_3T, WF3, STD);
		WriteFunctionWindow(pSMPLX_1T, WF1, STD);
		while(!(FKey & AllowedKeys))
		{
			TonePulser();
		}
		if(FKey & (K_FREQ | K_LEVEL | K_CFG))
		{
			ServiceSetup();
		}		
	}
}



//
//==============================================================================




void SetupSimplex(void)
//------------------------------------------------------------------------------
//	Setup for voltage measurements with weak" applied voltage.  If the 
//	measured line voltage delta deviates to a value closer to zero, sound the
//	beeper and display "BUZZ" in the appropriate box.  If the deviation has moved
//	back to "Vapplied" reset the BUZZ indicators.
//------------------------------------------------------------------------------
{

	sSMPLX;
	SetADCDiffGain(ADC_CH_MUXPOS_PIN1_gc, ADC_CH_GAIN_1X_gc);
	sG_EN;
}


void SetupDeltaVAC(void)
//------------------------------------------------------------------------------
//	Setup for voltage measurements for applied AC 577hz.  If the
//	measured line voltage delta deviates to a value closer to zero, sound the
//	beeper and display "BUZZ" in the appropriate box.  If the deviation has moved
//	back to "Vapplied" reset the BUZZ indicators.
//------------------------------------------------------------------------------
{

	switch(TestInfo.Terminal)
	{
		case TIP:
		{
			break;
		}
		case RING:
		{
			break;
		}
		case TR:
		{
			break;
		}
		case TRIPLEX:
		{
			rSMPLX;								// Invert Tip 180 degrees.
			break;
		}
	}

	//	Metering.
	//
	SetADCDiffGain(ADC_CH_MUXPOS_PIN1_gc, ADC_CH_GAIN_1X_gc);
	SetSineFreq(577);							// Set ToneGenerator Frequency.
}



void TonePulser(void)
//------------------------------------------------------------------------------
//	Pulse the Tone ON/OFF.  If SIMPLEX mode measure the DeltaDC to determine if
//	the pair finds a short, otherwise measure the DeltaAC for short/GND detection.
//
//			Sine generation is done via ISR service by TCD5 at 16 times per cycle.
//			The 1ms ISR determines when the length of ON/OFF times via "ToneTimer".
//------------------------------------------------------------------------------
{
//	ToneInfo.Mode = M_TONE;						// Line Sound.
	
	// DEBUG
	//TestInfo.SysStatFlag = DISABLE;

	//	Clear readings to allow first pass update.
	//
	Meter.Volts[TR]= Meter.Volts[RING] = Meter.Volts[TIP] = 0;
	LoadTonePattern(STD);						// Normal didit

	SetSineFreq(SYSdata.Frequency);
	while(!FKey)
	{
		TonePairStatus();
		KPstat(HOTKEYS | AllowedKeys);		// Temp switch to M_BEEP to ACK keypush...
		//
		//	Re-establish differential after battery check.
		SetADCDiffGain(ADC_CH_MUXPOS_PIN1_gc, ADC_CH_GAIN_1X_gc);

		while(XmitTone.Flag.Bit.EoS == OFF);
		if(TestInfo.Buzz)
		{
			LoadTonePattern(REV);
		}
		else
		{
			LoadTonePattern(STD);
		}
	}
}


void LoadTonePattern(uint8_t LineStatus)
//------------------------------------------------------------------------------
// Load the Transmit Tone parameters for On/Off timing and initiate the ISR
//	16x Freq service to generate a Sine Wave.
//
//	NOTE:	XmitSineTable[] has been loaded with Sine amplitude information. 
//------------------------------------------------------------------------------
{
	static uint16_t PreviousFreq;
	
//	while(ToneInfo.BeepStringFlag);
	
	if(LineStatus == STD)
	{
		XmitTone.Freq = SYSdata.Frequency;
		XmitTone.ActiveTm = 160;
		XmitTone.IdleTm = 160;
	}
	else
	{
		XmitTone.Freq = SYSdata.Frequency * 2;
		XmitTone.ActiveTm = 160;
		XmitTone.IdleTm = 160;
	}
	XmitTone.ActiveCnt = 0;
	XmitTone.IdleCnt = 0;
	XmitTone.Flag.Byte = OFF;
	if(PreviousFreq != XmitTone.Freq)
	{
		SetSineFreq(XmitTone.Freq);
		PreviousFreq = XmitTone.Freq;		
	}
	//	Turn on the ISR service for Transmit tone generation.
	//
	XmitTone.Flag.Bit.Enabled = ON;
	XmitTone.Flag.Bit.SoS = ON;
	TCD5.INTCTRLB |= TC45_CCBINTLVL_HI_gc;
}



void TonePairStatus(void)
//------------------------------------------------------------------------------
//	This process uses a delta "V" change and a minimum magnitude to determine
//	if a short/gnd condition exists.
//
// SIMPLEX Vdc Testing to determine a short:
// 
// PreviousWindow values can either be entirely positive, negative, or around
// Zero. 
//										:			Low        High
//												-----------------------
//			Previous Value Window (+)		+pw			+pw    % deviation window
//										 (-)		-pw			-pw
//										 (0)		-pw			+pw
//										 
//			SYSdata Window			 (+)		+cw			+cw
//										 (-)     -cw         -cw
//										 
// On entry:	XmitTone.Flag.Bit.SoS == ON
//------------------------------------------------------------------------------
{
	char String[15];
	uint8_t Color;
	int16_t Vcurrent, VppApplied;
	int16_t CDeltaZero, PDeltaZero,CPDelta, Min_DeltaAC;
	
	static uint8_t BuzzFlag = 0;
	static int16_t Vprevious = 0;

	
	switch(TestInfo.Terminal)
	{
		case SMPLX_1T:
		case SMPLX_3T:
		//------------------------------------------------------------------------
		//	Delta DC Check
		//------------------------------------------------------------------------
		//	Anything less than 2.0 volts, make it zero to compensate for any
		//	minor charging or internal offset error.
		{
			GetToneACDC();
			Vcurrent = ToneInfo.DCVoltsTR;
			CDeltaZero = abs(Vcurrent);
			PDeltaZero = abs(Vprevious);
			CPDelta = Vcurrent - Vprevious;
			//
			//---------------------------------------------------------------------
			//	IDLE LINE TESTS...
			//---------------------------------------------------------------------
			//	Anything less than 10 volts signifies a Short.
			//	
			if(abs(Vcurrent) < 10)
			{
				ToneInfo.LineStatus = BUZZ;
			}
			//	If the Current is within the applied Vdc window, this must be an
			//	idle condition.
			//
//			else if(Vcurrent > 90 && Vcurrent < 125)	//	~ +9 -> +12.5Vdc Applied
			else if(Vcurrent > 18 && Vcurrent < 25)	//	~ +18 -> +25Vdc Applied
			{
				ToneInfo.LineStatus = IDLE;
			}
			//
			//---------------------------------------------------------------------
			//	ACTIVE LINE TESTS...
			//---------------------------------------------------------------------
			// If the change is significant, and the absolute value is closer to
			// ZERO... it is a BUZZ...   SHORTED.
			// 
			//	If the change is significant, and the absolute value is further from
			//	ZERO, then it must be IDLE... NOT SHORTED>
			//	
			else if((abs(CPDelta) > MIN_DELTA_DC) && (CDeltaZero < PDeltaZero))
			{
				ToneInfo.LineStatus = BUZZ;
			}
			else if((abs(CPDelta) > MIN_DELTA_DC) && (CDeltaZero > PDeltaZero))
			{
				ToneInfo.LineStatus = IDLE;
			}
			//
			//---------------------------------------------------------------------
			break;
		}

		case TRIPLEX:
		//------------------------------------------------------------------------
		//	Delta AC Check using Vpp * 10 to improve resolution in integer math.
		//------------------------------------------------------------------------
		{
			GetToneACDC();
			Vcurrent = ToneInfo.ACVoltsTR;

			VppApplied = SYSdata.VacTR >> SYSdata.LevelIndex;
			Min_DeltaAC = VppApplied/2;
			//
			//	Looking for a difference greater than %threshold to signal a state
			//	change.
			//
			CPDelta = Vcurrent - Vprevious;
			if(Vcurrent < (VppApplied * 15)/100)			// 15%
			{
				ToneInfo.LineStatus = BUZZ;
			}
			else if((Vcurrent > (VppApplied * 80)/100))	// 80%
			{
				ToneInfo.LineStatus = IDLE;
			}
			else if((abs(CPDelta) > Min_DeltaAC) && (CPDelta < 0))
			{
				ToneInfo.LineStatus = BUZZ;
			}
			else if((abs(CPDelta) > Min_DeltaAC) && (CPDelta > 0))
			{
				ToneInfo.LineStatus = IDLE;
			}
			break;
		}
	}
	Vprevious = Vcurrent;
	//===========================================================================
	// DISPLAY THE RESULTS
	//---------------------------------------------------------------------------
	// Display the TG Result
	// 
	if((TestInfo.Terminal == SMPLX_3T) || (TestInfo.Terminal == TRIPLEX))
	{
		if((ToneInfo.LineStatus != BuzzFlag) && (ToneInfo.LineStatus == BUZZ) 
																&& (TestInfo.Terminal == TIP))
		{
			NewWindow(TG_W, SBORDER);
			IndicateBUZZ();
		}
		else if((Meter.Volts[TIP] != ToneInfo.ACVoltsTip) || (ToneInfo.LineStatus != BuzzFlag))
		{
			Meter.Volts[TIP] = ToneInfo.ACVoltsTip;
			NewWindow(TG_W, SBORDER);
			sprintf(String, "%2.1f VAC", Meter.Volts[TIP]);
			LCD_CtrStrg(String, FONTVWX5, C_MIDDLE, STD);
		}
	}
	//---------------------------------------------------------------------------
	// Display the RING GND Result
	// 
	if((TestInfo.Terminal == SMPLX_3T) || (TestInfo.Terminal == TRIPLEX))
	{
		if((ToneInfo.LineStatus != BuzzFlag) && (ToneInfo.LineStatus == BUZZ) 
																&& (TestInfo.Terminal == RING))
		{
			NewWindow(RG_W, SBORDER);
			IndicateBUZZ();
		}
		else if((Meter.Volts[RING] != ToneInfo.ACVoltsRing) || (ToneInfo.LineStatus != BuzzFlag))
		{
			Meter.Volts[RING] = ToneInfo.ACVoltsRing;
			NewWindow(RG_W, SBORDER);
			sprintf(String, "%2.1f VAC", Meter.Volts[RING]);
			LCD_CtrStrg(String, FONTVWX5, C_MIDDLE, STD);
		}
	}
	//--------------------------------------------------------------------------- 
	// Display the TR Result if different from previous BuzzFlag.
	//
	if((TestInfo.Terminal == SMPLX_3T) || (TestInfo.Terminal == SMPLX_1T)
															|| (TestInfo.Terminal == TRIPLEX))
	{
		if((ToneInfo.LineStatus != BuzzFlag) && (ToneInfo.LineStatus == BUZZ)) 
		{
			LCD_OpenWindow(TR_W, SBORDER);
			IndicateBUZZ();
		}
		else if((Meter.Volts[TR] != ToneInfo.DCVoltsTR)); // || (ToneInfo.LineStatus != BuzzFlag))
		{
			LCD_OpenWindow(TR_W, SBORDER);
			if(TestInfo.Terminal == SMPLX_3T)
			{
				Meter.Volts[TR] = ToneInfo.DCVoltsTR;
				sprintf(String, "%2.0f VDC", Meter.Volts[TR]);
			}
			else
			{
//				sprintf(String, "%2.0f VAC", Meter.Volts[TR]/10);
				Meter.Volts[TR] = ToneInfo.ACVoltsTR;
				sprintf(String, "%2.0f VAC", Meter.Volts[TR]);
			}
			if(BuzzFlag == BUZZ)		Color = REV;
			else                    Color = STD;
			LCD_ClearWindow(NOBORDER, Color);
			LCD_CtrStrg(String, FONTVWX5, C_MIDDLE, Color);
		}
	}
	//
	//	Update Tone Pattern based on Current BuzzFlag versus Previous BuzzFlag.
	//
	if((ToneInfo.LineStatus != BuzzFlag) && (ToneInfo.LineStatus == IDLE))
	{
		TestInfo.Buzz = OFF;								// Normal Tone pattern.
	}
	BuzzFlag = ToneInfo.LineStatus;
}


void IndicateBUZZ(void)
//------------------------------------------------------------------------------
//	On entry the proper window is active.  Put the "BUZZ" word and Alert the user.
//------------------------------------------------------------------------------
{
	TestInfo.Buzz = ON;									// Change Tone Pattern
	LCD_ClearWindow(NOBORDER, REV);
	LCD_CtrStrg("BUZZ", FONTVWX5, C_MIDDLE, REV);
	SpkrAlert(ToneBuzzSound);							// Sound beeper Once..
}



void GetToneACDC(void)
//------------------------------------------------------------------------------
//	On entry Simplex or TriPlex tone is being sent. There are 4 time slots for
//	measurements, T0, T1, T2, and T3.  THe first will be AC TG/RG/TR, then DC TR.
//	AC measurements are made while XmitTone.SoS (StartofSequence)is high.  DC
//	is measured once EoT has been detected.
//
//	Return with measurements residing in "ToneInfo" structure.
//------------------------------------------------------------------------------
{
	volatile int16_t VdcBits;
	volatile uint16_t VacBits;
	uint16_t VACfs_ground, VACfs_TipRing, TripValue;
	float Volts;
	
	uint16_t HoldOff;
	
	//	The HoldOff is the alloted time for Comb filter to settle after switching
	//	terminal input.
	// 
	HoldOff = 68;
	A2Dresult.Samples = SAMPLES_PER_CYCLE;
	InitComb();
	//---------------------------------------------------
	//	if system is in calibrate mode bypass Comb filter.
	if(TestInfo.Cflag == OFF)
	{
		Comb.FilterAction = CMD_BANDPASS;
	}
	else
	{
		Comb.FilterAction = CMD_BYPASS;
	}
	//
	//---------------------------------------------------
	#ifdef SPEAKER
	sSPKR_EN;
	#endif
	//
	// First time slot is Tip to Ground measurement.
	//
	SetADCDiffGain(TG_PIN, ADC_CH_GAIN_1X_gc);
	SetISRcount2(HoldOff);
	while(ISRcountStatus2 == ACTIVE);
	PeakDetector();
	VacBits = A2Dresult.Vpp;
	Volts = ((1.65 * (float)(VacBits))/(ACT_FEGAIN *2048));
	ToneInfo.CalACVtip = Volts;
	ToneInfo.ACVoltsTip = (uint16_t)(Volts);
	TripValue = SYSdata.VacTip >> SYSdata.LevelIndex;
	if(ToneInfo.ACVoltsTip > TripValue)
	{
		ToneInfo.ACVoltsTip = NORM_VAC_GND >> SYSdata.LevelIndex;
	}
	
	//	Second time slot, Ring to Ground.
	//
	HoldOff = 32;
	SetADCDiffGain(RG_PIN, ADC_CH_GAIN_1X_gc);
	SetISRcount2(HoldOff);
	while(ISRcountStatus2 == ACTIVE);
	PeakDetector();
	VacBits = A2Dresult.Vpp;
	Volts = ((1.65 * (float)(VacBits))/(ACR_FEGAIN *2048));
	ToneInfo.CalACVring = Volts;
	ToneInfo.ACVoltsRing = (uint16_t)(Volts);
	TripValue = SYSdata.VacRing >> SYSdata.LevelIndex;
	if(ToneInfo.ACVoltsRing > TripValue)
	{
		ToneInfo.ACVoltsRing = NORM_VAC_GND >> SYSdata.LevelIndex;
	}
	//
	//	Third time slot is only used during TriTone Transmission, otherwise time
	//	slot 4 (Vdc) will be used.
	//
	SetADCDiffGain(TR_PIN, ADC_CH_GAIN_1X_gc);
	SetISRcount2(HoldOff);
	while(ISRcountStatus2 == ACTIVE);
	PeakDetector();
//	VppDetector();
	VacBits = A2Dresult.Vpp;
	Volts = ((1.65 * (float)(VacBits))/(ACTR_FEGAIN *2048));
	ToneInfo.CalACVTR = Volts;
	ToneInfo.ACVoltsTR = (int16_t)(Volts) ;
	TripValue = SYSdata.VacTR >> SYSdata.LevelIndex;
	if(ToneInfo.ACVoltsTR > TripValue)
	{
		ToneInfo.ACVoltsTR = NORM_VAC_TR >> SYSdata.LevelIndex;
	}

	// Fourth time slot must wait until XmitTone.EoT flag is set indicating the
	//	tone sequence is now in idle state before next burst.
	//
	while((XmitTone.Flag.Bit.EoT == OFF) & (!KyReg));
	
	Comb.FilterAction = CMD_BYPASS;					// 60Hz avg in VppDetector.
	SetADCDiffGain(TR_PIN, ADC_CH_GAIN_1X_gc);
	SetISRcount2(HoldOff);
	while(ISRcountStatus2 == ACTIVE);
	VdcBits = VppDetector();
	VdcBits -= 2048;
	Volts = (int)((1.65 * (float)(VdcBits))/(DCTR_FEGAIN *2048));
	ToneInfo.DCVoltsTR = (int16_t)Volts;
	//
	//	+/-Vdc has been applied to Tip/Ring.
	// Polarity check window.... anything greater than +20 Vdc logged as 20V
	// unless it happens to be greater than 24 Vdc.
	if((ToneInfo.DCVoltsTR > 0) && (ToneInfo.DCVoltsTR <= 24))
	{
		if(ToneInfo.DCVoltsTR > 20)		ToneInfo.DCVoltsTR = 20;			
	}
	//
	//	Turn off Comb sampling to decrease display interruptions.
	//
	//------------------------------------------------------------------------
	//	Stop all synchronous Comb Sampling.
	//
	ADCA.EVCTRL = ADC_EVACT_NONE_gc;					// Cancel all ADC Actions.
	ADCA.CTRLA = (0 << ADC_ENABLE_bp);				// Disable the ADC.
	ADCA.CH0.INTCTRL = ADC_CH_INTLVL_OFF_gc;		// No further ISR service.
	TCC5.CTRLA = TC45_CLKSEL_OFF_gc;
	//
	//------------------------------------------------------------------------
}

int16_t ACDCvalue(void)
//------------------------------------------------------------------------------
//	Activate the Comb filter and collect enough samples to determine Vpp and
//	averaged Vdc values.
//------------------------------------------------------------------------------
{
	volatile int16_t BitValue;
	float Volts;
		
	uint16_t HoldOff;
		
	//	The HoldOff is the alloted time for Comb filter to settle after switching
	//	terminal input.
	//
	HoldOff = 48;
	A2Dresult.Samples = SAMPLES_PER_CYCLE;
	InitComb();

	SetADCDiffGain(4, ADC_CH_GAIN_1X_gc);
	SetISRcount2(HoldOff);
	while(ISRcountStatus2 == ACTIVE);
	BitValue = VppDetector();
	Volts = (int)((1.65 * (float)(BitValue))/(DCTR_FEGAIN *2048));
	ToneInfo.DCVoltsTR = (int16_t)Volts * 10;
	//
	// Polarity check window.... anything greater than 10 Vdc logged as 10V
	// unless it happens to be greater than 14 Vdc.
	if((ToneInfo.DCVoltsTR > 0) && (ToneInfo.DCVoltsTR <= 140))
	{
		if(ToneInfo.DCVoltsTR > 100)		ToneInfo.DCVoltsTR = 100;
	}
	//
	//	Turn off Comb sampling to decrease display interruptions.
	//
	TCC5.CTRLA = TC45_CLKSEL_OFF_gc;
}

void ToneTRBuzz(void)
//------------------------------------------------------------------------------
//	This function is used for Quick BUZZ check while sending SIMPLEX tone.
//
//	The T-R Vdc voltage is displayed in the F1 function window.  The analog meter
//	has a range of 0-10 (absolute). When the line voltage drops below 10Volts
// a "SHORT" is indicated in the F1 Window and Tone Cadence changes.  F3 allows
// the user to return to
//------------------------------------------------------------------------------
{
	volatile int16_t Percent, Refresh, AvgPercent;
	int16_t BitValue;
	uint8_t InitDisplay;
	uint16_t HoldOff;
	
	float Volts, Travel;
	char WrkStrg[10];
	
	PreviousBstat = ~Bstat;						// Force update of Batterystatus.

	Refresh = ON;

//	ToneInfo.Mode = M_TONE;						// Line Sound.
	ToneInfo.StatFlag = OFF;

	//	Force first pass update.
	//
	InitDisplay = ON;
	PreviousPercent = 56;
	
	LoadTonePattern(STD);						// Normal didit
	AllowedKeys = K_F1 | K_F3;
	NewWindow(F2_W, SBORDER);
	
	sSMPLX;											// Simplex Transmission
	#ifdef SPEAKER
	sSPKR_EN;
	#endif
	HoldOff = 48;
	A2Dresult.Samples = SAMPLES_PER_CYCLE;
	Comb.FilterAction = CMD_BYPASS;					// 60Hz avg in VppDetector.
	while(!FKey)
	{
		//------------------------------------------------------------------------
		//	Update the Analog Display.  This segment of code is placed before the
		//	synchronous fetch to allow maximum display time before the next fetch
		//	requirement (upon completion of tone burst- quiet interval).
		//
		Percent = abs(ToneInfo.DCVoltsTR)/10;// *10;
		if(Percent > 100)
		{
			Percent = 100;
		}
			
		Travel =(float)(Percent - PreviousPercent) * .63;
		AvgPercent = PreviousPercent + (int)(Travel);

		//
		//	Update the display only if there is a change in measured values.
		//
		if((AvgPercent != PreviousPercent) || InitDisplay)
		{
			if((Volts > 20) && (Volts < 25))		Volts = 20.0;
			sprintf(WrkStrg, "%3.1f VDC", Volts);
			LCD_ClearWindow(NOBORDER, STD);
			LCD_CtrStrg(WrkStrg, FONTVWX5, C_RIGHT, STD);

			SwapWindow();
			AnalogNeedle(AvgPercent, &Mtr54);
			SwapWindow();
			PreviousPercent = AvgPercent;
		}
		//
		//	If any time left after display wait until end of Sequence to begin 
		//	another.
		while(XmitTone.Flag.Bit.EoS == OFF);
		if(TestInfo.Buzz)
		{
			LoadTonePattern(REV);
		}
		else
		{
			LoadTonePattern(STD);
		}
		//
		//************************************************************************
		// Wait until quiet period, allow settle time before initiating Vdc fetch.
		//
		//	The HoldOff is the alloted time to settle after switching terminal input.
		// 
		InitComb();
		while((XmitTone.Flag.Bit.EoT == OFF) & (!KyReg));
		SetADCDiffGain(TR_PIN, ADC_CH_GAIN_1X_gc);
		SetISRcount2(HoldOff);
		while(ISRcountStatus2 == ACTIVE);
		BitValue = VppDetector();
		BitValue -= 2048;
		Volts = (int)((1.65 * (float)(BitValue))/(DCTR_FEGAIN *2048));
		ToneInfo.DCVoltsTR = (int16_t)Volts * 10;
		//------------------------------------------------------------------------
		//	Stop all synchronous Comb Sampling.
		//
		ADCA.EVCTRL = ADC_EVACT_NONE_gc;					// Cancel all ADC Actions.
		ADCA.CTRLA = (0 << ADC_ENABLE_bp);				// Disable the ADC.
		ADCA.CH0.INTCTRL = ADC_CH_INTLVL_OFF_gc;		// No further ISR service.
		TCC5.CTRLA = TC45_CLKSEL_OFF_gc;
		//		
		//------------------------------------------------------------------------
		//
		//	+/-Vdc has been applied to Tip/Ring.
		// Polarity check window.... anything greater than +20 Vdc logged as 20V
		// unless it happens to be greater than 24 Vdc.
		if((ToneInfo.DCVoltsTR > 0) && (ToneInfo.DCVoltsTR <= 240))
		{
			if(ToneInfo.DCVoltsTR > 200)		ToneInfo.DCVoltsTR = 200;			
		}
		//
		//************************************************************************
/*		if((AvgPercent < 95) && (TestInfo.Buzz != BUZZ))
		{
			BuzzAcknowledge();
			TestInfo.Buzz = BUZZ;
		}
		else if((AvgPercent >= 95) && (TestInfo.Buzz == BUZZ))
		{
			TestInfo.Buzz = OFF;
		}
*/		if((AvgPercent < 15) && (TestInfo.Buzz != BUZZ))
		{
			BuzzAcknowledge();
			TestInfo.Buzz = BUZZ;
		}
		else if((AvgPercent >= 15) && (TestInfo.Buzz == BUZZ))
		{
			TestInfo.Buzz = OFF;
		}
		//
		//------------------------------------------------------------------------
		KPstat(HOTKEYS | AllowedKeys);		// Temp switch to M_BEEP to ACK keypush...
	}
//	ToneInfo.Mode = M_BEEP;						// Set mode for permanent exit.
	rSPKR_EN;
}


void BuzzAcknowledge(void)
//------------------------------------------------------------------------------
//	This function initializes the local/remote "beep" buzz acknowledge without
//	waiting for completion of Tone String.
//
//	NOTE: The calling program should disable speaker and update ToneInfo.Mode
//			once beep cycle has been completed.
//------------------------------------------------------------------------------
{
	#ifdef SPEAKER
	sSPKR_EN;
	#endif
	InitBeepSequence(ToneBuzzSound);
}


uint8_t SelectItem(uint8_t Item)
//-----------------------------------------------------------------------------
//	Based on "Item" allow user to select Freq or Level setting.
//
//	Each frequency/level will be highlight using up/dwn keys and selected once
// the "SET" key is pressed.
//-----------------------------------------------------------------------------
{
	volatile uint8_t Index, HighLight, LineItems, TestIndex, *pFreqIndex;
	struct LocateData *pCmdTable;
	
	FunctionSetUpWindow();
	LCD_OpenWindow(DATA_W, SBORDER);
	//	
	if(Item == FREQSEL)
	{
		NewModeMessage("Select Freq");
		pCmdTable = &AxData;
		LineItems = (sizeof(AxData))/ (sizeof (struct LocateData));
		HighLight = SYSdata.AxIndex;
		TestIndex = SYSdata.AxIndex;
		pFreqIndex = &SYSdata.AxIndex;
	}
	else if(Item == LEVELSEL)
	{
		NewModeMessage("Output Level");
		pCmdTable = &LvlData;
		LineItems = (sizeof(LvlData))/ (sizeof (struct LocateData));
		HighLight = SYSdata.LevelIndex;
		TestIndex = SYSdata.LevelIndex;
		pFreqIndex = &SYSdata.LevelIndex;
	}
	
	for(Index = 0; Index < LineItems; Index++)
	{
		DisplaySelection((pCmdTable +Index), NOBORDER);
	}
	DisplaySelection((pCmdTable +HighLight), SBORDER);
	
	//---------------------------------------------------------------------------
	//	Information has been displayed.  Move the highlighted choice until the
	//	"SET" key is pressed.
	FKey = K_NONE;
	while(!FKey)
	{
		Wait4Key(HOTKEYS  | K_UP | K_DOWN | K_SET | AllowedKeys);
		if(FKey & (K_UP | K_DOWN | K_SET))
		{
			switch(FKey)
			{
				case K_UP:
				{
					if(TestIndex)
					{
						TestIndex--;
					}
					else
					{
						TestIndex = LineItems -1;
					}
					FKey = 0;
					break;
				}
				case K_DOWN:
				{
					if(TestIndex < (LineItems -1))
					{
						TestIndex++;
					}
					else
					{
						TestIndex = 0;
					}
					FKey = 0;
					break;
				}
				case K_SET:
				{
					break;
				}
			}
			DisplaySelection((pCmdTable +HighLight), NOBORDER);
			DisplaySelection((pCmdTable +TestIndex), SBORDER);
			HighLight = TestIndex;
		}
	}
	return(TestIndex);
}


void DisplaySelection(struct LocateData *pType, uint8_t HighLight)
//-----------------------------------------------------------------------------
//	Update the "Indexed" test with or without border determined by "HighLight".
//-----------------------------------------------------------------------------
{
	uint8_t x,y,w, Height;
	
	Height = 9;
	x = (*pType).x;
	y = (*pType).y;
	w = (*pType).w;
	LCD_OpenWindow(x,y,Height,w, HighLight);
	if(HighLight == SBORDER) 	LCD_ClearWindow(NOBORDER, STD);
	else								LCD_ClearWindow(SBORDER,STD);
	LCD_CtrStrg((*pType).pMsg, FONTVWX5, C_LEFT, STD);
}

void Calibrate(void)
//------------------------------------------------------------------------------
//	Calibrate function:
//			- Balance Simplex
//			- Set AC reference values for Short/Gnd detection
//------------------------------------------------------------------------------
{
		
	while(!FKey)
	{
		
	}
	NewWindow(DATA_W, SBORDER);
	WriteFunctionWindow("NULL", WF1, STD);
	WriteFunctionWindow("VAC CAL", WF2, STD);
	Wait4Key(K_CLR | K_F1 | K_F2);
	switch(FKey)
	{
		case K_F1:
		{
			CalNULL();
			break;
		}
		case K_F2:
		{
			CalVAC();
			break;
		}
	}
}



	
void CalNULL(void)
//------------------------------------------------------------------------------
//	Adjustment for minimum VAC TR in Simplex transmission.
//------------------------------------------------------------------------------
{
	uint16_t TempBalance, TempLevel;
	char WrkStrg[20];
	

	NewWindow(DATA_W, SBORDER);
	SetupDCMeterWindows();					// 3 Terminal Screen.
	DisplayFreqLevel();
	FunctionSetUpWindow();
	NewModeMessage("T/R NULL");
	TempLevel = SYSdata.LevelIndex;
	SYSdata.LevelIndex = 0;					// Force Max output level.
	TestInfo.Cflag = ON;						// Comb.FilterAction = CMD_BYPASS;

	LoadXmitLevel();
	SetupSimplex();
	TestInfo.Terminal = SMPLX_3T;
	
	//---------------------------------------------------------------------------
	//	Send simplex tone, measure and display the TG/RG results, and allow the
	//	user to modify the Balance.
	//
	//	Save the results if the "SET" key is pressed.
	//
	TempBalance = SYSdata.BALpot;
	FKey = K_NONE;
	while(!FKey)
	{
		NewWindow(TG_W, SBORDER);
		sprintf(WrkStrg, "%d", SYSdata.BALpot);
		LCD_CtrStrg(WrkStrg, FONTVWX5, C_MIDDLE, STD);
		UpDatePotentiometer(SYSdata.BALpot);
		
		while(!FKey)
		{
			LoadTonePattern(STD);
			GetToneACDC();
			NewWindow(TR_W, SBORDER);
			sprintf(WrkStrg, "%2.2f VAC", ToneInfo.CalACVTR);
			LCD_CtrStrg(WrkStrg, FONTVWX5, C_MIDDLE, STD);
			while(!XmitTone.Flag.Bit.EoS);
			//
			//	Check for keys and update Battery indicator.
			KPstat(K_CLR  | K_UP | K_DOWN | K_SET);
			//
			//	Re-establish differential.
			SetADCDiffGain(ADC_CH_MUXPOS_PIN1_gc, ADC_CH_GAIN_1X_gc);
		}
		//
		//	Service Keypress.
		//
		if(FKey & (K_UP | K_DOWN | K_SET))
		{
			switch(FKey)
			{
				case K_UP:
				{
					SYSdata.BALpot++;
					break;
				}
				case K_DOWN:
				{
					SYSdata.BALpot--;
					break;
				}
				case K_SET:
				{
					TempBalance = SYSdata.BALpot;
					SetParity();
					break;
				}
			}
			FKey = K_NONE;
		}
	}
	//	Put the Balance control back in the event the CLR key was pressed after
	//	an adjustment was made without the SET key being pressed..
	//
	SYSdata.LevelIndex = TempLevel;
	SYSdata.BALpot = TempBalance;
	UpDatePotentiometer(SYSdata.BALpot);
	TestInfo.Cflag = OFF;						// Comb.FilterAction = CMD_BANDPASS;
}


void CalVAC(void)
//------------------------------------------------------------------------------
//	With leads OPEN, measure and log VAC R/T/TR in SYSdata for use during BUZZ
//	check.
//------------------------------------------------------------------------------
{
	uint16_t TempLevel;
	uint8_t Video;
	char WrkStrg[20];

	NewWindow(DATA_W, SBORDER);
	SetupDCMeterWindows();					// 3 Terminal Screen.
	DisplayFreqLevel();
	FunctionSetUpWindow();
	NewModeMessage("VAC Reference");
	TempLevel = SYSdata.LevelIndex;
	SYSdata.LevelIndex = 0;					// Force Max output level.
	TestInfo.Cflag = ON;						// Comb.FilterAction = CMD_BYPASS;

	LoadXmitLevel();
	SetupDCMeterWindows();					// 3 Terminal Screen.
	DisplayFreqLevel();
	TestInfo.Terminal = TRIPLEX;
	rSMPLX;
	
	//---------------------------------------------------------------------------
	//
	//	Save the results if the "SET" key is pressed.
	//
	FKey = K_NONE;
	while(!FKey)
	{
		LoadTonePattern(STD);
		GetToneACDC();
		Video = STD;
		if(ToneInfo.CalACVTR < NORM_VAC_TR)		Video = REV;
		LCD_OpenWindow(TR_W, SBORDER);
		LCD_ClearWindow(NOBORDER, Video);
		sprintf(WrkStrg, "%2.2f VAC", ToneInfo.CalACVTR);
		LCD_CtrStrg(WrkStrg, FONTVWX5, C_MIDDLE, Video);
			
		Video = STD;
		if(ToneInfo.CalACVtip < NORM_VAC_GND)		Video = REV;
		LCD_OpenWindow(TG_W, SBORDER);
		LCD_ClearWindow(NOBORDER, Video);
		sprintf(WrkStrg, "%2.2f VAC", ToneInfo.CalACVtip);
		LCD_CtrStrg(WrkStrg, FONTVWX5, C_MIDDLE, Video);
			
			
		Video = STD;
		if(ToneInfo.CalACVring < NORM_VAC_GND)		Video = REV;
		LCD_OpenWindow(RG_W, SBORDER);
		LCD_ClearWindow(NOBORDER, Video);
		sprintf(WrkStrg, "%2.2f VAC", ToneInfo.CalACVring);
		LCD_CtrStrg(WrkStrg, FONTVWX5, C_MIDDLE, Video);
		while(!XmitTone.Flag.Bit.EoS);
		KPstat(K_CLR | K_SET);
	}
	if(FKey == K_SET)
	//	Compute the minimum normalized voltage value for display in Tone routine.
	{
		SYSdata.VacTR = (uint16_t)(ToneInfo.CalACVTR * 0.9);		// 90% trip 
		SYSdata.VacTip = (uint16_t)(ToneInfo.CalACVtip * 0.9);
		SYSdata.VacRing = (uint16_t)(ToneInfo.CalACVring * 0.9);
		SetParity();
	}
	SYSdata.LevelIndex = TempLevel;
	TestInfo.Cflag = OFF;						// Comb.FilterAction = CMD_BANDPASS;
}
