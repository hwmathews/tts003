//------------------------------------------------------------------------------
//
//	File Name: main.c
//
//	Content:
//
// 	Date:  28 July 2018
//
// 	Second Generation Tone Sender utilizing ATxmega32E5.
//
//	Copyright 2018 Allied Analogic, Inc
//	All rights reserved
//
//==============================================================================
//
//	Version History:
//------------------------------------------------------------------------------


#include "TTS003.h"
#include "lcd.h"
#include "Meter.h"
#include "Sound.h"
#include <avr/io.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stddef.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>


//-----------------------------------------------------------------------------
// The below Sine Table is used to produce a sine wave of 16 steps, or 22.5
//	degrees per step.  Amplitude is controlled by taking a percentage of sine
//	table values moved into SRAM and loaded into the associated DAC during
//	SineWave generation.  The use of SRAM preloaded before use by the DAC
//	minimizes ISR execution time.
//
//	TCC1 counter updates the DAC 16 times per SineWave Cycle, therefore directly
//	controlling the frequency being generated.
//
//		TCC1 ISR interval = (1/(16 * Freq));
//
//	CLKper of 8MHz reduces system current.  TC pre-scaler is set for /1 which
//	yields 31.250KHz, therefore with Counter PERiod TOP 255 defines the
//	resolution.  CCA center is thus 128 allowing +/- 127 PWM set points
//	within the PERiod.
//
//		Given Desired Freq = 577hz, and TCC1 clocks @8MHz, the ISR counter
//		should update the PWM compare registers every 108.32uSec, or each
//		108.32uS/(1/8e6) = 866.56 counts of Timer1. (867 = 576.7Hz)
//
//-----------------------------------------------------------------------------
const int SineTable16[] = { 0, 783, 1447, 1891, 2047, 1891, 1447, 783,
0, -783, -1447, -1891, -2047, -1891, -1447, -783};


//-----------------------------------------------------------------------------
// ISR key push values of 0-10 are used to index into the table to produce an
// unsigned integer equivalent to a function key equate (see TTS003.h).  This
// method allows for hardware changes while providing a mask for keys based on
// allowed key presses within a function.
//-----------------------------------------------------------------------------
uint16_t KeyXlate[] =
{
	K_F3,								// Key Push Value = 0;
	K_F2,
	K_F1,
	K_SPR1,
	K_CFG,
	K_LEVEL,
	K_FREQ,							// 6
	K_SPR1,
	K_SPR2,
	K_SPR3,							// 9
	K_CLR,							// 10
};
char *pSMPLX_1T	= "SMPLX 1T";
char *pSMPLX_3T	= "SMPLX 3T";
char *pTRIPLEX		= "TRI-PLEX";
//char *pSMPLX_1T	= "SEND";
//char *pSMPLX_3T	= "PROBE";
//char *pTRIPLEX		= "METER";


//-----------------------------------------------------------------------------
//	D105 Cold Start
//-----------------------------------------------------------------------------
int main(void)

{
	InitSysClk();
	//
	//	Enable all levels of ISR's and RoundRobin so no starvations can occur if
	//	ISR is lowest level.
	//
	PMIC.CTRL = PMIC_LOLVLEX_bm | PMIC_MEDLVLEX_bm |PMIC_HILVLEX_bm | PMIC_RREN_bm;
	sei();

	InitPorts();
	InitTCC4();									//	TCC4.CCD LCD BackLight, TCCper 1ms. 
//	InitTCC5();	
	InitTCD5();									// 16x Sine ISR.
	InitSPI_C();								// Serial LCD and Bal pot.
	InitA2D();
	InitDAC_A();

	GetSYSdata();								// Move EEPROM in local SRAM.
	InitPWM_BackLight();

	SYSdata.CorrectionFlag = ON;			// Use Meter Correction Constants.
	WarmStart();
	SetISRcount2(3000);
	while(ISRcountStatus2 == ACTIVE)
	{
		KPstat(HOTKEYS);
	}
	SystemIdle();
}


void WarmStart(void)
//-----------------------------------------------------------------------------
//	Entry point for ColdStart, First PowerUp, or Restart after PowerDown.
//
//	Indicate sign ON messages while doing a system calibration.
//-----------------------------------------------------------------------------
{
	uint8_t Scale;
	
	FKey = 0;
	TestInfo.SysStatFlag = ENABLE;			// Check battery level in KP fetch.

	#ifdef SPEAKER
	LoadSpkrLevel(OFF);
	#endif

	sPWR_EN;											// Turn On System Power.
	LCD_init();										// Services InitSPI_C....
	UpDatePotentiometer(SYSdata.BALpot);

	LCD_OpenWindow(0,0,64,128,SBORDER);
	LCD_CtrStrg("AALogic",TB14,C_MIDDLE, STD);
	LCD_OpenWindow(0,32,31,128,NOBORDER);
	LCD_CtrStrg("www.AALogic.com",FONT6X8,C_MIDDLE, STD);
//	ToneInfo.Mode = M_BEEP;
	SpkrAlert(PwrUpSound);
	
	#ifdef 	WDTENABLED
	wdt_enable(WDT_PER_4KCLK_gc);		// Reset on calls to GetKP.
	#endif

}


void SystemIdle(void)
//------------------------------------------------------------------------------
//	After initialization, or completion of a function, the system will loop
//	here waiting for the next function command.  The following function commands
//	are supported within the D105 system:
//		Test()
//		AutoTest()
//		Scan()
//		Tone()
//		Talk()
//		Configure()
//		SPLFunction()
//------------------------------------------------------------------------------
{

	while(1)
	{
		//		ToneInfo.Mode = M_DISABLE;			// Silence, but allow Keypush beep.
		//		ReleaseAll();
		if(FKey == K_CLR)
		{
			FKey = 0;
			DelayMS(500);
			KPstat(HOTKEYS);
			if((FKey == K_CLR))
			{
				PowerDown();
			}
		}

		if(!(FKey & HOTKEYS))
		{
			IdleScreen();
			AllowedKeys = K_F1 | K_F2 | K_F3;
			Wait4Key(HOTKEYS | AllowedKeys);
		}
		//------------------------------------------------------------------------
		//	Clear existing window in the event we have returned due to a HOTKEY.
		//
		// No Action is allowed if operating on external battery.
		//
		if((FKey != K_CLR) && (Bstat != EXTPWR))
		{
			NewModeMessage(PRODUCTSTRING);
			LCD_OpenWindow(DATA_W, SBORDER);
			LCD_ClearWindow(NOBORDER, STD);

			switch(FKey)
			{
				case K_FREQ:
				case K_LEVEL:
				case K_CFG:
				{
					ServiceSetup();
					break;
				}
				//
				//	Freq, Level, and Cfg can be modified within each tone mode as well.
				case K_F1:
				case K_F2:
				case K_F3:
				{
					Tone();
					break;
				}
			}
		}

		// Allow POWER OFF anytime.
		//
		else
		{
			DelayMS(500);
			FKey = 0;
			DelayMS(500);
			KPstat(HOTKEYS);
			if((FKey == K_CLR))
			{
				PowerDown();
			}
			FKey = K_CLR;
		}
	}
}



void IdleScreen(void)
//------------------------------------------------------------------------------
//	Initialize the display for startup.
//------------------------------------------------------------------------------
{
	uint8_t Video;

	Video = STD;

	FKey = 0;
	LCD_ClearDspl();
	NewModeMessage(PRODUCTSTRING);

	LCD_OpenWindow(DATA_W, SBORDER);
	LCD_CtrStrg("AALogic",TB14,C_MIDDLE, Video);

	WriteFunctionWindow(pSMPLX_1T, WF1, Video);
	WriteFunctionWindow(pSMPLX_3T, WF2, Video);
	WriteFunctionWindow(pTRIPLEX, WF3, Video);
}

void ServiceSetup(void)
//------------------------------------------------------------------------------
// Service FREQ/LEVEL/CFG based on FKey value.
//------------------------------------------------------------------------------
{
	uint8_t Index;
	
	NewWindow(DATA_W, SBORDER);		// Clear the main screen.
	switch(FKey)
	{
		case K_FREQ:
		{
			Index = SelectItem(FREQSEL);
			if(FKey == K_SET)
			{
				SYSdata.AxIndex = Index;
				SYSdata.Frequency = AxData[Index].Fbase;
				SetParity();
			}
			break;
		}
		case K_LEVEL:
		{
			Index = SelectItem(LEVELSEL);
			if(FKey == K_SET)
			{
				SYSdata.LevelIndex = Index;
				SYSdata.ToneLevel = LvlData[Index].Fbase;
				LoadXmitLevel();
				SetParity();
			}
			break;
		}
		case K_CFG:
		{
			//Special entry into Calibrate by pressing CFG, then LEVEL within 750mS.
			//
			SetISRcount2(750);
			while(ISRcountStatus2 == ACTIVE)
			{
				KPstat(K_LEVEL);
			}
			if(FKey == K_LEVEL)
			{
				Calibrate();
			}
			else
			{
				Config();						// System Setup
			}
			break;
		}
	}
	if(FKey != K_CLR)		FKey = K_NONE;
}


void Wait4Key(uint16_t Mask)
//------------------------------------------------------------------------------
// Wait for a keypush.  Translate the binary keypush value to a bit assigned
// value.  If the value is contained within the mask return with value placed
// in the global register "FKey".
//
// Keypush value will be 1 to 16 relating to bit position d15 -> d0 as
// referenced in the keypush translation array "KeyXlate".
//-----------------------------------------------------------------------------
{
	uint16_t Function;

	FKey = 0;						// Zero the last keypress translation.
	KyReg = 0;
	while(1)
	{
		//--------------------------------------------------------------------------
		// While waiting for a keypress update the low battery indicator.
		//
		while(!KyReg)
		{
			#ifdef WDTENABLED
			// Reset the WatchDogTimer before timeout as setup in WarmStart.
			//
			wdt_reset();
			#endif

			if(TestInfo.SysStatFlag == ENABLE)
			{
				CheckBatteryVoltage();
			}
		}
		//
		//------------------------------------------------------------------------
		// A KeyPress has been detected, sound beeper if key is valid, then return
		// to the calling program.
		//
		RTC.CNT = 0;					// Restart the RTCCountdown till shutdown..
		if(FKey == K_CLR)				// System coming out of AutoShutdown...
		{
			KyReg = 0;
			return;
		}
		else
		{
			Function = GetKp(Mask);

			if(Function)
			{
				FKey = Function;
				return;
			}
		}
	}
}



uint16_t GetKp(uint16_t ValidKeys)
//-------------------------------------------------------------------------------
// Sound the Beeper and pickup the local keypush.  The Keypush will be convert
// an equivalent function key.  If key is invalid, return void.
//-------------------------------------------------------------------------------
{
	unsigned long FunctionKey;

	// Convert the binary key value to bit assigned key value.
	//
	FunctionKey = KyReg;
	FunctionKey &= ValidKeys;

	if(FunctionKey & ValidKeys)
	{
		AcknowledgeSound(KPsound);    // Valid keypush tone.
	}

	else
	{
		FunctionKey = 0;
		AcknowledgeSound(KPerror);    // InValid keypush tone.
	}

	KyReg = 0;
	return(FunctionKey);
}


void KPstat(uint16_t Mask)
//-----------------------------------------------------------------------------
//	Get the KeyPush if one is present.  Unlike "Wait4Key()" this function
// returns if the "Keys" regster is not active.  The "Mask" has the bit values
// that constitute a valid keypush.
//
// NOTE:	Do NOT call this function during critical timing functions.
//
// Uses Global "KyReg" for Return Value:
//       	KyReg   = any allowed keypush value (defined in "system.h").
//				KyReg = void, no active keys
//          KyReg   = void, non-allowed keypush as defined by "Mask".
//
// In addition to checking the "Keys" register check USB service flags to
// allow abort of nested functions such as "Wait4DTMFtrigger()".
//-----------------------------------------------------------------------------
{
	#ifdef WDTENABLED
	// Reset the WatchDogTimer before timeout as setup in WarmStart.
	//
	wdt_reset();
	#endif


	if(TestInfo.SysStatFlag == ENABLE)
	{
		CheckBatteryVoltage();
	}
	
	if(KyReg)
	{
		RTC.CNT = 0;					// Restart TimeOut RTC.
		FKey = GetKp(Mask);
	}
}


void BIupdate(void)
//-----------------------------------------------------------------------------
//	Update the battery indicator status windows without getting Battery Voltage.
//-----------------------------------------------------------------------------
{
	if(PreviousBstat != Bstat)
	{
		SwapWindow();

		if(Bstat == EXTPWR)
		//
		//	Erase battery indicators.
		{
			LCD_OpenWindow(BAT_W, SBORDER);
			LCD_ClearWindow(SBORDER, STD);
			LCD_OpenWindow(BBUTTON_W, SBORDER);
			LCD_ClearWindow(SBORDER, STD);
			LCD_OpenWindow(BATLVL1_W, NOBORDER);
			LCD_ClearWindow(SBORDER, STD);
			LCD_OpenWindow(BATLVL2_W, NOBORDER);
			LCD_ClearWindow(SBORDER, STD);
			LCD_OpenWindow(BATLVL3_W, NOBORDER);
		}
		else
		{
			LCD_OpenWindow(BAT_W, SBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			LCD_OpenWindow(BBUTTON_W, SBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			if((Bstat == BEMPTY) || (Bstat == BHALF) || (Bstat == BFULL))
			{
				LCD_OpenWindow(BATLVL1_W, BORDER);
				LCD_ClearWindow(SBORDER, REV);
			}
			if((Bstat == BHALF) || (Bstat == BFULL))
			{
				LCD_OpenWindow(BATLVL2_W, BORDER);
				LCD_ClearWindow(SBORDER, REV);
			}
			if(Bstat == BFULL)
			{
				LCD_OpenWindow(BATLVL3_W, BORDER);
				LCD_ClearWindow(SBORDER, REV);
			}
		}
		PreviousBstat = Bstat;
		SwapWindow();
	}
}

	
void SwapWindow(void)
//------------------------------------------------------------------------------
//	This function swaps the active window parameters with a fixed temp structure
//	working much like a PUSH/POP function.  Called once is a PUSH, second call
//	POPS, or restores the original window variables. 
//------------------------------------------------------------------------------
{
	static uint8_t Stack = 0;

	if(!Stack)
	//	SAVE
	{
		SWindow.p = Window.p;
		SWindow.x = Window.x;
		SWindow.y = Window.y;
		SWindow.h = Window.h;
		SWindow.w = Window.w;
		SWindow.Xposition = Window.Xposition;
		SWindow.Yposition = Window.Yposition;
		Stack = 1;
	}

	else
	//	RESTORE
	{
		Window.p = SWindow.p;
		Window.x = SWindow.x;
		Window.y = SWindow.y;
		Window.h = SWindow.h;
		Window.w = SWindow.w;
		Window.p = SWindow.p;
		Window.Xposition = SWindow.Xposition;
		Window.Yposition = SWindow.Yposition;
		Stack = 0;
	}
 }


void SpkrAlert(uint16_t *pSoundArray)
//------------------------------------------------------------------------------
//	Enable speaker amplifier and make noise.
//	
//	On Exit:	Disable speaker if Not required by calling program.
//------------------------------------------------------------------------------
{
	uint8_t	TempMode;

	#ifdef SPEAKER
	sSPKR_EN;									// Data sheet indicates 9mS
	#endif
									
	InitBeepSequence(pSoundArray);
	while(ToneInfo.BeepStringFlag);		// Wait for the last beep to finish.
}

void InitBeepSequence(uint16_t *pSoundArray)
//-----------------------------------------------------------------------------
// A string of "beep" sounds are to be played.  Initialize the sequence.  At the
// end of each tone a low level interrupt is enabled to service the new timer
// values and advance the "pSound" pointer.  This process is used to allow normal
// program execution with minimum system impact.
//-----------------------------------------------------------------------------
{
	while(ToneInfo.BeepStringFlag);		// Wait for the last beep to finish.

	ToneInfo.pSound = pSoundArray;		// Advanced via low level isr.
	ToneInfo.BeepStringFlag = 1;			// Initialize multiple beep flag.
	SpkrBeeper();						// Chose SpkrBeeper/TransducerBeeper
	TCD5.INTFLAGS = TC5_CCAIF_bm;			// Reset ISR flag.
	ToneInfo.BeepStringFlag = ON;			// Allow ISR 1ms to take control.
	TCD5.INTCTRLB |= TC45_CCAINTLVL_HI_gc;
}

#ifdef TRANSDUCER	
//==============================================================================
void SpkrBeeper(void)
 //-----------------------------------------------------------------------------
 //	A request has been made to sound the beeper with "freq" for "duration".
 //
 //	Frequency of the tone is determine by 16 CycleSteps of TCC1 clocked at 2MHz.
 //
 //	The "duration" time in milliseconds is controlled by system ISR TCC0_CCA.
 //	Upon completion of time in milliseconds the tone is turned off.
 //-----------------------------------------------------------------------------
 {
	 uint16_t GenFreq;

	 GenFreq = *ToneInfo.pSound;
	 ToneInfo.BeepDuration = *(ToneInfo.pSound+1);
	 if(GenFreq == 0)
	 //
	 // Silent duration.
	 {
		 //	Turn off tone.
	 }
	 else if((GenFreq > 100) && (GenFreq < 10000))
	 {
		 SetBeepFreq(GenFreq);
		 
	 }
	 else
	 {
		 //	Turn off tone.
	 }
 }


 void SetBeepFreq(uint16_t Freq);
 void SetBeepFreq(uint16_t Freq)
 //------------------------------------------------------------------------------
 //	Setup TCC5 timer to generate an interrupt for ON/OFF control of Key press
 //	beep sound based on the alert frequency.
 //
 //	CCA ISR Sets the Beeper Pulse while PER OVFL resets the Pulse.  If the
 //	state flag has been turned OFF by the duration timer in 1mS ISR, further TCC5
 //	ISR's will be disabled.
 //------------------------------------------------------------------------------
 {
	 volatile float PulseCnt;
	 
	 TCD5.CTRLA = TC45_CLKSEL_DIV4_gc;			// 32MHz/4 = 8MHz resolution, 125nS
	 
	 PulseCnt = 8e6/((float)(Freq));				// InitTCC5 sets clock freq = 8MHz.
	 TCD5.CNT = 0;
	 TCD5.PER = TCD5.PERBUF = (uint16_t)(PulseCnt);
	 
	 TCD5.CTRLB = TC45_WGMODE_SINGLESLOPE_gc;	//	PER is Top;
	 TCD5.CCA = TCD5.CCABUF = TCD5.PERBUF/2;	// 50% duty cycle.
	 TCD5.INTCTRLA = TC45_OVFINTLVL_LO_gc;		// Turns Off the Pulse
	 TCD5.INTCTRLB = TC45_CCAINTLVL_LO_gc;		// Turns ON the Pulse.
 }
//==============================================================================
#else
//==============================================================================

void SpkrBeeper(void)
//-----------------------------------------------------------------------------
//	A request has been made to sound the beeper with "freq" for "duration".
//
//	Frequency of the tone is determine by 16 CycleSteps of TCC1 clocked at 2MHz.
//
//	The "duration" time in milliseconds is controlled by system ISR TCC4_OVF_vect.
//	Upon completion of time in milliseconds the tone is turned off when the
//	SineIndex becomes ZERO.
//-----------------------------------------------------------------------------
{
	uint16_t GenFreq;

	GenFreq = *ToneInfo.pSound; 
	ToneInfo.BeepDuration = *(ToneInfo.pSound+1);
	if(GenFreq == 0)
	//
	// Silent duration. Load Sine buffer with MID-SCALE, but keep servicing the
	//	DAC ISR.
	{
		LoadSpkrLevel(OFF);					//	Turn off tone.
		TCD5.INTCTRLB |= TC45_CCAINTLVL_HI_gc;
	}
	else if((GenFreq > 100) && (GenFreq < 10000))
	{
		SetSineFreq(GenFreq);
		LoadSpkrLevel(ON);					//	Turn ON tone.
		TCD5.INTCTRLB |= TC45_CCAINTLVL_HI_gc;
	}
	else
	{
		LoadSpkrLevel(OFF);	//	Turn off tone.
	}
}


void LoadSpkrLevel(uint8_t Status)
//-----------------------------------------------------------------------------
//	Using the basic 16 entry SineTable and User selected amplitude compute the
//	16 steps used during ISR service for the Speaker Tones.
//-----------------------------------------------------------------------------
{
	uint8_t Index;
	long Accumulator;

	while(SpkrTone.SineIndex);
	for(Index = 0; Index < 16; Index++)
	{
		if(SpkrTone.Flag.Bit.Enabled)
		{
			if(Status == ON)
			{
				Accumulator = SineTable16[Index] *(long)(SYSdata.BeeperLevel);
				SpkrSineTable[Index] = (int16_t)(Accumulator/100) +SINE_MIDSCALE;
			}
			else
			{
				SpkrSineTable[Index] = SINE_MIDSCALE;
			}
		}
	}
}
//==============================================================================
#endif

void LoadXmitLevel(void)
//------------------------------------------------------------------------------
//	Using the Transmit Level Selection in SYSdata update the XmitSineTable[];
//------------------------------------------------------------------------------
{
	uint8_t Index;
	long Accumulator, ToneLevel;

	ToneLevel = (long)(LvlData[SYSdata.LevelIndex].Fbase);
	for(Index = 0; Index < 16; Index++)
	{
		Accumulator = SineTable16[Index] *ToneLevel;
		XmitSineTable[Index] = (int16_t)(Accumulator/100) +SINE_MIDSCALE;
	}
}



void SetSineFreq(uint16_t Freq)
//-----------------------------------------------------------------------------
//	On entry the TCD5 pre-scaler has been selected.  Compute the integer needed
//	to generate an ISR 16 times per SineCycle.
//
//	PER register sets the frequency while CCA and CCB is loaded with values below
//	PER so as to generate separate ISR's for each yet maintain timing of 16x.
//
//	NOTE: While OVFL can generate an ISR as well, only CCA and CCB are used to
//			service DAC outputs 0/1.
//
//			Interrupt levels must be set to enable ISR flags.
//-----------------------------------------------------------------------------
{
	volatile float CycleTime, ClockTime, PreScaler;
	uint16_t Divisor;
	
	CycleTime = (1/(float)(Freq)) / 16.0;		// uSec per desired ISR
	ClockTime = 1/CLKper;							// Timer Input clock.
	PreScaler = GetTC0PreScaler(&TCD5);

	ClockTime *= PreScaler;
	TCD5.CNT = 0;
	TCD5.PERBUF = (uint16_t)(CycleTime/ClockTime);	// uSec whole number.
	TCD5.PER = TCD5.PERBUF;
	TCD5.CCABUF = TCD5.CCA = TCD5.PERBUF/4;
 	TCD5.CCBBUF = TCD5.CCB = TCD5.PERBUF/2;
}


uint16_t GetTC0PreScaler(void *tc)
//-----------------------------------------------------------------------------
//	Get the latest loaded PreScale value for the counter "tc" type 0.
//-----------------------------------------------------------------------------
{
	uint16_t PreScaler, Divisor;
	
	Divisor = (((TC5_t *)tc)->CTRLA) & TC5_CLKSEL_gm;	// Timer Clock Divisor value.
	switch(Divisor)
	{
		case TC45_CLKSEL_DIV1_gc:
		{
			PreScaler = 1;
			break;
		}
		case TC45_CLKSEL_DIV2_gc:
		{
			PreScaler = 2;
			break;
		}
		case TC45_CLKSEL_DIV4_gc:
		{
			PreScaler = 4;
			break;
		}
		case TC45_CLKSEL_DIV8_gc:
		{
			PreScaler = 8;
			break;
		}
		case TC45_CLKSEL_DIV64_gc:
		{
			PreScaler = 64;
			break;
		}
		case TC45_CLKSEL_DIV256_gc:
		{
			PreScaler = 256;
			break;
		}
		case TC45_CLKSEL_DIV1024_gc:
		{
			PreScaler = 1024;
			break;
		}
	}
	return(PreScaler);
}


void DelayMS(unsigned int DelayTime)
//-----------------------------------------------------------------------------
//	Using the variables updated by the 1mS ISR set the desired time and wait for
//	the variable to decrement to ZERO.
//
//	Reset the WDT every 16ms interval.
//-----------------------------------------------------------------------------
{
	int16_t TripPoint;

	ISRcount1 = DelayTime;
	TripPoint = DelayTime -16;
	ISRcountStatus1 = ACTIVE;
	while(ISRcountStatus1 == ACTIVE)
	{
		if(ISRcount1 <= TripPoint)				// Reset WDT every 16mSec
		{
			TripPoint -= 16;
			#ifdef WDTENABLED
			// Reset the WatchDogTimer before timeout as setup in WarmStart.
			//
			wdt_reset();
			#endif
		}
	}
}

void SetISRcount2(unsigned int Time)
//------------------------------------------------------------------------------
//	Load the MilliSecond Counter with "Time", then return to the calling program.
//	ISRcountStatus2 will "COMPLETE" once count reaches Zero.
//------------------------------------------------------------------------------
{
	ISRcount2 = Time;
	ISRcountStatus2 = ACTIVE;
}


void AcknowledgeSound(uint16_t *pSound)
//------------------------------------------------------------------------------
//	Momentarily stop any tone transmission and send a local AcknowledgeSound.
//------------------------------------------------------------------------------
{
		SpkrAlert(pSound);
		SetSineFreq(SYSdata.Frequency);		// Exit with XmitTone Freq Active.
}



void SetParity(void)
//------------------------------------------------------------------------------
//	Compute the new Checksum for the changed CAL settings.
//------------------------------------------------------------------------------
{
	uint16_t Uwork;
	uint8_t	TempMode;

	Uwork = SYSdata.BALpot;
	Uwork += SYSdata.BeeperLevel + SYSdata.ToneLevel;
	Uwork += CKSUMOFFSET;
	SYSdata.CheckSum = Uwork;
	eeprom_write_block(&SYSdata, EECAL, sizeof SYSdata);	// *scr, *dst, qty

	SpkrAlert(&OKSound[0][0]);
	SYSdata.CorrectionFlag = ON;			// Use Meter Correction Constants.
}




void GetSYSdata(void)
//------------------------------------------------------------------------------
//	Move EEPROM Calibration factors into the local SRAM SYSdata structure.
//------------------------------------------------------------------------------
{
	uint16_t CheckSum, DataWords;

	DataWords = sizeof SYSdata;
	eeprom_read_block(&SYSdata, EECAL, DataWords);		// *dst, *scr, qty

	CheckSum = SYSdata.BALpot;
	CheckSum += SYSdata.BeeperLevel +SYSdata.ToneLevel;
	CheckSum += CKSUMOFFSET;
	if(SYSdata.CheckSum != CheckSum)
	//
	//	Load Default Settings.
	{
		SYSdata.BeeperLevel = MIDBEEP;
		SYSdata.ToneLevel = MAXMETALLIC;
		SYSdata.BALpot = 117;
		SYSdata.BackLight = LCDBKLT_M;
		SYSdata.Contrast = LCDCONTRAST_M;
		SYSdata.Frequency = 577;
		SYSdata.LevelIndex = 1;					// Medium Level
		SYSdata.AxIndex = 1;						// Frq = 577Hz
		SetParity();
	}
}




void UpDatePotentiometer(uint16_t Balance)
//-----------------------------------------------------------------------------
//	Send the byte Qty placed in SPIbuffer to the potentiometer Port.  The first byte
//	will be loaded into the SPI xmit buffer, and interrupts enabled to complete
//	the total byte transmission.
//-----------------------------------------------------------------------------
{
	uint8_t ByteQty;
	
	ByteQty = 2;
	SPIinfo.Buffer[0] = 0x00;
	sBAL_CS;
	SPIinfo.Buffer[1] = (uint8_t)Balance;
	SendSPI(ByteQty);
	rBAL_CS;
	}


//==============================================================================			
//	System Configuration includes:
//			1-	Setting VoP
//==============================================================================
#ifdef SPEAKER
void Config(void)
//------------------------------------------------------------------------------
//	HOTKEY Entry K_CFG.		The following CFG items can be modified.
//		K_F1:	VoP
//------------------------------------------------------------------------------		
{
	uint8_t Video;
	uint16_t LocalAllowedKeys;

	NewModeMessage("Config System");
	LCD_OpenWindow(DATA_W, SBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	Video = STD;
	NewWindow(DATA_W, SBORDER);
	WriteFunctionWindow("BCKLTE", WF1, Video);
	WriteFunctionWindow("CONTRST", WF2, Video);
	WriteFunctionWindow("SOUND", WF3, Video);

	LocalAllowedKeys = K_F1 | K_F2 | K_F3;
	Wait4Key(HOTKEYS | LocalAllowedKeys);
	switch(FKey)
	{
		case K_F1:
		{
			SetBackLight();
			break;
		}
		case K_F2:
		{
			SetContrast();
			break;
		}
		case K_F3:
		{
			SetSound();
			break;
		}
	}
}

#else
void Config(void)
//------------------------------------------------------------------------------
//	HOTKEY Entry K_CFG.		The following CFG items can be modified.
//		K_F1:	VoP
//------------------------------------------------------------------------------
{
	uint8_t Video;
	uint16_t LocalAllowedKeys;

	NewModeMessage("Config System");
	LCD_OpenWindow(DATA_W, SBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	Video = STD;
	NewWindow(DATA_W, SBORDER);
	WriteFunctionWindow("BCKLTE", WF1, Video);
	WriteFunctionWindow("CONTRST", WF2, Video);
	DeleteFunctionWindow(WF3, STD);

	LocalAllowedKeys = K_F1 | K_F2;
	Wait4Key(HOTKEYS | LocalAllowedKeys);
	switch(FKey)
	{
		case K_F1:
		{
			SetBackLight();
			break;
		}
		case K_F2:
		{
			SetContrast();
			break;
		}
	}
}
#endif

void SetSound(void)
//------------------------------------------------------------------------------
//	Two options, set volume level or monitor on/off.
//------------------------------------------------------------------------------
{
	NewModeMessage("Sound");
	DeleteFunctionWindow(WF1, STD);
	WriteFunctionWindow("VOLUME", WF2, STD);
	WriteFunctionWindow("MONITOR", WF3, STD);
	Wait4Key(K_F2 | K_F3 | K_CLR);
	switch(FKey)
	{
		case K_F2:
		{
			SetVolume();
			break;
		}
		case K_F3:
		{
			SetMonitor();
			break;
		}
	}

}

void SetMonitor(void)
//------------------------------------------------------------------------------
//	Allow user to turn ON or OFF received tone to Speaker.
//------------------------------------------------------------------------------
{
	char WrkStrg[20];
	uint16_t Temp;

	NewModeMessage("Monitor");
	NewWindow(DATA_W, SBORDER);
	FunctionSetUpWindow();
	WriteFunctionWindow("LOW VOL", WF1, STD);
	WriteFunctionWindow("NORM VOL", WF2, STD);
	WriteFunctionWindow("OFF", WF3, STD);
	
	// Get the EEPROM value for LCD Volume.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	if(SYSdata.MonitorLevel == LOW_VOL)
	{
		LCD_CtrStrg("Low Volume", TB8, C_RIGHT, STD);
	}
	if(SYSdata.MonitorLevel == HIGH_VOL)
	{
		LCD_CtrStrg("Norm Volume", TB8, C_RIGHT, STD);
	}
	else
	{
		LCD_CtrStrg("OFF", TB8, C_RIGHT, STD);
	}
	
	Temp = SYSdata.MonitorLevel;
	Wait4Key(K_F1 | K_F2 | K_F3 | K_CLR);
	switch(FKey)
	{
		case K_F1:
		{
			SYSdata.MonitorLevel = LOW_VOL;
			break;
		}
		case K_F2:
		{
			SYSdata.MonitorLevel = HIGH_VOL;
			break;
		}
		case K_F3:
		{
			SYSdata.MonitorLevel = OFF;
			break;
		}
	}
	if(FKey == K_CLR)
	{
		SYSdata.MonitorLevel = Temp;
	}	
	else
	{
		SetParity();
	}
}

#ifdef SPEAKER
void SetVolume(void)
//------------------------------------------------------------------------------
//	Change BeeperVolume.
//------------------------------------------------------------------------------
{
	char WrkStrg[20];
	uint16_t Temp, LocalAllowedKeys;

	NewModeMessage("Volume");
	FunctionSetUpWindow();

	// Get the EEPROM value for LCD Volume.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("VOLUME: ", TB8, C_RIGHT, STD);

	LCD_OpenWindow(CFG_D_W, NOBORDER);
	sprintf(&WrkStrg[0], "%d", SYSdata.BeeperLevel/10);
	LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);

	FKey = 0;
	Temp = SYSdata.BeeperLevel;
	LocalAllowedKeys = K_UP | K_DOWN | K_SET;
	while(!(FKey & (K_CLR | K_SET)))
	{
		LoadSpkrLevel(ON);
		Wait4Key(HOTKEYS | LocalAllowedKeys);
		{
			switch(FKey)
			{
				case K_DOWN:
				{
					if(SYSdata.BeeperLevel > SPKR_LVL_L)
					{
						SYSdata.BeeperLevel -= 10;
					}
					break;
				}
				case K_SET:
				{
					SetParity();
					break;
				}
				case K_UP:
				{
					if(SYSdata.BeeperLevel < SPKR_LVL_H)
					{
						SYSdata.BeeperLevel += 10;
					}
					break;
				}
			}
			LCD_OpenWindow(CFG_D_W, NOBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			sprintf(&WrkStrg[0], "%d", SYSdata.BeeperLevel/10);
			LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);
		}
	}
	if(FKey == K_CLR)
	{
		SYSdata.BeeperLevel = Temp;
	}
}
#endif



void SetBackLight(void)
//------------------------------------------------------------------------------
//	 Allow the user to adjust the backlight, then save in SYSdata structure.
//------------------------------------------------------------------------------
{
	uint8_t Temp;
	char WrkStrg[20];
	uint16_t LocalAllowedKeys;

	NewModeMessage("BackLight");
	FunctionSetUpWindow();

	// Get the EEPROM value for LCD Backlight.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("BackLight: ", TB8, C_LEFT, STD);

	LCD_OpenWindow(CFG_D_W, NOBORDER);
	sprintf(&WrkStrg[0], "%d", SYSdata.BackLight/10);
	LCD_CtrStrg(&WrkStrg[0], TB8, C_MIDDLE, STD);

	FKey = 0;
	Temp = SYSdata.BackLight;
	LocalAllowedKeys = K_UP | K_DOWN | K_SET;
	while(!(FKey & (K_CLR | K_SET)))
	{
		Wait4Key(K_CLR | LocalAllowedKeys);
		{
			switch(FKey)
			{
				case K_DOWN:
				{
					if(SYSdata.BackLight > LCDBKLT_L)
					{
						SYSdata.BackLight -= 10;
					}
					break;
				}
				case K_UP:
				{
					if(SYSdata.BackLight < LCDBKLT_H)
					{
						SYSdata.BackLight +=10;
					}
					break;
				}
				case K_SET:
				{
					SetParity();
					break;
				}
			}
			BackLightDutyCycle(SYSdata.BackLight);
			LCD_OpenWindow(CFG_D_W, NOBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			sprintf(&WrkStrg[0], "%d", SYSdata.BackLight/10);
			LCD_CtrStrg(&WrkStrg[0], TB8, C_MIDDLE, STD);
		}
	}
	if(FKey == K_CLR)
	//	Exit without saving the new value. 
	{
		SYSdata.BackLight = Temp;					// Original value.
		BackLightDutyCycle(SYSdata.BackLight);
	}
	
	
			FKey = K_NONE;		//	No further action.
}

void SetContrast(void)
//------------------------------------------------------------------------------
//	 Allow the user to adjust the Contrast, then save in SYSdata structure.
//------------------------------------------------------------------------------
{
	char WrkStrg[20];
	uint8_t Temp;

	NewModeMessage("Contrast");
	FunctionSetUpWindow();

	// Get the EEPROM value for LCD Contrast.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("CNTRST: ", TB8, C_RIGHT, STD);

	LCD_OpenWindow(CFG_D_W, NOBORDER);
	sprintf(&WrkStrg[0], "%d", SYSdata.Contrast);
	LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);

	FKey = 0;
	Temp = SYSdata.Contrast;
	AllowedKeys = K_UP | K_DOWN | K_SET;
	while(!(FKey & (K_CLR | K_SET)))
	{
		Wait4Key(K_CLR | AllowedKeys);
		{
			switch(FKey)
			{
				case K_DOWN:
				{
					if(SYSdata.Contrast > LCDCONTRAST_L)	SYSdata.Contrast--;
					break;
				}
				case K_SET:
				{
					SetParity();
					break;
				}
				case K_UP:
				{
					if(SYSdata.Contrast < LCDCONTRAST_H)	SYSdata.Contrast++;
					break;
				}
			}
			LCD_CmdWrite(LCD_CMD_ELECTRONIC_VOLUME_MODE_SET);	//	Contrast CMD
			LCD_CmdWrite(SYSdata.Contrast);							// Contrast Value

			LCD_OpenWindow(CFG_D_W, NOBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			sprintf(&WrkStrg[0], "%d", SYSdata.Contrast);
			LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);
		}
	}
	if(FKey == K_CLR)
	//	Exit without saving the new value. 
	{
		SYSdata.Contrast = Temp;
		LCD_CmdWrite(LCD_CMD_ELECTRONIC_VOLUME_MODE_SET);	//	Contrast CMD
		LCD_CmdWrite(SYSdata.Contrast);							// Contrast Value
	}
}



struct SPI_information volatile SPIinfo;

void SendSPI(char ByteQty)
//-----------------------------------------------------------------------------
//	Send the byte Qty placed in SPIbuffer.  The first byte will be loaded into
//	the SPI xmit buffer, and interrupts enabled to complete the total byte
//	transmission.
//-----------------------------------------------------------------------------
{
	uint8_t Reg;
	
	while(SPIinfo.Status != COMPLETE);
	Reg = SPIC_STATUS;						// Clear IF flag.
	Reg = SPIC_DATA;
	SPIinfo.Status = ACTIVE;
	SPIinfo.ByteCnt = ByteQty;
	SPIinfo.XmitCnt = 1;
	SPIC_INTCTRL = SPI_INTLVL_LO_gc;		// Enable Low Level Interrupts
	SPIC_DATA = SPIinfo.Buffer[0];		// Send first byte.
	while(SPIinfo.Status == ACTIVE);		// Wait for transmission to be complete.
}

void SetSPI_Mode2(void)
//-----------------------------------------------------------------------------
//	Mode2, CLK idles HIGH, Data Sampled on Falling Edge.
//-----------------------------------------------------------------------------
{
	SPIC_CTRL &= (~SPI_MODE_gm);
	SPIC_CTRL |= (0x02 << SPI_MODE_gp);
}

void SetSPI_Mode3(void)
//-----------------------------------------------------------------------------
//	Mode3, CLK idles HIGH, Data Sampled on Rising Edge.
//-----------------------------------------------------------------------------

{
	SPIC_INTCTRL &= ~SPI_INTLVL_gm;				// Disable Interrupts.
	SPIC_CTRL &= (~SPI_MODE_gm);
	SPIC_CTRL |= (0x03 << SPI_MODE_gp);			// Mode 3, Rising Edge CLock
}


uint16_t GetTCPreScaler(void *tc)
//-----------------------------------------------------------------------------
//	Get the latest loaded PreScale value for the counter "tc" type 0.
//-----------------------------------------------------------------------------
{
	uint16_t PreScaler, Divisor;
	
	Divisor = (((TC5_t *)tc)->CTRLA) & TC5_CLKSEL_gm;	// Timer Clock Divisor value.
	switch(Divisor)
	{
		case TC45_CLKSEL_DIV1_gc:
		{
			PreScaler = 1;
			break;
		}
		case TC45_CLKSEL_DIV2_gc:
		{
			PreScaler = 2;
			break;
		}
		case TC45_CLKSEL_DIV4_gc:
		{
			PreScaler = 4;
			break;
		}
		case TC45_CLKSEL_DIV8_gc:
		{
			PreScaler = 8;
			break;
		}
		case TC45_CLKSEL_DIV64_gc:
		{
			PreScaler = 64;
			break;
		}
		case TC45_CLKSEL_DIV256_gc:
		{
			PreScaler = 256;
			break;
		}
		case TC45_CLKSEL_DIV1024_gc:
		{
			PreScaler = 1024;
			break;
		}
	}
	return(PreScaler);
}
