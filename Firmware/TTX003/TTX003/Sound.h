/*
 * Sound.h
 *
 * Created: 7/10/2014 8:54:57 AM
 *  Author: Administrator
 */ 


#ifndef SOUND_H
#define SOUND_H

#include <avr/io.h>
#include "stddef.h"

enum VOLUME_LEVEL
{
	MONITOR_OFF,
	LOW_VOL,
	HIGH_VOL,
	MAX_VOL,
};

//------------------------------------------------------------------------------
//	Sound Timing Equates:
//
#define dit			65
#define dah			(3*dit)
#define cwspace	dit
#define charspace dah
//
//------------------------------------------------------------------------------

extern const uint16_t PwrUpSound[9][2];
extern const uint16_t PwrDwnSound[9][2];
extern const uint16_t KPsound[3][2];
extern const uint16_t ToneBuzzSound[9][2];
extern const uint16_t TalkBeepSound[7][2];

extern const uint16_t OKSound[9][2];
extern const uint16_t CalibrateSound[7][2];
extern const uint16_t KPerror[3][2];


#endif /* SOUND_H_ */