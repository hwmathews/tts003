//----------------------------------------------------------------------------
//	File Name: dds.c
//
//	Content:
//
//		Direct Digital Synthesis Functions
//
// 	Date:  18 Feb 2014		hwm
//
//	Copyright 2014 Allied Analogic, Inc
//	All rights reserved
//
//===========================================================================


#define	__AVR_ATMEGA48__	1

#include "avr/io.h"
#include "DDS.h"
#include "TTX003.h"
#include "avr/io.h"

 
struct SPI_information volatile SPIinfo;

void DDSfreq(float FreqHz, unsigned int Channel)
//-----------------------------------------------------------------------------
//	Set DDS Freq.  On entry FreqHz is desired frequency in "Hz".
//
//	Channel	= 0x4000 for Freq channel 0,
//				= 0x8000 for Freq channel 1
//
//	DDS_Frequency = Freq * (f_mclk/2pi), where 2pi = 0x1000 0000, or 2^28,
//						 or decimal 268,435,456.
//
//	Frequency resolution is:  BCLK/2pi, 0.059605 Hz
//
//-----------------------------------------------------------------------------
{
	float Freq;
	unsigned char FreqCh, Quantity;
	unsigned long Lfreq;

	FreqCh = Channel >> 8;
	//
	//	Given the desired "Freq", compute the binary value to write to the DDS.
	
	Freq = FreqHz/DDS_FREQ_RESOLUTION;
	Lfreq = (unsigned long)Freq;
	
	// Translate into 4 unsigned characters.
	//--------------------------------------------------------------------------
	//	It takes two separate word writes to set the frequency. Turn on the proper
	// chip select then send the Bytes.
	//
	Quantity = 4;
	SPIinfo.Buffer[0] = (unsigned char)(( (Lfreq >> 8) & 0x3f) | FreqCh);	// 6 LSB
	SPIinfo.Buffer[1] = (unsigned char)(Lfreq & 0xff);							// 8 LSB
//	SendSPI(Port, Quantity);
//	DelayMS(1);
	
	SPIinfo.Buffer[2] = (unsigned char)(( (Lfreq >> 22) & 0x3f) | FreqCh);// 6 MSB
	SPIinfo.Buffer[3] = (unsigned char)( (Lfreq >> 14) & 0xff);				// 8 MSB
	SetSPI_Mode2();
	sFSYNC;
	SendSPI(Quantity);
	rFSYNC;

}


void DDSphase(float Phase, unsigned int Channel)
//-----------------------------------------------------------------------------
//	On entry "Device" relates to R_DDS and/or T_DDS,  and Phase is the desired
// phase offset in degrees.
//
//	Channel	= 0xc000 for Phase channel 0,
//				= 0xe000 for Phase channel 1
//
//	DDS_PhaseOffset = PhaseRegister * (2pi/4096), where 2pi = 360 degrees,
//
//	Phase resolution is:  360degrees/4096 = .087890625 degrees.
//
//	Therefore:
//		PhaseRegister = DesiredPhaseShift / PhaseResolution.
//-----------------------------------------------------------------------------
{
	unsigned int PhaseRegister;
	char Port, Quantity;
	
	//
	//	Given the desired "Phase", compute the binary value to write to the DDS.
	
	PhaseRegister = (unsigned int)(Phase/DDS_PHASE_RESOLUTION);
	PhaseRegister |= Channel;
	//
	// Translate into 4 unsigned characters.
	//
	Port = 0;																	// Not used att
	Quantity = 2;
	SPIinfo.Buffer[0] = (unsigned char)(PhaseRegister >> 8);		// MSB & Channel
	SPIinfo.Buffer[1] = (unsigned char)(PhaseRegister & 0xff);	//	LSB
	
	//--------------------------------------------------------------------------
	//	Turn on the proper chip selects then send the two bytes, MSB and LSB.
	SetSPI_Mode2();
	sFSYNC;
	SendSPI(Quantity);
	rFSYNC;
}


void DDSctl(unsigned int CtlWord)
//-----------------------------------------------------------------------------
//	Send the Control Word "CtlWord" to the DDS "Device".
//-----------------------------------------------------------------------------
{
	char Quantity;
	
	SPIinfo.Buffer[0] = (unsigned char)(CtlWord >> 8);			//	MSB.
	SPIinfo.Buffer[1] = (unsigned char)(CtlWord & 0xff);		//	LSB.
	//--------------------------------------------------------------------------
	//	Turn on the proper chip selects then send the two bytes, MSB and LSB.
	Quantity = 2;
	SetSPI_Mode2();
	sFSYNC;
	SendSPI(Quantity);
	rFSYNC;
}


void DDSreset(void)
//-----------------------------------------------------------------------------
//	Reset the DDS device, keeping full 28 bit freq consecutive refresh.
//-----------------------------------------------------------------------------
{
	DDSctl(DDS_B28 | DDS_RST);		// Reset DDS's, 28 bit Freq.
	DelayMS(2);
	DDSctl(DDS_B28);					// Release Reset.
}

/*
void SendSPI(char ByteQty)
//-----------------------------------------------------------------------------
//	Send the byte Qty placed in SPIbuffer.  The first byte will be loaded into
//	the SPI xmit buffer, and interrupts enabled to complete the total byte
//	transmission.
//-----------------------------------------------------------------------------
{
	uint8_t Reg;
	
	while(SPIinfo.Status != COMPLETE);	
	Reg = SPID_STATUS;						// Clear IF flag.
	Reg = SPID_DATA;
	SPIinfo.Status = ACTIVE;
	SPIinfo.ByteCnt = ByteQty;
	SPIinfo.XmitCnt = 1;
	SPID_INTCTRL = SPI_INTLVL_LO_gc;		// Enable Low Level Interrupts
	SPID_DATA = SPIinfo.Buffer[0];		// Send first byte.
	while(SPIinfo.Status == ACTIVE);		// Wait for transmission to be complete.
}


void SetSPI_Mode2(void)
//-----------------------------------------------------------------------------
//	Mode2, CLK idles HIGH, Data Sampled on Falling Edge.
//-----------------------------------------------------------------------------
{
	SPID_CTRL &= (~SPI_MODE_gm);
	SPID_CTRL |= (0x02 << SPI_MODE_gp);
}	

void SetSPI_Mode3(void)
//-----------------------------------------------------------------------------
//	Mode3, CLK idles HIGH, Data Sampled on Rising Edge.
//-----------------------------------------------------------------------------

{
	SPID_INTCTRL &= ~SPI_INTLVL_gm;				// Disable Interrupts.
	SPID_CTRL &= (~SPI_MODE_gm);
	SPID_CTRL |= (0x03 << SPI_MODE_gp);			// Mode 3, Rising Edge CLock
}
*/