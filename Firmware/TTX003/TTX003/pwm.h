//------------------------------------------------------------------------------
//
// File Name: pwm.h
//
//	Date:  18 Feb 2014
//
// Content:
//
//
// Copyright 2014 Allied Analogic, Inc
// All rights reserved
//
//=============================================================================
// $Log:$
//=============================================================================

#ifndef PWM_H
#define PWM_H

#include "avr/io.h"
#include "tc.h"



//------------------------------------------------------------------------------
/*! \brief This macro is used to test fatal errors.
 *
 * The macro tests if the expression is false. If it is, a fatal error is
 * detected and the application hangs up. If TEST_SUITE_DEFINE_ASSERT_MACRO
 * is defined, a unit test version of the macro is used, to allow execution
 * of further tests after a false expression.
 *
 * \param expr  Expression to evaluate and supposed to be nonzero.
 */
#if defined(_ASSERT_ENABLE_)
#  if defined(TEST_SUITE_DEFINE_ASSERT_MACRO)
	// Assert() is defined in unit_test/suite.h
#    include "unit_test/suite.h"
#  else
#    define Assert(expr) \
	{\
		if (!(expr)) while (true);\
	}
#  endif
#else
#  define Assert(expr) ((void) 0)
#endif
//------------------------------------------------------------------------------


enum pwm_channel_t {
	/** Channel A. PWM output on pin 0 */
	PWM_CH_A = 1,
	/** Channel B. PWM output on pin 1 */
	PWM_CH_B = 2,
	/** Channel C. PWM output on pin 2 */
	PWM_CH_C = 3,
	/** Channel D. PWM output on pin 3 */
	PWM_CH_D = 4,
};

 /**
 * \brief Valid timer/counters to use
 * \note Not all timer/counters are available on all devices.
 * Please refer to the datasheet for more information on what
 * timer/counters are available for the device you are using.
 */
enum pwm_tc_t {
	/** PWM on port C, pin 0, 1, 2 or 3 (depending on
	    \ref pwm_channel_t "channel") */
	PWM_TCC0,
	/** PWM on port C, pin 4 or 5 (depending on
	    \ref pwm_channel_t "channel") */
	PWM_TCC1,
	/** PWM on port D, pin 0, 1, 2 or 3 (depending on
	    \ref pwm_channel_t "channel") */
	PWM_TCD0,
	/** PWM on port D, pin 4 or 5 (depending on
	    \ref pwm_channel_t "channel") */
	PWM_TCD1,
	/** PWM on port E, pin 0, 1, 2 or 3 (depending on
	    \ref pwm_channel_t "channel") */
	PWM_TCE0,
	/** PWM on port E, pin 4 or 5 (depending on
	    \ref pwm_channel_t "channel") */
	PWM_TCE1,
	/** PWM on port F, pin 0, 1, 2 or 3 (depending on
	    \ref pwm_channel_t "channel") */
	PWM_TCF0,
	/** PWM on port F, pin 4 or 5 (depending on
	    \ref pwm_channel_t "channel") */
	PWM_TCF1,
};

 /**
 * \brief Valid clock source indexes
 */
enum pwm_clk_sel {
	PWM_CLK_OFF     = TC_CLKSEL_OFF_gc,
	PWM_CLK_DIV1    = TC_CLKSEL_DIV1_gc,
	PWM_CLK_DIV2    = TC_CLKSEL_DIV2_gc,
	PWM_CLK_DIV4    = TC_CLKSEL_DIV4_gc,
	PWM_CLK_DIV8    = TC_CLKSEL_DIV8_gc,
	PWM_CLK_DIV64   = TC_CLKSEL_DIV64_gc,
	PWM_CLK_DIV256  = TC_CLKSEL_DIV256_gc,
	PWM_CLK_DIV1024 = TC_CLKSEL_DIV1024_gc,
};

 /*
 //! Timer Counter Capture Compare Channel index
enum tc_cc_channel_mask_enable_t {
	//! Channel A Enable mask
	TC_CCAEN = TC0_CCAEN_bm,
	//! Channel B Enable mask
	TC_CCBEN = TC0_CCBEN_bm,
	//! Channel C Enable mask
	TC_CCCEN = TC0_CCCEN_bm,
	//! Channel D Enable mask
	TC_CCDEN = TC0_CCDEN_bm,
};

 */
 
 
 /**
 * \brief PWM configuration
 */
struct pwm_config {
	void *tc;
	enum pwm_channel_t channel;
	enum tc_cc_channel_mask_enable_t cc_mask;
	enum pwm_clk_sel clk_sel;
	uint16_t period;
};
struct pwm_config pwm_cfg[4];			/* The 4 PWM config structs */






/** \brief Interrupt callback type */
//typedef void (*pwm_callback_t) (void);

void pwm_init(struct pwm_config *config, enum pwm_tc_t tc,
		enum pwm_channel_t channel, uint16_t freq_hz);
void pwm_set_frequency(struct pwm_config *config, uint16_t freq_hz);
void pwm_start(struct pwm_config *config, uint8_t duty_cycle_scale);


/**
 * \brief Function to set PWM duty cycle
 *
 * The duty cycle can be set on a scale between 0-100%. This value
 * will be used to update the CCx register for the selected PWM channel.
 *
 * \param *config           Pointer to the PWM configuration struct
 * \param duty_cycle_scale  Duty cycle as a value between 0 and 100.
 */
static inline void pwm_set_duty_cycle_percent(struct pwm_config *config,
		uint8_t duty_cycle_scale)
{
	Assert( duty_cycle_scale <= 100 );
	tc_write_cc_buffer(config->tc, config->channel,
			(uint16_t)(((uint32_t)config->period *
			(uint32_t)duty_cycle_scale) / 100));
}


#endif