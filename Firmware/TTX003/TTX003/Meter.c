//------------------------------------------------------------------------------
//
// File Name: Meter.c
//
//	Date:  25 Dec 2012
//
// Content:
//
//
// Copyright 2012 Allied Analogic, Inc
// All rights reserved
//
//=============================================================================
// $Log:$
//=============================================================================
//-----------------------------------------------------------------------------
// Header file Includes
//-----------------------------------------------------------------------------
#include	<stdio.h>
#include	<avr/io.h>
#include "TTX003.h"
#include "Meter.h"
#include "lcd.h"
#include "math.h"
#include "string.h"
#include "DDS.h"
#include "tc.h"
#include <avr/wdt.h>

enum ADCgain{
	ADC_GAINx1
	};



void FreqSweep(void)
//-----------------------------------------------------------------------------
//	This function accommodates 5 types of plots:
//
//			TestInfo.DataType = IMPEDANCE			// Plots Impedance Result.
//									= PHASE				// Plots Phase Angle.
//									= DECIBEL			// Mixer Output Signal Level
//
//			TestInfo.PlotType = SINGLE				// Single Terminal Plot TR/T/R.
//									= DIFFERENTIAL		//	Dual Terminal TG/RG.
//
//	Prior to plotting data route the signal path and using Zmeter information
//	setup the Graph Tags, Initiate the sweep sequence, and plot each point as
//	measured.
//
//----------------------------------------------------------------------------
{
	float Frequency;

	int yPosition, xPosition, yPrevious;
//	double Rimp, Timp;
	uint8_t GraphMask[LCDPAGES], GraphGrat[LCDPAGES], PagePtr, RowPtr;
	uint8_t IndexCnt, PageData, Pass, ClearIndex, OneShot;
	uint8_t AvgIndex, PixelIndex, SamplePass;
	
	int16_t PixelBuffer[PIXAVGQTY];

	// Turn off battery check to expedite the plot process.
	//
	TestInfo.SysStatFlag = DISABLE;
	
	SelectTerminal();					// Route the Terminal to Meter buss and
	LCD_OpenWindow(GRAPH_W, NOBORDER);
	//--------------------------------------------------------------------------
	//	Build a column mask of bits for the height of the GRAPH_W.
	//
	for(PagePtr = 0; PagePtr < LCDPAGES; PagePtr++)
	{
		GraphMask[PagePtr] = 0x00;
		GraphGrat[PagePtr] = 0x00;
	}
	IndexCnt = 4;
	RowPtr = GRAPH_WY;
	while(RowPtr < (GRAPH_WY + GRAPH_WH))
	{
		PagePtr = RowPtr >> 3;										// Page to write.
		GraphMask[PagePtr] |= (1 << (RowPtr & 0x07));		// Bit Position to change.
		
		// Place a Vertical pixel graticule marker every 5th row.
		//
		IndexCnt++;
		if(IndexCnt == 8)
		{
			GraphGrat[PagePtr] |= (1 << (RowPtr & 0x07));
			IndexCnt = 0;
		}
		RowPtr++;
	}
	//
	//--------------------------------------------------------------------------
	FKey = 0;
	do
	{
		Frequency = Zmeter.StartFreq;
		Pass = 0;
		IndexCnt = 0;						// Determines Horiz Graticule pixel spacing
		SamplePass = PixelIndex = 0;
		if(TestInfo.DataType == DECIBEL)
		//
		//	RF Freq Sweep
		{
			DDSfreq(Zmeter.StartFreq +1e3, DDS_F0);
			Mixer_db(STME8MS);
		}

		for(xPosition = 0; xPosition < Zmeter.StepQty; xPosition++)
		{
			//-----------------------------------------------------------------
			//	Clear columns and plot the pixel.
			if(TestInfo.PlotType == SINGLE)
			{
				if (TestInfo.DataType == DECIBEL)
				//
				//	The measurement will be shifted whereas -36dB will be bottom of
				//	display for scale0, and -48dB for scale 1.  The top of scale 0,1
				//	will be 36dB higher.
				{

					Meter.SignalBin[TestInfo.Terminal][xPosition] = SinglePoint.Magnitude;
					if(TestInfo.Scale == 0)		yPosition = SinglePoint.Magnitude +48;
					else								yPosition = SinglePoint.Magnitude +60;

					//	Rreverse graph, but limit range to stay within Graph Height.
					//
					if(yPosition < 0)				yPosition = 0;
					if(yPosition > GRAPH_WH)	yPosition = GRAPH_WH;
					yPosition = GRAPH_WH - yPosition;
				}
			}
			//
			//--------------------------------------------------------------------
			//	Don't allow values outside the Graph window.
			//
			if(yPosition > GRAPH_WH)	yPosition = GRAPH_WH;
			else if(yPosition < 0)		yPosition = 0;
			
			//	Clear the screen Column by writing ZERO's to the active pages in
			//	GraphMask.  After IndexCnt the Gratitude pixels will be OR's
			//	back into the LCD_buffer before being written to the LCD.
			//
			//	NOTE: Multiple columns are cleared for each one written to indicate
			//			movement during trace update.
			//--------------------------------------------------------------------
			// Average the pixels over several points to smooth display of data.
			//
			if(SamplePass < PIXAVGQTY)
			{
				PixelBuffer[SamplePass] = yPosition;
				SamplePass++;
				yPosition = 0;
				for(AvgIndex = 0; AvgIndex < SamplePass; AvgIndex++)
				{
					yPosition += PixelBuffer[AvgIndex];
				}
				yPosition /= SamplePass;
			}
			else
			{
				PixelBuffer[PixelIndex] = yPosition;
				yPosition = 0;
				for(AvgIndex = 0; AvgIndex < PIXAVGQTY; AvgIndex++)
				{
					yPosition += PixelBuffer[AvgIndex];
				}
				yPosition /= PIXAVGQTY;
				if(PixelIndex < (PIXAVGQTY -1))		PixelIndex++;
				else											PixelIndex = 0;
			}
			//
			//--------------------------------------------------------------------
			ClearIndex = OneShot = 0;
			do
			{
				for(PagePtr = 0; PagePtr < LCDPAGES; PagePtr++)
				{
					if(GraphMask[PagePtr] || (GraphGrat[PagePtr] && ((IndexCnt & 0x07) == 7)))
					{
						LCD_set_column_address(xPosition +ClearIndex +GRAPH_WX);
						LCD_set_page_address(PagePtr);
						PageData = ~GraphMask[PagePtr];
						LCD_Buffer[PagePtr][xPosition +ClearIndex +GRAPH_WX] &= PageData;
						if(((IndexCnt & 0x07) == 7) && (!OneShot))
						//
						// Horizontal Graticule pixel spacing
						{
							LCD_Buffer[PagePtr][xPosition +ClearIndex +GRAPH_WX] |=
							GraphGrat[PagePtr];
						}
						LCD_DataWrite(LCD_Buffer[PagePtr][xPosition +ClearIndex +GRAPH_WX]);
					}
				}
				if((ClearIndex + xPosition) < GRAPH_WW)	ClearIndex++;
				OneShot++;
				
			} while(((ClearIndex + xPosition) < GRAPH_WW) && (ClearIndex < 6));
			IndexCnt++;
			
			// Plot the new point and draw a line between the two yPositions.
			//
			LCD_PutPixel(xPosition, yPosition, FILL);
			if(Pass)
			{
				if(yPrevious > yPosition)
				{
					LCD_DiagonalLine(xPosition, yPosition, xPosition,
					yPrevious, FILL);
				}
				else
				{
					LCD_DiagonalLine(xPosition, yPrevious, xPosition,
					yPosition, FILL);
				}
			}
			Pass = 1;
			yPrevious = yPosition;
			if(KyReg)	break;			// Can exit in the middle of a scan....
			//--------------------------------------------------------------------
			//	Do the next Frequency step.
			//
			Frequency = Zmeter.StartFreq + (Zmeter.StepSize * xPosition);
			if(TestInfo.DataType == DECIBEL)
			{
				if(xPosition == 67)
				{
					DelayMS(2);	//	Debug
				}
				
				DDSfreq(Frequency, DDS_F0);
				DelayMS(TestInfo.Delay);
				Mixer_db(STME8MS);
			}
			//	Check KeyPush each pixel plot.
			//
			KPstat(HOTKEYS | AllowedKeys);
			if(FKey)					break;
		}
		
		//	After each sweep, or if keypush during sweep, allow the user to change
		//	plot scales.
		//
	}while(!FKey && (TestInfo.TestIndex == MULTISWEEP) );
	
	// Turn on battery check again.
	//
	TestInfo.SysStatFlag = ENABLE;
	
}

void CheckBatteryVoltage(void)
//------------------------------------------------------------------------------
//	Check the System Battery Voltage.  If voltage within usable limits update
//	battery display on a condition change (Bstat != PreviousBstat).
//------------------------------------------------------------------------------
{

	GetBatteryVoltage();
	if(Bstat == BSHUTDOWN)
	{
		PowerDown();
	}
	else
	{
		BIupdate();
	}

}

 
void GetBatteryVoltage(void)
//-----------------------------------------------------------------------------
//	Check the system battery voltage.  Return with battery status flag.
//	Returned Status:
//					HIGH				// 3 Bars
//					MED				// 2 Bars
//					LOW				// 1 Bar
//					Critical			// Operates, but accuracy not guaranteed. 
//					ShutDown			// System turns Off.....
//					
//		Vbat divisor:		R79	49.9K
//								R81	12.1K
//		Max voltage = 95% of Vref (1.0V)
//
//		NOTE:::
//			ADC setting CONVMODE is global for all channels.  If ADC is set for
//			SignedMode, then the single ended channels have a range from 0-> 2047.
//	
//-----------------------------------------------------------------------------
{
	uint8_t Channel, EventInfo;			//, RefInfo;
	volatile uint16_t BitValue;
	float Vbat;

//	ADC_CtrlB = ADCA.CTRLB;					// Don't disturb CONVMODE (signed/unsigned bit)
	EventInfo = ADCA.EVCTRL;				// Setup for Program control sampling.
//	RefInfo = ADCA.REFCTRL;
	
	Channel = 2;
	ADCA.REFCTRL = ADC_REFSEL_AREFB_gc;
	SetADCSingleChannel(Channel, ADC_CH_MUXPOS_PIN9_gc);

	BitValue = ADCBitValue(Channel, STME4MS);
//	Vbat = ((1.0 *BitValue)/(4095)) * ((R5+R3)/R5);
	Vbat = ((3.3 *BitValue)/(2047)) * ((R5+R3)/R5);
	
	if(Vbat > 3.8)
	{
		Bstat = BFULL;
	}
	else if(Vbat > 3.6)
	{
		Bstat = BHALF;
	}
	else if(Vbat > 3.4)
	{
		Bstat = BEMPTY;
	}
	else if(Vbat > 3.2)							// System Uncal'd if Vbat < 3.3
	{
		Bstat = BCRITICAL;
	}
	else
	{
		Bstat = BSHUTDOWN;
	}
}

	
//==============================================================================
// Below code from STX-101




float AutoVAC(uint8_t Path)
//------------------------------------------------------------------------------
//	On entry "Path" has the table constant to determine ADC MUX selection.
//	Beginning with Gain == 1 , step the gain up without saturation to obtain the
//	highest resolution.
//------------------------------------------------------------------------------
{
	volatile uint8_t LocalPath, Gain;
	volatile int16_t TripValue, BitValue;
	uint16_t HoldOff;
	volatile float Volts, Accumulator;


	HoldOff = 54;									// Accumulate 2-3 60Hz intervals
	TripValue = 2048;	
	if(TestInfo.Mode == M_VDC)
	{
		TripValue /= 2;
	}
	
	Gain = 0;										// Unity gain.
	BitValue = 0;
	LocalPath = Path;
	while((abs(BitValue) < TripValue) && (Gain <= 4))
	{
		SetADCDiffGain(LocalPath, Gain);
		SetISRcount2(HoldOff);
		while(ISRcountStatus2 == ACTIVE);
		BitValue = PeakDetector();
		BitValue = A2Dresult.Vpp;
		Gain += 1;								// Gain 0->6 = x1->x64
	}
	//
	//	Adjust the "BIT" value according to calibrated ratio xxxx/1024.
	// 0 = x1, 1 = x2, 2 = x4, etc.
	//	NOTE: Gain range on exit of while loop:  1-> 5
	Accumulator = (float)(SysData.VACgain[TestInfo.Terminal])/1024.0;
	Accumulator *= ((float)(BitValue) / (float)(1 << (Gain -1)));
	
	Volts = ((ADCVREF *Accumulator)/(ACTR_FEGAIN * 2047));

	if(TestInfo.Cflag == OFF)
	{
		if(Volts > SysData.FloorNoise)
		{
			Volts -= SysData.FloorNoise;
			//
			// Front end configuration for Non-Inverting has divisor, 2;
			//
			if(TestInfo.Terminal == RING)
			{
				Volts *=2;
			}	
		}
		else
		{
			Volts = 500e-6;			// Less than -70db....
		}
	}
	return(Volts);					// Estimated system noise.
}


float AutoVDC(uint8_t Path)
//------------------------------------------------------------------------------
//	On entry "Path" has the table constant to determine ADC MUX selection.
//	Beginning with Gain == 1 , step the gain up without saturation to obtain the
//	highest resolution.
//------------------------------------------------------------------------------
{
	volatile uint8_t LocalPath, Gain;
	volatile int16_t TripValue, BitValue;
	uint16_t HoldOff;
	volatile float Volts, Accumulator;


	HoldOff = 54;									// Accumulate 2-3 60Hz intervals
	TripValue = 1024;	
	
	Gain = 0;										// Unity gain.
	BitValue = 0;
	LocalPath = Path;
	while((abs(BitValue) < TripValue) && (Gain <= ADC_G16))
	{
		SetADCDiffGain(LocalPath, Gain);
		SetISRcount2(HoldOff);
		while(ISRcountStatus2 == ACTIVE);
		BitValue = PeakDetector();
		BitValue -= 2048;
		Gain += 1;								// Gain 0->6 = x1->x64
	}
	//
	//	Adjust the "BIT" value according to calibrated ratio xxxx/1024.
	// 0 = x1, 1 = x2, 2 = x4, etc.
	//	NOTE: Gain range on exit of while loop:  1-> 5
	//
	BitValue -= (SysData.Voffset[TestInfo.Terminal] * (1 << (Gain -1)));
	Accumulator = (float)(SysData.VDCgain[TestInfo.Terminal])/1024.0;
	Accumulator *= ((float)(BitValue) / (float)(1 << (Gain -1)));
	
	Volts = ((ADCVREF *Accumulator)/(DCTR_FEGAIN * 2047));
	return(Volts);
}


void SetFunctionPath(void)
//------------------------------------------------------------------------------
//	On entry TestInfo.Terminal has desired terminal to perform next measurement.
//	Route the signal path through relay matrix.
//-------------------------------------------------------------------------------
{
	switch(TestInfo.Function)
	{
		case F_METER:
		{
			rDAP;
			// Terminal Selection is handled separately based on TestInfo.Terminal.
			rMX_EN;
			rTDR;
			rMXSEL;
			if(TestInfo.Mode == M_VAC)
			{
				sTONE;				// Measure TR and TG/RG terminated.
				sVAC;
			}
			else
			{
				rTONE;
				sVDC;
			}
			break;
		}
		case F_TDR:
		{
			SSRData = 0;		// Reset Tip and Ring path on next RLY latch sequence.
			rDAP;
			rMX_EN;
			rMXSEL;
			rTONE;
			sTDR;
			break;
		}
		case F_TONE:
		{
			//	VAC measurement TG and RG are selected via ADC MUX. 
			rDAP;
			rMX_EN;
			rMXSEL;
			rTDR;
			sTONE;				// Send Simplex/TriPlex tone.
			sTR;					// Tip and Ring for differential measurements.
			if(TestInfo.Mode == M_VAC)
			{
				sVAC;
			}
			else
			{
				sVDC;
			}
			break;
		}
		case F_PROBE:
		{
			rTDR;
			rTONE;
			rE2S;
			sMX_EN;				// ADC path selected via DAP105/DAP125 routines.
			sDAP;
			break;
		}
	}	
	
}

void SelectTerminal(void)
//------------------------------------------------------------------------------
// This function is used in F_METER mode to select T2G, R2G, or T2R measurement.
//
//	NOTE: Prior to entry SetFunctionPath has been preformed.
//------------------------------------------------------------------------------
{
	switch(TestInfo.Terminal)
	{
		case TR:
		{
			rE2S;
			sR2MH;
			sT2ML;
			break;
		}
		case TIP:
		{
			rR2MH;
			sE2S;
			sT2ML;
			break;
		}
		case RING:
		{
			rT2ML;
			sE2S;
			sR2MH;
			break;
		}
	}
}

//------------------------------------------------------------------------------
//	Port Pins for ADC inputs based on below equates found in "Meter.h".
//------------------------------------------------------------------------------
//			ACV_T			= 0
//			ACV_R			= 1
//			TDR_ADC
//			ACDC
//			FLT_AC
//------------------------------------------------------------------------------
/*
uint8_t ADC_Terminals[5][2] =
{
	{ADC_CH_MUXPOS_PIN6_gc, ADC_CH_MUXNEG_PIN0_gc},		// ACV_T
	{ADC_CH_MUXPOS_PIN5_gc, ADC_CH_MUXNEG_PIN0_gc},		// ACV_R
	{ADC_CH_MUXPOS_PIN3_gc, ADC_CH_MUXNEG_PIN4_gc},		// TDR
	{ADC_CH_MUXPOS_PIN2_gc, ADC_CH_MUXNEG_PIN4_gc},		// ACDC (TR)
	{ADC_CH_MUXPOS_PIN1_gc, ADC_CH_MUXNEG_PIN4_gc},		// AC (BP Filter)
};
*/
uint8_t ADC_MUX_Selection[5] =
{
	{ADC_CH_MUXPOS_PIN6_gc | ADC_CH_MUXNEG_PIN4_gc},		// ACV_T // (no gain)
	{ADC_CH_MUXPOS_PIN5_gc | ADC_CH_MUXNEG_PIN4_gc},		// ACV_R //	(no gain)
	{ADC_CH_MUXPOS_PIN3_gc | ADC_CH_MUXNEG_PIN4_gc},		// TDR
	{ADC_CH_MUXPOS_PIN2_gc | ADC_CH_MUXNEG_PIN4_gc},		// ACDC (TR)
	{ADC_CH_MUXPOS_PIN1_gc | ADC_CH_MUXNEG_PIN4_gc},		// AC (BP Filter)
};


void SetADCDiffGain(uint8_t Path, uint8_t Gain)
//-----------------------------------------------------------------------------
//	Set the A2D differential on ADC1 with gain == ADC_CH_GAIN_nX_gc.  
//
// On Entry "Path" points to a table for Mux inputs, ADCPATH[ for both POS and NEG.
//
//				CONVMODE		== 1									// Signed
//				INPUTMODE	== 11									// Differential With Gain
//				Gain "n"    == x1, x2,x4,x8,x16,x32,x64	// Gain Values 0 -> 6
//
//				ACV_T,
//				ACV_R,
//				TDR_ADC,
//				ACDC,
//				FLT_AC,
//-----------------------------------------------------------------------------
{
	//--------------------------------------------------------------------------
	// Setup for Differential mode, Unity Internal CPU GAIN.
	//
	ADCA.CTRLB = 0x00;										//	Clear Free Run..
	ADCA.CTRLB |= (1 << ADC_CONMODE_bp);				// Signed Mode.
	
	ADCA.CH0.CTRL &= ~ADC_CH_INPUTMODE_gm;				// Clear Single/Diff Bits.
	ADCA.CH0.CTRL |= ADC_CH_INPUTMODE_DIFFWGAIN_gc;	// 4MSBits for MUX NEG
	ADCA.CH0.CTRL &= ~ADC_CH_GAIN_gm;					// Clear Previous Gain.
	ADCA.CH0.CTRL |= (Gain << ADC_CH_GAIN_gp);		// Gain Group Position.
	
	ADCA.CH0.MUXCTRL = ADC_MUX_Selection[Path];		// Mux Positive & Negative
	//
	//--------------------------------------------------------------------------
}

void SetTDR_ADCDiffGain(uint8_t Path, uint8_t Gain)
//-----------------------------------------------------------------------------
//	Set the A2D differential on ADC1 with gain == ADC_CH_GAIN_nX_gc.  
//
// On Entry "Path" points to a table for Mux inputs, ADCPATH both POS and NEG.
//
//				CONVMODE		== 1									// Signed
//				INPUTMODE	== 11									// Differential With Gain
//				Gain "n"    == x1, x2,x4,x8,x16,x32,x64	// Gain Values 0 -> 6
//
//				ACV_T,
//				ACV_R,
//				TDR_ADC,
//				ACDC,
//				FLT_AC,
//-----------------------------------------------------------------------------
{
	//--------------------------------------------------------------------------
	// Setup for Differential mode.
	//
	ADCA.CTRLB = 0x00;										//	Clear Free Run..
	ADCA.CTRLB |= (1 << ADC_CONMODE_bp);				// Signed Mode.
	
	ADCA.CH1.CTRL &= ~ADC_CH_INPUTMODE_gm;				// Clear Single/Diff Bits.
	ADCA.CH1.CTRL |= ADC_CH_INPUTMODE_DIFFWGAIN_gc;	// 4MSBits for MUX NEG
	ADCA.CH1.CTRL &= ~ADC_CH_GAIN_gm;					// Clear Previous Gain.
	ADCA.CH1.CTRL |= (Gain << ADC_CH_GAIN_gp);		// Gain Group Position.
	
	ADCA.CH1.MUXCTRL = ADC_MUX_Selection[Path];		// Mux Positive & Negative
	//
	//--------------------------------------------------------------------------
}

int ADCBitValue(uint8_t Channel, uint8_t Samples)
//------------------------------------------------------------------------------
//  Get the DC Bit Value on the ADC Channel for the interval Samples where
//  Samples = # of 1mSec Ticks to Average.
//
//	NOTE: On entry SetADCSingleChannel() must have initialized ADCA channel number.
//			Xmega ADC used in UNsigned Mode has a 200bit offset (See XmegaAU ADC
//			data sheet for explanation)
//------------------------------------------------------------------------------
{
	uint16_t SampleIndex;
	long Accumulator;

	ADC_CH_t volatile *pChannel;

	// Assign a pointer to ADCA ch0 and advance to the "ADC_channel"
	//
	pChannel = &(ADCA.CH0.CTRL);
	pChannel += Channel;										// Channel 0->4.


	Accumulator = 0;
	ADCA.CTRLA |= (1 << ADC_ENABLE_bp);					// Enable A2D.
	for(SampleIndex = 0; SampleIndex < Samples; SampleIndex++)
	{
		(*pChannel).INTFLAGS |= (1 << ADC_CH_CHIF_bp);		// Clear Interrupt Flag.
		(*pChannel).CTRL |= ADC_CH_START_bm;					// Start Conversion.
//		DelayMS(2);
		Accumulator += (*pChannel).RES;
	}

	Accumulator /= Samples;
	return((int16_t)Accumulator);
}


int16_t PeakDetector(void)
//------------------------------------------------------------------------------
//	Sampling Process Completed with values placed in global A2Dbuffer. Determine
//	the Highest and lowest values in the sample array.  
//	
//	Upon completion A2Dresult structure will contain an averaged High/Low value,
//	and Vpp.
//
//	Returned value is average integer value over ADC sample interval.
//------------------------------------------------------------------------------
{
	volatile int16_t  HIndex, LIndex, Index, Value;
	uint16_t SampleIndex, LowestHigh, HighestLow;


	A2Dresult.Accumulator = 0;
	A2Dresult.LowPeak = 0;
	A2Dresult.HighPeak = 0;
	HIndex = LIndex = 0;

	//	Find the Average Value to insert into the Peak Detector Buffers.
	//
	for(SampleIndex = 0; SampleIndex < A2Dresult.Samples; SampleIndex++)
	{
		A2Dresult.Accumulator += A2Dbuffer[SampleIndex];
	}
	Value = (uint16_t)(A2Dresult.Accumulator/(int32_t)(A2Dresult.Samples));


	for(Index = 0; Index < PK_ELEMENTS; Index++)	// Clear High Peak Array buffer.
	{
		HighArray[Index] = Value;
		LowArray[Index] = Value;
	}
	//---------------------------------------------------------------------------
	//	Find the highest and lowest value within the Sample Buffer.
	//
	for(SampleIndex = 0; SampleIndex < A2Dresult.Samples; SampleIndex++)
	{
		LowestHigh = 4095;
		for(Index = 0; Index < PK_ELEMENTS; Index++)
		{
			if(HighArray[Index] < LowestHigh)
			{
				HIndex = Index;
				LowestHigh = HighArray[Index];
			}
		}
		// Point to the Highest LowValueArray element.
		//
		HighestLow = 0;
		for(Index = 0; Index < PK_ELEMENTS; Index++)
		{
			if(LowArray[Index] > HighestLow)
			{
				LIndex = Index;
				HighestLow = LowArray[Index];
			}
		}
		//
		// If the value is lower than the lowest, put the value in the low array,
		// else if value is higher than the highest, put it in the high array.
		//
		if(A2Dbuffer[SampleIndex] > HighArray[HIndex])
		{
			HighArray[HIndex] = A2Dbuffer[SampleIndex];
		}
		else if (A2Dbuffer[SampleIndex] < LowArray[LIndex])
		{
			LowArray[LIndex] = A2Dbuffer[SampleIndex];
		}
		
	}
	//--------------------------------------------------------------------------
	// Average the high peak and low peak elements.  The difference is the Peak
	// to Peak AC value.
	//
	for(Index = 0; Index < PK_ELEMENTS; Index++)
	A2Dresult.HighPeak += HighArray[Index];
	for(Index = 0; Index < PK_ELEMENTS; Index++)
	A2Dresult.LowPeak += LowArray[Index];

	A2Dresult.HighPeak /= PK_ELEMENTS;
	A2Dresult.LowPeak /= PK_ELEMENTS;
	A2Dresult.Vpp = (A2Dresult.HighPeak) -	(A2Dresult.LowPeak);
	return (Value);
}

int16_t VppDetector(void)
//------------------------------------------------------------------------------
//	Rapidly compute the Vpp bit value of A2Dbuffer[] Array.
//
//	NOTE:	The values in A2Dbuffer are "unsigned 0->4096".
//			Return with averaged value over sampled interval (Vdc), and
//			A2Dresult.Vpp containing averaged Vpp.
//------------------------------------------------------------------------------
{
	//	uint16_t Hprevious[PK_ELEMENTS], Lprevious[PK_ELEMENTS];
	long LowAccumulator, HighAccumulator;
	unsigned long AvgAccumulator;
	uint16_t SampleIndex, PK_index;

	for(PK_index = 0; PK_index < PK_ELEMENTS; PK_index++)
	{
		HighArray[PK_index] = 2048;
		LowArray[PK_index] = 2048;
	}
	
	PK_index = 0;
	AvgAccumulator = 0;
	while(PK_index < PK_ELEMENTS)
	{
		for(SampleIndex = 0; SampleIndex < A2Dresult.Samples; SampleIndex++)
		{
			if(PK_index == 0)
			{
				AvgAccumulator += A2Dbuffer[SampleIndex];
			}
			if(A2Dbuffer[SampleIndex] > HighArray[PK_index])
			{
				HighArray[PK_index] = A2Dbuffer[SampleIndex];
				A2Dbuffer[SampleIndex] = 2048;
			}
			else if(A2Dbuffer[SampleIndex] < LowArray[PK_index])
			{
				LowArray[PK_index] = A2Dbuffer[SampleIndex];
				A2Dbuffer[SampleIndex] = 2048;
			}
		}
		PK_index++;
	}
	
	LowAccumulator = 0;
	HighAccumulator = 0;
	for(PK_index = 0; PK_index < PK_ELEMENTS; PK_index++)
	{
		LowAccumulator += LowArray[PK_index];
		HighAccumulator += HighArray[PK_index];
	}
	A2Dresult.Vpp = (HighAccumulator - LowAccumulator)/PK_ELEMENTS;
	AvgAccumulator /= A2Dresult.Samples;
	return((int16_t)(AvgAccumulator));
}
//
//==============================================================================





void SetADCSingleChannel(uint8_t ADC_channel, uint8_t POSmux)
//-----------------------------------------------------------------------------
//	Setup ADCA channel number for SingleEnded mode 12bit, unity gain, on the
//	requested CPU PIN number.
//	
//			CONVMODE		== 0	Unsigned			// Vbat measurement (0-4095).
//			CONVMODE		==	1	Signed			// Vbat measurement (0-2047).
//			INPUTMODE	== 01	Single Ended
//			
//			MUXPOS:		ADC0 -> ADC15
//
//	NOTE::::
//			The ADC CONVMODE is global for all ADC channels.
//-----------------------------------------------------------------------------
{
	ADC_CH_t *pChannel;

	// Assign a pointer to ADCA ch0 and advance to the "ADC_channel"
	//
	pChannel = &(ADCA.CH0.CTRL);
	pChannel += ADC_channel;			// Channel 0->4.

	//	Update the operating Mode and select the pin for measurement.
	//
	(*pChannel).CTRL	= 0x00;			// Clear Single/Diff Bits.
 	(*pChannel).CTRL	= ADC_CH_INPUTMODE_SINGLEENDED_gc;
	
	(*pChannel).MUXCTRL	&= ~ADC_CH_MUXPOS_gm;		// Positive Channel.
	(*pChannel).MUXCTRL	|= POSmux;
}




void AudioTrackEnable(void)
//------------------------------------------------------------------------------
//	Enable the system to generate timed interrupt ADC samples at 3KHz with Speaker
//	DAC updates each sample point.
//------------------------------------------------------------------------------
{
	float ClkTm;
	uint16_t Count;
	
	SetADCDiffGain(ADC_CH_MUXPOS_PIN1_gc, ADC_CH_GAIN_1X_gc);
	while(ToneInfo.BeepStringFlag);
//	ToneInfo.Mode = M_DISABLE;						// Disable Standard Audio ISR's.
	TestInfo.SysStatFlag = DISABLE;				// No Battery Indicator Service.
	//
	//---------------------------------------------------------------------------
	//	Begin Synchronous Sampling process.  Each sample is trigger by TCC1.CCA
	//
	TCC1.CTRLA = TC_CLKSEL_OFF_gc;			// Stop TCC1 Clock to Initialize.
	TCC1.INTFLAGS = 0xF3;						// Clear any pending Interrupts.
	TCC1.CNT = 0;
	ClkTm = 1/((float)(sysclk_get_per_hz()));
	Count = (uint16_t)(83.3e-6/ClkTm);
	TCC1.CCABUF = Count;							// 8MHz/667 = 83.3uS * 4Samples = 3KHz

	EVSYS.CH0MUX =  EVSYS_CHMUX_TCC1_CCA_gc;	// TCC1_CCA Triggers ADC conversion.

	ADCA.CH0.INTFLAGS |= (1 << ADC_CH0IF_bp);	// Clear Interrupt Flag.
	ADCA.CTRLA = 0;
	ADCA.CTRLA = (1<< ADC_FLUSH_bp);				// Start clean.
	ADCA.CTRLA &= ~(1<< ADC_FLUSH_bp);
	ADCA.CTRLA = (1<< ADC_ENABLE_bp);			// Enable A2D.

	// Set the Event Source for CCA to trigger A2D conversion.  An interrupt
	// will be generated after each conversion.
	//
	TCC1.CTRLD	= TC_EVSEL_CH1_gc;			// CCA will be event 1.

	ADCA.CH0.INTCTRL = TC_INT_LVL_HI;		// High Interrupt Level.
	ADCA.EVCTRL = 0;								// CH0MUX == E5, TCE0.CCB.
	ADCA.EVCTRL |= ADC_EVACT0_bm;				// Trigger A2D conversion.

	TCC1.CTRLA = TC_CLKSEL_DIV1_gc;			// 32MHz system Clock
	//
	//---------------------------------------------------------------------------
//	ToneInfo.Mode = M_DISABLE;					// Silence, but allow Keypush beep.
	A2Dresult.Samples = 0;						// Start ADC collection.

}

void AudioTrackDisable(void)
//------------------------------------------------------------------------------
//	Revert to normal ADC sampling functions.
//------------------------------------------------------------------------------
{
	ADCA.CH0.INTCTRL = TC_INT_LVL_OFF;		//	Interrupt Level OFF
	EVSYS.CH0MUX = EVSYS_CHMUX_OFF_gc;
	TCC1.INTCTRLB = TC_INT_LVL_OFF;			// Interrupt Enable, OFF
	ADCA.CTRLA &= ~(1 << ADC_ENABLE_bp);	// Disable A2D.
	ADCA.EVCTRL = 0;								// Clear Event Action, NO Event ACTION.

//	ToneInfo.Mode = M_BEEP;						// Enable Standard Audio ISR's.
	TestInfo.TestIndex = NONE;
	TestInfo.SysStatFlag = ENABLE;			// Update Battery Indicator again.
	rSPKR_EN;

	DDSctl(DDS_SLEEP);
//	ReleaseAll();
}


#define PKarrayElements		10

uint16_t TrackHighLow(void)
//------------------------------------------------------------------------------
//	Find the Vpp bit value of the TrackSamples.
//------------------------------------------------------------------------------
{
	uint16_t Vhigh, Vlow, Vdiff;
	long AccPKhigh, AccPKlow, ACavg;
	uint8_t Index;

	static uint8_t PKindex = 0;
	static uint16_t PKhigh[PKarrayElements], PKlow[PKarrayElements];

	//	Fill the PeakDetector with sample average.
	//
	ACavg = 0;
	for(Index = 0; Index < TRACKSAMPLES; Index++)
	{
		ACavg += A2Dbuffer[Index];
	}
	ACavg /= TRACKSAMPLES;
	Vhigh = Vlow = ACavg;

	//	Determine the High/Low voltage values.  Place values in the next Peak
	//	array element.
	//
	for(Index = 0; Index < TRACKSAMPLES; Index++)
	{
		if(A2Dbuffer[Index] > Vhigh)				Vhigh = A2Dbuffer[Index];
		else if(A2Dbuffer[Index] < Vlow)		Vlow = A2Dbuffer[Index];
	}
	PKhigh[PKindex] = Vhigh;
	PKlow[PKindex] = Vlow;

	//	Average the High/Low value within the Peak Array.
	//
	AccPKhigh = AccPKlow = 0;
	for(Index = 0; Index < PKarrayElements; Index++)
	{
		AccPKhigh += PKhigh[Index];
		AccPKlow += PKlow[Index];
	}
	AccPKhigh /= PKarrayElements;
	AccPKlow /= PKarrayElements;

	//	Advance the Peak Array pointer. Zero if all elements are complete.
	//
	PKindex++;
	if(PKindex == PKarrayElements)		PKindex = 0;

	//	Return the averaged Peak Difference.
	//
	Vdiff = (uint16_t)(AccPKhigh - AccPKlow);
	return (Vdiff);
}


void SetBandPassFreq(uint16_t Freq)
//------------------------------------------------------------------------------
//	Compute the Bandpass clock for generation of FX100. Resource: TCF0 ch A.
//------------------------------------------------------------------------------
{
	volatile float CycleTime, ClockTime, PreScaler;
	uint16_t CycleCnt;
	
	PreScaler = 8;
	TCF0.CTRLA = TC_CLKSEL_DIV8_gc;					// 8Mhz/1
	
	CycleTime = (1/(float)(Freq)) / 100.0;			// uSec per desired ISR
	ClockTime = 1/(float)(sysclk_get_cpu_hz());
	ClockTime *= PreScaler;
	CycleCnt = (uint16_t)(CycleTime/ClockTime);
	TCF0.PERBUF = CycleCnt;
	TCF0.CCABUF = CycleCnt/2;
}



void VppToDecibels(void)
//------------------------------------------------------------------------------
//	Convert the Vpp reading to dB.
//------------------------------------------------------------------------------
{
	volatile float Vpp, Decibels, SysGain;

	Vpp = (ADCVREF * A2Dresult.Vpp)/4095;
	SysGain = TestInfo.ADCgain;
	Decibels = 20 * log10((Vpp/(SysGain)/TERM135_Vpp));	//	1.039Vpp;
	
	//	Compensate based on attenuator, mixer_gain, and Calibration data.
	//
	Decibels -= (-13.5 + MIXERGAIN_db - SysData.MixerCorrection);
	SinglePoint.Magnitude = Decibels;
}
