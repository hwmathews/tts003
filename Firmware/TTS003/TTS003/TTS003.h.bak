//-----------------------------------------------------------------------------
//
// File Name: TriToneInfo.h
//
// Content:
//
//    Date:  30 July 2018
//
//    Header file for the TriTone Sender TTS003 using the ATxmega32e5.
//
// Copyright 2018 Allied Analogic, Inc
// All rights reserved
//
//=============================================================================
// $Log:$
//	V1.03		17 Sept 2018	hwm
//			- V1.02 had display overwrite bottom right corner.  By removing yPixel
//			  and TDR HTagStrg the problem went away.  Given the SRAM used was
//			  3900 +bytes .. suspect stack overflow.
//			- Deleted  WF1 fragment in Sound/Monitor
//			- ISR for Comb now tests for != OFF to allow monitor volume shift.
//	V1.04		20 Sept 2018	hwm
//			- Modified technique for Short determination
//			- Changed method for null calibration
//			- Added method to measure TriPlex for BuZZ and verify output level
//	V1.05		20 Sept 2018 hwm
//			- Removed 1.004KHz from Freq List.
//	v1.06		24 Oct 2018	hwm
//			- Increased Beep Freqencies.
// v1.07		14 Jan 2019	hwm
//			- Disabled ADC after completion of Comb filter selection rather than
//			  stopping TCC5 clock (See TTX )
//			- Deleted NOP in Beep ISR to allow turn OFF of keypush beep.
//	 
//=============================================================================
//	Calibrate press CFG, then LEVEL within 750mS
//=============================================================================
#ifndef _TTS003_H
#define	_TTS003_H

#include <avr/io.h>
#include <stdio.h>
#include <stddef.h>

#define PRODUCTSTRING "TTS 003 V1.07"

//#define TRANSDUCER	TRUE	
//#define SPEAKER		TRUE
#define SPEAKER		FALSE
//-----------------------------------------------------------------------------
// Use 32 MHz RC Clock Source.... PSA divide by 4... PSBC default divide by 1.
//
#define CONFIG_SYSCLK_SOURCE			CLK_SCLKSEL_RC32M_gc
#define CONFIG_SYSCLK_PSADIV			CLK_PSADIV_4_gc
#define CONFIG_SYSCLK_PSBCDIV			CLK_PSBCDIV_1_1_gc
#define CLKper								32e6						// Peripheral CLK freq.
#define CLKcpu								CLKper					// CPU Clock Freq.

enum pwm_clk_sel {
	PWM_CLK_OFF     = TC45_CLKSEL_OFF_gc,
	PWM_CLK_DIV1    = TC45_CLKSEL_DIV1_gc,
	PWM_CLK_DIV2    = TC45_CLKSEL_DIV2_gc,
	PWM_CLK_DIV4    = TC45_CLKSEL_DIV4_gc,
	PWM_CLK_DIV8    = TC45_CLKSEL_DIV8_gc,
	PWM_CLK_DIV64   = TC45_CLKSEL_DIV64_gc,
	PWM_CLK_DIV256  = TC45_CLKSEL_DIV256_gc,
	PWM_CLK_DIV1024 = TC45_CLKSEL_DIV1024_gc,
};

//-----------------------------------------------------------------------------
//	General Operation Definitions.
//-----------------------------------------------------------------------------
#define	ON					1
#define	OFF				0

#define ENABLE				ON
#define DISABLE			OFF

#define	HIGH				1
#define	LOW				0

#define	ACTIVE			1
#define	COMPLETE			0

#define	BUZZ				ON
#define	IDLE				OFF

#define SPKR            1
#define PIEZO           0

//-----------------------------------------------------------------------------
//	A2D related Definitions.
//-----------------------------------------------------------------------------
#define	A2Dsamples			2
#define	PK_ELEMENTS			8
#define	CHANNEL_MASK 		0x0f
		
//-----------------------------------------------------------------------------
//	Battery / FrontEnd Divisors.
//-----------------------------------------------------------------------------
#define		R3			49.9e3
#define		R5			4.9e3				// Debug using STX-101
//#define		R5			12.1e3
#define		R40		499e3
#define		R42		6.8e3

#define		R45		200e3				// AC Tip to Earth Input
#define		R47		20e3

#define		R44		R45
#define		R46		R47

#define DCTR_FEGAIN		R42/R40		// AC/DC Tip to Ring Gain.
#define ACTR_FEGAIN		DCTR_FEGAIN
#define ACT_FEGAIN		R47/R45		// AC Tip to Earth Gain
#define ACR_FEGAIN		R46/R44		// AC Ring to Earth Gain
#define SINE_MIDSCALE	2047
//------------------------------------------------------------------------------
//	Timing Equates in MilliSeconds.
//------------------------------------------------------------------------------
#define	ONWAITTM				1000		// Keypress hold time for TURN ON
#define	KEY_RPT_TM1			1500		//	First Time interval for Repeat Key ACK.
#define	KEY_RPT_TM2			450		//150
#define	DEBI					60			//30 Number of Timer0 ISR counts to debounce

#define ROW_MASK_gm			0xE0
#define ROW_MASK_bp			5

#define COLUMN_MASK_gm		0x1C
#define COLUMN_MASK_bp		2

//-----------------------------------------------------------------------------
//	Meter and Tone Terminal equates.
//-----------------------------------------------------------------------------
enum Pattern_id{
	BLUE,
	GREEN,
	RED,
	CYAN,
};

enum Termial_id{
	TR,
	RING,
	TIP,
	SMPLX_1T,
	SMPLX_3T,
	TRIPLEX,
};

#define AVAILABLE_TERMINALS 3
#define NORM_VAC_TR					40			// Typical VAC TR in TriPlex
#define NORM_VAC_GND					20			// Typical VAC TG/RG Smplx and TriPlex

//-----------------------------------------------------------------------------
//	KeyPush Equates   for 4x4 matrix.  (See KeyXlate[] table in TTS003.c)
//-----------------------------------------------------------------------------
enum FunctionKey
{
	K_NONE		= 0x0000,
	K_FREQ		= 0x0001,
	K_LEVEL		= 0x0002,
	K_CFG			= 0x0004,
	K_F1			= 0x0008,
	K_F2			= 0x0010,
	K_F3			= 0x0020,
	K_PWR			= 0x0040,
	K_CLR			= 0x0080,
	K_SPR1		= 0x0100,
	K_SPR2		= 0x0200,
	K_SPR3		= 0x0400,
	K_SPR4		= 0x0800,
	K_SPR5		= 0x1000,
	K_SPR6		= 0x2000,
	K_SPR7		= 0x4000,
	K_SPR8		= 0x8000,
	K_START	   = K_F1,
	K_STOP		= K_F1,
	K_EVALUATE	= K_F2,
	K_EXIT		= K_F2,
	K_SET			= K_F1,
	K_AUTO		= K_F3,
	K_AERIAL		= K_F1,
	K_BURIED		= K_F3,
	K_UP			= K_F3,
	K_DOWN		= K_F2,
	K_OK			= K_F1,
	K_RETEST		= K_F1,
	K_IGNORE		= K_F3,
};

#define HOTKEYS	(K_CLR | K_FREQ | K_CFG | K_LEVEL)
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//	System Settings:
//------------------------------------------------------------------------------
#define LCD_WIDTH_PIXELS					(128)
#define LCD_HEIGHT_PIXELS              (32)

#define LCD_DISPLAY_CONTRAST_MAX			80
#define LCD_DISPLAY_CONTRAST_MIN			70
#define SPKR_LVL_L							10
#define SPKR_LVL_H							100

//------------------------------------------------------------------------------
//	Port Definitions
//------------------------------------------------------------------------------
enum PortR_id
{
	EN_G_bp,
	PWR_EN_bp,
};
#define sG_EN			(PORTR.OUTCLR = (1 << EN_G_bp))
#define rG_EN			(PORTR.OUTSET = (1 << EN_G_bp))
#define sPWR_EN		(PORTR.OUTSET = (1 << PWR_EN_bp))
#define rPWR_EN		(PORTR.OUTCLR = (1 << PWR_EN_bp))

//#define sTP1			(PORTR.OUTSET = (1 << TP1_bp))
//#define rTP1			(PORTR.OUTCLR = (1 << TP1_bp))

enum PortA
{
	VBAT_bp,
	SMPLX_bp,
	SINE_bp,
	SPKR_bp,
	DCV_TR_bp,
	ACV_T_bp,
	ACV_R_bp,
	CHRG_bp,
};

#define sSMPLX			(PORTA.OUTSET = (1 << SMPLX_bp))
#define rSMPLX			(PORTA.OUTCLR = (1 << SMPLX_bp))

#define MODE_SMPLX			sSMPLX
#define MODE_TRIPLEX			rSMPLX
#define sTRIPLEX				rSMPLX
#define rTRIPLEX				sSMPLX

enum PortC_id
{
	LCD_CS_bp,
	LCD_RST_bp,
	LCD_CMD_bp,
	LCD_BKLT_bp,
	BAL_CS_bp,
	SCLK_bp,
	MISO_bp,
	STXD_bp,
};
#define sBAL_CS		(PORTC.OUTCLR = (1 << BAL_CS_bp))
#define rBAL_CS		(PORTC.OUTSET = (1 << BAL_CS_bp))

// DEBUG ONLY..!!..ST-101 hardware.....
//#define sPWR_EN		(PORTC.OUTSET = (1 << PWR_EN_bp))	
//#define rPWR_EN		(PORTC.OUTCLR = (1 << PWR_EN_bp))
//#define sPWR_EN		(PORTA.OUTSET = (1 << 4))
//#define rPWR_EN		(PORTA.OUTCLR = (1 <<4))

#define sLCD_BKLT		(PORTC.OUTSET = (1 << LCD_BKLT_bp))
#define rLCD_BKLT		(PORTC.OUTCLR = (1 << LCD_BKLT_bp))

#define sLCD_DATA		(PORTC.OUTSET = (1 << LCD_CMD_bp))
#define sLCD_CMD		(PORTC.OUTCLR = (1 << LCD_CMD_bp))

#define sLCD_CS		(PORTC.OUTCLR = (1 << LCD_CS_bp))
#define rLCD_CS		(PORTC.OUTSET = (1 << LCD_CS_bp))

#define sLCD_RST		(PORTC.OUTCLR = (1 << LCD_RST_bp))
#define rLCD_RST		(PORTC.OUTSET = (1 << LCD_RST_bp))

enum CSpotentiometer
{
	BALadj,
};


enum PortD_id
{
	V1_65_bp,
	SPKR_EN_bp,
	COL3_bp,
	COL2_bp,
	COL1_bp,
	ROW3_bp,
	ROW2_bp,
	ROW1_bp,
};
#define sSPKR_EN		{	SpkrTone.Flag.Bit.Enabled = ON;			\
								(PORTD.OUTSET = (1 << SPKR_EN_bp));		\
								DelayMS(10);	};
								
#define rSPKR_EN		{	(PORTD.OUTCLR = (1 << SPKR_EN_bp));		\
								SpkrTone.Flag.Bit.Enabled = OFF;	}
								
#define PWRKEY			(~(PORTD.IN) & (1 << ROW1_bp))


#define sBEEP				(PORTD.OUTSET = (1 << SPKR_EN_bp))
#define rBEEP				(PORTD.OUTCLR = (1 << SPKR_EN_bp))

// The below Debug bit should only be used when SPKR is Idle (not being used).
#define sDEBUG			(PORTD.OUTSET = (1 << SPKR_EN_bp))
#define rDEBUG			(PORTD.OUTCLR = (1 << SPKR_EN_bp))
//------------------------------------------------------------------------------ 
// Global Structures
//------------------------------------------------------------------------------
/*
volatile struct SysVars
{
	uint8_t AudioType;
	uint16_t *pSound;				// Pointer to active beep string
	uint8_t BeepStringFlag;
	uint16_t BeepDuration;
	uint8_t CycleComplete;
	uint8_t Test;
};
volatile struct SysVars SysInfo;
*/
//
//---------------------------------------------------
//	Speaker Enable/Disable (see SpkrTone.Flag.Bit.Enabled)
enum BeepTbl
{
	SPKR_OFF,
	SPKR_ON = 0x02,					// PD1, HWD dependent.
};

struct TestResults
{
	unsigned int TestCompleted;					// Bit equate for test performed.
	
	float Volts[AVAILABLE_TERMINALS];
	
	uint8_t	CktType;							// 2+ 4 +4
};

volatile struct TestResults Meter;

struct ToneSettings
{
	uint8_t Mode;					// Beeper M_BEEP, M_TONE
	uint16_t *pSound;				// Pointer to active beep string
	uint8_t BeepStringFlag;
	uint8_t SpkrEnable;
	uint16_t BeepDuration;
	uint8_t BeepState;
	uint8_t StatFlag;
	uint8_t LineStatus;
	uint16_t ACVoltsRing;
	uint16_t ACVoltsTip;
	uint16_t ACVoltsTR;
	int16_t DCVoltsTR;
	float CalACVTR;
	float CalACVring;
	float CalACVtip;
};
struct ToneSettings volatile ToneInfo;
uint16_t SendSound[7][2];		// Line/Local Tone Sounds.

//------------------------------------------------------------------------------
//	The below structure is used to synchronize tone transmission with metering. 
struct ToneSync
{
	uint16_t Freq;
	uint16_t	ActiveTm;
	uint16_t IdleTm;
	uint16_t	ActiveCnt;
	uint16_t IdleCnt;
	uint8_t SineIndex;
	union Flag
	{
		uint8_t Byte;
		struct Bit
		{
			uint8_t RefD:1;
			uint8_t Enabled:1;		// Tone sequence Enabled, HWD dependent PD1.
			uint8_t SoT:1;				// Start of Tone Flag.
			uint8_t EoT:1;				// End of Tone
			uint8_t SoS:1;				// Start of Sequence.
			uint8_t EoS:1;				// End of Sequence.
			uint8_t Extra:2;
		}Bit;
	}Flag;
};
volatile struct ToneSync SpkrTone, XmitTone;
extern volatile struct ToneSync SpkrTone;
//
//---------------------------------------------------
//	Test Flags
struct TestSettings
{
	uint8_t TestIndex;			// Test index for Active Test.
	uint8_t Terminal;				// Terminal being tested.
	uint8_t SysStatFlag;			// If Battery Test is Allowed.
	uint8_t	DAC_EnableFlag;	//	ENABLE if DAC should update in PROBE mode.
	uint8_t Buzz;					// When Xmit tone has Short/fault.
	int8_t Cflag;
};
struct TestSettings TestInfo;
//
//-----------------------------------------------------------------------------
//
#define AERIAL_FREQS	2			// Number of Transmit frequency choices.
#define AXWIDTH	36				// Width of the Msg box.
#define LEVELS		3				// Levels of tone output

struct LocateData {
	uint16_t Fbase;				// Freq
	uint8_t x;						// Screen Location
	uint8_t y;
	uint8_t w;
	char *pMsg;						// Freq String
};

extern const struct LocateData AxData[AERIAL_FREQS];
extern const struct LocateData LvlData[LEVELS];
//
//---------------------------------------------------

//------------------------------------------------------------------------------
//	SPI Device
struct SPI_information
{
	unsigned char XmitCnt;
	unsigned char ByteCnt;
	volatile unsigned char Status;
	unsigned char Buffer[4];
};
extern struct SPI_information volatile SPIinfo;
//
//-----------------------------------------------------------------------------
//
#define	EECAL			(uint16_t *)(0x0000)
#define CKSUMOFFSET		0x55			// CALibrate Checksum Offset

enum Choices{
	FREQSEL,
	LEVELSEL,
	CALIBRATE,
};


struct CAL_information
{
	uint16_t BALpot;
	uint16_t	BeeperLevel;
	uint16_t	ToneLevel;
	uint8_t MonitorLevel;				// Directs ADC to Speaker.
//	int16_t	Vapplied;					// Weak Tone pull- ups
//	int16_t	VoffsetCorrection;
	uint16_t VacTR;
	uint16_t VacTip;
	uint16_t VacRing;
	uint16_t Frequency;
	uint8_t AxIndex;
	uint8_t LevelIndex;
	uint8_t	BackLight;
	uint8_t	Contrast;
	uint8_t CorrectionFlag;				// ON if correction constants are to be used.
	uint16_t CheckSum;
}SYSdata;
//
//---------------------------------------------------
//
/*
struct A2D
{
	uint16_t Samples;
	long HighPeak;
	long LowPeak;
	int16_t Vpp;
	int32_t Accumulator;
	uint8_t SampleFlag;
	uint8_t Group;
};
extern struct A2D A2Dresult;
*/
//
//----------------------------------------------------------------------------
int XmitSineTable[16];
int SpkrSineTable[16];

//-----------------------------------------------------------------------------
//	Global Variables
//-----------------------------------------------------------------------------
extern volatile unsigned int DelayTimer1, DelayTimer2, ToneTimer;
extern uint8_t Keys, PreviousKeys, KeyDebounce;
extern uint8_t LedIndex, LEDserviceFlag;
volatile uint8_t ISRcountStatus1, ISRcountStatus2;

extern uint16_t RepeatTm, RepeatCnt, Repeat;
extern uint8_t data, BeepEnable, RepeatFlag;

extern volatile unsigned char ISRcountStatus1, ISRcountStatus2;
extern volatile uint16_t ISRcount1, ISRcount2;
extern unsigned int Previous_OCR1B_cnt, BeepDuration, DACvolume;

//-----------------------------------------------------------------------------
uint16_t KyReg, FKey, AllowedKeys;
uint8_t TalkFlag, PreviousTalkFlag, PreviousBstat;

unsigned int BeepDuration, SampleIndex, SampleArrayIndex;
extern unsigned int BeepDuration, SampleIndex, SampleArrayIndex;

extern uint16_t KyReg, FKey; 
extern int SpkrSineTable[16];
extern int XmitSineTable[16];
extern uint16_t KeyXlate[];

extern const uint16_t PwrDwnSound[9][2];
extern const uint16_t KPsound[3][2];
extern const uint16_t ToneBuzzSound[9][2];
extern const uint16_t TalkBeepSound[7][2];
extern const uint16_t CALAdjSound[7][2];
extern const uint16_t CALAckSound[7][2];

extern uint8_t volatile VolumeLevel;
extern volatile uint16_t *pSound;
uint8_t PreviousPercent;
//
//-----------------------------------------------------------------------------
extern char *pSMPLX_1T, *pSMPLX_3T, *pTRIPLEX;
/*
uint16_t KyReg, AllowedKeys;
volatile uint16_t FKey;
extern volatile uint16_t FKey;
extern uint16_t KyReg, AllowedKeys;
uint8_t PreviousBstat;
extern uint8_t PreviousBstat, Bstat, SysStatFlag;
*/
//------------------------------------------------------------------------------
// Prototypes:
//------------------------------------------------------------------------------
int main(void);

void InitPorts(void);
void InitA2D(void);
void InitDAC_A(void);
void InitTCC4(void);
void InitTCC5(void);
void InitTCD5_QDEC(void);
void InitSPI_C(void);
void InitSystem(void);

uint16_t GetKp(uint16_t ValidKeys);

void Delay(int ISRticks);
unsigned char KeyPushService(void);
int16_t GetADC_Channel(uint8_t Channel);

//extern void LoadTonePattern(uint8_t Color);
extern void LoadSpkrLevel(uint8_t);
extern void LoadXmitLevel(void);


void SetISRcount2(unsigned int Value);
uint16_t GetTC0PreScaler(void *tc);
void UpDatePotentiometer(uint16_t Balance);
uint8_t GetBeepVolume(void);
void PowerDown(void);
void SwapTonePattern(void);

extern void SendSPI(char ByteQty);
extern void SetSPI_Mode2(void);
extern void SetSPI_Mode3(void);

extern uint8_t LCD_PutCharacter(char *pString, int FontType, int x, int y, int video);
extern int ACDCvalue(void);
extern int ADCBitValue(uint8_t Channel, uint8_t Samples);
extern void ServiceSetup(void);
extern void DisplayFreqLevel(void);
extern void LCD_OpenWindow(int x, int y, int h, int w, unsigned char border);
extern void LCD_DrawBox(int x, int y, int h, int w, int lines, unsigned char pattern);
extern void NewModeMessage(char *pString);

//------------------------------------------------------------------------------
//	TTS003 Analog Method Support.
//
extern struct AnalogMeter Mtr60;
extern struct AnalogMeter Mtr54;
struct AnalogMeter
{
	uint16_t	x;							// Horizontal Position
	uint16_t	y;							// Vertical Position
	uint16_t	r;							// Height
	uint16_t x30;						// Radius * cos30, Meter swing for 120 degrees.
	uint16_t a45;						// Pixel for x=y (45 degrees) for given radius.
};
extern int LocateMeter_Y(int Xa, struct AnalogMeter *Type);
extern int16_t Ypoint(int Xa, struct AnalogMeter *Mtr);
extern int LocateMeter_X(uint8_t Percent, struct AnalogMeter *Mtr);
extern void LCD_LinearAnalogFace(struct AnalogMeter *Type);




#endif

