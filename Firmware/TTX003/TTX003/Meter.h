//----------------------------------------------------------------------------
//	File Name: Meter.h
//
//	Content:
//
//		Header files associated with TTS003 "Meter.c" 
//
// 	Date:  30 July 2018
//
//	Copyright 2018 Allied AnaLogic, Inc
//	All rights reserved
//
//===========================================================================
#ifndef METER_H
#define METER_H


#include "TTX003.h"
#include <avr/io.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stddef.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>



#define V_THRESHOLD		2.5		// Clearance value for Resistance Testing.
#define ADCVREF			1.65		// ADC_REFSEL_AREFA_gc CPU ADC reference.

//==============================================================================
#define  VPP_ZERO_DBM   2.19056	// Volts Peak to Peak into 600 ohms = 0 dbM

//	The below are multiplied x10 for higher resolution integer math.
//
#define  MAXVDC_APPLIED		120			//	High Voltage PS Level on Tone Output
#define	VPP_APPLIED			120			// Nominal Vpp Applied
#define  VPP_TRIPLEX_MAX	40				// Approx TG or RG * 2
#define  VPP_APPLIED_L		100

#define	BUZZ_THRESHOLD	20				// % AC change to set buzz condition.
#define	IDLE_THRESHOLD	15				//	% AC change to reset buzz condition.
//
#define ATTENUATIONLIMIT	4			// 8 //Divisor used in ISR ADC->DAC

//------------------------------------------------------------------------------
//	Mixer gain is approximately 20db. Since the maximum Vpp from the mixer is
//	~900mV a post gain of 4 is used to amplify ADC input.
//
#define MIXERGAIN_db		12				// Approximated Mixer Gain.
#define TERM135_Vpp		2.19		//1.039			// Vpp into 135 ohms (0dbm)
#define POSTGAIN_db		12				// Gain x4 = 12 db
//
//------------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//	A2D Channel Equates
//-----------------------------------------------------------------------------
#define	DIFF_W_GAIN			ON			// Differential with Gain, Signed
#define	UNSIGNED_SINGLE	OFF		// Unsigned SingleEnded

enum A2D_posmux_id{
	ADC_POSMUX_0,
	ADC_POSMUX_1,
	ADC_POSMUX_2,
	ADC_POSMUX_3,
	ADC_POSMUX_4,
	ADC_POSMUX_5,
	ADC_POSMUX_6,
	ADC_POSMUX_7,

	ADC_POSMUX_8,
	ADC_POSMUX_9,
	ADC_POSMUX_10,
	ADC_POSMUX_11,
	ADC_POSMUX_12,
	ADC_POSMUX_13,
	ADC_POSMUX_14,
	ADC_POSMUX_15,
};

enum ADCGAIN
{
	ADC_G1,
	ADC_G2,
	ADC_G4,
	ADC_G8,
	ADC_G16,
	ADC_G32,
	ADC_G64,
	};
	

enum ADC_PATH_SEL
{
	ACV_T,
	ACV_R,
	TDR_ADC,
	ACDC,
	FLT_AC,
};
	

extern uint8_t ADC_MUX_Selection[5];
/*
enum A2Dbitmode
{
	A2D_8R	= 0x04,
	A2D_12R	= 0x00,
	A2D_12L	= 0x06,
};
*/

#define	A2D_SE_MAXVALUE	4095

//A2D Intervals of 83.37uSec determined by TCC1 CCA count of 667 * 125nSec Clock 
//-----------------------------------------------------------------------------
#define	STME16MS			200		// value * 83.3uSec =16.67mS
#define	STME8MS			100		// 8ms
#define	STME4MS			50			// 4ms
#define	STME2MS			25			// 25 samples at 83.375uSec/Sample

#define	LEVEL_L		10					// Lower than 10% of Full Scale.
#define	LEVEL_H		90					// Higher than 90% of Full Scale.

#define	ACLOW	1
#define	ACHIGH	0

#define	REFVBITS		225				// 1.1Vref/4.883mV = 225

#define SIGNALBINS	100

#define	MAXMETALLIC			65
#define	MIDBEEP				20

//------------------------------------------------------------------------------

enum BatteryStatus
{
	BEMPTY,
	BHALF,
	BFULL,
	EXTPWR,
	BCRITICAL,									// Flag system to be turned off...
	BSHUTDOWN,
};
#define EXTCHGFLG		(~PORTA.IN & (1 << CHRG_bp))


//	NOTE:  MODIFY for production.......	
//-----------------------------------------------------------------------------
//	The Tone sender places a tone and high impedance source voltage on the line.
//	Simplex tone uses a change in Vdc to determine if a short is on the line.
//	All other tone modes measures Vac. 
//	
//	Internal +/-7 volts source applied through 50k resistors.  Meter "R"
//	is approx 500K, therefore:
//-----------------------------------------------------------------------------
#define	BUZZ_THRESHOLD	20		// % AC change to set buzz condition.
#define	DASH_THRESHOLD	15		//	% AC change to reset buzz condition.
#define  DELTA_HI			120	// % DC change for buzz condition.
#define	DELTA_LO			80
//#define	VAC_MINTONE		5		// Circuit Developing < 5Vac has a short.
#define	MIN_DELTA_DC	2		// +/- 2V Change
//#define  MIN_DELTA_AC	50		// 5 volt Vpp change to trip buzz/idle flag.


//---------------------------------------------------
//
struct A2D
{
	unsigned int Samples;
	long HighPeak;
	long LowPeak;
	int Vpp;
	int32_t Accumulator;
	uint8_t SampleFlag;
	uint8_t Group;
};
struct A2D A2Dresult;
//
//---------------------------------------------------
//
#define SAMPLES_PER_CYCLE 400
//**********
#define A2DBUFFERSIZE	SAMPLES_PER_CYCLE
int16_t A2Dbuffer[A2DBUFFERSIZE];

int16_t HighArray[PK_ELEMENTS];
int16_t LowArray[PK_ELEMENTS];

uint8_t Bstat;
//
//------------------------------------------------------------------------------
//TDR
//-----------------------------------------------------------------------------
// FreqSweep() Equates
//-----------------------------------------------------------------------------
enum Plot_t
{
	SINGLE,
	DIFFERENTIAL,
};

enum Data_t
{
//	IMPEDANCE,						// Display & Log Impedance for Terminal.
//	PHASE,							// Display & Log Phase for Terminal.
	DECIBEL,
//	ZXIMPEDANCE,					// Mixer Vdc Impedance scan.
	TDR,
};


//------------------------------------------------------------------------------
//	Circuit ID sample frequencies.  enum FREQ is used to point into the FreqStep
//	structure.
//------------------------------------------------------------------------------
struct FreqStep
{
	uint16_t	Frq22Khz;
	uint16_t	Frq32Khz;
	uint16_t	Frq260Khz;
	
	uint16_t	Frq276Khz;
	uint16_t	Frq382Khz;
	uint16_t	Frq400Khz;
	
	uint16_t	Frq700Khz;
	uint16_t	Frq771Khz;
	uint16_t	Frq1121Khz;
	
	uint16_t	Frq1544Khz;
	uint16_t	Frq2800Khz;
	uint16_t	Frq3200Khz;
};

struct FreqStep FreqTable;
//
//------------------------------------------------------------------------------

#endif /* METER_H_ */
