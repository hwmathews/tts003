|-----------------------------------------------------------------------------------------------------------|
|Bill Of Materials for DS-TT3-0001revA2_Logic0_Logic0.sch_1 on Sat Oct 06 10:21:49 2018                     |
|-----------------------------------------------------------------------------------------------------------|
|Item |Qty|Reference|Manufacturer       |Part Number        |Description                  |Value    |Cost   |
|-----+---+---------+-------------------+-------------------+-----------------------------+---------+-------|
|1    |3  |Q2-4     |FairChild/ONsemi   |BSS138             |NMOSFET, SOT23               |         |$ 0.10 |
|2    |6  |D10-15   |MOTOROLA           |BAV99LT1           |Dual Switching Diode, SMT    |         |$ 0.04 |
|3    |17 |C2 C5 C14|Kemet              |C0805C105K3RAC     |CAP0805, 10%, X7R, 25V       |1.0uF    |$ 0.07 |
|     |   |C18      |                   |                   |                             |         |       |
|     |   |C27-35   |                   |                   |                             |         |       |
|     |   |C39      |                   |                   |                             |         |       |
|     |   |C43-44   |                   |                   |                             |         |       |
|     |   |C50      |                   |                   |                             |         |       |
|4    |7  |C1 C21   |Kemet              |C0805C104K5RAC     |CAP0805, 10%, X7R            |100nF    |$ 0.03 |
|     |   |C45-47   |                   |                   |                             |         |       |
|     |   |C51-52   |                   |                   |                             |         |       |
|5    |4  |C36 C23  |Kemet              |C0805C104K5RAC     |CAP0805, 10%, X7R, 50V       |100nF    |$ 0.03 |
|     |   |C75-76   |                   |                   |                             |         |       |
|6    |1  |C41      |Kemet              |C0805C103J5RAC     |CAP0805, 5%, X7R             |10nF     |$ 0.03 |
|7    |1  |C16      |Murata             |GRM21BR71H154KA01L |CAP0805, 10%, X7R            |150nF    |$ 0.08 |
|8    |1  |C20      |Kemet              |C0805C102J5GAC     |CAP0805, 5%, COG             |1nF      |$ 0.03 |
|9    |1  |C12      |Kemet              |C0805C221J5GAC     |CAP0805, 5%, COG             |220pF    |$ 0.03 |
|10   |1  |C42      |Kemet              |C0805C223K3RAC     |CAP0805, 10%, X7R            |22nF     |$ 0.03 |
|11   |1  |C48      |Kemet              |C0805C391J5RAC     |CAP0805, 5%, X7R             |390pF    |$ 0.03 |
|     |   |         |                   |                   |                             |(DNS)    |       |
|12   |2  |C37-38   |Kemet              |C0805C472K5RACTU   |CAP0805, 10%, X7R            |4.7nF    |$ 0.03 |
|13   |1  |C24      |Kemet              |C0805C471K5RACTU   |CAP0805, 10%, X7R            |470pF    |$ 0.03 |
|14   |9  |C3-4 C11 |Yageo              |CC1206KKX5R8BB106  |CAP1206, Ceramic X7R         |10uF     |$ 0.12 |
|     |   |C13 C15  |                   |                   |                             |         |       |
|     |   |C17 C19  |                   |                   |                             |         |       |
|     |   |C22 C74  |                   |                   |                             |         |       |
|15   |5  |C6-10    |Kemet              |T491B226M016AT     |CAP3528, "B", 20%            |22uF     |$ 0.47 |
|16   |2  |C40 C49  |Panasonic          |ECQ-E2475JF        |Radial Film Capacitor        |4.7uF    |$ 0.95 |
|17   |1  |J6       |                   |                   |Do Not Populate              |         |NS     |
|18   |1  |J2       |Molex              |22-23-2021         |GENERIC 2 PIN SIP HEADER .100|         |$ 0.24 |
|     |   |         |                   |                   |CENTERS                      |         |       |
|19   |1  |HS1      |Ohmite             |DA-T263-201        |Extruded Heat Sink, SMD, D2  |         |$ 2.10 |
|20   |1  |U13      |Siliconix          |DG470EQ            |SPDT MSOP8 Analog Switch     |         |$ 1.04 |
|21   |2  |L5 L8    |Coilcraft          |LPS6235-685MR      |SMT EMI Filter               |6.8mH    |$ 1.21 |
|22   |2  |U5-6     |Fairchild          |FDC6330L           |Integrated Load Switch       |         |$ 0.35 |
|23   |1  |J4       |Molex              |52207-0633         |FFC / FPC ZIF, 6 position,   |         |$ 0.86 |
|     |   |         |                   |                   |Top Contact                  |         |       |
|24   |1  |J3       |CnC Tech           |3221-10-0300-00    |Header, 10pin, 2x5, 1.27mm,  |         |$ 0.57 |
|     |   |         |                   |                   |Open Sides                   |         |       |
|25   |3  |L1-3     |Panasonic          |ELL-6UH-100        |SMT Inductor                 |10uH     |$ 0.62 |
|26   |1  |L4       |Panasonic          |ELL-6PG-2R2N       |SMT Inductor                 |2.2uH    |$ 0.62 |
|27   |2  |L6-7     |Coilcraft          |1812LS-104XJ       |Inductor                     |100uH    |$ 0.71 |
|28   |1  |D3       |KingBright         |APTD1608-ZGC       |SMT LED, GRN, 780mcd, 525nm  |         |$ 0.33 |
|29   |1  |D4       |KingBright         |APTD-1608-SURCK    |LED0603, Red                 |         |$ 0.16 |
|30   |1  |U2       |TI (National)      |LM2731YMF          |Boost Converter              |         |$ 1.21 |
|31   |1  |U4       |TI (National)      |LM2611BMF/NOPB     |Boost Inverter               |         |$ 1.21 |
|32   |1  |U10      |TI                 |TLV2172 ID         |RR op Amp, SO8, Low Offset,  |         |$ 1.38 |
|     |   |         |                   |                   |36 Vdd                       |         |       |
|33   |5  |D1-2 D5-7|On Semi            |MBRA160T3          |SMA Schottky                 |1A, 60V  |$ 0.23 |
|34   |1  |U12      |MicroChip          |MCP4151-103EMS     |Digital Pot, SO8, 10K, 8Bit  |         |$ 0.92 |
|35   |1  |U3       |MicroChip          |MCP73843-420 I/MS  |Lithium Ion Charge Controller|         |$ 1.02 |
|36   |2  |Q5-6     |Diodes Inc         |FMMT551 TA         |PNP Si, SOT-23               |         |$ 0.29 |
|37   |2  |Q7-8     |Diodes Inc         |FMMT451 TA         |NPN Si, SOT-23               |         |$ 0.29 |
|38   |1  |LCD1     |Display Tech       |64128L-FC-BW-3     |Graphic LCD                  |         |$ 18.00|
|39   |1  |TRD1     |PUI Audio          |AT-1224-TWT-R      |Piezo Transducer, 5V         |         |$ 1.04 |
|40   |1  |J1       |CUI                |PJ-102BH           |Pwr Cntr, 2.5mm Center, Thru |         |$ 0.34 |
|     |   |         |                   |                   |Hole                         |         |       |
|41   |1  |R19      |Xicon              |292-1.0K-RC        |RES0805, 1%, 1/10W           |1.0K     |$ 0.01 |
|42   |1  |R12      |Xicon              |292-1.5K-RC        |RES0805, 1%, 1/10W           |1.50K    |$ 0.01 |
|43   |1  |R20      |Xicon              |292-10R0-RC        |RES0805, 1%, 1/10W           |10.0     |$ 0.01 |
|44   |9  |R1-2 R9  |Xicon              |292-100K-RC        |RES0805, 1%, 1/10W           |100K     |$ 0.01 |
|     |   |R13      |                   |                   |                             |         |       |
|     |   |R15-16   |                   |                   |                             |         |       |
|     |   |R37-38   |                   |                   |                             |         |       |
|     |   |R52      |                   |                   |                             |         |       |
|45   |1  |R18      |Xicon              |292-10K-RC         |RES0805, 1%, 1/10W           |10K      |$ 0.01 |
|46   |1  |R5       |Xicon              |292-12.1K-RC       |RES0805, 1%, 1/10W           |12.1K    |$ 0.01 |
|47   |2  |R25 R27  |Xicon              |292-165K-RC        |RES0805, 1%, 1/10W           |165K     |$ 0.01 |
|48   |3  |R28      |Xicon              |292-2.21K-RC       |RES0805, 1%, 1/10W           |2.21K    |$ 0.01 |
|     |   |R33-34   |                   |                   |                             |         |       |
|49   |4  |R24 R29  |Xicon              |292-20K-RC         |RES0805, 1%, 1/10W           |20.0K    |$ 0.01 |
|     |   |R46-47   |                   |                   |                             |         |       |
|50   |2  |R44-45   |Xicon              |292-200K-RC        |RES0805, 1%, 1/10W           |200K     |$ 0.01 |
|51   |2  |R48-49   |Xicon              |292-3.32K-RC       |RES0805, 1%, 1/10W           |3.32K    |$ 0.01 |
|52   |1  |R51      |Xicon              |292-330-RC         |RES0805, 1%, 1/10W           |330      |$ 0.01 |
|53   |2  |R111-112 |Xicon              |292-365K-RC        |RES0805, 1%, 1/10W           |365K     |$ 0.01 |
|54   |2  |R6 R14   |Yageo              |RC0805FR-074K75L   |RES0805, 1%, 1/10W           |4.75K    |$ 0.01 |
|55   |1  |R17      |Xicon              |292-4.99K-RC       |RES0805, 1%, 1/10W           |4.99K    |$ 0.01 |
|56   |2  |R7 R11   |Xicon              |292-46.4K-RC       |RES0805, 1%, 1/10W           |46.4K    |$ 0.01 |
|57   |3  |R3 R35-36|Xicon              |292-49.9K-RC       |RES0805, 1%, 1/10W           |49.9K    |$ 0.01 |
|58   |2  |R23 R26  |Xicon              |292-499-RC         |RES0805, 1%, 1/10W           |499      |$ 0.01 |
|59   |4  |R4 R8    |Xicon              |292-499K-RC        |RES0805, 1%, 1/10W           |499K     |$ 0.01 |
|     |   |R40-41   |                   |                   |                             |         |       |
|60   |2  |R42-43   |Xicon              |292-6.81K-RC       |RES0805, 1%, 1/10W           |6.81K    |$ 0.01 |
|61   |4  |R21-22   |Xicon              |292-7.5K-RC        |RES0805, 1%, 1/10W           |7.5K     |$ 0.01 |
|     |   |R31-32   |                   |                   |                             |         |       |
|62   |1  |R10      |Vishay/Dale        |UR732BTTDR100F     |RES1206, 1%, 1/2W            |0.100    |$ 0.62 |
|63   |2  |R30 R39  |TE Connectivity    |3521330RFT         |RES2512, 2W, 1%              |330      |$ 0.16 |
|64   |1  |J5       |Molex              |43860-0007         |RJ45-8 with Light Pipes, RA  |         |$ 0.86 |
|     |   |         |                   |                   |Thru Hole, Non-Shield        |         |       |
|65   |1  |Q1       |NXP Semiconductor  |PMK50XP,518        |Power MOSFET, SO8            |         |$ 0.42 |
|66   |1  |SPKR1    |                   |IPH-5              |iPhone Speaker               |         |$ 2.20 |
|67   |2  |U11 U14  |TI                 |TLV2772ID          |RR op Amp, SO8, Low Offset   |         |$ 2.20 |
|68   |3  |U7 U9 U15|TI                 |TLV2371IDBV        |OPAMP, RR I/O, SOT23-5       |         |$ 1.50 |
|69   |4  |TP3 TP8  |DNS                |                   |Test Point, 0.050" dia. pad  |         |       |
|     |   |TP10-11  |                   |                   |                             |         |       |
|70   |9  |TP1-2    |                   |                   |Test Point, 0.050" dia. pad  |DNS      |       |
|     |   |TP4-7 TP9|                   |                   |                             |         |       |
|     |   |TP13-14  |                   |                   |                             |         |       |
|71   |1  |MH_BL    |DNS                |                   |Test Point, 0.050" dia. pad  |MH BL    |       |
|72   |1  |MH_BR    |DNS                |                   |Test Point, 0.050" dia. pad  |MH BR    |       |
|73   |1  |MH_ML    |DNS                |                   |Test Point, 0.050" dia. pad  |MH ML    |       |
|74   |1  |MH_MR    |DNS                |                   |Test Point, 0.050" dia. pad  |MH MR    |       |
|75   |1  |MH_TL    |DNS                |                   |Test Point, 0.050" dia. pad  |MH TL    |       |
|76   |1  |MH_TR    |DNS                |                   |Test Point, 0.050" dia. pad  |MH TR    |       |
|77   |1  |U33      |TI                 |TPA2005D1DGN       |Class D, 1.4W Audio          |         |$ 1.27 |
|     |   |         |                   |                   |Amplifier, MSOP8             |         |       |
|78   |1  |U1       |TI                 |LM3671MF-3.3/NOPB  |Buck Controller, 3.3V Fixed  |         |$ 0.86 |
|79   |2  |D8-9     |SemTech            |UCLAMP3304A-TCT    |Uni-directional TVS Array    |         |$ 0.43 |
|80   |1  |K1       |Vishay             |VOR1142-B4         |SSR, 1 Form A                |         |$ 1.86 |
|81   |1  |U8       |ATMEL              |ATXMEGA32E5-AU     |CPU, Xmega 32K flash, 4K SRAM|         |$ 4.50 |
|-----------------------------------------------------------------------------------------------------------|
