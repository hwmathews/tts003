/*
 * DDDS.h
 *
 * Created: 1/13/2013 9:46:40 PM
 *  Author: Administrator
 */ 


#ifndef DDS_H_
#define DDS_H_

//-----------------------------------------------------------------------------
//	DDS_Frequency = Freq * (f_mclk/2pi), where pi = 0x1000 0000, or 2^28,
//						 or decimal 2* 268,435,456.
//
//	Frequency resolution is:  BCLK/2pi, 0.059605 Hz
//
//	DDS_PhaseOffset = PhaseRegister * (2pi/4096), where 2pi = 360 degrees,
//	Phase resolution is:  360degrees/4096 = .087890625 degrees.
//-----------------------------------------------------------------------------
//	CPU Clock Frequency of 8MHz, Timer 1 PreScaler = 8, clock period 1uSec.
//	NOTE: 2 ISR cycles = 1 period.
//
#ifdef	CLK8MHZ
//
#define CNT_1MS				125		// 1mS timer uses /64 to yield 8us 8 125 = 1mS
//
//=============================================================================
//	CPU Clock Frequency of 16MHz.
#else
#define CNT_1MS				250		// 1mS timer uses /64 to yield 4us * 250 = 1mS


//-----------------------------------------------------------------------------
//	DDS Defines.              .18626451562		//
//-----------------------------------------------------------------------------
//#define	DDS_FREQ_RESOLUTION		.09313226		//	50MHz input
//#define	DDS_FREQ_RESOLUTION		(.09313226*2)	//	25MHz input
//#define DDS_FREQ_RESOLUTION		0.18626451562
#define DDS_FREQ_RESOLUTION		0.059604644775		// 16 MHz input.
#define	DDS_PHASE_RESOLUTION		0.087890625
#define	DDS_BIT			2											//	2
#define	DDS_PORT			PORTB										// PORTB
#define	DDS_ENABLE		{(DDS_PORT &= ~(1 << DDS_BIT));}
#define	DDS_DISABLE		{(DDS_PORT |= (1 << DDS_BIT));}
	
//	CONTROL Register Selection, Lower bits are data variables.
#define	DDS_P1		0xe000
#define	DDS_P0		0xc000					// Phase CH 0, DDS word routing address.
#define	DDS_F1		0x8000
#define	DDS_F0		0x4000					// Freq CH 0, DDS word routing address
#define	DDS_CR		0x0000					// Control Register.

// CONTROL Register DATA VARIABLES
#define	DDS_B28		0x2000					// Requires two 14 bit words for freq.
#define	DDS_P1SEL	0x0400					// Select Phase Register 1.
#define	DDS_F1SEL	0x0800					// Select Freq Register 1.
#define	DDS_RST		0x0100
//#define	DDS_DSLEEP	0x0040					// DAC sleep Bit
#define	DDS_DSLEEP	0x00c0					// DAC sleep Bits
#define	DDS_MSBSEL	0x0020					// MSB Output Select
#define	DDS_DIV1		0x0008					// MSB Output divide by 1 (ZERO = /2).
#define	DDS_TRIAG	0x0002					// Triangle instead of SIN output.
//#define	DDS_SLEEP	(DDS_B28 | DDS_MSBSEL | 0x0080)					// Sleep Bit
//#define  DDS_WAKE		(DDS_B28 | DDS_MSBSEL | DDS_DIV1)	// 
#define	DDS_SLEEP	(DDS_B28 | DDS_DSLEEP)					// Sleep Bit
#define  DDS_WAKE		(DDS_B28 | DDS_DIV1)	// 
/*
//---------------------------------------------------
//
struct SPI_information
{
	unsigned char XmitCnt;
	unsigned char ByteCnt;
	volatile unsigned char Status;
	unsigned char Buffer[4];
};
extern struct SPI_information volatile SPIinfo;
//
//---------------------------------------------------
*/

extern void DDSreset(void);
extern void SendSPI(char ByteQty);
extern void SetSPI_Mode2(void);
extern void SetSPI_Mode3(void);
extern void DDSfreq(float FreqHz, unsigned int Channel);
extern void DDSphase(float Phase, unsigned int Channel);
extern void DDSctl(unsigned int CtlWord);


#endif 

#endif /* DDS_H_ */