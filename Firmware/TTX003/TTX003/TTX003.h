//-----------------------------------------------------------------------------
//
// File Name: TTX003.h
//
// Content:
//
//    Date:  30 July 2018
//
//    Header file for the TriTone Transceiver using ATxmega128a3u.
//
// Copyright 2018 Allied Analogic, Inc
// All rights reserved
//
//=============================================================================
// $Log:$
//	V1.00		14 Oct 2018	hwm			// Initial work Files.
//	V1.01		10 Jan 2019	hwm		
//				- reversed Simplex3T voltage sign to negative to match analog
//				- TDR gain indicator from "A" to "G".
//				- Moved GetSysData before Init of BackLight.
//				- Modified Tone routines to disable ADC rather than stop TCC1 Clock
//				  (Stopping clock continually triggered ADC preventing 1mS ISR svc)
//	V1.02		13 Jan 2019 hwm
//				- Clear ZOOM mode on exit of VoP setting to force display update.
// V2.01		28 Jan 2019	hwm
//				- Added firmware to measure voltage and noise levels.
//					* 16MHz xtal, x2 PLL
//					* 
// v2.02		18 Feb 2019	hwm
//				- Correct Send Freq in SMPLX-1T after level change "Set". 
// v2.03		18 Feb 2019 hwm
//				- Added rMXSEL in SetFunction Path to allow Vdc readings
// v2.04		21 Feb 2019	hwm
//				- Added SysData.MeterType to SetParity Requirement.
// v2.05		23 Feb 2019 hwm
//				- Added FloorNoise calibration.
//				- Use VDC path for TR values above ~-6db
//	v2.06		26 Feb	2019	hwm
//				- Correct Tone Transmit Volts TR after DAP125 Scan function.
// v2.07		26 Feb	2019 hwm
//				- Noise Floor correction using Vpp over time.
// v2.08		11 Mar	2019 hwm
//				- Modified input resistance to extend Vdc range.
//				- Improved User Interface for switching between Vac/Vdc.
//				- Removed CFG option for Probe Volume in Idle.
//	v2.09		12 Mar	2019	hwm
//				- Eliminate KEYPUSH after CFG, Volume.
//				- For compatibility with field units revert R84 back to 365K input.....
//=============================================================================
#ifndef TTX003_H_
#define TTX003_H_

#include <avr/io.h>
#include <stdio.h>
#include "stddef.h"

#define PRODUCTSTRING "TTX 003 V2.09"
//
//-----------------------------------------------------------------------------
// Use 32 MHz RC Clock Source.... PSA divide by 4... PSBC default divide by 1.
//
#define CONFIG_SYSCLK_SOURCE			CLK_SCLKSEL_RC32M_gc
#define CONFIG_SYSCLK_PSADIV			CLK_PSADIV_1_gc
#define CONFIG_SYSCLK_PSBCDIV			CLK_PSBCDIV_1_1_gc
#define CLKper								32e6						// Peripheral CLK freq.
#define CLKcpu								CLKper					// CPU Clock Freq.

enum ADC_EVSEL_EVACT
{
	ADC_EVACT_CH01 = ADC_EVACT1_bm,
	ADC_EVSEL_CH0123 = 0x00,
};
/*
enum pwm_clk_sel {
	PWM_CLK_OFF     = TC45_CLKSEL_OFF_gc,
	PWM_CLK_DIV1    = TC45_CLKSEL_DIV1_gc,
	PWM_CLK_DIV2    = TC45_CLKSEL_DIV2_gc,
	PWM_CLK_DIV4    = TC45_CLKSEL_DIV4_gc,
	PWM_CLK_DIV8    = TC45_CLKSEL_DIV8_gc,
	PWM_CLK_DIV64   = TC45_CLKSEL_DIV64_gc,
	PWM_CLK_DIV256  = TC45_CLKSEL_DIV256_gc,
	PWM_CLK_DIV1024 = TC45_CLKSEL_DIV1024_gc,
};
*/

#if !defined(__bool_true_false_are_defined)
#define false     0
#define true      1
#endif

typedef	unsigned char uint8_t;
typedef	unsigned char	bool;

//-----------------------------------------------------------------------------
//	General Operation Definitions.
//-----------------------------------------------------------------------------
#define	ON					1
#define	OFF				0

#define ENABLE				ON
#define DISABLE			OFF

#define	HIGH				1
#define	LOW				0

#define	ACTIVE			1
#define	COMPLETE			0

#define	BUZZ				ON
#define	IDLE				OFF

//-----------------------------------------------------------------------------
//	Probe DSL, Tone
//-----------------------------------------------------------------------------
#define NEWGRAPH		ON
#define UPDATE_GR		OFF

enum FunctionMsg
{
	GAIN_MSG,
	SQL_MSG,
	VOL_MSG,
};

enum Function
{
	F_METER,
	F_TONE,
	F_TDR,
	F_PROBE,
};

enum Mode
{
	M_VAC,
	M_VDC,
};

enum VACfunction
{
	NOISE,
	ACMETER,
};

//-----------------------------------------------------------------------------
//	A2D related Definitions.
//-----------------------------------------------------------------------------
#define	A2Dsamples			2
#define	PK_ELEMENTS			8
#define	CHANNEL_MASK 		0x0f

//------------------------------------------------------------------------------
//	TDR 
//
#define CAL_ITEMS			10				// Production Calibration Steps
#define TDRGAINSTEPS		4
#define TDR_ACAVG			3				//TDR Sample Groups
#define VOP_FS			.98356			// VoP Feet/nSec in free space.
		
//-----------------------------------------------------------------------------
//	Battery / FrontEnd Divisors.
//-----------------------------------------------------------------------------
#define		R3			49.9e3
//#define		R5			4.9e3					// Debug using STX-101
#define		R5			12.1e3

#define		R40		6.8e3
#define		R42		6.8e3
#define		R84		365e3

#define		R45		200e3						// AC Tip to Earth Input
#define		R47		20e3
#define		R35		49.9e3

#define		R44		R45
#define		R46		R47

#define DCTR_FEGAIN		R42/(R40+R84)		// AC/DC Tip to Ring Gain.
#define ACTR_FEGAIN		R42/R40
#define GADJ				1		//.64
#define ACT_FEGAIN		((R47/R45) * GADJ)	// AC Tip to Earth Gain
#define ACR_FEGAIN		((R46/R44) * GADJ)	// AC Ring to Earth Gain
#define SINE_MIDSCALE	2047
//------------------------------------------------------------------------------
//	Timing Equates in MilliSeconds.
//------------------------------------------------------------------------------
#define	ONWAITTM				1000		// Keypress hold time for TURN ON
#define	KEY_RPT_TM1			750		//	First Time interval for Repeat Key ACK.
#define	KEY_RPT_TM2			450		//150
#define	DEBI					60			//30 Number of Timer0 ISR counts to debounce

#define ROW_MASK_gm			0x07		// PORTC, D2, D2, D0..... Input pullups
#define ROW_MASK_bp			0

#define COLUMN_MASK_gm		0x39		// PORTC, D3,4,5     .....Output LOW..
#define COLUMN_MASK_bp		3

//-----------------------------------------------------------------------------
//	Meter and Tone Terminal equates.
//-----------------------------------------------------------------------------
enum Pattern_id{
	BLUE,
	GREEN,
	RED,
	CYAN,
};

enum Termial_id{
	TR,
	RING,
	TIP,
	SMPLX_1T,
	SMPLX_3T,
	TRIPLEX,
};

#define AVAILABLE_TERMINALS 3
#define NORM_VAC_TR					40			// Typical VAC TR in TriPlex
#define NORM_VAC_GND					20			// Typical VAC TG/RG Smplx and TriPlex

//------------------------------------------------------------------------------
//	TestIndex
//
enum TestType
{
	NONE,
	TDRSWEEP,
	MULTISWEEP,
	TRACK,
	PROBE,
};

#define NONE	OFF
#define TRACKSAMPLES		16

//-----------------------------------------------------------------------------
//	Frequency Span equates.
//-----------------------------------------------------------------------------
enum span_code
{
	VOICEBANDLOW,
	VOICEBANDMED,
	VOICEBANDHIGH,
	WIDEBANDSPAN,
};

enum mx_span
{
	MX_ADSL,
	MX_ADSL2,
	MX_VDSL,
	MX_VDSLh,
	LN_AUDIO,
	MX_HDSL,
	MX_T1,
};
#define MX_SPANS		3
#define MX_SCALES		2

//-----------------------------------------------------------------------------
//	KeyPush Equates   for 4x4 matrix.  (See KeyXlate[] table in TTX003.c)
//-----------------------------------------------------------------------------

enum FunctionKey
{
	K_NONE		= 0x0000,
	K_FREQ		= 0x0001,
	K_LEVEL		= 0x0002,
	K_CFG			= 0x0004,
	K_F1			= 0x0008,
	K_F2			= 0x0010,
	K_F3			= 0x0020,
	K_PWR			= 0x0040,
	K_CLR			= 0x0080,
	K_SPR1		= 0x0100,
	K_SPR2		= 0x0200,
	K_SPR3		= 0x0400,
	K_SPR4		= 0x0800,
	K_SPR5		= 0x1000,
	K_SPR6		= 0x2000,
	K_SPR7		= 0x4000,
	K_SPR8		= 0x8000,
	K_START	   = K_F1,
	K_STOP		= K_F1,
	K_EVALUATE	= K_F2,
	K_EXIT		= K_F2,
	K_SET			= K_F1,
	K_RANGE		= K_FREQ,
	K_UP			= K_F3,
	K_DOWN		= K_F2,
	K_OK			= K_F1,
	K_RETEST		= K_F1,
	K_IGNORE		= K_F3,
};

#define HOTKEYS	(K_CLR | K_FREQ | K_CFG | K_LEVEL)
#define ALLKEYS	(K_CLR | K_FREQ | K_CFG | K_LEVEL | K_F1 | K_F2 | K_F3)
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//	System Settings:
//------------------------------------------------------------------------------
#define LCD_WIDTH_PIXELS					(128)
#define LCD_HEIGHT_PIXELS              (32)

#define LCD_DISPLAY_CONTRAST_MAX			80
#define LCD_DISPLAY_CONTRAST_MIN			70
#define SPKR_LVL_L							10
#define SPKR_LVL_H							100


//------------------------------------------------------------------------------
//	Port Definitions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//	Port A definitions
//------------------------------------------------------------------------------
enum PortA
{
	V1_650_bp,
	FLT_AC_bp,
	ACDC_bp,
	TDR_ADC_bp,
	V1_651_bp,
	ACV_T_bp,
	ACV_R_bp,
	CHRG_bp,
};

//------------------------------------------------------------------------------
//	Port B definitions
//------------------------------------------------------------------------------
enum PortB
{
	V_CPU_bp,
	VBAT_bp,
	SPKR_AUD_bp,
	SINE_bp,
	CS_BAL_bp,
	MXSEL_bp,
	SMPLX_bp,
	SPKR_EN_bp,
};
#define sBAL_CS		(PORTB.OUTCLR = (1 << CS_BAL_bp))
#define rBAL_CS		(PORTB.OUTSET = (1 << CS_BAL_bp))

#define sSMPLX			(PORTB.OUTSET = (1 << SMPLX_bp))
#define rSMPLX			(PORTB.OUTCLR = (1 << SMPLX_bp))
	
#define sMXSEL			(PORTB.OUTSET = (1 << MXSEL_bp))
#define rMXSEL			(PORTB.OUTCLR = (1 << MXSEL_bp))

#define sSPKR_EN		{	SpkrTone.Flag.Bit.Enabled = ON;			\
								(PORTB.OUTSET = (1 << SPKR_EN_bp));		\
								DelayMS(10);	};
								
#define rSPKR_EN		{	(PORTB.OUTCLR = (1 << SPKR_EN_bp));		\
								SpkrTone.Flag.Bit.Enabled = OFF;	}

//------------------------------------------------------------------------------
//	Port C definitions
//------------------------------------------------------------------------------
enum PortC_id
{
		ROW1_bp,
		ROW2_bp,
		ROW3_bp,
		COL1_bp,
		COL2_bp,
		COL3_bp,
		PC6__bp,
		CLKPER_bp,
};

#define PWRKEY			(~(PORTC.IN) & (1 << ROW1_bp))

enum CSpotentiometer
{
	BALadj,
};


//------------------------------------------------------------------------------
//	Port D definitions
//------------------------------------------------------------------------------
enum PortD_id
{
	LCD_CS_bp,
	LCD_RST_bp,
	LCD_CMD_bp,
	LCD_BKLT_bp,
	CS_DDS_bp,
	MOSI_bp,
	PD6_bp,
	SCLK_bp,
};

#define sLCD_BKLT		(PORTD.OUTSET = (1 << LCD_BKLT_bp))
#define rLCD_BKLT		(PORTD.OUTCLR = (1 << LCD_BKLT_bp))

#define sLCD_DATA		(PORTD.OUTSET = (1 << LCD_CMD_bp))
#define sLCD_CMD		(PORTD.OUTCLR = (1 << LCD_CMD_bp))

#define sLCD_CS		(PORTD.OUTCLR = (1 << LCD_CS_bp))
#define rLCD_CS		(PORTD.OUTSET = (1 << LCD_CS_bp))

#define sLCD_RST		(PORTD.OUTCLR = (1 << LCD_RST_bp))
#define rLCD_RST		(PORTD.OUTSET = (1 << LCD_RST_bp))

#define sFSYNC			(PORTD.OUTCLR = (1 << CS_DDS_bp))
#define rFSYNC			(PORTD.OUTSET = (1 << CS_DDS_bp))

#define sCS_DDS		(PORTD.OUTCLR = (1 << CS_DDS_bp))
#define rCS_DDS		(PORTD.OUTSET = (1 << CS_DDS_bp))
								

//------------------------------------------------------------------------------
//	Port E definitions
//------------------------------------------------------------------------------
enum PortE_id
{
	TDR_DRV_bp,
	RLYCOM_bp,
	RING2MH_bp,
	TIP2ML_bp,
	DAPSEL_bp,
	RLY_ACDC_bp,
	RLY_TONE_bp,
	RLY_TDR_bp,
};
#define	SSRMASK			((1 << RING2MH_bp) | (1 << TIP2ML_bp) | (1 << DAPSEL_bp))
//
//------------------------------------------------------------------------------
//	Port F definitions
//------------------------------------------------------------------------------
enum PortF_id
{
	FX100_bp,
	PWR_EN_bp,
	TP9_bp,
	EN_G_bp,
	MX_EN_bp,
	PF5_bp,
	CHRGR_bp,
	LTC_CLK_bp,
};

#define	CLK_E				{sRLY_CLK; SoftDelay(); rRLY_CLK;}
#define	SSR_SEL			{PORTE.OUT = (SSRData & SSRMASK); CLK_E;}
//------------------------------------------------------------------------------
//	SSRData stores true bit value with output inverted so as to produce a logic
//	value of ZERO to turn on the ring and tip relays.
//
//	NOTE: Modifying the below bits should NOT molest latching relay bits.
#define sR2MH	{SSRData |= (1 << RING2MH_bp); SSR_SEL;}
#define rR2MH	{SSRData &= (~(1 << RING2MH_bp)); SSR_SEL;}
#define sT2ML	{SSRData |= (1 << TIP2ML_bp);  SSR_SEL;}
#define rT2ML	{SSRData &= (~(1 << TIP2ML_bp));  SSR_SEL;}
#define sTR		{SSRData = (1 << RING2MH_bp) | (1 << TIP2ML_bp); SSR_SEL;}
#define rTR		{rR2MH; rT2ML;}
#define sDAP	{SSRData |= (1 << DAPSEL_bp);  SSR_SEL;}
#define rDAP	{SSRData &= (~(1 << DAPSEL_bp));  SSR_SEL;}
		
//------------------------------------------------------------------------------
#define sPWR_EN	(PORTF.OUTSET = (1<< PWR_EN_bp))
#define rPWR_EN	(PORTF.OUTCLR = (1<< PWR_EN_bp))

#define sE2S		(PORTF.OUTCLR = (1 << EN_G_bp))
#define rE2S		(PORTF.OUTSET = (1 << EN_G_bp))

#define sTP9		(PORTF.OUTSET = (1 << TP9_bp))
#define rTP9		(PORTF.OUTCLR = (1 << TP9_bp))

#define sMX_EN		(PORTF.OUTSET = (1 << MX_EN_bp))
#define rMX_EN		(PORTF.OUTCLR = (1 << MX_EN_bp))
//------------------------------------------------------------------------------
//	Latching Relay Operation Macro's
//------------------------------------------------------------------------------
#define sRLY_CLK		(PORTF.OUTSET = (1 << LTC_CLK_bp))
#define rRLY_CLK		(PORTF.OUTCLR = (1 << LTC_CLK_bp))
//
//-----------------------------------------------------------------------------
// Relay Pulse Select Macros..... with pulse width/collision control.
// NOTE:	Relay Common is attached to the SET pin.
//
#define RLYTM			{DelayMS(5);}				// Pulse Width in MilliSeconds
#define STROBE	{CLK_E; RLYTM; PORTE.OUT = SSRData; CLK_E; RLYTM;}

enum RLY_id{						// d1 = Common = HIGH to SET
	RLY0_R = 0x7f,					// (+) Side High, (-) side Low
	RLY1_R = 0xbf,					// 1011 1111b, All high except selected relay.
	RLY2_R = 0xdf,
	RLY3_R = 0xef,
	RLY4_R = 0xfe,
	RLY5_R = 0xfd,

	RLY0_S = 0x80,					// 1000 0000b, All low except selected relay.
	RLY1_S = 0x40,					// d1 = Common = LOW to RESET
	RLY2_S = 0x20,
	RLY3_S = 0x10,
	RLY4_S = 0x08,
	RLY5_S = 0x04,	
};

//	NOTE: Retain SSRData bits while Latching Set/Reset Relay Coil.
//
#define sTDR	{rE2S; PORTE.OUT = (RLY0_S | SSRData); STROBE;}
#define rTDR	{PORTE.OUT = (RLY0_R & ~SSRMASK) | SSRData; STROBE;}
	
#define sTONE	{sE2S; PORTE.OUT = (RLY1_S | SSRData); STROBE;}
#define rTONE	{PORTE.OUT = ((RLY1_R & ~SSRMASK) | SSRData); STROBE;}

//	NOTE:  E2S must be set to measure RG or TG, released for TR....	
#define sVAC	{PORTE.OUT = (RLY2_S | SSRData); STROBE;}
//
//	NOTE:  E2S must be set to measure RG or TG, released for TR....
#define sVDC	{PORTE.OUT = ((RLY2_R & ~SSRMASK) | SSRData); STROBE;}

#define sDAP_HF	{rTDR; rE2S; sMX_EN; sMXSEL;} 
#define sDAP_LF	{rTDR; rE2S; sMX_EN; rMXSEL;} 
	
#define TDRSTEP				0
#define TDRPULSE				1
//-----------------------------------------------------------------------------
//	Graph structures containing x,y coordinates for vertical and horizontal
//	positions and messages.
//-----------------------------------------------------------------------------
#define HVTAGQTY		5

struct GraphTags
{
	const char *pStrg;
};
extern struct GraphTags TDRSpanTags[1][HVTAGQTY];

//------------------------------------------------------------------------------ 
// Global Structures
//------------------------------------------------------------------------------
/*
volatile struct SysVars
{
	uint8_t AudioType;
	uint16_t *pSound;				// Pointer to active beep string
	uint8_t BeepStringFlag;
	uint16_t BeepDuration;
	uint8_t CycleComplete;
	uint8_t Test;
};
volatile struct SysVars SysInfo;
*/
//
//---------------------------------------------------
//	Speaker Enable/Disable (see SpkrTone.Flag.Bit.Enabled)
enum BeepTbl
{
	SPKR_OFF,
	SPKR_ON = 0x02,					// PD1, HWD dependent.
};

#define SIGNALBINS	100
#define	SPLID_BINS		KHz3200 +1	// Number of Sample Frequencies.
//------------------------------------------------------------------------------
//	Circuit ID sample frequencies.  enum FREQ is used to point into the FreqStep
//	structure.
//------------------------------------------------------------------------------
//	Circuit ID Position freq pointers for array and structure index.
enum FREQ
{
	KHz22,
	KHz32,
	KHz50,							//KHz260,
	
	KHz276,
	KHz382,
	KHz400,
	
	KHz700,
	KHz771,
	KHz1121,
	
	KHz1544,
	KHz2800,
	KHz3200,
};

struct TestResults
{
	unsigned int TestCompleted;					// Bit equate for test performed.
	
	float Volts[AVAILABLE_TERMINALS];
	float StartFreq;
	int16_t SignalBin[AVAILABLE_TERMINALS][SIGNALBINS];
	uint8_t SplSignalBin[SPLID_BINS];
	uint8_t	CktType;							// 2+ 4 +4
};

volatile struct TestResults Meter;

//-----------------------------------------------------------------------------
//	Structure types used for Impedance measurements.
//-----------------------------------------------------------------------------
//	SweepSetup: Calibration and Test Indexes for freq sweep.
struct SweepSetup
{
	double StartFreq;					// First Freq of Sweep
	double StepSize;					// StepSize
	uint16_t StepQty;
	float Magnitude;
};
struct SweepSetup Zmeter;

//	Single Impedance measurement for the current frequency.
struct SingleFreq
{
	double Freq;
	double Magnitude;
	double Rvalue;
	double Xvalue;
	double Cvalue;
	uint8_t Flag;
};
struct SingleFreq SinglePoint;
//
//------------------------------------------------------------------------------
//
struct ToneSettings
{
	uint8_t Mode;					// Beeper M_BEEP, M_TONE
	uint16_t *pSound;				// Pointer to active beep string
	uint8_t BeepStringFlag;
	uint8_t SpkrEnable;
	uint16_t BeepDuration;
	uint8_t StatFlag;
	uint8_t LineStatus;
	uint16_t ACVoltsRing;
	uint16_t ACVoltsTip;
	uint16_t ACVoltsTR;
	int16_t DCVoltsTR;
	float CalACVTR;
	float CalACVring;
	float CalACVtip;
};
struct ToneSettings volatile ToneInfo;
uint16_t SendSound[7][2];		// Line/Local Tone Sounds.

//------------------------------------------------------------------------------
//	The below structure is used to synchronize tone transmission with metering. 
struct ToneSync
{
	uint16_t Freq;
	uint16_t	ActiveTm;
	uint16_t IdleTm;
	uint16_t	ActiveCnt;
	uint16_t IdleCnt;
	uint8_t SineIndex;
	union Flag
	{
		uint8_t Byte;
		struct Bit
		{
			uint8_t RefD:1;
			uint8_t Enabled:1;		// Tone sequence Enabled, HWD dependent PD1.
			uint8_t SoT:1;				// Start of Tone Flag.
			uint8_t EoT:1;				// End of Tone
			uint8_t SoS:1;				// Start of Sequence.
			uint8_t EoS:1;				// End of Sequence.
			uint8_t Extra:2;
		}Bit;
	}Flag;
};
volatile struct ToneSync SpkrTone, XmitTone;
extern volatile struct ToneSync SpkrTone;
//
//---------------------------------------------------
//	Test Flags
struct TestSettings
{
	uint8_t Function;				// Send/Probe/Meter
	uint8_t Mode;					// Vac or Vdc measurement type.
	uint8_t TestIndex;			// Test index for Active Test.
	uint8_t Terminal;				// Terminal being tested.
	uint8_t Span;					// Freq Span Range (voice/wideband)
	uint8_t Scale;					// Meter Scale
	uint8_t ScaleLimit;			// Number of scales supported for FreqScan.
	uint8_t PlotType;				// SINGLE: T or R or TR, DIFFERENTIAL: TG + RG
	uint8_t DataType;				// IMPEDANCE, PHASE, dB.
	float FrontEndGain;			// FrontEnd Resistor Ratio
	uint8_t Mux;					// ADC Mux Pos | Neg channel path.
	uint8_t ADCgain;				// Differential Gain within the A2D converter.
	uint8_t MsgFlag;				// Gain/Volume/Squelch display decisions.
	uint16_t Delay;				// Delay Between Measurements.
	uint8_t SysStatFlag;			// If Battery Test is Allowed.
	uint16_t Vthreshold;			// AudioTrack DAC threshold.
	uint8_t	DAC_EnableFlag;	//	ENABLE if DAC should update in PROBE mode.
	uint8_t Buzz;					// When Xmit tone has Short/fault.
	int8_t Cflag;
};
struct TestSettings TestInfo;
//
//-----------------------------------------------------------------------------
//
struct TDRSettings
{
	uint16_t	PulseCycle;			// TCE0 count for TDR Sample period.
	uint8_t Mode;					// STEP/PULSE
	uint16_t PulseWidth;			//
	uint16_t Gain;					// Display gain setting.
	uint16_t SampleQuantity;	// TDR Samples/sweep
	int16_t	FirstStep;			// First measurement step in TDR sweep.
	uint16_t	StepSize;			// Number of clock cycles between TDR samples
	uint8_t	Function;			// Function keys, CURSOR/GAIN/PW (pulse only).
	uint8_t	ZoomSpan;
	uint8_t	CursorPosition;	//	0->100 pixel position of cursor
	uint8_t StatFlag;				// TDR Active/Complete.
	float PixelDistance;			// Pixel resolution
	float FirstStepDistance;	// Equates to VoP function of FirstStep in Feet.
};
struct TDRSettings volatile TDRInfo;

enum TDR_FKeys
{
	CURSOR,
	GAIN,
	PW,
	RANGE,
	ZOOM,
};
#define MAXRANGE	3				// TDR Coarse ranges 
#define MAXZOOMSPAN	3			// Limit Zoom Spans  
//
//-------------------------------------
//
#define AERIAL_FREQS	2			// Number of Transmit frequency choices.
#define AXWIDTH	36				// Width of the Msg box.
#define LEVELS		3				// Levels of tone output

struct LocateData {
	uint16_t Fbase;				// Freq
	uint8_t x;						// Screen Location
	uint8_t y;
	uint8_t w;
	char *pMsg;						// Freq String
};

extern const struct LocateData AxData[AERIAL_FREQS];
extern const struct LocateData LvlData[LEVELS];
//
//---------------------------------------------------

//------------------------------------------------------------------------------
//	SPI Device
struct SPI_information
{
	unsigned char XmitCnt;
	unsigned char ByteCnt;
	volatile unsigned char Status;
	unsigned char Buffer[4];
};
extern struct SPI_information volatile SPIinfo;
//
//-----------------------------------------------------------------------------
//
#define	EECAL			(uint16_t *)(0x0000)
#define CKSUMOFFSET		0x55			// CALibrate Checksum Offset

enum Choices{
	FREQSEL,
	LEVELSEL,
	CALIBRATE,
};


struct CAL_information
{
	uint8_t	VoP;							// 0-> 100 for percent of Velocity
	uint16_t BALpot;
	uint16_t	BeeperLevel;
	uint16_t	ToneLevel;
	uint8_t MonitorLevel;				// Directs ADC to Speaker.
	//---------------------------------------------------------------------------
	int16_t Voffset[3];					// Differential Path Offset Correction.
	//---------------------------------------------------------------------------
	uint16_t VDCgain[3];					// Differential Path DCgain Correction.
	//---------------------------------------------------------------------------
	uint16_t VACgain[3];					// Differential Path ACgain Correction.
	//---------------------------------------------------------------------------
	uint16_t VacTR;						// Tone Transmission Window Center.
	uint16_t VacTip;
	uint16_t VacRing;
	//---------------------------------------------------------------------------
	uint16_t Frequency;
	int8_t MixerCorrection;
	uint8_t AxIndex;
	uint8_t LevelIndex;
	uint8_t	BackLight;
	uint8_t	Contrast;
	uint8_t CorrectionFlag;				// ON if correction constants are to be used.
	uint8_t MeterType;					// db noise versus VAC.
	float FloorNoise;
	uint16_t CheckSum;
}SysData;
//
//---------------------------------------------------
//
/*
struct A2D
{
	uint16_t Samples;
	long HighPeak;
	long LowPeak;
	int16_t Vpp;
	int32_t Accumulator;
	uint8_t SampleFlag;
	uint8_t Group;
};
extern struct A2D A2Dresult;
*/
//
//----------------------------------------------------------------------------
int XmitSineTable[16];
int SpkrSineTable[16];

//-----------------------------------------------------------------------------
//	Global Variables
//-----------------------------------------------------------------------------
extern volatile unsigned int DelayTimer1, DelayTimer2, ToneTimer;
extern uint8_t Keys, PreviousKeys, KeyDebounce;
extern uint8_t LedIndex, LEDserviceFlag;
volatile uint8_t ISRcountStatus1, ISRcountStatus2;

extern uint16_t RepeatTm, RepeatCnt, Repeat;
extern uint8_t data, BeepEnable, RepeatFlag;

extern volatile unsigned char ISRcountStatus1, ISRcountStatus2;
extern volatile uint16_t ISRcount1, ISRcount2;
extern unsigned int Previous_OCR1B_cnt, BeepDuration, DACvolume;

//-----------------------------------------------------------------------------
uint16_t KyReg, FKey, AllowedKeys;
uint8_t TalkFlag, SSRData, PreviousBstat;

	volatile uint8_t Loop;					// Debug only.

unsigned int BeepDuration, SampleIndex, SampleArrayIndex;
extern unsigned int BeepDuration, SampleIndex, SampleArrayIndex;

extern uint16_t KyReg, FKey; 
extern int SpkrSineTable[16];
extern int XmitSineTable[16];
extern uint16_t KeyXlate[];

/*
extern const uint16_t PwrDwnSound[9][2];
extern const uint16_t KPsound[3][2];
extern const uint16_t ToneBuzzSound[9][2];
extern const uint16_t TalkBeepSound[7][2];
extern const uint16_t CALAdjSound[7][2];
extern const uint16_t CALAckSound[7][2];
*/
extern uint8_t volatile VolumeLevel;
extern volatile uint16_t *pSound;
uint8_t PreviousPercent;

#define VBARQTY	4
extern uint16_t Yprevious[VBARQTY];

//
//-----------------------------------------------------------------------------
extern char *pSMPLX_1T, *pSMPLX_3T, *pTRIPLEX;
/*
uint16_t KyReg, AllowedKeys;
volatile uint16_t FKey;
extern volatile uint16_t FKey;
extern uint16_t KyReg, AllowedKeys;
uint8_t PreviousBstat;
extern uint8_t PreviousBstat, Bstat, SysStatFlag;
*/
//------------------------------------------------------------------------------
// Prototypes:
//------------------------------------------------------------------------------
int main(void);

void InitPorts(void);
void InitA2D(void);
void InitDAC_A(void);
void InitTCC4(void);
void InitTCC5(void);
void InitTCD5_QDEC(void);
void InitSPI_C(void);
void InitSystem(void);

uint16_t GetKp(uint16_t ValidKeys);

void Delay(int ISRticks);
unsigned char KeyPushService(void);
int16_t GetADC_Channel(uint8_t Channel);

//extern void LoadTonePattern(uint8_t Color);
extern void LoadSpkrLevel(uint8_t);
extern void LoadXmitLevel(void);


void SetISRcount2(unsigned int Value);
uint16_t GetTC0PreScaler(void *tc);
void UpDatePotentiometer(uint16_t Balance);
uint8_t GetBeepVolume(void);
void PowerDown(void);
void SwapTonePattern(void);

extern void SendSPI(char ByteQty);
extern void SetSPI_Mode2(void);
extern void SetSPI_Mode3(void);

extern uint8_t LCD_PutCharacter(char *pString, int FontType, int x, int y, int video);
extern int ACDCvalue(uint8_t SignFlag, uint16_t Samples);
extern int ADCBitValue(uint8_t Channel, uint8_t Samples);
extern void ServiceSetup(void);
extern void DisplayFreqLevel(void);
extern void LCD_OpenWindow(int x, int y, int h, int w, unsigned char border);
extern void LCD_DrawBox(int x, int y, int h, int w, int lines, unsigned char pattern);
extern void NewModeMessage(char *pString);

//------------------------------------------------------------------------------
//	TTS003 Analog Method Support.
//
extern struct AnalogMeter Mtr60;
extern struct AnalogMeter Mtr54;
struct AnalogMeter
{
	uint16_t	x;							// Horizontal Position
	uint16_t	y;							// Vertical Position
	uint16_t	r;							// Height
	uint16_t x30;						// Radius * cos30, Meter swing for 120 degrees.
	uint16_t a45;						// Pixel for x=y (45 degrees) for given radius.
};
extern int LocateMeter_Y(int Xa, struct AnalogMeter *Type);
extern int16_t Ypoint(int Xa, struct AnalogMeter *Mtr);
extern int LocateMeter_X(uint8_t Percent, struct AnalogMeter *Mtr);
extern void LCD_LinearAnalogFace(struct AnalogMeter *Type);
extern uint16_t DeltaA2Dbuffer(void);
//

extern void DelayMS(unsigned int DelayTime);
extern void AcknowledgeSound(const uint16_t *pSound);
//
//	TDR definitions.
extern void LCD_PutCursorDistance(void);
extern void PutGainMsg(void);
extern void PutPWMsg(void);
extern void SetVoP(void);
extern void TDRplot(void);
extern void UpDateTDRFunctionWindows(void);
//
//
extern void Beeper(void);
extern void BuzzAcknowledge(void);
//
//
extern void CalNULL(void);
extern void CalVAC(void);
extern void Config(void);

extern void DisplaySelection(struct LocateData *pType, uint8_t HighLight);
extern void CheckBatteryVoltage(void);
extern void GetSysData(void);

extern void GetToneACDC(void);
extern void IdleScreen(void);
extern void IndicateBUZZ(void);
extern void InitBeepSequence(uint16_t *pSoundArray);
extern void InitPWM_BackLight(void);
extern void KPstat(uint16_t Mask);

extern void LCD_ClearWindow(uint8_t border, uint8_t video);
extern void LCD_CtrStrg(char *pString, int FontType, int Position, int video);
extern void LCD_DrawBox(int x, int y, int h, int w, int lines, unsigned char pattern);

extern void LCD_PutPixel(int x, int y, int action);
extern void LCD_PutString(char *pString, int FontType, int x, int y, int video);
extern void LoadTonePattern(uint8_t LineStatus);
//
//	Tone Definitions
//
extern void TonePairStatus(void);
extern void TonePulser(void);
extern void ToneTRBuzz(void);
extern void ToneTypeSimplex(void);
extern void ToneTypeSimplex(void);

extern void Wait4Key(uint16_t Mask);
extern void WarmStart(void);

extern void SetBackLight(void);
extern void SetContrast(void);
extern void SetMonitor(void);
extern void SetParity(void);
extern void SetSineFreq(uint16_t Freq);

extern void SetSound(void);
extern void SetVolume(void);
extern void SpkrAlert(const uint16_t *pSoundArray);
extern void SwapWindow(void);

extern void PutADCgainMsg(void);
extern void SetSnoopSpan(void);

extern int8_t GTValue(int16_t Largest, int16_t Smallest, uint8_t Amount);
extern int8_t WinValue(int16_t FirstValue, uint16_t SecondValue, uint8_t Amount);
extern void PutSqlMsg(uint8_t Squelch);
extern void PutADCgainMsg(void);
extern void PutSpkrVolumeMsg(void);
extern void UpdateDSL(void);
extern void SetFunctionPath(void);

extern void DoVAC(void);
extern void DoVDC(void);

extern void BackLightDutyCycle(uint8_t Percent);
extern void GetBatteryVoltage(void);
extern void SelectTerminal(void);
extern void SetADCDiffGain(uint8_t Path, uint8_t Gain);
extern void SetADCSingleChannel(uint8_t ADC_channel, uint8_t POSmux);
extern void SoftDelay(void);
extern void Dap105(void);
extern void Dap125(void);
extern void InitComb(void);
extern void PutMeter(char *pString);

extern float AutoVAC(uint8_t Path);
extern float AutoVDC(uint8_t Path);
extern void Volts2db(float Volts);
extern void GetVDC(void);
extern void GetVAC(void);


//extern void DisplayVOP(struct CmdWindow *pType, uint8_t HighLight);


#endif /* TTX003_H_ */