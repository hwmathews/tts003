/********************************************************************/
/*    Font map and character pixel data                             */
/********************************************************************/

#include <avr/pgmspace.h>

PROGMEM const unsigned char Times8Bold032bmp[] = {
	/* ../Times8Bold032bmp */
	0x3, 00, 00, 00, 00, 00, 
 /*

static const unsigned char Times8Bold033bmp[] = {
	/* ../Times8Bold033bmp */
	0x2, 0x3f, 0x53, 0xc0,
	/*

static const unsigned char Times8Bold034bmp[] = {
	/* ../Times8Bold034bmp */
	0x5, 0x6, 0xf7, 0xb0, 00, 00, 00, 00, 
	/*

static const unsigned char Times8Bold035bmp[] = {
	/* ../Times8Bold035bmp */
	0x6, 00, 0xa2, 0x8a, 0xfd, 0x4f, 0xd4, 0x50, 00, 00, 
	/*

static const unsigned char Times8Bold036bmp[] = {
	/* ../Times8Bold036bmp */
	0x5, 0x23, 0xab, 0xc7, 0x1c, 0xb5, 0x71, 00, 
	/*

static const unsigned char Times8Bold037bmp[] = {
	/* ../Times8Bold037bmp */
	0x8, 00, 0x42, 0xa4, 0xa8, 0xaa, 0x55, 0x15, 0x25, 0x42, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold038bmp[] = {
	/* ../Times8Bold038bmp */
	0x8, 00, 0x1c, 0x34, 0x34, 0x3b, 0x5a, 0xcc, 0xed, 0x76, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold039bmp[] = {
	/* ../Times8Bold039bmp */
	0x2, 0x3f, 00, 00, 
	/*

static const unsigned char Times8Bold040bmp[] = {
	/* ../Times8Bold040bmp */
	0x4, 0x1, 0x64, 0xcc, 0xcc, 0x46, 0x10, 
	/*

static const unsigned char Times8Bold041bmp[] = {
	/* ../Times8Bold041bmp */
	0x4, 0x8, 0x62, 0x33, 0x33, 0x26, 0x80, 
	/*

static const unsigned char Times8Bold042bmp[] = {
	/* ../Times8Bold042bmp */
	0x5, 0x1, 0x9, 0xf2, 0x28, 00, 00, 00, 
	/*

static const unsigned char Times8Bold043bmp[] = {
	/* ../Times8Bold043bmp */
	0x5, 00, 00, 0x42, 0x7c, 0x84, 00, 00, 
	/*

static const unsigned char Times8Bold044bmp[] = {
	/* ../Times8Bold044bmp */
	0x2, 00, 0x3, 0xd8, 
	/*

static const unsigned char Times8Bold045bmp[] = {
	/* ../Times8Bold045bmp */
	0x3, 00, 00, 0x38, 00, 00, 
	/*

static const unsigned char Times8Bold046bmp[] = {
	/* ../Times8Bold046bmp */
	0x2, 00, 0x3, 0xc0, 
	/*

static const unsigned char Times8Bold047bmp[] = {
	/* ../Times8Bold047bmp */
	0x3, 0x4, 0xa4, 0x94, 0x80, 00, 
	/*

static const unsigned char Times8Bold048bmp[] = {
	/* ../Times8Bold048bmp */
	0x5, 0x3, 0xb7, 0xbd, 0xef, 0x7b, 0x70, 00, 
	/*

static const unsigned char Times8Bold049bmp[] = {
	/* ../Times8Bold049bmp */
	0x4, 0xe, 0x66, 0x66, 0x66, 0xf0, 00, 
	/*

static const unsigned char Times8Bold050bmp[] = {
	/* ../Times8Bold050bmp */
	0x5, 0x3, 0xbf, 0x31, 0x9, 0x8f, 0xf8, 00, 
	/*

static const unsigned char Times8Bold051bmp[] = {
	/* ../Times8Bold051bmp */
	0x5, 0x3, 0xa6, 0x33, 0xc, 0x73, 0x70, 00, 
	/*

static const unsigned char Times8Bold052bmp[] = {
	/* ../Times8Bold052bmp */
	0x6, 00, 0x21, 0x8e, 0x5a, 0x6f, 0xc6, 0x18, 00, 00, 
	/*

static const unsigned char Times8Bold053bmp[] = {
	/* ../Times8Bold053bmp */
	0x5, 0x3, 0xdc, 0x8f, 0x7c, 0x3a, 0xe0, 00, 
	/*

static const unsigned char Times8Bold054bmp[] = {
	/* ../Times8Bold054bmp */
	0x5, 00, 0xd8, 0x8f, 0x6f, 0x7b, 0x70, 00, 
	/*

static const unsigned char Times8Bold055bmp[] = {
	/* ../Times8Bold055bmp */
	0x5, 0x7, 0xfe, 0x21, 0x8, 0x84, 0x20, 00, 
	/*

static const unsigned char Times8Bold056bmp[] = {
	/* ../Times8Bold056bmp */
	0x5, 0x3, 0xb7, 0xb7, 0x3b, 0x7b, 0x70, 00, 
	/*

static const unsigned char Times8Bold057bmp[] = {
	/* ../Times8Bold057bmp */
	0x5, 0x3, 0xb7, 0xbd, 0xbc, 0x46, 0xc0, 00, 
	/*

static const unsigned char Times8Bold058bmp[] = {
	/* ../Times8Bold058bmp */
	0x2, 00, 0xf3, 0xc0, 
	/*

static const unsigned char Times8Bold059bmp[] = {
	/* ../Times8Bold059bmp */
	0x2, 00, 0xf3, 0xd8, 
	/*

static const unsigned char Times8Bold060bmp[] = {
	/* ../Times8Bold060bmp */
	0x5, 00, 00, 0x17, 0x41, 0xc1, 00, 00, 
	/*

static const unsigned char Times8Bold061bmp[] = {
	/* ../Times8Bold061bmp */
	0x5, 00, 00, 0xf, 0x83, 0xe0, 00, 00, 
	/*

static const unsigned char Times8Bold062bmp[] = {
	/* ../Times8Bold062bmp */
	0x5, 00, 0x1, 0x7, 0x5, 0xd0, 00, 00, 
	/*

static const unsigned char Times8Bold063bmp[] = {
	/* ../Times8Bold063bmp */
	0x5, 0x3, 0xb7, 0xb1, 0x10, 0xc, 0x60, 00, 
	/*

static const unsigned char Times8Bold064bmp[] = {
	/* ../Times8Bold064bmp */
	0x9, 00, 0xf, 0x8, 0x48, 0x18, 0xdc, 0x9e, 0xcb, 0x6f, 
	0x5a, 0x90, 0x87, 0x80, 
	/*

static const unsigned char Times8Bold065bmp[] = {
	/* ../Times8Bold065bmp */
	0x8, 00, 0x18, 0x18, 0x1c, 0x2c, 0x3c, 0x46, 0x46, 0xef, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold066bmp[] = {
	/* ../Times8Bold066bmp */
	0x7, 0x1, 0xf9, 0x9b, 0x37, 0xcc, 0xd9, 0xb3, 0xfc, 00, 
	00, 
	/*

static const unsigned char Times8Bold067bmp[] = {
	/* ../Times8Bold067bmp */
	0x7, 00, 0x75, 0x9e, 0x1c, 0x18, 0x30, 0x31, 0x3c, 00, 
	00, 
	/*

static const unsigned char Times8Bold068bmp[] = {
	/* ../Times8Bold068bmp */
	0x8, 00, 0xfc, 0x66, 0x63, 0x63, 0x63, 0x63, 0x66, 0xfc, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold069bmp[] = {
	/* ../Times8Bold069bmp */
	0x6, 0x3, 0xf6, 0x5a, 0x79, 0xa6, 0x19, 0xfc, 00, 00, 
	/*

static const unsigned char Times8Bold070bmp[] = {
	/* ../Times8Bold070bmp */
	0x6, 0x3, 0xf6, 0x5a, 0x79, 0xa6, 0x18, 0xf0, 00, 00, 
	/*

static const unsigned char Times8Bold071bmp[] = {
	/* ../Times8Bold071bmp */
	0x8, 00, 0x3a, 0x66, 0xc2, 0xc0, 0xc0, 0xcf, 0x66, 0x3c, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold072bmp[] = {
	/* ../Times8Bold072bmp */
	0x9, 00, 0x7b, 0xd8, 0xcc, 0x67, 0xf3, 0x19, 0x8c, 0xc6, 
	0xf7, 0x80, 00, 00, 
	/*

static const unsigned char Times8Bold073bmp[] = {
	/* ../Times8Bold073bmp */
	0x4, 0xf, 0x66, 0x66, 0x66, 0xf0, 00, 
	/*

static const unsigned char Times8Bold074bmp[] = {
	/* ../Times8Bold074bmp */
	0x6, 00, 0xf1, 0x86, 0x18, 0x6d, 0xb6, 0x70, 00, 00, 
	/*

static const unsigned char Times8Bold075bmp[] = {
	/* ../Times8Bold075bmp */
	0x8, 00, 0xf7, 0x64, 0x68, 0x78, 0x78, 0x6c, 0x66, 0xf7, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold076bmp[] = {
	/* ../Times8Bold076bmp */
	0x7, 0x1, 0xe1, 0x83, 0x6, 0xc, 0x18, 0xb3, 0xfe, 00, 
	00, 
	/*

static const unsigned char Times8Bold077bmp[] = {
	/* ../Times8Bold077bmp */
	0xa, 00, 0x3c, 0xf7, 0x39, 0xce, 0x5d, 0x97, 0x65, 0xd9, 
	0x26, 0xeb, 0xc0, 00, 00, 
	/*

static const unsigned char Times8Bold078bmp[] = {
	/* ../Times8Bold078bmp */
	0x8, 00, 0xe7, 0x72, 0x5a, 0x5a, 0x4e, 0x46, 0x46, 0xe2, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold079bmp[] = {
	/* ../Times8Bold079bmp */
	0x8, 00, 0x3c, 0x66, 0xc3, 0xc3, 0xc3, 0xc3, 0x66, 0x3c, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold080bmp[] = {
	/* ../Times8Bold080bmp */
	0x7, 0x1, 0xf9, 0x9b, 0x36, 0x6f, 0x98, 0x30, 0xf0, 00, 
	00, 
	/*

static const unsigned char Times8Bold081bmp[] = {
	/* ../Times8Bold081bmp */
	0x8, 00, 0x3c, 0x66, 0xc3, 0xc3, 0xc3, 0xc3, 0x66, 0x3c, 
	0x18, 0xf, 00, 
	/*

static const unsigned char Times8Bold082bmp[] = {
	/* ../Times8Bold082bmp */
	0x8, 00, 0xfc, 0x66, 0x66, 0x78, 0x6c, 0x6c, 0x66, 0xf7, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold083bmp[] = {
	/* ../Times8Bold083bmp */
	0x5, 0x3, 0x77, 0x8f, 0x3c, 0x7b, 0xb0, 00, 
	/*

static const unsigned char Times8Bold084bmp[] = {
	/* ../Times8Bold084bmp */
	0x6, 0x3, 0xfb, 0x4c, 0x30, 0xc3, 0xc, 0x78, 00, 00, 
	/*

static const unsigned char Times8Bold085bmp[] = {
	/* ../Times8Bold085bmp */
	0x8, 00, 0xf7, 0x62, 0x62, 0x62, 0x62, 0x62, 0x62, 0x3c, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold086bmp[] = {
	/* ../Times8Bold086bmp */
	0x8, 00, 0xf7, 0x62, 0x62, 0x34, 0x34, 0x34, 0x18, 0x18, 
	00, 00, 00,
	/*

static const unsigned char Times8Bold087bmp[] = {
	/* ../Times8Bold087bmp "W"  */
	0xc, 00, 0xe, 0xf7, 0x66, 0x26, 0x24, 0x37, 0x43, 0x74, 
	0x19, 0x81, 0x98, 0x19, 0x80, 00, 00, 00, 
	/*

static const unsigned char Times8Bold088bmp[] = {
	/* ../Times8Bold088bmp  "X"  */
	0x7, 0x1, 0xed, 0x91, 0xc3, 0x3, 0xe, 0x26, 0xee, 00, 
	00, 
	/*

static const unsigned char Times8Bold089bmp[] = {
	/* ../Times8Bold089bmp  "Y"  */
	0x8, 00, 0xf7, 0x62, 0x34, 0x34, 0x18, 0x18, 0x18, 0x3c, 
	00, 00, 00, 
	/*

static const unsigned char Times8Bold090bmp[] = {
	/* ../Times8Bold090bmp "Z"  */
	0x7, 00, 0xfd, 0x32, 0xc1, 0x86, 0xc, 0xb3, 0xfe, 00, 
	00, 
	/*

static const unsigned char Times8Bold091bmp[] = {
	/* ../Times8Bold091bmp "["  */
	0x3, 0x1f, 0x6d, 0xb6, 0xdb, 0x80, 
	/*

static const unsigned char Times8Bold092bmp[] = {
	/* ../Times8Bold092bmp "\"  */
	0x3, 0x12, 0x24, 0x91, 0x20, 00, 
	/*

static const unsigned char Times8Bold093bmp[] = {
	/* ../Times8Bold093bmp  "]"  */
	0x3, 0x1d, 0xb6, 0xdb, 0x6f, 0x80, 
	/*
//==============================================================================
static const unsigned char Times8Bold094bmp[] = {
	/* ../Times8Bold094bmp  "^"  */
//	0x5, 00, 0x8, 0xa5, 0x44, 00, 00, 00,
				// 
				// Test of the Ohms Symbol.... 11 pixels wide...!!!..
				//
	0xb, 0x1f,0x06,0x31,0x83,0x60,0x3c,0x06,0xc1,
	0x8c,0x60,0x88,0xf1,0xe0,0x00,0x00,
	/*
Remove 8 bytes to sync with lower case characters, loosing "-" character.

static const unsigned char Times8Bold095bmp[] = {
	/* ../Times8Bold095bmp */
	0x6, 00,// 00, 00, 00, 00, 00, 00, 0xf, 0xc0, 
	/*
//==============================================================================
static const unsigned char Times8Bold096bmp[] = {
	/* ../Times8Bold096bmp */
	0x2, 0x24, 00, 00, 
	/*

static const unsigned char Times8Bold097bmp[] = {
	/* ../Times8Bold097bmp */
	0x6, 00, 00, 00, 0x72, 0x67, 0xb6, 0x7c, 00, 00, 
	/*

static const unsigned char Times8Bold098bmp[] = {
	/* ../Times8Bold098bmp */
	0x5, 0x6, 0x31, 0x8f, 0x6f, 0x7b, 0xb0, 00, 
	/*

static const unsigned char Times8Bold099bmp[] = {
	/* ../Times8Bold099bmp */
	0x4, 00, 00, 0x7d, 0xcc, 0x70, 00, 
	/*

static const unsigned char Times8Bold100bmp[] = {
	/* ../Times8Bold100bmp */
	0x6, 00, 0xe1, 0x86, 0x7b, 0x6d, 0xb6, 0x7c, 00, 00, 
	/*

static const unsigned char Times8Bold101bmp[] = {
	/* ../Times8Bold101bmp */
	0x5, 00, 00, 0x7, 0x6f, 0xf8, 0x78, 00, 
	/*

static const unsigned char Times8Bold102bmp[] = {
	/* ../Times8Bold102bmp */
	0x5, 0x1, 0xda, 0xcf, 0x31, 0x8c, 0xf0, 00, 
	/*

static const unsigned char Times8Bold103bmp[] = {
	/* ../Times8Bold103bmp */
	0x6, 00, 00, 00, 0x7f, 0x67, 0x30, 0x7f, 0x37, 0x80, 
	/*

static const unsigned char Times8Bold104bmp[] = {
	/* ../Times8Bold104bmp */
	0x7, 0x1, 0xc1, 0x83, 0x7, 0x8d, 0x9b, 0x36, 0xee, 00, 
	00, 
	/*

static const unsigned char Times8Bold105bmp[] = {
	/* ../Times8Bold105bmp */
	0x4, 0x6, 0x60, 0xe6, 0x66, 0xf0, 00, 
	/*

static const unsigned char Times8Bold106bmp[] = {
	/* ../Times8Bold106bmp */
	0x3, 0xd, 0x8e, 0xdb, 0x6f, 00,
	/*

static const unsigned char Times8Bold107bmp[] = {
	/* ../Times8Bold107bmp */
	0x7, 0x1, 0xc1, 0x83, 0x6, 0xef, 0x1b, 0x36, 0xee, 00, 
	00, 
	/*

static const unsigned char Times8Bold108bmp[] = {
	/* ../Times8Bold108bmp */
	0x4, 0xe, 0x66, 0x66, 0x66, 0xf0, 00, 
	/*

static const unsigned char Times8Bold109bmp[] = {
	/* ../Times8Bold109bmp */
	0xa, 00, 00, 00, 00, 00, 0xfb, 0x1b, 0x66, 0xd9, 
	0xb6, 0xed, 0xc0, 00, 00, 
	/*

static const unsigned char Times8Bold110bmp[] = {
	/* ../Times8Bold110bmp */
	0x7, 00, 00, 00, 0xf, 0x8d, 0x9b, 0x36, 0xee, 00, 
	00, 
	/*

static const unsigned char Times8Bold111bmp[] = {
	/* ../Times8Bold111bmp */
	0x5, 00, 00, 0x7, 0x6f, 0x7b, 0x70, 00, 
	/*

static const unsigned char Times8Bold112bmp[] = {
	/* ../Times8Bold112bmp */
	0x5, 00, 00, 0xf, 0x6f, 0x7b, 0xf6, 0x38, 
	/*

static const unsigned char Times8Bold113bmp[] = {
	/* ../Times8Bold113bmp */
	0x6, 00, 00, 00, 0x6b, 0x6d, 0xb6, 0x78, 0x63, 0xc0, 
	/*

static const unsigned char Times8Bold114bmp[] = {
	/* ../Times8Bold114bmp */
	0x5, 00, 00, 0xf, 0xb5, 0x8c, 0xf0, 00, 
	/*

static const unsigned char Times8Bold115bmp[] = {
	/* ../Times8Bold115bmp */
	0x4, 00, 00, 0x7d, 0x6b, 0xe0, 00, 
	/*

static const unsigned char Times8Bold116bmp[] = {
	/* ../Times8Bold116bmp */
	0x4, 00, 0x22, 0xf6, 0x66, 0x70, 00, 
	/*

static const unsigned char Times8Bold117bmp[] = {
	/* ../Times8Bold117bmp */
	0x7, 00, 00, 00, 0xe, 0xcd, 0x9b, 0x36, 0x3e, 00, 
	00, 
	/*

static const unsigned char Times8Bold118bmp[] = {
	/* ../Times8Bold118bmp */
	0x5, 00, 00, 0xd, 0xeb, 0x4c, 0x60, 00, 
	/*

static const unsigned char Times8Bold119bmp[] = {
	/* ../Times8Bold119bmp */
	0x8, 00, 00, 00, 00, 0xdb, 0xda, 0xda, 0x6c, 0x6c, 
	00, 00, 00,
	/*

static const unsigned char Times8Bold120bmp[] = {
	/* ../Times8Bold120bmp */
	0x6, 00, 00, 00, 0xed, 0xc3, 0xe, 0xdc, 00, 00, 
	/*

static const unsigned char Times8Bold121bmp[] = {
	/* ../Times8Bold121bmp */
	0x6, 00, 00, 00, 0xed, 0xa7, 0xc, 0x22, 0x8c, 00, 
	/*

static const unsigned char Times8Bold122bmp[] = {
	/* ../Times8Bold122bmp */
	0x5, 00, 00, 0xf, 0x99, 0xcc, 0xf8, 00, 
	/*

static const unsigned char Times8Bold123bmp[] = {
	/* ../Times8Bold123bmp  "{"  */
	0x4, 0x3, 0x66, 0x48, 0x46, 0x66, 0x30, 
	/*

static const unsigned char Times8Bold124bmp[] = {
	/* ../Times8Bold124bmp "|"  */
	0x1, 0x7f, 0xe0, 
	/*

static const unsigned char Times8Bold125bmp[] = {
	/* ../Times8Bold125bmp  "}"  */
	0x4, 0xc, 0x66, 0x21, 0x26, 0x66, 0xc0, 
	/*

static const unsigned char Times8Bold126bmp[] = {
	/* ../Times8Bold126bmp */
	0x5, 00, 00, 00, 0x66, 0xe0, 00, 00,
	/*

static const unsigned char Times8Bold127bmp[] = {
	/* ../Times8Bold127bmp */
	0x6, 0x3, 0xf8, 0x61, 0x86, 0x18, 0x61, 0xfc, 00, 00, 
	};

PROGMEM const unsigned int Times8BoldOffset[] = {
	0, 6, 10, 18, 28, 36, 49, 
	62, 66, 73, 80, 88, 
	96, 100, 106, 110, 116, 
	124, 131, 139, 147, 157, 
	165, 173, 181, 189, 197, 
	201, 205, 213, 221, 229, 
	237, 251, 264, 275, 286, 
	299, 309, 319, 332, 346, 
	353, 363, 376, 387, 402, 
	415, 428, 439, 452, 465, 
	473, 483, 496, 509, 527, 
	538, 551, 562, 568, 574, 
	580, 588, 598, 602, 612, 
	620, 627, 637, 645, 653, 
	663, 674, 681, 687, 698, 
	705, 720, 731, 739, 747, 
	757, 765, 772, 779, 790, 
	798, 811, 821, 831, 839, 
	846, 849, 856, 864, 874, 
	};


