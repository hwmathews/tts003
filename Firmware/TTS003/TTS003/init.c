//----------------------------------------------------------------------------
//	File Name: init.c
//
//	Content:
//
// 	Date:  20 March 2010
//
// 	Contains initialization and ISR functions for BlueBox Tone Generator.
//
//	Copyright 2010 Allied Analogic, Inc
//	All rights reserved
//
//==============================================================================
#include "TTS003.h"
#include "Meter.h"
#include "lcd.h"
#include "SnoopyComb.h"
#include <avr/io.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>


extern const int SineTable16[16];




void ccp_write_io(void *Address, unsigned char value);

void ccp_write_io(void *Address, unsigned char value)
{
	asm("out	0x003b, r1");			// Reset bits 23:16 of Z
	asm("movw    r30, r24");		// Address of value to change (ADDR).
	asm("ldi     r18, 0xd8");		// Load magic CCP value
	asm("out     0x0034, r18");	// Start CCP handshake
	asm("st      Z, r22");			// Write value to I/O register
}


void InitSysClk(void)
//-----------------------------------------------------------------------------
//	Initialize System Clocks.... 32MHZ, with ADIV == 4... 
//-----------------------------------------------------------------------------
{
	OSC.CTRL	|= OSC_RC32MEN_bm;						// Enable the 32MHz Osc.
   while ((OSC.STATUS & OSC_RC32MRDY_bm)==0); 
	
	ccp_write_io((void *)&CLK.CTRL, CLK_SCLKSEL_RC32M_gc);
	
	ccp_write_io((void *)&CLK.PSCTRL, CLK_PSADIV_1_gc);
	
	OSC.CTRL &= ~(OSC_RC2MEN_bm);					// Disable the 2MHz Osc.
}



void InitPorts(void)
//------------------------------------------------------------------------------
//	Setup Ports I/O.
//	-----------------------------------------------------------------------------
{
	// Init PortA.
	//
//	PORTA.DIR = (( 1 << SMPLX_bp) | (1 << SPKR_bp) | (1 << SINE_bp));
//	sSMPLX;
//	Debug only..!!
	PORTA.DIR = (( 1 << SMPLX_bp) | (1 << SINE_bp));
	sSMPLX;

	// Init PortC.
	//
	PORTC.DIR = (( 1 << LCD_CS_bp) | (1 << LCD_RST_bp) | (1 << LCD_CMD_bp)
					| (1 << BAL_CS_bp) | (1 << SCLK_bp) 
					| (1 << LCD_BKLT_bp) | (1 << STXD_bp));
	rLCD_CS;
	rBAL_CS;
	rLCD_RST;
	//
	// Init PortD.
	//				
	PORTD.DIR = ((1 << COL3_bp) | (1 << COL2_bp)	| (1 << COL1_bp)
					| (1 << SPKR_EN_bp));

	//
	//	Add Pull-ups on pushbutton rows.
	//
	PORTCFG.MPCMASK = ((1 << ROW3_bp) | (1 << ROW2_bp) | (1 << ROW1_bp));
	PORTD.PIN1CTRL	= PORT_OPC_PULLUP_gc;

	// Init PortR.
	//
	PORTR.DIR = ((1 << EN_G_bp) | (1 << PWR_EN_bp));
	sPWR_EN;

}


void InitTCC4(void)
//-----------------------------------------------------------------------------
// TCC4 is used to generate a 1mS interrupt for system timer using OVF IF.  
//	Compare register TCC4.CCD resets the output pin to control the duty cycle
//	of the LCD backlight drive.
//------------------------------------------------------------------------------
{
	InitPWM_BackLight();
	//
	//	Enable ISR for counter Overflow.
	//
	TCC4.INTCTRLA = TC45_OVFINTLVL_LO_gc;	// Low priority ISR on timer overflow.
}


void InitPWM_BackLight(void)
//-----------------------------------------------------------------------------
//	Initialize TimerType4 on PortC as the 1000 HZ Timebase for LCD backlight PWM.
//-----------------------------------------------------------------------------
{
	// Set the port output for CH_D.
	//
	TCC4.CTRLE = TC45_CCDMODE_COMP_gc;			// Enable Output Compare bit CCD.
	
	//	Set the TimerCounter for Single Slope PWM operation. PERiod Buffer is the
	//	top or counter reset when counting UP.
	//
	TCC4.CTRLB = TC45_WGMODE_SINGLESLOPE_gc;	// Interrupt on PER Top.
	
	// Setup coarse and fine divisor for counter.
	//
	TCC4.CTRLA = TC45_CLKSEL_DIV8_gc;			// 32MHz/8 = 4MHz
	TCC4.PERBUF = 4000;								// 4MHz/4000 = 1000Hz (1mSec)
	
	//	Set the duty cycle based on the initial default in SYSdata.
	//
	TCC4.CCD = 0;
	BackLightDutyCycle(SYSdata.BackLight);
}

void BackLightDutyCycle(uint8_t Percent)
//------------------------------------------------------------------------------
//	PWM duty cycle is based on the percent of maximum count.
//------------------------------------------------------------------------------
{
	long Count;
	
	cli();								// Disable System Interrupts for 16 bit update.
	Count = TCC4.PERBUF;
	
	Count *= Percent;
	Count /= 100;
	TCC4.CCDBUF = (uint16_t)Count;
	sei();
}



void InitTCC5(void)
//------------------------------------------------------------------------------
// Initialize TimerType5 on PortC.  This timer is used to trigger ADC conversion
//	and is modified as needed at the end of each interval.
//------------------------------------------------------------------------------
{
	TCC5.CTRLA = TC45_CLKSEL_DIV1_gc;				// 16MHz.   ?????
	TCC5.CTRLB = TC45_WGMODE_NORMAL_gc;
	TCC5.CNT = 0;
	TCC5.PERBUF = 1333;								// adctimer_rate >> 16;
	TCC5.INTFLAGS = TC5_OVFIF_bm;
//	TCC5.INTCTRLA = TC45_OVFINTLVL_HI_gc;			// Enable overflow.. Start ADC...
}



void InitTCD5(void)
//------------------------------------------------------------------------------
// Initialize TimerType5 on PortC.  This timer will be used to update DAC CH 1
// at a frequency set by CNT, 16x times per cycle.
//
//	The SingleSlope mode will allow use of multiple ISR routines, CCA/CCB/OVFL.
//	The PER determines frequency.
//
//	NOTE:
//		Sine Tables will have been previously loaded with level settings based on 
//		SineTable[]
//
//		Interrupt level for CCA/CCB will enable ISR service only during tone
//		transmission.
//------------------------------------------------------------------------------
{
	TCD5.CTRLA = TC45_CLKSEL_DIV4_gc;			// 32MHz/4 = 8MHz resolution, 125nS
	TCD5.CTRLB = TC45_WGMODE_SINGLESLOPE_gc;	//	When CNT == PER, ISR OVFL;
	SetSineFreq(577);									// Updates PERBUF/PER
}





void InitSPI_C(void)
//-----------------------------------------------------------------------------
//	Initialize SPI Ch C for MODE 3 Master Operation at 32MHz/16.
//
//	With PSADIV = 4, PSBCDIV = 1, CLKper == 8MHz.
//-----------------------------------------------------------------------------
{
	uint8_t Data;

	SPIC_CTRL = 0;
	Data = SPIC_STATUS;
	Data = SPIC_DATA;
	SPIC_CTRL |= (0x00 << SPI_PRESCALER_gp);	// CLKper/4.
	SPIC_CTRL &= ~(1<< SPI_DORD_bp);				// MSB Bit transmitted first.
	SPIC_CTRL |= (0x03 << SPI_MODE_gp);			// Mode 3, Rising Edge CLock
	SPIC_CTRL |= (1 << SPI_MASTER_bp);			// Master Operation.
	SPIC_CTRL |= (1 << SPI_ENABLE_bp);			// Enable SPI "C".
	SPIinfo.Status = COMPLETE;
}


void InitDAC_A(void)
//------------------------------------------------------------------------------
// Initialize DAC0 on PORTA for 12 bit Sine Generator.
//	Initialize DAC1 on PORTA for 12 bit Beeper operation.  (DAC_REFSEL_AREFA_gc)
//------------------------------------------------------------------------------
{
	DACA_CTRLA |= (1 << DAC_CH0EN_bp);			// Output Enable for CHannel 0.
	DACA_CTRLA |= (1 << DAC_CH1EN_bp);			// Output Enable for CHannel 1.
	DACA_CTRLA |= (1 << DAC_ENABLE_bp);			// Enable DAC on Port "A"

	DACA_CTRLB = DAC_CHSEL_DUAL_gc;				//	Activate both CH0 and CH1.

	DACA_CTRLC &= ~(1 << DAC_LEFTADJ_bp);		// Make sure it's RIGHT Adjusted..
	DACA_CTRLC |= (0x01 << DAC_REFSEL_gp);		// AVCC reference

	DACA_CH0DATA = 2048;								// SineWave Generator.
	DACA_CH1DATA = 2048;								// I_SRC ohmmeter.
}

void InitComb(void)
//------------------------------------------------------------------------------
// Initialize the Comb Filter for operation.
//------------------------------------------------------------------------------
{
	uint16_t Index;
	
	InitADC_A();
//	CalData.adctimer_rate = INITIAL_ADCTIMER_RATE;
//	CalData.adctimer_carry = 0;

	for(Index=0; Index < SAMPLES_PER_CYCLE; Index++)
	{
		waveform[Index] = 0x8000;
	}
	wave_idx = 0;							// Global index into Comb Buffer "waveform".
}


void InitADC_A(void)
//------------------------------------------------------------------------------
//	Setup the A2D for operation.  The trigger source will be TCC5 on
//	it's overflow via event channel 0.
//
//------------------------------------------------------------------------------
{
	uint8_t ADC_Channel, Gain;
	uint8_t POSmux, NEGmux;
	
	ADCA.PRESCALER &= ~ADC_PRESCALER_gm;
	ADCA.PRESCALER |= ADC_PRESCALER_DIV32_gc;
	
//	ADCA.REFCTRL &= ~ADC_REFSEL_gm;			// PortA0 Vref.
//	ADCA.REFCTRL |= ADC_REFSEL_AREFA_gc;
	
	ADCA.CTRLB &= ~ADC_CONMODE_bm;
	ADCA.CTRLB |= (1 << ADC_CONMODE_bp);	// Differential Mode.
	ADCA.CTRLB &= ~ADC_RESOLUTION_gm;		// Resolution 12 Bit Right Justified.

	//	Event Channel 1, Event Action Trigger ADC CH0.
	//
	ADCA.EVCTRL = (ADC_EVSEL_1_gc | ADC_EVACT_CH0_gc);
	ADCA.CTRLA = ADC_ENABLE_bm;
	ADCA.CH0.INTCTRL = ADC_CH_INTLVL_MED_gc;		// 0x02, Interrupt level 2.
	
	InitTCC5();									// TCC5 Overflow initiates conversion.
	//---------------------------------------------------------------------------
	//	Begin Sampling process.  Each sample is trigger by TCC1 CCA.
	//
	EVSYS.CH1MUX = EVSYS_CHMUX_TCC5_OVF_gc;	// Puts TCC5 OVF Flag on EVent CH 1
	TCC5.CTRLD	= TC45_EVSEL_CH1_gc;				// Overflow will be event 1.
}



void InitA2D(void)
//------------------------------------------------------------------------------
//	Setup the A2D for operation.  The trigger source will be TCC5 (16x timer) on
//	it's overflow via event channel 0.
//
//	Calls to ACDCvalue will modify ADMUX MUX3:0 with proper channel.  Options
//	are:
//					ADC2				// Xmit Voltage
//					ADC1				// Amplifier Current
//					ADC0				// VBAT
//------------------------------------------------------------------------------
{
	ADCA.PRESCALER &= ~ADC_PRESCALER_gm;
	ADCA.PRESCALER |= ADC_PRESCALER_DIV32_gc;
	
	ADCA.REFCTRL &= ~ADC_REFSEL_gm;			// PortA0 Vref.
	ADCA.REFCTRL |= ADC_REFSEL_AREFD_gc;
	
	ADCA.CTRLB &= ~ADC_CONMODE_bm;
	ADCA.CTRLB |= (1 << ADC_CONMODE_bp);	// Differential Mode.
	ADCA.CTRLB &= ~ADC_RESOLUTION_gm;		// Resolution 12 Bit Right Justified.
	
	//	Event Channel 0, Event Action Trigger ADC CH0.
	//
	ADCA.EVCTRL = (ADC_EVSEL_1_gc | ADC_EVACT_CH0_gc);
	EVSYS.CH1MUX = EVSYS_CHMUX_TCC5_CCA_gc;
}



void LCD_init(void)
//-----------------------------------------------------------------------------
//	Initialize display and SPI communications.
//
//	Command:			(See Sitronix ST7565R.pdf for Complete details.)
//
//				0b1110 0010			// Soft Reset, default values.
//
//				0b01xx xxxx			//	Display Start Line Number
//				0b1011 xxxx			// Display Start Page
//				0b1010 111x			// Display ON/OFF
//				0b1010
//				0b0000 xxxx			// Column Address Start Address, LSN
//				0b0001 xxxx			// Column Address Start Address, MSN
//
//				0b1010 0000			// Normal Segment Drive Direction
//				0b1010 0001			// Reverse Segment Drive
//
//				0b1010 0110			// Normal video
//				0b1010 0111			// Reverse video
//
//				0b1010 0010			// LCD Bias Selection
//				0b1010 0011			//
//
//				0b1110 0000			// Sets Column Read/Write Modify Start address.
//				0b1110 1110			// Last Column in Read/Write scheme
//------------------------------------------------------------------------------
{
	// Do a hard reset of the LCD display controller.
	//
	LCD_hard_reset();

	// Initialize SPI port.
	//
	InitSPI_C();

	LCD_CmdWrite(LCD_CMD_LCD_BIAS_1_DIV_6_DUTY33);		// Bias Setting (0xa2/0xa3)
	//---------------------------------------------------------------------------
	// Reverse Rotation (180 degrees) LCD pins pointing UP, Column 131->4..
	//	
	LCD_CmdWrite(LCD_CMD_ADC_REVERSE);
	LCD_CmdWrite(LCD_CMD_NORMAL_SCAN_DIRECTION);	
	//
	//	Standard (0 degrees) LCD pins pointing down, Column 0-> 127
//		LCD_CmdWrite(LCD_CMD_ADC_NORMAL);						//	Segment Driver Direction.
//		LCD_CmdWrite(LCD_CMD_REVERSE_SCAN_DIRECTION);		//	Output Scan Direction.
	//---------------------------------------------------------------------------
	
	
	LCD_CmdWrite(LCD_CMD_START_LINE_SET(0));				// Line Start = 0.
	LCD_CmdWrite(LCD_CMD_VOLTAGE_RESISTOR_RATIO_5);		// Voltage Regulator Setting.

	LCD_CmdWrite(LCD_CMD_ELECTRONIC_VOLUME_MODE_SET);	//	Contrast MODE Set
	LCD_CmdWrite(SYSdata.Contrast);							// Contrast Setting
	
	LCD_CmdWrite(LCD_CMD_POWER_CTRL_ALL_ON);				// Power Control Settings.
	LCD_CmdWrite(LCD_CMD_DISPLAY_ON);						// Display ON....0xae = OFF

	LCD_ClearDspl();
}



void PowerDown(void)
//------------------------------------------------------------------------------
//	The system is ON and the user wants to turn it off.  Setup the Wake-Up
//	register before snoozing.
//------------------------------------------------------------------------------
{
	unsigned char Wait, PreviousBatteryState, Percent;

	PreviousBatteryState = Bstat;						// State of Battery at PowerDown.
	SpkrAlert(PwrDwnSound);

	// Turn off the LCD BackLight, then wait for the Keypress to
	//	be released.
	//
	LCD_hard_reset();
	rLCD_BKLT;
	Percent = 1;
	BackLightDutyCycle(Percent);

	Wait = ON;
	while(Wait)
	{
		
		while(PWRKEY);							// Wait for KeyPush Release.
		DelayMS(1000);

		if(!PWRKEY)								// If Power Keypush has been released.
		{
			cli();								// Disable System Interrupts.
			
			// Disable PWR to Peripherals.
			//
			rPWR_EN;
			rSPKR_EN;
			//	Force INPUT (float) on all drive ports.
			//
			PORTA.DIR		= 0b00000000;	
			PORTC.DIR		= 0b00000000;
			PORTD.DIR		= 0b00010000;		// Power button drive must remain Low.
			PORTR.DIR		= 0b00000010;		// Power Enable must be keep Low.
			
			// Setup Wake Interrupt.
			//
			PORTCFG.MPCMASK = ((1 << ROW2_bp) | (1 << ROW3_bp));
			PORTD.PIN1CTRL	= PORT_OPC_TOTEM_gc;		// Remove Pull-ups on Two Rows.

			// Keep Pull-up for On/Off Key and enable ISR on desired edge. MPCMASK
			// determines actual pin.
			//
			PORTD.DIR		= (1 << COL1_bp);		//	On/Off Column Key LOW.
			PORTD.OUTCLR	= (1 << COL1_bp);
			
			PORTD.PIN7CTRL = PORT_ISC_BOTHEDGES_gc;
			PORTD.INTMASK = PORT_INT7IF_bm;
			PORTD.INTCTRL	= PORT_INTLVL_LO_gc;
			PORTD.INTFLAGS = (1 << PORT_INT7IF_bp);

			// In the PWR_DOWN mode all peripheral clocks are stopped.
			//
			wdt_disable();					// Kill the Dog.

			set_sleep_mode(SLEEP_MODE_PWR_SAVE);
			sleep_enable();
			sei();							// Global Interrupts ON.
			sleep_cpu();
			sleep_disable();
			
			InitPorts();
			InitSPI_C();
			
			//	PushButton Should remain ON for "ONWAITTM".  If released
			//	prior, continue to sleep.
			//
			DelayMS(ONWAITTM);
			if(PWRKEY)
			{
				sPWR_EN;
				BackLightDutyCycle(SYSdata.BackLight);
				DelayMS(100);
				GetBatteryVoltage();			// Update current Battery Status.
				//
				//	If the current status isn't healthy, just keep the system off.
				// 
				if((Bstat == BSHUTDOWN) || (Bstat == BCRITICAL))
				{
					rPWR_EN;
				}
				//
				//	IF previous Bstat forced shutdown, and battery is now in better
				//	state, restart system at Main.
				//
				else if((Bstat != BSHUTDOWN) && (Bstat != BCRITICAL) &&
				((PreviousBatteryState == BSHUTDOWN) || (PreviousBatteryState ==BCRITICAL)))
				{
					wdt_enable(WDT_PER_1KCLK_gc);		// Reset on calls to GetKP.
					while(1);
				}
				//
				//	If last power down wasn't forced, and all is OK now, do a normal
				//	startup.
				//
				else if((Bstat != BSHUTDOWN) && (Bstat != BCRITICAL)) 
				{
					PreviousBstat = ~Bstat;	// Force update for battery status indicator. 
					while(PWRKEY);							// Wait for KeyPush Release.
					Wait = OFF;
					KyReg = 0;
					FKey = 0;				// Clear all KeyPresses.
					WarmStart();
				}
			}
		}
	}
}






