//----------------------------------------------------------------------------
//	File Name: lcd.h
//
//	Content:
//
//		Header files associated with "lcd.c"
//
// 	Date:  29 Aug 2012
//
//	Copyright 2012 Allied AnaLogic, Inc
//	All rights reserved
//
//===========================================================================

#include "TTS003.h"
#ifndef LCD_H_INCLUDED
#define LCD_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

//extern struct GraphTags SpanTags[AVAILABLE_SPANS][HVTAGQTY];
//extern struct GraphTags MixerSpanTags[AVAILABLE_SPANS][HVTAGQTY];



//-----------------------------------------------------------------------------
// Command Names for ST7565R display controller driver
//-----------------------------------------------------------------------------
#define LCD_CMD_DISPLAY_ON                     0xAF
#define LCD_CMD_DISPLAY_OFF                    0xAE
#define LCD_CMD_START_LINE_SET(line)           (0x40 | (line))
#define LCD_CMD_PAGE_ADDRESS_SET(page)         (0xB0 | (page))
#define LCD_CMD_COLUMN_ADDRESS_SET_MSB(column) (0x10 | (column))
#define LCD_CMD_COLUMN_ADDRESS_SET_LSB(column) (0x00 | (column))
#define LCD_CMD_ADC_NORMAL                     0xA0
#define LCD_CMD_ADC_REVERSE                    0xA1
#define LCD_CMD_DISPLAY_NORMAL                 0xA6
#define LCD_CMD_DISPLAY_REVERSE                0xA7
#define LCD_CMD_DISPLAY_ALL_POINTS_OFF         0xA4
#define LCD_CMD_DISPLAY_ALL_POINTS_ON          0xA5
#define LCD_CMD_LCD_BIAS_1_DIV_5_DUTY33        0xA1
#define LCD_CMD_LCD_BIAS_1_DIV_6_DUTY33        0xA2
#define LCD_CMD_NORMAL_SCAN_DIRECTION          0xC0
#define LCD_CMD_REVERSE_SCAN_DIRECTION         0xC8
#define LCD_CMD_VOLTAGE_RESISTOR_RATIO_0       0x20
#define LCD_CMD_VOLTAGE_RESISTOR_RATIO_1       0x21
#define LCD_CMD_VOLTAGE_RESISTOR_RATIO_2       0x22
#define LCD_CMD_VOLTAGE_RESISTOR_RATIO_3       0x23
#define LCD_CMD_VOLTAGE_RESISTOR_RATIO_4       0x24
#define LCD_CMD_VOLTAGE_RESISTOR_RATIO_5       0x25
#define LCD_CMD_VOLTAGE_RESISTOR_RATIO_6       0x26
#define LCD_CMD_VOLTAGE_RESISTOR_RATIO_7       0x27
#define LCD_CMD_POWER_CTRL_ALL_ON              0x2F
#define LCD_CMD_SLEEP_MODE                     0xAC
#define LCD_CMD_NORMAL_MODE                    0xAD
#define LCD_CMD_RESET                          0xE2
#define LCD_CMD_NOP                            0xE3
#define LCD_CMD_ELECTRONIC_VOLUME_MODE_SET     0x81
#define LCD_CMD_ELECTRONIC_VOLUME(volume)      (0x3F & (~volume))
#define LCD_CMD_BOOSTER_RATIO_SET              0xF8
#define LCD_CMD_BOOSTER_RATIO_2X_3X_4X         0x00
#define LCD_CMD_BOOSTER_RATIO_5X               0x01
#define LCD_CMD_BOOSTER_RATIO_6X               0x03
#define LCD_CMD_STATUS_READ                    0x00
#define LCD_CMD_END                            0xEE
#define LCD_CMD_READ_MODIFY_WRITE              0xE0

//	Window index.
//-----------------------------------------------------------------------------
enum WINDOWS{
	WF1,
	WF2,
	WF3,
};

enum CursorFlag
{
	CURSOR_OFF	= OFF,
	CURSOR_ON	= ON,
	AUTOCURSOR	= ON +0x80,
};

//-----------------------------------------------------------------------------
// LCD Position Equates within an Open Window
//-----------------------------------------------------------------------------
#define T_LEFT    0     //	Top Left.
#define B_LEFT	   1     //	Bottom Left.
#define C_LEFT    2		//	Center Left.

#define T_MIDDLE	3
#define B_MIDDLE	4
#define C_MIDDLE	5

#define T_RIGHT	6
#define B_RIGHT	7
#define C_RIGHT	8


//----------------------------
//	Box/Line Draw definitions.
//----------------------------
#define CLEAR			0x00
#define CLEAR_W       0x00		// Clear Window.
#define FILL			0xff		// Write 1's to the graphic area.
#define	DELETE		0xfe		// Delete Marker(s).
#define NOBUF			0xfd		// Do NOT write to the LCD_Buffer

#define BORDER       0xff		// Draw a double line box window border.
#define NOBORDER		0x00
#define SBORDER      0x01		// Draw a single line window box border.
#define SOLID			0xff
#define DOT          0xCC  	// Dotted line.
#define DASH			0xf0
#define BORDERPIXELS 1        // Box Border width/height.
#define NEEDLE_FILL	0xFE		// Code to limit Ypixel bottom limit.
#define NEEDLE_CLR	0x01

//----------------------------
//	Character PUT definitions.
//----------------------------
#define STD_CHAR_SPACE  2	// Pixel spacing between characters.
#define NTR_CHAR_SPACE  1
#define LETTERBASE      1	// Height correction for CtrStrgBox Centering.
#define LINESPACE       3  // Distance in pixels between lines
#define VCHARSPACE      1  // Pixels between vertical characters

#define TRANSPARENT     2  // True "Overlay" "OR'S" LCD data.
#define REV	            FILL			//1  // Reverse image.
#define STD 				CLEAR_W		//0  // Standard image.

#define CAPT_SX         4  // "X" position inside active Window for 1st char.
#define WRITE           1  // Write Marker.
#define WRITE_NOTAIL    2  // Write the Maker without a pointer tail.

//-----------------------------------------------------------------------------
//Graphic Vertical TAG string constants.
#define TRZ_TAGS		5				// (((sizeof)(TRzTags))/ ((sizeof)(GraphTags)))
#define VTAG_HP		2				// Horizontal Position of Vertical Messages.
#define VTAG_VP		10				// Vertical Position, Vertical TAGS
#define VTAG_OS		8				// Vertical TAG Offsets (next line)

#define HTAG_VP		48						// Vertical Position of Horiz Messages.
#define HTAG_HP		14
#define HTAG_OS		23


//------------------------------------------------------------------------
//	Index for Font address Table as defined by structure TTF_Font *pFont[].
//------------------------------------------------------------------------
enum FONTS
{
	TB14,
	TB8,
	//------//
	FONT6X8,
	FONT5X7,
	FONT4X6,
	FONTVWX5,
};	

enum pwm_ports
{
	PC,
	BKLT,
	PE,
	PF,
};


//-----------------------------------------------------
// LCD Screen Cursor Equates.
//-----------------------------------------------------
#define CURSOR_LENGTH   2			// "X"  Length of Cursor.
#define CURSOR_WIDTH CURSOR_LENGTH
#define CURSOR_HEIGHT   4
#define BLANKSPACE      6			// Blank Pixels after Cursor.
#define CAPTDATAROWS    2			// Ans/Org Rows (TB10 Height +2) *2
#define MARKERLINES     10			// Display line number for marker placement.
#define CAPTFONT			TB10		// Font size used for Capture mode.
#define CTLFONT			LU6      // Font size for mode control characters.
#define MAXCHARWIDTH		(16+1)	// Char bytes for display pixel width.
#define MAXWINDOWS		6			// Number of window layers

#define LCDCOLUMNS		128
#define LCDLINES			64
#define LCDPAGES			(LCDLINES/8)
#define LCDSIZE			((LCDCOLUMNS) * LCDLINES)
#define LCDBKLT_H			80
#define LCDBKLT_L			10
#define LCDBKLT_M			20

#define LCDCONTRAST_H		38
#define LCDCONTRAST_L		15
#define LCDCONTRAST_M		21


#define PIXAVGQTY			2	//4		//8			// Elements used to smooth graphic display.

//	Full Screen Window
//----------------------------------------------------------------------------
#define FULL_WX		0
#define FULL_WY		0
#define FULL_WH		64
#define FULL_WW		128
#define FULL_W			FULL_WX, FULL_WY, FULL_WH, FULL_WW

// MODE window (Top Window)
//-----------------------------------------------------------------------------
#define MODE_WX		0
#define MODE_WY		0
#define MODE_WH		11
#define MODE_WW		128
#define MODE_W			MODE_WX, MODE_WY, MODE_WH, MODE_WW

// DATA window (Middle information Window)
//-----------------------------------------------------------------------------
#define DATA_WX		0
#define DATA_WY		10
#define DATA_WH		(LCDLINES -DATA_WY)
#define DATA_WW		128
#define DATA_W			DATA_WX, DATA_WY, DATA_WH, DATA_WW

//	STATUS window showing test progress.
//-----------------------------------------------------------------------------
#define STATUS_WX		0
#define STATUS_WY		55
#define STATUS_WH		9
#define STATUS_WW		128
#define STATUS_W		STATUS_WX, STATUS_WY, STATUS_WH, STATUS_WW

// Message windows (Circuit Test Results).
//-----------------------------------------------------------------------------
#define MSG1_WX		1					//0
#define MSG1_WY		55					//48
#define MSG1_WH		9
#define MSG1_WW		57					//50
#define MSG1_W			MSG1_WX, MSG1_WY, MSG1_WH, MSG1_WW

#define MSG2_WX		70
#define MSG2_WY		51
#define MSG2_WH		13
#define MSG2_WW		50
#define MSG2_W			MSG2_WX, MSG2_WY, MSG2_WH, MSG2_WW

//	Terminal Letter Windows
//-----------------------------------------------------------------------------
#define LTR_T_WX		4
#define LTR_T_WY		(DATA_WY +4)
#define LTR_T_WH		11
#define LTR_T_WW		12			//	11
#define LTR_T_W		LTR_T_WX, LTR_T_WY, LTR_T_WH, LTR_T_WW

#define LTR_R_WX		LCDCOLUMNS -LTR_T_WW -LTR_T_WX
#define LTR_R_WY		LTR_T_WY
#define LTR_R_WH		LTR_T_WH
#define LTR_R_WW		LTR_T_WW
#define LTR_R_W		LTR_R_WX, LTR_R_WY, LTR_R_WH, LTR_R_WW

#define LTR_G_WX		((LCDCOLUMNS/2) -(LTR_T_W/2))
#define LTR_G_WY		(LCDLINES - LTR_T_WH -2)
#define LTR_G_WH		LTR_T_WH
#define LTR_G_WW		LTR_T_WW
#define LTR_G_W		LTR_G_WX, LTR_G_WY, LTR_G_WH, LTR_G_WW

// Terminal Value Windows
//-----------------------------------------------------------------------------
#define TR_WX			40
#define TR_WY			(LTR_T_WY +1)
#define TR_WH			12
#define TR_WW			48
#define TR_W			TR_WX, TR_WY, TR_WH, TR_WW

#define TG_WX			11
#define TG_WY			(DATA_WY + (DATA_WH/2) -4)
#define TG_WH			12
#define TG_WW			48
#define TG_W			TG_WX, TG_WY, TG_WH, TG_WW

#define RG_WX			(LTR_G_WX + LTR_G_WW)
#define RG_WY			TG_WY
#define RG_WH			12
#define RG_WW			48
#define RG_W			RG_WX, RG_WY, RG_WH, RG_WW

#define SNGL_W			TR_WX, 28, TR_WH, TR_WW		

//	Dot Indication Windows...
//-----------------------------------------------------------------------------
#define DWTR_WX	37						// 3 Terminal, Tip/Ring
#define DWTR_WY	15
#define DWTR_WH	4	
#define DWTR_WW	4
#define DWTR_W		DWTR_WX, DWTR_WY, DWTR_WH, DWTR_WW

#define DWT_WX		8
#define DWT_WY		33
#define DWT_WH		4
#define DWT_WW		4
#define DWT_W		DWT_WX, DWT_WY, DWT_WH, DWT_WW

#define DWR_WX		67						// 3 Terminal, Ring
#define DWR_WY		33
#define DWR_WH		4
#define DWR_WW		4
#define DWR_W		DWR_WX, DWR_WY, DWR_WH, DWR_WW

#define DWNM_WX		NMET_WX+1		// NoiseMetallic
#define DWNM_WY		NMET_WY+1
#define DWNM_WH		4
#define DWNM_WW		4
#define DWNM_W		DWNM_WX, DWNM_WY, DWNM_WH, DWNM_WW

#define DWNG_WX		NGND_WX+1		// NoiseGnd
#define DWNG_WY		NGND_WY+1
#define DWNG_WH		4
#define DWNG_WW		4
#define DWNG_W		DWNG_WX, DWNG_WY, DWNG_WH, DWNG_WW


// Talk battery window TOP RIGHT Display.
//-----------------------------------------------------------------------------
#define  TALKBAT_WX	104
#define  TALKBAT_WY	2
#define  TALKBAT_WH	7
#define  TALKBAT_WW	22
#define	TALKBAT_W			TALKBAT_WX, TALKBAT_WY, TALKBAT_WH, TALKBAT_WW


//
//	Function Windows..
//------------------------------------------------------------------------------
#define F1_WX		1
#define F1_WY		55
#define F1_WH		9
#define F1_WW		40
#define F1_W		F1_WX, F1_WY, F1_WH, F1_WW

#define F2_WX		44
#define F2_WY		55
#define F2_WH		9
#define F2_WW		40
#define F2_W		F2_WX, F2_WY, F2_WH, F2_WW

#define F3_WX		87
#define F3_WY		55
#define F3_WH		9
#define F3_WW		40
#define F3_W		F3_WX, F3_WY, F3_WH, F3_WW

#define ATDR_WX		87
#define ATDR_WY		12
#define ATDR_WH		9
#define ATDR_WW		40
#define ATDR_W		ATDR_WX, ATDR_WY, ATDR_WH, ATDR_WW

#define AF1_WX		1
#define AF1_WY		45
#define AF1_WH		9
#define AF1_WW		30
#define AF1_W		AF1_WX, AF1_WY, AF1_WH, AF1_WW

#define AF3_WX		87
#define AF3_WY		45
#define AF3_WH		9
#define AF3_WW		40
#define AF3_W		AF3_WX, AF3_WY, AF3_WH, AF3_WW

//	Graph Data Window
//-----------------------------------------------------------------------------
#define GRAPH_WX		26
#define GRAPH_WY		(DATA_WY +2)				// Pixel 12 (0-10 == DataY)
#define GRAPH_WH		(64 -GRAPH_WY -16)		// (64 -GRAPH_WY -15)
#define GRAPH_WW		100
#define GRAPH_W		GRAPH_WX, GRAPH_WY, GRAPH_WH, GRAPH_WW

extern unsigned char LCD_Buffer[LCDPAGES][LCDCOLUMNS];

#define AMP_WX			1
#define AMP_WY			23
#define AMP_WH			9
#define AMP_WW			24
#define AMP_W			AMP_WX, AMP_WY, AMP_WH, AMP_WW

#define PW_WX			1
#define PW_WY			31
#define PW_WH			9
#define PW_WW			24
#define PW_W			PW_WX, PW_WY, PW_WH, PW_WW

#define PGA_WX			1
#define PGA_WY			44
#define PGA_WH			9
#define PGA_WW			24
#define PGA_W			PGA_WX, PGA_WY, PGA_WH, PGA_WW


//-----------------------------------------------------------------------------
//	3 Point Data Windows.
//-----------------------------------------------------------------------------
#define PTF1_WX			38
#define PTF1_WY			17
#define PTF1_WH			14
#define PTF1_WW			52
#define PTF1_W			PTF1_WX, PTF1_WY, PTF1_WH, PTF1_WW

#define PTF2_WX			38
#define PTF2_WY			33
#define PTF2_WH			14
#define PTF2_WW			52
#define PTF2_W			PTF2_WX, PTF2_WY, PTF2_WH, PTF2_WW

#define PTF3_WX			38
#define PTF3_WY			49
#define PTF3_WH			14
#define PTF3_WW			52
#define PTF3_W			PTF3_WX, PTF3_WY, PTF3_WH, PTF3_WW

#define DBM_WX				39
#define DBM_WY				41
#define DBM_WH				10
#define DBM_WW				50
#define DBM_W			DBM_WX, DBM_WY, DBM_WH, DBM_WW

//-----------------------------------------------------------------------------
//	Noise Display Windows.
//-----------------------------------------------------------------------------
#define NMET_WX			12
#define NMET_WY			17
#define NMET_WH			16
#define NMET_WW			98
#define NMET_W			NMET_WX, NMET_WY, NMET_WH, NMET_WW

#define NGND_WX			12
#define NGND_WY			36
#define NGND_WH			16
#define NGND_WW			98
#define NGND_W			NGND_WX, NGND_WY, NGND_WH, NGND_WW

// Low battery window TOP LEFT Display.
//-----------------------------------------------------------------------------
#define	BATLVL1_WX	5
#define	BATLVL1_WY	4
#define	BATLVL1_WH	3
#define	BATLVL1_WW	3
#define	BATLVL1_W	BATLVL1_WX, BATLVL1_WY, BATLVL1_WH, BATLVL1_WW

#define	BAT_WX		(BATLVL1_WX -2)
#define	BAT_WY		(MODE_WY + 2)
#define	BAT_WH		7
#define	BAT_WW		15
#define	BAT_W			BAT_WX, BAT_WY, BAT_WH, BAT_WW

#define	BATLVL2_WX	(BATLVL1_WX + BATLVL1_WW +1)
#define	BATLVL2_W	BATLVL2_WX, BATLVL1_WY, BATLVL1_WH, BATLVL1_WW

#define	BATLVL3_WX	(BATLVL2_WX + BATLVL1_WW + 1)
#define	BATLVL3_W	BATLVL3_WX, BATLVL1_WY, BATLVL1_WH, BATLVL1_WW

#define	BBUTTON_WX	(BAT_WX + BAT_WW)
#define	BBUTTON_WY	(BAT_WY + 1)
#define	BBUTTON_WH	5
#define	BBUTTON_WW	1
#define	BBUTTON_W	BBUTTON_WX, BBUTTON_WY, BBUTTON_WH, BBUTTON_WW	

// CFG window Text/Data.
//-----------------------------------------------------------------------------
#define	CFG_T_WX	24
#define	CFG_T_WY	26
#define	CFG_T_WH	14
#define	CFG_T_WW	65
#define	CFG_T_W	CFG_T_WX, CFG_T_WY, CFG_T_WH, CFG_T_WW

#define	CFG_D_WX	89
#define	CFG_D_WY	26
#define	CFG_D_WH	14
#define	CFG_D_WW	30
#define	CFG_D_W	CFG_D_WX, CFG_D_WY, CFG_D_WH, CFG_D_WW

//------------------------------------------------------------------------------
//
#define SWT_WX	104
#define SWT_WY	(DATA_WY +3)
#define SWT_WH	9
#define SWT_WW	22
#define SWT_W	SWT_WX, SWT_WY, SWT_WH, SWT_WW
#define SWM_W	SWT_WX, SWT_WY +10, SWT_WH, SWT_WW
#define SWB_W	SWT_WX, SWT_WY+20, SWT_WH, SWT_WW

//-----------------------------------------------------------------------------
//	Graphic Related Variables.
//-----------------------------------------------------------------------------
struct WindowPosition
{
	int p;				// Graphics page for Window
	int x;				// Upper Left corner Horizontal "X".
	int y;				// Upper Left Vertical "Y".
	int w;				// Pixel Width of Window.
	int h;				// Pixel Height of Window.
	unsigned char b;	// If Window has a border.
	int Xposition; 	// Last cursor window relative cursor position.
	int Yposition; 	// Last cursor window relative position.
}Window, SWindow;



//uint8_t yPixels[100];			// LCD Screen Yposition for 100x points.
//extern uint8_t yPixels[];

//uint8_t CursorData[8][3];		// 8 Vertical Pages per column (3 columns)
//extern uint8_t CursorData[8][3];

//	Establish a block array of character strings modified by VoP and Range.
//char TDR_HTagStrg[5][10];
//extern char TDR_HTagStrg[5][10];
//
//------------------------------------------------------------------------------



#if defined(__DOXYGEN__)
//! \brief Select the a USART SPI interface.
# define LCD_USART_SPI_INTERFACE
//! \brief Select the normal SPI pheripheral interface.
# define LCD_SPI_INTERFACE
#endif

#if defined(LCD_USART_SPI_INTERFACE) || defined(LCD_SPI_INTERFACE)
//
# define LCD_SERIAL_INTERFACE
#endif


static inline void LCD_CmdWrite(uint8_t command)
//-----------------------------------------------------------------------------
//	Write a Command Byte to the LCD via SPI_C.  Wait for the byte to be sent
//	before returning to the calling program.
//-----------------------------------------------------------------------------
{
	int delay;

	SetSPI_Mode3();
	SPIC_STATUS |= SPI_IF_bm;	
	sLCD_CMD;
	sLCD_CS;
	SPIC_DATA = command;
	for(delay = 2; delay; delay--);
	while((SPIC_STATUS & SPI_IF_bm) == 0);
	rLCD_CS;
}


static inline void LCD_DataWrite(uint8_t data)
//-----------------------------------------------------------------------------
//	Write a Data Byte to the LCD via SPI_C.  Wait for the byte to be sent
//	before returning to the calling program.
//-----------------------------------------------------------------------------
{
	int delay;

	SetSPI_Mode3();
	SPIC_STATUS |= SPI_IF_bm;	
	sLCD_DATA;
	sLCD_CS;
	SPIC_DATA = data;
	for(delay = 2; delay; delay--);
	while((SPIC_STATUS & SPI_IF_bm) == 0);
	rLCD_CS;

}




//! \name LCD Controller reset
//@{
/**
 * \brief Perform a soft reset of the LCD controller
 *
 * This functions will reset the LCD controller by sending the reset command.
 * \note this funtions should not be confused with the \ref LCD_hard_reset()
 * function, this command will not control the RST pin.
 */
static inline void LCD_soft_reset(void)
{
	LCD_CmdWrite(LCD_CMD_RESET);
}

/**
 * \brief Perform a hard reset of the LCD controller
 *
 * This functions will reset the LCD controller by setting the reset pin low.
 * \note this funtions should not be confused with the \ref LCD_soft_reset()
 * function, this command will control the RST pin.
 */
static inline void LCD_hard_reset(void)
{
	sLCD_RST;
	DelayMS(2);
	rLCD_RST;
	DelayMS(2);
	
}

//! \name Address setup for the LCD
//@{
/**
 * \brief Set current page in display RAM
 *
 * This command is usually followed by the configuration of the column address
 * because this scheme will provide access to all locations in the display
 * RAM.
 *
 * \param address the page address
 */
static inline void LCD_set_page_address(uint8_t address)
{
	// Make sure that the address is 4 bits (only 8 pages)
	address &= 0x0F;
	LCD_CmdWrite(LCD_CMD_PAGE_ADDRESS_SET(address));
}

/**
 * \brief Set current column in display RAM
 *
 * \param address the column address
 */
static inline void LCD_set_column_address(uint8_t address)
{
	// Make sure the address is 7 bits
	address &= 0x7F;
	//
	// if display is rotated 180 degrees the column address becomes 131-> 4,
	// otherwise 0->127.
	address +=4;
	LCD_CmdWrite(LCD_CMD_COLUMN_ADDRESS_SET_MSB(address >> 4));
	LCD_CmdWrite(LCD_CMD_COLUMN_ADDRESS_SET_LSB(address & 0x0F));
}

/**
 * \brief Set the display start draw line address
 *
 * This function will set which line should be the start draw line for the LCD.
 */
static inline void LCD_set_display_start_line_address(uint8_t address)
{
	// Make sure address is 6 bits
	address &= 0x3F;
	LCD_CmdWrite(LCD_CMD_START_LINE_SET(address));
}
//@}

//! \name Display hardware control
//@{
/**
 * \brief Turn the LCD display on
 *
 * This function will turn on the LCD.
 */
static inline void LCD_display_on(void)
{
	LCD_CmdWrite(LCD_CMD_DISPLAY_ON);
}

/**
 * \brief Turn the LCD display off
 *
 * This function will turn off the LCD.
 */
static inline void LCD_display_off(void)
{
	LCD_CmdWrite(LCD_CMD_DISPLAY_OFF);
}


/**
 * \brief Set the LCD contrast level
 *
 * \warning This will set the voltage for the LCD, settings this value too high
 * may result in damage to the LCD. Hence the limit for these settings must be
 * defined in the \ref conf_st7565r.h file.
 *
 * Contrast values outside the max and min values will be clipped to the defined
 * \ref LCD_DISPLAY_CONTRAST_MAX and \ref LCD_DISPLAY_CONTRAST_MIN.
 *
 * \param contrast a number between 0 and 63 where the max values is given by
 *                 the LCD.
 *
 * \retval contrast the contrast value written to the LCD controller
 */
/*static inline uint8_t LCD_set_contrast(uint8_t contrast)
{
	if (contrast < LCD_DISPLAY_CONTRAST_MIN) {
		contrast = LCD_DISPLAY_CONTRAST_MIN;
	}
	if (contrast > LCD_DISPLAY_CONTRAST_MAX) {
		contrast = LCD_DISPLAY_CONTRAST_MAX;
	}
	LCD_CmdWrite(LCD_CMD_ELECTRONIC_VOLUME_MODE_SET);
	LCD_CmdWrite(LCD_CMD_ELECTRONIC_VOLUME(contrast));
	return contrast;
}
*/
/**
 * \brief Invert all pixels on the device
 *
 * This function will invert all pixels on the LCD
 *
 */
static inline void LCD_display_invert_enable(void)
{
	LCD_CmdWrite(LCD_CMD_DISPLAY_REVERSE);
}

/**
 * \brief Disable invert of all pixels on the device
 *
 * This function will disable invert on all pixels on the LCD
 *
 */
static inline void LCD_display_invert_disable(void)
{
	LCD_CmdWrite(LCD_CMD_DISPLAY_NORMAL);
}
//@}

//! \name Initialization
//@{
void LCD_init(void);
//@}

/** @} */

#ifdef __cplusplus
}
#endif

#endif /* LCD_H_INCLUDED */
