//------------------------------------------------------------------------------
//
// File Name: TDR.c
//
//	Date:  10 July 2014
//
// Content:	
//			Functions specifically related to TDR operations.
//
//
// Copyright 2014 Allied Analogic, Inc
// All rights reserved
//
//=============================================================================
// $Log:$
//=============================================================================
//-----------------------------------------------------------------------------
// Header file Includes
//-----------------------------------------------------------------------------
#include	<stdio.h>
#include	<avr/io.h>
#include "TTX003.h"
#include "pwm.h"
//#include "twi.h"
#include "lcd.h"
#include <avr/interrupt.h>
#include <math.h>
#include <string.h>
#include "Meter.h"
#include "DDS.h"
#include <avr/eeprom.h>

//#define TDR_DCAVG		8

struct CmdWindow{
	uint8_t x;
	uint8_t y;
	uint8_t w;
	char *pMsg;
};
//extern struct CmdWindow TestSelectionWindow[CAL_ITEMS];

struct CmdWindow VOPTable[] =
{
	{4, 12, 56, "22 PULP .68"},
	{4, 20, 56, "24 PULP .67"},
	{4, 28, 56, "26 PULP .60"},
	{4, 36, 56, "22 PIC   .67"},
	{4, 44, 56, "24 PIC   .66"},
	{68, 12, 56, "26 PIC  .64"},
	{68, 20, 56, "22 GEL  .62"},
	{68, 28, 56, "24 GEL  .60"},
	{68, 36, 56, "26 GEL  .58"},
	{68, 44, 56, "CUSTOM"},

};
const uint8_t VOPTableValues[] = {68, 67, 60, 67, 66, 64, 62, 60, 58};
//
//------------------------------------------------------------------------------


void ComputeTDRVF(void)
//------------------------------------------------------------------------------
//	Based on Value of FKey selected TDR type and range compute the new horizontal
//	distance tags and pixel distance set by VoP.
//	
//	NOTE: The FirstStep is adjusted to start before the pulse so the user can
//			see the edge of the applied step/pulse source. 
//------------------------------------------------------------------------------
{
	uint8_t Index;
	float Frequency, VelocityFactor, Distance, StepDistance;
	char WrkStrg[20];

	Frequency = 32e6;
	
	int Xstart,Ystart,Height, Width;
	uint8_t Xend, Yend, Offset, TagPosition;
	struct GraphTags *pHorizTag;


	if(TDRInfo.Function != ZOOM)
	{
		switch(TestInfo.Scale)
		{
			case 0:
			//---------------------------------------------------------------------
			// PULSE MODE:	Approx range: 4Kft.
			// 
			//	The Line Drive will be a narrow pulse generate that the beginning of
			//	the period held HIGH for PulseWidth time (31.25nS/cnt).
			{
				TDRInfo.Mode = TDRPULSE;					// MODE Flag.
				TDRInfo.StepSize = 1;						// Sample Intervals of 32MHz.

				TDRInfo.PulseWidth = 4;
				TDRInfo.PulseCycle = Frequency/16e3;	// Fixed PERiod at 32KHz.
				TDRInfo.FirstStep = 0;						//-5; //-15;
				break;
			}
			//
			//---------------------------------------------------------------------

			case 1:
			//---------------------------------------------------------------------
			// PULSE MODE:	Approx range: 4Kft.
			// 
			//	The Line Drive will be a narrow pulse generate that the beginning of
			//	the period held HIGH for PulseWidth time (31.25nS/cnt).
			{
				TDRInfo.Mode = TDRPULSE;					// MODE Flag.
				TDRInfo.StepSize = 4;						// 31.25nS * 4 = 125nSec
				
				TDRInfo.PulseWidth = 4;						// Dual slope = 2x
				TDRInfo.PulseCycle = Frequency/16e3;	// Fixed PERiod at 16KHz.
				TDRInfo.FirstStep = 0;//-16;				// 1st Sample from leading edge
				break;
			}
			case 2:
			// PULSE MODE:	Approx range: 8Kft.
			{
				TDRInfo.Mode = TDRPULSE;					// MODE Flag.
				TDRInfo.StepSize = 8;						// 31.25nS * 8 = 250nSec
				
				TDRInfo.PulseWidth = 8;
				TDRInfo.PulseCycle = Frequency/16e3;	// Fixed PERiodh at 16KHz.
				TDRInfo.FirstStep =	0;// -16;					// First Sample Count. 
				break;
			}
		}
	}
	//---------------------------------------------------------------------------
	//	Initial entry into the ZOOM mode uses the cursor position and "step size"
	//	to determine the displayed span. 
	//
	//	Modification of zoom position keeps cursor constant while shifting span by
	//	shifting the "TDRInfo.FirstStep".  Pressing F1 while in Zoom will exit
	// Zoom move and allow cursor movement.
	//
	//	"CFG, ZOOM" will exit zoom mode and re-initialize the range as selected
	//	on entry.
	//
	else if(TDRInfo.Function == ZOOM)
	{
		TDRInfo.Mode = TDRPULSE;					// MODE Flag.
		TDRInfo.PulseCycle = Frequency/16e3;	// Fixed PERiod at 16KHz.
		TDRInfo.StepSize = 1;						// 31.25nS *2
	}
	//
	//------------------------------------------------------------------------------
	//
	VelocityFactor = (((float)(SysData.VoP)/100) * VOP_FS)/2;
	StepDistance =  VelocityFactor * (1/Frequency * 1e9);
	TDRInfo.PixelDistance = StepDistance * TDRInfo.StepSize;

	//	Compute the distance to the first sample.
	//
	TDRInfo.FirstStepDistance = 0;
	if(TDRInfo.FirstStep > 0)
	{
		TDRInfo.FirstStepDistance = TDRInfo.FirstStep * StepDistance;
	}
	//

	for(Index =0; Index < TRZ_TAGS; Index++)
	{
		Distance = ((TDRInfo.PixelDistance * Index * 25) +TDRInfo.FirstStepDistance);
		sprintf(&TDR_HTagStrg[Index][0],"%3.1fK", Distance/1000);
	}
	//	Setup display information..Plot TDR graph information tags.
	//
	TestInfo.DataType = TDR;
	TestInfo.PlotType = SINGLE;
	if(TDRInfo.Function != ZOOM)
	{
		ZGraph2LCD();
		strcpy(WrkStrg, "TDR");
	}
	else
	{
		// Plot the Horizontal Strings associated with graph.
		//
		strcpy(WrkStrg, "TDR Zoom");

		Height = 8;
		Width = 22;
		Offset = HTAG_OS;
		TagPosition = HTAG_HP;
		Ystart = HTAG_VP;
		pHorizTag = &TDRSpanTags[0][0];
		for(Index =0; Index < TRZ_TAGS; Index++)
		{
			Xstart = TagPosition + (Offset * Index);
			LCD_OpenWindow(Xstart, Ystart, Height, Width, NOBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			LCD_CtrStrg((*pHorizTag).pStrg, FONTVWX5, C_MIDDLE, STD);
			pHorizTag++;
		}

	}
	NewModeMessage(WrkStrg);
	PutPWMsg();
	PutGainMsg();
}




void DoTDR(void)
//------------------------------------------------------------------------------
//	Allow user to select range.  Before each Scan check pair status. Pressing any
//	HOTKEY will exit back to SystemIdle();
//	
//	Groups:	0	100+ Samples	(TestInfo.SampleQuantity, 1 sample every Period)
//				1
//				2
//				3
//------------------------------------------------------------------------------
{
	int16_t	xPrevious, xPosition, Video;
	volatile float Distance, NewPixelDistance;

	Video = STD;
	WriteFunctionWindow("0-1KFT", WF1, Video);
	WriteFunctionWindow("0-4KFT", WF2, Video);
	WriteFunctionWindow("0-8KFT", WF3, Video);
	NewModeMessage("TDR");
	
	TestInfo.Function = F_TDR;
	SetFunctionPath();
	//---------------------------------------------------------------------------
	//	Setting System clock to 32MHz requires changes to System 1mS timer and
	//	keypush sine generator. 
	//
	while(ToneInfo.BeepStringFlag);
	DelayMS(10);
	TestInfo.TestIndex = TDRSWEEP;						// Modifies TCF1 calculations
	//
	//---------------------------------------------------------------------------
	//
	TDRInfo.Function = CURSOR;						// Default entry, cursor.
	TDRInfo.SampleQuantity = 104;
	TDRInfo.Gain = 2;									// Graphic Amplification ratio.

	AllowedKeys = K_F1 | K_F2 | K_F3 | K_CLR;
	Wait4Key(AllowedKeys);
	if(FKey != K_CLR)
	{
		switch(FKey)
		{
			case K_F1:
			{
				TestInfo.Scale = 0;
				break;
			}
			case K_F2:
			{
				TestInfo.Scale = 1;
				break;
			}
			default:
			{
				TestInfo.Scale = 2;
				break;
			}
		}
		//-----------------------------------------------------------------------
		//	Fill in the Horizontal Tag strings with % of total distance.
		//
		ComputeTDRVF();										// Pixel Data from VoP.	
		UpDateTDRFunctionWindows();						// Funct Window from Mode.
		TDRInfo.CursorPosition = xPrevious = 50;		// First cursor mid-scale
		//------------------------------------------------------------------------
		//	Meter test routing.
		//
		TestInfo.Terminal = TR;
		//
		//------------------------------------------------------------------------
		AllowedKeys = ALLKEYS;
		while(!(FKey & K_CLR))
		{
			TDRplot();	

			//	If "AutoCursor" is ON compute the first pixel position that shows a
			//	slope pixel value	greater than 4 and place the cursor with distance
			//	location.
			//
			if(TDRInfo.Function == AUTOCURSOR)
			{
				for(xPosition = 3; xPosition < 97; xPosition++)
				{
					if(abs(yPixels[xPosition] - yPixels[xPosition +2]) > 4 ) break;
				}
				if(xPosition < 98)
				{
					LCD_PutCursor(0,xPosition);
					LCD_PutCursorDistance();
				}
			}

			else if(TDRInfo.Function == CURSOR)
			//
			//	Add Cursor Offset Pixels plus first sample one pixel from start.
			{
				LCD_PutCursor(xPrevious,TDRInfo.CursorPosition);
				LCD_PutCursorDistance();
				DelayMS(100);						// Wait to improve cursor visibility
			}
			//---------------------------------------------------------------------
			//	If an "AllowedKey" was pressed start a timer to allow user a chance
			//	to move cursor to a desired position.  Once the timer expires 
			//	without another key press continue.
			//
			TestInfo.SysStatFlag = ENABLE;			//	Update Battery Indicator.
			do 
			{
				FKey = K_NONE;
				KPstat(AllowedKeys);
				if(FKey && (FKey != K_CLR))
				{
					TestInfo.SysStatFlag = DISABLE;	//	NO Battery Update if KyPush.
					SetISRcount2(500);					// Multiple keypress wait time.
					switch(FKey)
					{
						case K_F1:
						//
						//	Within any mode switch to Cursor display.
						{
							TDRInfo.Function = CURSOR;
							UpDateTDRFunctionWindows();
							break;
						}
						
						case K_DOWN:
						//------------------------------------------------------------
						//	Modify variables Downward dependent on active function.
						{
							switch(TDRInfo.Function)
							{
								case CURSOR:
								{
									if(RepeatFlag)
									{
										if(TDRInfo.CursorPosition >5)
										{
											TDRInfo.CursorPosition -= 5;
										}
									}
									else
									{
										if(TDRInfo.CursorPosition > 0)
										{
											TDRInfo.CursorPosition -= 1;
										}
									}
									break;
								}
								case GAIN:
								{
									if(TDRInfo.Gain > 1)
									{
										TDRInfo.Gain -= 1;
										PutGainMsg();
									}
									break;
								}
								case PW:
								{
									if(TDRInfo.PulseWidth > 2)
									{
										TDRInfo.PulseWidth -= 1;
										PutPWMsg();
									}
									break;
								}
								case RANGE:
								{
									if(TestInfo.Scale > 0)
									{
										TestInfo.Scale--;
									}
									else 
									{
										TestInfo.Scale = MAXRANGE -1;
									}
									ComputeTDRVF();			// Refresh DATA_W & Length info.
									UpDateTDRFunctionWindows();
									break;
								}
								case ZOOM:
								{
									if(TDRInfo.FirstStep > 10) 
									{
										TDRInfo.FirstStep -= 10;
										ComputeTDRVF();			// Refresh DATA_W & Length info.
										UpDateTDRFunctionWindows();
									}
									break;
								}
							}
							break;
						}
						//
						//------------------------------------------------------------
						//
						case K_UP:
						//------------------------------------------------------------
						//	Modify variables upward dependent on active function.
						{
							switch(TDRInfo.Function)
							{
								case CURSOR:
								{
									if(RepeatFlag)
									{
										if(TDRInfo.CursorPosition < 93)
										{
											TDRInfo.CursorPosition += 5;
										}
									}
									else
									{
										if(TDRInfo.CursorPosition < 97)
										{
											TDRInfo.CursorPosition += 1;
										}
									}
									break;
								}
								case GAIN:
								{
									if(TDRInfo.Gain < TDRGAINSTEPS)
									{
										TDRInfo.Gain += 1;
										PutGainMsg();
									}
									break;
								}
								case PW:
								{
									if(TDRInfo.PulseWidth < 20)
									{
										TDRInfo.PulseWidth += 1;
										PutPWMsg();
									}
									break;
								}
								case RANGE:
								{
									if(TestInfo.Scale < (MAXRANGE -1))
									{
										TestInfo.Scale++;
									}
									else
									{
										TestInfo.Scale = 0;
									}
									ComputeTDRVF();			// Refresh DATA_W & Length info.
									UpDateTDRFunctionWindows();
									break;
								}
								
								case ZOOM:
								{
									if((TDRInfo.PixelDistance * TDRInfo.SampleQuantity) +
										(TDRInfo.FirstStep * TDRInfo.PixelDistance) < 5000) 
									{
										TDRInfo.FirstStep += 10;
										ComputeTDRVF();			// Refresh DATA_W & Length info.
										UpDateTDRFunctionWindows();
									}
									break;
								}

							}
							break;
						}
						//
						//------------------------------------------------------------
						//
						case K_LEVEL:
						{
							TDRInfo.Function = GAIN;
							UpDateTDRFunctionWindows();
							break;
						}
						//
						//------------------------------------------------------------
						//
						case K_FREQ:
						//
						//	Set the Function to Range Select.
						{
							TDRInfo.Function = RANGE;
							UpDateTDRFunctionWindows();
							break;
						}
						case K_CFG:
						//
						//	Allow the user to set the VOP or ZOOM mode.
						{
							LCD_OpenWindow(GRAPH_W, NOBORDER);
							LCD_ClearWindow(SBORDER,STD);
							
							WriteFunctionWindow("VOP", WF1, STD);
							WriteFunctionWindow("ZOOM", WF2, STD);
							WriteFunctionWindow("PW", WF3, STD);
							Wait4Key(K_F1 | K_F2 | K_F3 | K_CLR);
							switch(FKey)
							{
								case K_F1:
								{
									SetVoP();							// Modifies AllowedKeys.
									TDRInfo.Function = CURSOR;		// Clear ZOOM mode.
									AllowedKeys = ALLKEYS;	// Reload to service all keys.
									break;
								}
								case K_F2:
								{
									if(TDRInfo.Function == ZOOM)
									{
										TDRInfo.Function = CURSOR;
									}
									else
									{
										TDRInfo.Function = ZOOM;
										// Compute the current Cursor Distance
										//
										Distance = (TDRInfo.PixelDistance * (TDRInfo.CursorPosition +1));
										Distance += (TDRInfo.FirstStepDistance + (2 * TDRInfo.PixelDistance));
	
										// Now subtract current cursor position footage to
										//	determine start point using zoom resolution.
										//
										NewPixelDistance = (TDRInfo.PixelDistance/(float)(TDRInfo.StepSize));
										Distance -= ((float)(TDRInfo.CursorPosition) * NewPixelDistance);
										Distance /= NewPixelDistance;
										TDRInfo.FirstStep = (int16_t)(Distance);
									}
									break;
								}
								case K_F3:
								{
									TDRInfo.Function = PW;
									break;
								}
							}
							ComputeTDRVF();			// Refresh DATA_W & Length info.
							UpDateTDRFunctionWindows();
						}

						//------------------------------------------------------------
						//
						//
						//------------------------------------------------------------
					}


					if(((xPrevious != TDRInfo.CursorPosition) || (FKey == K_F2))
																 && (TDRInfo.Function == CURSOR))
					{
						LCD_PutCursor(xPrevious,TDRInfo.CursorPosition);
						LCD_PutCursorDistance();
						xPrevious = TDRInfo.CursorPosition;
					}
				}
			} while((ISRcountStatus2 == ACTIVE) && (FKey != K_CLR));
			//
			//---------------------------------------------------------------------
		}
	}
	//---------------------------------------------------------------------------
	//
	while(ToneInfo.BeepStringFlag);						// Wait for KP ack to complete.
	TestInfo.TestIndex = OFF;								// Modifies TCF1 calculations
}

void LCD_PutCursorDistance(void)
//------------------------------------------------------------------------------
//	Compute the distance to cursor and put in WF2 Screen Position..
//	
//	NOTE: The first pixel plotted is an average of 4 sample points, therefore
//			the distance must be compensated to the middle of the averaged 
//			distance, or 2 pixel distances.			
//------------------------------------------------------------------------------
{
	float Distance;
	char	WrkStrg[20];

	Distance = (TDRInfo.PixelDistance * (TDRInfo.CursorPosition +1));
	Distance += (TDRInfo.FirstStepDistance + (2 * TDRInfo.PixelDistance));
	sprintf(&WrkStrg[0], "%d FT", (uint16_t)(Distance));
	WriteFunctionWindow(&WrkStrg[0], WF1, STD);
}
	

void TDR_StartTrigger(void)
//------------------------------------------------------------------------------
//	Timer TCE0 uses Dual Slope mode to generate STEP and PULSE signals. TCE0.CCA
//	controls pulse width.  Operating in dual slope pulse resolution is 2x the
//	clock cycle of 31.25nS.	TCE0.CCA is inverted at the port output.
//	
//	TCE1 operates Single Slope with TCE1.CCA starting TCE0 at count of 2 using
//	the event system.  TCE1.CCB, set at a +/-value from TCE0.CCA, triggers an ADC
//	conversion.
//	
//	TCE1.CCB is incremented at the completion of the ADC sample in preparation
//	for the next Cycle until all TDR samples have been taken.
//------------------------------------------------------------------------------ 
//	TCE1 Single Slope PWM running at ~ 17.06kHz.
//	TCE0 Dual Slope PWM Generating the TDR Pulse (allowing PulseWidth Control)
//				Starts Pulse on TCE0.CCA	UPCNT
//				End Pulse on TCE0.CCA		DWNCNT
//
//	1.)	TCE1.CCA			Starts TCE0						//	using	EVSYS CH0MUX
//	2.)	TCE1.CCB			Initiates ADC conversion	// using EVSYS CH1MUX
//	3.)	TCE0.CCABUF/CCBBUF is incremented by STEP on each TCE1 cycle until
//			all samples are completed.
//
//				MIN STEP Size = 1/32MHZ (31.25nSec).
//				STEP = 31.25nS * Resolution Distance.
//------------------------------------------------------------------------------
//	Event System CHANNEL Number Input                Event Output (EVSEL Action)
//------------------------------------------------------------------------------
//	Comb		TCC1.PER OVFL	-> (EVSYS.CH0MUX = 11001000) -> ADC conversion CH0
//	TDR		TCE1.CCB			-> (EVSYS.CH1MUX = 11101101) -> ADC conversion CH1
//	TDR		TCE1.CCA			-> (EVSYS.CH2MUX = 11101100) -> Start TCE0
//------------------------------------------------------------------------------			
{

	TestInfo.SysStatFlag = DISABLE;			//	NO Battery Update if KyPush.
	while(ToneInfo.BeepStringFlag);
	TCC0.CTRLA = TC_CLKSEL_OFF_gc;			// Halt 16x ISR Clock
	TCD0.INTCTRLA = TC_OVFINTLVL_OFF_gc;	// Halt 1mS ISR KeyPress Service.

	SetTDR_ADCDiffGain(TDR_ADC, ADC_CH_GAIN_1X_gc);
	//
	//------------------------------------------------------------------------
	//	Setup the two timers for pulse generation and ADC triggering
	//	
	TCE0.CTRLA = TC_CLKSEL_OFF_gc;			// Stop TCE0 Clock to Initialize.
	TCE1.CTRLA = TC_CLKSEL_OFF_gc;
	TCE0.INTFLAGS = 0xF3;						// Clear any pending Interrupts.
	TCE1.INTFLAGS = 0xF3;						// Clear any pending Interrupts.
	TCE0.CNT = 0;
	TCE1.CNT = 0;

	TCE0.CTRLB = TC_WG_DS_B;					// Dual Slope, Bottom->Top->Bottom.
	TCE1.CTRLB = TC_WG_SS;						// Single Slope, Bottom->TOP
	TCE0.PER = TDRInfo.PulseCycle/2;			// Dual Slope, half up, half down.
	TCE0.PERBUF = TDRInfo.PulseCycle/2;
	TCE1.PER = TDRInfo.PulseCycle;
	TCE1.PERBUF = TDRInfo.PulseCycle;

	TCE0.CCA = TCE0.PERBUF - (TDRInfo.PulseWidth/2);	// Pulse Start Count
	TCE0.CCABUF = TCE0.PERBUF - (TDRInfo.PulseWidth/2); 

// Sliding scale test.......
	TCE1.CCB = TCE0.CCABUF + TDRInfo.FirstStep;			// Shifts 1 sample <- | ->
	TCE1.CCBBUF = TCE0.CCABUF + TDRInfo.FirstStep;


//	TCE1.CCBBUF = TDRInfo.FirstStep;			// Shifts 1 sample <- | ->
//	TCE1.CCB = TCE1.CCBBUF;


	//	Enable/Disable Pulse output via ISR.  On positive slope the pulse is
	//	enabled, negative slope disabled.
	//
	TCE0.CCB = 100;
	TCE0.CCBBUF = 100; 
	TCE0.INTCTRLB = TC_CCBINTLVL_LO_gc;
	//
	//
	A2Dresult.Samples = 0;
	TDRInfo.StatFlag = ACTIVE;
	//
	// Set the Event Source for TCE1.CCB to trigger A2D conversion (interrupt
	// generated after each conversion), and TCE1.CCA to start the TCE0 counter.
	//
	EVSYS.CH2MUX =  EVSYS_CHMUX_TCE1_OVF_gc;	// TCE1_OVF = ReStart TCE0.
	TCE0.CTRLD = TC_EVACT_RESTART_gc | TC_EVSEL_CH2_gc;

	EVSYS.CH1MUX =  EVSYS_CHMUX_TCE1_CCB_gc;	// TCE1_CCB = ADC Event Trigger.
	ADCA.EVCTRL = ADC_EVSEL_CH0123 | ADC_EVACT_CH01;	// EVch1 triggers ADC CH1
	//
	//---------------------------------------------------------------------------
	// Setup ADC and enable Counter clocks.
	// 
	ADCA.CH1.INTFLAGS |= (1 << ADC_CH1IF_bp);	// Clear Interrupt Flag.
	ADCA.CTRLA = 0;
	ADCA.CTRLA = (1<< ADC_FLUSH_bp);				// Start clean.
	ADCA.CTRLA &= ~(1<< ADC_FLUSH_bp);
	ADCA.CTRLA = (1<< ADC_ENABLE_bp);			// Enable A2D.
	ADCA.CTRLB &= ~(ADC_RESOLUTION_gm);

	ADCA.CH1.INTCTRL = TC_INT_LVL_HI;		// High Interrupt Level.

	TCE0.CTRLA = TC_CLKSEL_DIV1_gc;			// Start TCE0 32MHz system Clock
	TCE1.CTRLA = TC_CLKSEL_DIV1_gc;
}


void TDR_StopTrigger(void)
//------------------------------------------------------------------------------
//	Resume normal System Operation.
//------------------------------------------------------------------------------
{
	EVSYS.CH0MUX = 0;								// No event trigger source.
	EVSYS.CH1MUX = 0;
	TCE0.CTRLA = TC_CLKSEL_OFF_gc;			// Stop TCE0 Clock.
	TCE1.CTRLA = TC_CLKSEL_OFF_gc;			// Stop TCE1 Clock.
	TCC0.CTRLA = TC_CLKSEL_DIV2_gc;			// 16x Sine Generator Clock.
	TCD0.INTFLAGS = 0x01;						// Clear any pending OVFL ISR.	
	TCD0.INTCTRLA = TC_OVFINTLVL_LO_gc;		// Keypush Svc and system timing.
	ADCA.CH0.INTCTRL = TC_INT_LVL_OFF;		// High Interrupt Level.
	DelayMS(2);										// Wait for A2D to complete any pending conversions.
	ADCA.CTRLA = 0;								// Turn Off the A2D.
	ADCA.EVCTRL = 0;								// Stop Event control.
	ADCA.PRESCALER &= ~ADC_PRESCALER_gm;
	ADCA.PRESCALER |= ADC_PRESCALER_DIV16_gc;
}



void TDRplot(void)
//------------------------------------------------------------------------------
//	Send a TDR pulse and plot the results.
//	
//	On entry Scale and routing has been completed.
//	
//	To obtain a consistent value for each pixel there are "TDR_ACAVG" groups of
//	the SampleQuantity taken.  The groups are then averaged.
//	
//	In addition to averaging the groups, adjacent samples are also averaged. Since
//	the distance is computed using pixel position, and pixel position is ahead by
//	approximately two pixels, the distance is offset by adding the distance of
//	the two pixels. 
//------------------------------------------------------------------------------
{
	uint8_t	Index, SampleNumber, GroupNumber;
	int16_t Delta, Delta_GainAdjusted, BiasAccumulator, SamplePoint;
	long yPosition, SampleAccumulator;


	A2Dresult.Group = 0;
	for(Index = 0; Index < TDR_ACAVG; Index++)
	{
		TDR_StartTrigger();							// Begin TDR sampling.
		ADCA.PRESCALER &= ~ADC_PRESCALER_gm;
		ADCA.PRESCALER |= ADC_PRESCALER_DIV4_gc;

		while(TDRInfo.StatFlag == ACTIVE);
		TDR_StopTrigger();							// Service Sys timer/key pushes.
		A2Dresult.Group++;
	}
	A2Dresult.Samples = TDRInfo.SampleQuantity;

	//	Compute pixel position for placement.  The trace will be centered
	//	on the display by using maximum deviation across all measurements
	//	with gain applied.
	//	
	//	The lowest value, along with gain adjusted Delta, is used to 
	//	determine the bias for centering.  
	//
	Delta = DeltaA2Dbuffer() / 2;
	Delta_GainAdjusted = (DeltaA2Dbuffer() * 2)/TDRInfo.Gain;
	BiasAccumulator = LowestA2Dbuffer();


	for(SampleNumber = 0; SampleNumber < TDRInfo.SampleQuantity; SampleNumber++)
	{	
		//------------------------------------------------------------------------
		//	Average the current Sample Point for the groups of samples measured.
		//	
		SampleAccumulator = 0;
		for(GroupNumber = 0; GroupNumber < TDR_ACAVG; GroupNumber++)
		{
			SamplePoint = SampleNumber +
										(GroupNumber * TDRInfo.SampleQuantity);
			for(Index = 0; Index < 4; Index++)
			{
				SampleAccumulator += A2Dbuffer[SamplePoint +Index];
			}
		}
		SampleAccumulator /= (TDR_ACAVG *4);	// Group Sample Average.

		// Ensure all sample values are on the positive side. Then adjust
		// the Delta center around 0 to obtain +/- VfullScale.
		// 
		//------------------------------------------------------------------------
		SampleAccumulator -= BiasAccumulator;
		SampleAccumulator -= Delta;
		//
		// Scale the value before applying the gain factor.
		//
		SampleAccumulator = (SampleAccumulator * (GRAPH_WH/2))/ Delta;
		yPosition = (GRAPH_WH/2) -((SampleAccumulator * TDRInfo.Gain)/TDRGAINSTEPS);
		//------------------------------------------------------------------
		//	Make sure the values are within the LCD graphics window range
		//------------------------------------------------------------------
		if(yPosition < 0)						yPosition = 0;
		else if(yPosition > GRAPH_WH)		yPosition = GRAPH_WH;
		yPixels[SampleNumber] = (uint8_t)yPosition;
	}
	Graph100Pixels(&yPixels[0]);		// Large Pixel values at window bottom.
}



int16_t SlopeDetector(void)
//------------------------------------------------------------------------------
// On entry ADC measurements have been placed in A2Dbuffer.  Compute the maximum
// slope deviation.  The result will be used to scale a single point slope to
// reside in the LCD graphic area.
//------------------------------------------------------------------------------
{
	int16_t SlopePoint, HighestSlope, LowestSlope;
	uint8_t Index;

	HighestSlope = LowestSlope = 0;
	for(Index = 0; Index < TDRInfo.SampleQuantity -1; Index++)
	{
		SlopePoint = A2Dbuffer[Index] - A2Dbuffer[Index+1];
		if(abs(SlopePoint) > 1)
		{
			if (SlopePoint > HighestSlope)  HighestSlope = SlopePoint;
			else if(SlopePoint < LowestSlope) LowestSlope = SlopePoint;
		}
	}
	//	Return with the largest variant.
	//
	if(HighestSlope > abs(LowestSlope))	return(HighestSlope);
	else if(abs(LowestSlope) > HighestSlope)	return(abs(LowestSlope));
}


uint16_t DeltaA2Dbuffer(void)
//------------------------------------------------------------------------------
//	Compute the deviation of the A2Dbuffer.
//------------------------------------------------------------------------------
{
	uint8_t Index;
	int16_t Highest, Lowest;
	long Accumulator;

	Accumulator = 0;
	for(Index = 0; Index < TDRInfo.SampleQuantity; Index++)
	{
		Accumulator += A2Dbuffer[Index];
	}
	Accumulator /= TDRInfo.SampleQuantity;

	Highest = Lowest = Accumulator;
	for(Index = 0; Index < TDRInfo.SampleQuantity; Index++)
	{
		if(A2Dbuffer[Index] > Highest)
		{
			Highest = A2Dbuffer[Index];
		}
		else if(A2Dbuffer[Index] < Lowest)
		{
			Lowest = A2Dbuffer[Index];
		}
	}
	return(Highest - Lowest);
}


int16_t LowestA2Dbuffer(void)
//------------------------------------------------------------------------------
// Find the lowest value in the signed integer buffer.
//------------------------------------------------------------------------------
{
	uint8_t Index;
	int16_t Lowest;
	long Accumulator;

	// Validate the lowest number by first finding the average value in the array.
	//
	Accumulator = 0;
	for(Index = 0; Index < TDRInfo.SampleQuantity; Index++)
	{
		Accumulator += A2Dbuffer[Index];
	}
	Accumulator /= TDRInfo.SampleQuantity;

	Lowest = Accumulator;
	for(Index = 0; Index < TDRInfo.SampleQuantity; Index++)
	{
		if(A2Dbuffer[Index] < Lowest)
		{
			Lowest = A2Dbuffer[Index];
		}
	}
	return(Lowest);
}


void UpDateTDRFunctionWindows(void)
//------------------------------------------------------------------------------
// Update the Function Windows based on Active Mode.
//------------------------------------------------------------------------------
{
	switch(TDRInfo.Function)
	{
		case GAIN:
		{
			WriteFunctionWindow("GAIN", WF1, STD);	
			WriteFunctionWindow("]", WF2, STD);
			WriteFunctionWindow("[", WF3, STD);
			break;			
		}
		case RANGE:
		{
			WriteFunctionWindow("RANGE", WF1, STD);
			WriteFunctionWindow("]", WF2, STD);
			WriteFunctionWindow("[", WF3, STD);
			break;			
		}
		case PW:
		{
			WriteFunctionWindow("PW", WF1, STD);
			WriteFunctionWindow("]", WF2, STD);
			WriteFunctionWindow("[", WF3, STD);
			break;			
		}
		case ZOOM:
		{
			WriteFunctionWindow("ZOOM", WF1, STD);	
			WriteFunctionWindow("<", WF2, STD);
			WriteFunctionWindow(">", WF3, STD);
			break;			
		}
		case CURSOR:
		{
			WriteFunctionWindow("CURSOR", WF1, STD);
			WriteFunctionWindow("<", WF2, STD);
			WriteFunctionWindow(">", WF3, STD);
			break;			
		}
	}
}


void PutGainMsg(void)
//------------------------------------------------------------------------------
//	Display the TDRgain value.
//------------------------------------------------------------------------------
{
	char WrkStrg[10];

	SwapWindow();
	NewWindow(AMP_W, NOBORDER);
	sprintf(WrkStrg, "G= %d", TDRInfo.Gain);
	LCD_CtrStrg(WrkStrg, FONTVWX5, C_LEFT, STD);
	SwapWindow();
}

void PutPWMsg(void)
//------------------------------------------------------------------------------
//	Display the TDRgain value.
//------------------------------------------------------------------------------
{
	char WrkStrg[10];

	SwapWindow();
	NewWindow(PW_W, NOBORDER);
	sprintf(WrkStrg, "P= %d", TDRInfo.PulseWidth);
	LCD_CtrStrg(WrkStrg, FONTVWX5, C_LEFT, STD);
	SwapWindow();
}




void GetVoP(void)
//------------------------------------------------------------------------------
//	Allow the user to set the Velocity of Propagation.  The value will be saved
//	in EEPROM for later use by TDR functions.
//
//	Returns with FKey active if any HOTKEYS were pressed.
//------------------------------------------------------------------------------
{
	char WrkStrg[10];

	NewModeMessage("Custom VoP");
	NewWindow(DATA_W, SBORDER);
	FunctionSetUpWindow();

	// Get the EEPROM value for VoP.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("VOP: .", TB8, C_RIGHT, STD);

	LCD_OpenWindow(CFG_D_W, NOBORDER);
	sprintf(&WrkStrg[0], "%d", SysData.VoP);
	LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);

	FKey = 0;
	AllowedKeys = K_F1 | K_F2 | K_F3;
	while(!(FKey & (HOTKEYS | K_F1)))
	{
		Wait4Key(HOTKEYS | AllowedKeys);
		{
			if(!(FKey & HOTKEYS))
			{
				switch(FKey)
				{
					case K_F2:
					{
						if(SysData.VoP > 60)	SysData.VoP--;
						FKey = 0;
						break;
					}
					case K_F1:
					{
						SetParity();
						break;
					}
					case K_F3:
					{
						if(SysData.VoP < 80)	SysData.VoP++;
						FKey = 0;
						break;
					}
				}
				LCD_OpenWindow(CFG_D_W, NOBORDER);
				LCD_ClearWindow(NOBORDER, STD);
				sprintf(&WrkStrg[0], "%d", SysData.VoP);
				LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);
			}
		}
	}
}


uint8_t SelectTest(struct CmdWindow *pCmdStructure, uint8_t Lines)
//-----------------------------------------------------------------------------
//	Place the various available test routine on the LCD and allow the user to
//	select the desired test.
//
//	Each test will be highlight using up/dwn keys and selected once the "TEST"
//	key is pressed.
//-----------------------------------------------------------------------------
{
	uint8_t Index, HighLight, LineItems, TestIndex;
	struct CmdWindow *pCmdWindow;
	
	static uint8_t PreviousIndex =0;
	
	LineItems = Lines;
	FKey = 0;

	LCD_OpenWindow(DATA_W, SBORDER);
	LCD_ClearWindow(NOBORDER, STD);

	FunctionSetUpWindow();
	LCD_OpenWindow(DATA_W, SBORDER);

	HighLight = PreviousIndex;
	TestIndex = PreviousIndex;
	pCmdWindow = pCmdStructure;
	for(Index = 0; Index < LineItems; Index++)
	{
		DisplayVOP((pCmdWindow +Index), NOBORDER);
	}
	DisplayVOP((pCmdWindow +HighLight), SBORDER);
	
	AllowedKeys = (K_F1 | K_F2 | K_F3);
	while(!FKey)
	{
		Wait4Key(HOTKEYS | AllowedKeys);
		if(!(FKey & HOTKEYS))
		{
			switch(FKey)
			{
				case K_F3:
				{
					if(TestIndex)
					{
						TestIndex--;
					}
					else
					{
						TestIndex = LineItems -1;
					}
					FKey = 0;
					break;
				}
				case K_F2:
				{
					if(TestIndex < (LineItems -1))
					{
						TestIndex++;
					}
					else
					{
						TestIndex = 0;
					}
					FKey = 0;
					break;
				}
				case K_F1:
				{
					break;
				}
			}
			DisplayVOP((pCmdWindow +PreviousIndex), NOBORDER);
			DisplayVOP((pCmdWindow +TestIndex), SBORDER);
			PreviousIndex = TestIndex;
		}
	}
	while(ToneInfo.BeepStringFlag);
	return(TestIndex);
}

void DisplayVOP(struct CmdWindow *pType, uint8_t HighLight)
//-----------------------------------------------------------------------------
//	Update the "Indexed" test with or without border determined by "HighLight".
//-----------------------------------------------------------------------------
{
	uint8_t x,y,w, Height;
	
	Height = 9;
	x = (*pType).x;
	y = (*pType).y;
	w = (*pType).w;
	LCD_OpenWindow(x,y,Height,w, HighLight);
	if(HighLight == SBORDER) 	LCD_ClearWindow(NOBORDER, STD);
	else								LCD_ClearWindow(SBORDER,STD);
	LCD_CtrStrg((*pType).pMsg, FONTVWX5, C_LEFT, STD);
}


void SetVoP(void)
//------------------------------------------------------------------------------
//	Allow the user to set the Velocity of Propagation.  The value will be saved
//	in EEPROM for later use by TDR functions.
//
//	Returns with FKey active if any HOTKEYS were pressed.
//------------------------------------------------------------------------------
{
	struct CmdWindow *pCmdTable;
	uint8_t Entries, TestSelection;

	NewModeMessage("VoP Setup");
	NewWindow(DATA_W, SBORDER);

	FKey = 0;
	Entries = (sizeof(VOPTable))/ (sizeof (struct CmdWindow));
	pCmdTable = VOPTable;
	TestSelection = SelectTest(pCmdTable, Entries);
	if(FKey == K_F1)
	{
		FKey = 0;
		if(TestSelection == (Entries -1))
		{
			GetVoP();
		}
		else
		{
			SysData.VoP = VOPTableValues[TestSelection];
			SetParity();
		}
		FKey = K_NONE;	
	}
}
