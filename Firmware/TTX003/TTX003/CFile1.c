/*
 * CFile1.c
 *
 * Created: 10/18/2018 11:22:49 AM
 *  Author: hwmathews
 */ 
#include "lcd.h"
#include "TTX003.h"
#include "DDS.h"
#include "Meter.h"
#include "string.h"
#include "Sound.h"
#include "SnoopyComb.h"

//------------------------------------------------------------------------------
//	Circuit ID sample frequencies.  enum FREQ is used to point into the FreqStep
//	structure.
//------------------------------------------------------------------------------
const float SplBinFreq[SPLID_BINS] =
{
	22e3,
	32e3,
	50e3,						//260e3,
	
	276e3,
	382e3,
	400e3,
	
	700e3,
	771e3,
	1121e3,
	
	1544e3,
	2800e3,
	3200e3,
};

uint16_t Yprevious[VBARQTY];


void Probe(void)
//------------------------------------------------------------------------------
//	Listen mode allows selection of DSL monitor or use of the Audio Probe.
//------------------------------------------------------------------------------
{

	NewWindow(DATA_W, SBORDER);
	NewModeMessage("PROBE");
	WriteFunctionWindow("TONE", WF1, STD);		// UP/DWN arrows to change value.
	WriteFunctionWindow("DSL FREQ", WF2, STD);
	WriteFunctionWindow("DSL SCAN", WF3, STD);

	TestInfo.Function = F_PROBE;
	SetFunctionPath();
	
	AllowedKeys = K_F1 | K_F3 | K_F2;
	Wait4Key(HOTKEYS | AllowedKeys);
	switch(FKey)
	{
		case K_F1:
		{
			Dap105();						// Audio Monitor
			break;
		}
		case K_F2:
		{
			Dap125();						// RF Monitor, T-Bug type operation.
			break;
		}
		case K_F3:
		{
			SpecialCircuitID();			// Freq Scan, Energy detector..
			break;
		}
	}
}


void Dap125(void)
//------------------------------------------------------------------------------
//	Sample the Mixed incoming signal and place it in the DAC.  ISR timer
//	will synchronously output the information to the audio DAC.
//------------------------------------------------------------------------------
{
//	uint8_t GainFunctionFlag;
	int16_t BitVpp, AvgLoop, HoldOff;
	uint16_t Percent, Vcurrent, Vprevious;
	char WrkStrg[20];

	PreviousPercent = 100;
	NewWindow(DATA_W, SBORDER);
	
	//	Enable Mixer and Mixer Routing.
	//
	sDAP_HF;
	TestInfo.Mux = ACDC;
	//
	//	Initial ADC Gain
	//
	TestInfo.ADCgain = ADC_G4;
	//	
	// Initially Select Frequency of Interest.  This can be changed by pressing
	// CFG within the function loop.
	//
	DDSctl(DDS_WAKE);
	TestInfo.Span = MX_HDSL;
	SetSnoopSpan();
	
	TestInfo.MsgFlag = GAIN_MSG;				// Default.
	SetFunctionMsg();
	HorizBarGraph(50, NEWGRAPH, SBORDER);
	//
	//---------------------------------------------------------------------------
	//
	sSPKR_EN;
	TestInfo.ADCgain = ADC_G2;
	PutADCgainMsg();
	PutSpkrVolumeMsg();
	A2Dresult.Samples = SAMPLES_PER_CYCLE;	// 60Hz Notch
	//	The HoldOff is the alloted time for Comb filter to settle
	// 
	HoldOff = 33;									// two cycles of 60Hz.
	Comb.FilterAction = CMD_BANDPASS;
	InitComb();
	SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
	TestInfo.DAC_EnableFlag = ENABLE;
	//
	//---------------------------------------------------------------------------
	//
	NewWindow(DBM_W, NOBORDER);
	FKey = 0;
	AllowedKeys = K_LEVEL | K_FREQ | K_CLR | K_F2 | K_F3;
	while(!FKey)
	{
		//
		//	Get  highest value over approx. 660mSec (20 * 33mS) for Squelch setting.
		//
		Vcurrent = 0;
		for(AvgLoop = 0; AvgLoop < 14; AvgLoop++)
		{
			SetISRcount2(HoldOff);					// Wait for buffer to update.
			while(ISRcountStatus2 == ACTIVE);
			PeakDetector();
			BitVpp = A2Dresult.Vpp;
			if(BitVpp > Vcurrent)
			{
				Vcurrent = BitVpp;
			}
		}

		if(abs(Vcurrent - Vprevious) > 10)
		{
			Percent = (int)(((float)Vcurrent * 100)/4095);
			HorizBarGraph(Percent, UPDATE_GR, NOBORDER);
			NewWindow(DBM_W, NOBORDER);
			sprintf(WrkStrg, "%d %%", Percent);
			LCD_ClearWindow(NOBORDER, STD);
			LCD_CtrStrg(WrkStrg, FONT6X8, C_MIDDLE, STD);
			Vprevious = Vcurrent;
		}
		
		KPstat(AllowedKeys);
		if(FKey && !(FKey & (K_CLR)))
		{
//				BitVpp = TrackHighLow();
			switch(FKey)
			{
				case K_DOWN:
				// DWNkey
				{
					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						if(TestInfo.ADCgain > 0)
						{
							TestInfo.ADCgain--;
							SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
						}
					}
					else  if(TestInfo.MsgFlag == VOL_MSG)
					{
						if(SysData.MonitorLevel > 0)
						{
							SysData.MonitorLevel -= 1;
							PutSpkrVolumeMsg();
						}
					}
					break;
				}

				case	K_FREQ:	
				{
					switch(TestInfo.Span)
					{
						case MX_VDSL:
						{
							TestInfo.Span = MX_HDSL;
							break;
						}
						case MX_HDSL:
						{
							TestInfo.Span = MX_T1;
							break;
						}
						case MX_T1:
						{
							TestInfo.Span = MX_VDSL;
							break;
						}
					}
					SetSnoopSpan();
					break;
				}

				case K_UP:					// UPkey
				{
					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						//	Don't Allow gain change if it might saturate signal path.
						//
						if((BitVpp < 3500) && (TestInfo.ADCgain < ADC_G64))
						{
							TestInfo.ADCgain++;
							SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
						}
					}
					else if(TestInfo.MsgFlag == VOL_MSG)
					{
						if(SysData.MonitorLevel < MAX_VOL)
						{
							SysData.MonitorLevel += 1;
							PutSpkrVolumeMsg();
						}
					}
					break;
				}
				case K_LEVEL:
				//
				//	Toggle Gain/Volume setting.
				{
					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						TestInfo.MsgFlag = VOL_MSG;
					}
					else
					{
						TestInfo.MsgFlag = GAIN_MSG;
					}
					SetFunctionMsg();						
					break;
				}
			}
			FKey = 0;
			//
			// Update Display info after changes.
			//
			PutADCgainMsg();
			PutSpkrVolumeMsg();
			sSPKR_EN;			// KeyPush Service turned off speaker.
		}
	}
	AudioTrackDisable();
}


void SetSnoopSpan(void)
//------------------------------------------------------------------------------
// Set DDS to freq based on Selected Span. 
//------------------------------------------------------------------------------
{
	char ModeStrg[20], *pTypeStrg;
	uint16_t DDSChannel;

	//	Update all CHOICE strings.
	//
	LCD_OpenWindow(SWB_W, NOBORDER);
	LCD_ClearWindow(SBORDER, STD);
	LCD_CtrStrg(pResultStrg(VDSL), FONTVWX5, C_MIDDLE, STD);
	
	LCD_OpenWindow(SWM_W, NOBORDER);
	LCD_ClearWindow(SBORDER, STD);
	LCD_CtrStrg(pResultStrg(T1), FONTVWX5, C_MIDDLE, STD);
	
	LCD_OpenWindow(SWT_W, NOBORDER);
	LCD_ClearWindow(SBORDER, STD);
	LCD_CtrStrg(pResultStrg(HDSL), FONTVWX5, C_MIDDLE, STD);
	DDSChannel = DDS_F0;

	switch(TestInfo.Span)
	{
		case MX_VDSL:
		{
			pTypeStrg = pResultStrg(VDSL);
			NewWindow(SWB_W, SBORDER);
			LCD_CtrStrg(pTypeStrg, FONTVWX5, C_MIDDLE, STD);
			Meter.StartFreq = 2900e3;
			break;
		}
		case MX_HDSL:
		{
			pTypeStrg = pResultStrg(HDSL);
			NewWindow(SWT_W, SBORDER);
			LCD_CtrStrg(pTypeStrg, FONTVWX5, C_MIDDLE, STD);
			Meter.StartFreq = 30e3;
			break;
		}
		case MX_T1:
		{
			pTypeStrg = pResultStrg(T1);
			NewWindow(SWM_W, SBORDER);
			LCD_CtrStrg(pTypeStrg, FONTVWX5, C_MIDDLE, STD);
			Meter.StartFreq = 771e3;
			break;
		}
	}
	
	strcpy(ModeStrg, "Rcve ");

	DDSfreq(Meter.StartFreq, DDSChannel);
	strcat(ModeStrg,pTypeStrg);
	NewModeMessage(ModeStrg);
	NewWindow(DBM_W, NOBORDER);  // Re-establish db window.
}



void Dap105(void)
//------------------------------------------------------------------------------
//	Sample the incoming audio and place it in the DAC.  ISR timer
//	will synchronously output the information to the audio DAC.
//	
//------------------------------------------------------------------------------
{
	int16_t BitVpp, Vcurrent, Vprevious, Percent, TopThreshold;
	uint16_t Freq, AvgLoop;
	float Ratio;
	char WrkStrg[10];
//	uint8_t GainFunctionFlag;

	uint16_t HoldOff;
	
	// On Entry FKey == K_F1 or K_F2.
	//
	Freq = 577;
	SetAudioSpan(Freq);

	//	Update the function window with new choices.
	//
	TestInfo.Span = LN_AUDIO;
	TestInfo.TestIndex = PROBE;
	TestInfo.MsgFlag = SQL_MSG;
	SetFunctionMsg();					// Default SET SQL.

	Vcurrent = 300;								// Initial Minimum Signal level.
//		Ratio = 114.0/100.0;						// Percent increase at Squelch Set.
	Ratio = 115.0/100.0;							// Percent increase at Squelch Set.
	TopThreshold = (int)(4095.0/Ratio);
	TestInfo.Vthreshold = (int)((float)Vcurrent * Ratio);
	Percent = (int)(((float)Vcurrent * 100)/4095);
	HorizBarGraph(Percent, NEWGRAPH, SBORDER);

	sSPKR_EN;
	sDAP_LF;											// Low Frequency, Audio DAP.
	TestInfo.Terminal = TR;
	TestInfo.Mux = FLT_AC;
	TestInfo.ADCgain = ADC_G2;
	A2Dresult.Samples = SAMPLES_PER_CYCLE;	// 60Hz Notch
	
	//	The HoldOff is the alloted time for Comb filter to settle
	// 
	HoldOff = 33;									// two cycles of 60Hz.
	Comb.FilterAction = CMD_BANDPASS;
	InitComb();
	SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
	//
	//---------------------------------------------------------------------------
	//
	PutADCgainMsg();
	PutSpkrVolumeMsg();

	NewWindow(DBM_W, NOBORDER);
	FKey = 0;
	AllowedKeys = K_CLR | K_FREQ | K_LEVEL |K_F1 | K_F3 | K_F2;
	while(!FKey)
	{
		//
		//	Get  highest value over approx. 660mSec (20 * 33mS) for Squelch setting.
		//
		Vcurrent = 0;
		for(AvgLoop = 0; AvgLoop < 14; AvgLoop++)
		{
			SetISRcount2(HoldOff);					// Wait for buffer to update.
			while(ISRcountStatus2 == ACTIVE);
			PeakDetector();
			BitVpp = A2Dresult.Vpp;
			if(BitVpp > Vcurrent)
			{
				Vcurrent = BitVpp;
			}
		}
		
		if(abs(Vcurrent - Vprevious) > 10)
		{
			Percent = (int)(((float)Vcurrent * 100)/4095);
			HorizBarGraph((int)Percent, UPDATE_GR, NOBORDER);
			NewWindow(DBM_W, NOBORDER);
			sprintf(WrkStrg, "%d %%", (int)Percent);
			LCD_ClearWindow(NOBORDER, STD);
			LCD_CtrStrg(WrkStrg, FONT6X8, C_MIDDLE, STD);
			Vprevious = Vcurrent;
		}
		if(Vcurrent > TestInfo.Vthreshold)	TestInfo.DAC_EnableFlag = ENABLE;
		else                                TestInfo.DAC_EnableFlag = DISABLE;

		KPstat(AllowedKeys);
		if(FKey && !(FKey & (K_CLR)))
		{
			switch(FKey)
			{
				case K_DOWN:					// DWNkey, Lower the DAC speaker value.
				{

					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						if(TestInfo.ADCgain > 0)
						{
							TestInfo.ADCgain--;
							SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
						}
					}
					else  if(TestInfo.MsgFlag == VOL_MSG)
					{
						if(SysData.MonitorLevel > 0)
						{
							SysData.MonitorLevel -= 1;
							PutSpkrVolumeMsg();
						}
					}
					else if(TestInfo.MsgFlag == SQL_MSG)
					//	
					//	Turn off the Squelch function.
					{
						TestInfo.Vthreshold = 0;
						Percent = 1;
						HorizBarGraph(Percent, NEWGRAPH, SBORDER);
					}
					break;
				}

				case K_F1:
				//
				//	Set the audio threshold at Ratio (14%) above current measurement,
				//	and, don't allow threshold if detection beyond saturation.
				{
					TestInfo.Vthreshold = (int)((float)Vcurrent * Ratio);
					if(TestInfo.Vthreshold > TopThreshold)
					{
						TestInfo.Vthreshold = TopThreshold;
					}
					Percent = (int)(((float)TestInfo.Vthreshold * 100)/4095);
					HorizBarGraph(Percent, NEWGRAPH, SBORDER);
					break;
				}

				case K_UP:					// UPkey, Raise the DAC speaker value 6db.
				{
					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						//	Don't Allow gain change if it might saturate signal path.
						//
						if((BitVpp < 3500) && (TestInfo.ADCgain < ADC_G64))
						{
							TestInfo.ADCgain++;
							SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
						}
					}
					else if(TestInfo.MsgFlag == VOL_MSG)
					{
						if(SysData.MonitorLevel < MAX_VOL)
						{
							SysData.MonitorLevel += 1;
							PutSpkrVolumeMsg();
						}
					}
					else if(TestInfo.MsgFlag == SQL_MSG)
					//	
					//	Turn on the Squelch function.
					{
						
					}
					break;
				}
				case K_LEVEL:
				//
				//	Toggle Gain/Volume/Squelch setting.
				{
					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						TestInfo.MsgFlag = VOL_MSG;
					}
					else if(TestInfo.MsgFlag == VOL_MSG)
					{
						TestInfo.MsgFlag = SQL_MSG;
					}
					else if(TestInfo.MsgFlag == SQL_MSG)
					{
						TestInfo.MsgFlag = GAIN_MSG;
					}
					SetFunctionMsg();						
					break;
				}
				
				case K_FREQ:
				//
				//	Toggle Filter Freq.
				{
					FKey = 0;
					if(Freq == 577)
					{
						Freq= 987;
					}
					else if(Freq == 987)
					{
						Freq = 577;
					}
					SetAudioSpan(Freq);
					break;
				}
			}
			FKey = K_NONE;
			PutADCgainMsg();
			PutSpkrVolumeMsg();
			sSPKR_EN;			// KeyPush Service turned off speaker.
		}
	}
	AudioTrackDisable();
}



void SetAudioSpan(uint16_t Freq)
//------------------------------------------------------------------------------
// Set DDS to freq based on Selected Span. 
//------------------------------------------------------------------------------
{
	char ModeStrg[20], *pTypeStrg;

	//	Update all CHOICE strings.
	//
	LCD_OpenWindow(SWT_W, NOBORDER);
	LCD_ClearWindow(SBORDER, STD);
	LCD_CtrStrg(pResultStrg(F577), FONTVWX5, C_MIDDLE, STD);
	
	LCD_OpenWindow(SWM_W, NOBORDER);
	LCD_ClearWindow(SBORDER, STD);
	LCD_CtrStrg(pResultStrg(F987), FONTVWX5, C_MIDDLE, STD);

	strcpy(ModeStrg, "Tone FLTR ");
	switch(Freq)
	{
		case 577:
		{
//			sBP_FILTER;
			pTypeStrg = pResultStrg(F577);
			NewWindow(SWT_W, SBORDER);
			LCD_CtrStrg(pTypeStrg, FONTVWX5, C_MIDDLE, STD);
			SetBandPassFreq(Freq);
			break;
		}
		case 987:
		{
//			sBP_FILTER;
			pTypeStrg = pResultStrg(F987);
			NewWindow(SWM_W, SBORDER);
			LCD_CtrStrg(pTypeStrg, FONTVWX5, C_MIDDLE, STD);
			SetBandPassFreq(Freq);
			break;
		}
	}
	strcat(ModeStrg,pTypeStrg);
	NewModeMessage(ModeStrg);
	NewWindow(DBM_W, NOBORDER);  // Re-establish db window.
}



#define NOSIG	-1

void SpecialCircuitID(void)
//------------------------------------------------------------------------------
//	Using the DAP125 probe sniff the line and display the detected xDSL type.
//
//	"SplBinFreq[SPLID_BINS]" Contain sample frequencies, and "enum FREQ" is used
//	as a pointer into the structure for ease in troubleshooting.
//------------------------------------------------------------------------------
{
	int16_t Vcurrent, BitVpp;
	uint16_t *pFreqIndex, HoldOff;
	uint8_t AvgLoop, Bin;

	float MixerFreq;

	NewModeMessage("DSL SCAN");
	NewWindow(DATA_W, SBORDER);
	for(Bin = 0; Bin < VBARQTY; Bin++)
	{
		Yprevious[Bin] = 0;
	}
	//	Enable Mixer and Mixer Routing to BandPass Filter.
	//
	DDSctl(DDS_WAKE);
	sDAP_HF;
	TestInfo.Mux = ACDC;
	//
	//---------------------------------------------------------------------------
	//
	sSPKR_EN;
	TestInfo.ADCgain = ADC_G8;
	
	TestInfo.MsgFlag = GAIN_MSG;					// Default to allowing gain change.
	A2Dresult.Samples = SAMPLES_PER_CYCLE;		// 60Hz Notch
	HoldOff = 33;										// Comb filter settle Time mSed.
	Comb.FilterAction = CMD_BANDPASS;
	InitComb();
	SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
	TestInfo.DAC_EnableFlag = ENABLE;
	//
	//---------------------------------------------------------------------------
	//
	FKey = 0;
	AllowedKeys = K_CLR | K_LEVEL | K_UP | K_DOWN;
	//
	//	Scan the frequencies.  Based on Highest frequency found signal the user
	//	with beep sounds.
	//
	PutADCgainMsg();
	PutSpkrVolumeMsg();
	SetFunctionMsg();									// Arrow Key Legends
	NewWindow(AF1_W, NOBORDER);
	LCD_CtrStrg("LF", FONTVWX5, C_MIDDLE, STD);
	NewWindow(AF3_W, NOBORDER);
	LCD_CtrStrg("HF", FONTVWX5, C_MIDDLE, STD);
	
	LCD_OpenWindow(DGRAPH_W, SBORDER);
	while(!FKey)
	{
		Bin = 0;
		pFreqIndex = &FreqTable;
		while(Bin < SPLID_BINS)
		{
			MixerFreq = SplBinFreq[Bin];
			DDSfreq(MixerFreq, DDS_F0);

			Vcurrent = 0;
			for(AvgLoop = 0; AvgLoop < 2; AvgLoop++)
			{
				SetISRcount2(HoldOff);					// Wait for buffer to update.
				while(ISRcountStatus2 == ACTIVE);
				PeakDetector();
				BitVpp = A2Dresult.Vpp;
				if(BitVpp > Vcurrent)
				{
					Vcurrent = BitVpp;
				}
			}
			*(pFreqIndex +Bin) = (uint16_t)(Vcurrent);
			Bin++;
		}
		UpdateDSL();
		KPstat(AllowedKeys);

		if(FKey && !(FKey & (K_CLR)))
		{
			switch(FKey)
			{
				case K_DOWN:					// DWNkey, Lower the DAC speaker value.
				{

					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						if(TestInfo.ADCgain > 0)
						{
							TestInfo.ADCgain--;
							SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
							PutADCgainMsg();
						}
					}
					else  if(TestInfo.MsgFlag == VOL_MSG)
					{
						if(SysData.MonitorLevel > 0)
						{
							SysData.MonitorLevel -= 1;
							PutSpkrVolumeMsg();
						}
					}
					break;
				}

				case K_UP:					// UPkey, Raise the DAC speaker value 6db.
				{
					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						//	Don't Allow gain change if it might saturate signal path.
						//
						if((BitVpp < 3500) && (TestInfo.ADCgain < ADC_G64))
						{
							TestInfo.ADCgain++;
							SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
							PutADCgainMsg();
						}
					}
					else if(TestInfo.MsgFlag == VOL_MSG)
					{
						if(SysData.MonitorLevel < MAX_VOL)
						{
							SysData.MonitorLevel += 1;
							PutSpkrVolumeMsg();
						}
					}
					break;
				}
				case K_LEVEL:
				//
				//	Toggle Gain/Volume/Squelch setting.
				{
					if(TestInfo.MsgFlag == GAIN_MSG)
					{
						TestInfo.MsgFlag = VOL_MSG;
					}
					else if(TestInfo.MsgFlag == VOL_MSG)
					{
						TestInfo.MsgFlag = GAIN_MSG;
					}
					SetFunctionMsg();	
					PutSpkrVolumeMsg();				// Update Borders.
					PutADCgainMsg();					
					break;
				}
			}
			FKey = K_NONE;
		}
	}
	ToneInfo.SpkrEnable = DISABLE;
	TestInfo.DAC_EnableFlag = DISABLE;
}



int8_t GTValue(int16_t Largest, int16_t Smallest, uint8_t Amount)
//------------------------------------------------------------------------------
//	Largest and Smallest are indexes into FreqTable dB values.  Compare the 1st
//	value to the Second value and return (+) if first is larger than the first
//	by "Amount".
//
//	NOTE:  IF Smallest is a negative compare Largest to ZERO (0).
//------------------------------------------------------------------------------
{
	int16_t dBhigh, dBlow;
	uint16_t *pValue;
	
	pValue = &FreqTable;
	dBhigh = *(pValue +Largest);
	if(Smallest >= 0)			dBlow = *(pValue +Smallest);
	else                    dBlow = 0;
	
	if(dBhigh > (dBlow + (uint16_t)Amount))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int8_t WinValue(int16_t FirstValue, uint16_t SecondValue, uint8_t Amount)
//------------------------------------------------------------------------------
//	Compare the 1st value to the Second value and return (+) if first is within
//	+/- Amount.
{
	int16_t Work, dBfirst, dBsecond;
	uint16_t *pValue;
	
	pValue = &FreqTable;
	dBfirst = *(pValue +FirstValue);

	dBsecond = *(pValue +SecondValue);
	Work = dBfirst - dBsecond;
	if(abs(Work) < Amount)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/*
void SvcGainLevelKeypress(void)
//------------------------------------------------------------------------------
//	On entry FKey has UP/DWN/SQUELCH key value.
//------------------------------------------------------------------------------
{
			switch(FKey)
			{
				case K_F2:
				// DWNkey
				{
					if(GainFunctionFlag == GAIN_MSG)
					{
						if(TestInfo.ADCgain > 0)
						{
							TestInfo.ADCgain--;
							SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
						}
					}
					else  if(GainFunctionFlag == VOL_MSG)
					{
						if(SysData.MonitorLevel > 0)
						{
							SysData.MonitorLevel -= 1;
							PutSpkrVolumeMsg();
						}
					}
					break;
				}

					SetSnoopSpan();
					break;
				}

				case K_F3:					// UPkey
				{
					if(GainFunctionFlag == GAIN_MSG)
					{
						//	Don't Allow gain change if it might saturate signal path.
						//
						if((BitVpp < 3500) && (TestInfo.ADCgain < ADC_G64))
						{
							TestInfo.ADCgain++;
							SetADCDiffGain(TestInfo.Mux, TestInfo.ADCgain);
						}
					}
					else if(GainFunctionFlag == VOL_MSG)
					{
						if(SysData.MonitorLevel < MAX_VOL)
						{
							SysData.MonitorLevel += 1;
							PutSpkrVolumeMsg();
						}
					}
					break;
				}
				case K_LEVEL:
				//
				//	Toggle Gain/Volume setting.
				{
					if(GainFunctionFlag == GAIN_MSG)
					{
						GainFunctionFlag = VOL_MSG;
					}
					else
					{
						GainFunctionFlag = GAIN_MSG;
					}
					SetFunctionMsg(GainFunctionFlag);						
					break;
				}
			}
			FKey = 0;
*/