//----------------------------------------------------------------------------
//	File Name: lcd.c
//
//	Content:
//
//		LCD Service routines for NHD-C12864LZ-FSW-FSW-FBW-3v3 using
//		ST7565R display controller driver.
//		
// 	Date:  18 Feb 2014
//
//	Copyright 2014 Allied AnaLogic, Inc
//	All rights reserved
//
//===========================================================================

#include "lcd.h"
#include "string.h"
#include "TTS003.h"
#include "math.h"
#include "Meter.h"
#include <avr/pgmspace.h>

//
//----------------------------------------------------------------------
// Global TrueType Font Variables located in individual "C" files.
//----------------------------------------------------------------------
extern unsigned char Times8Bold032bmp[];
extern unsigned int Times8BoldOffset[];

extern unsigned char Times14Bold_bmp[];
extern unsigned int Times14Bold_Offset[];

extern unsigned char Font6x8[];
extern unsigned char Font5x7[];
extern unsigned char Font4x6[];
extern unsigned char Font5HVW[];

//-----------------------------------------------------------------------------
// Font Pointer and Height information.
//-----------------------------------------------------------------------------
typedef struct TTF_Font
{
	unsigned char *pFont;          // Base address of the font.
	unsigned int *pOffset;         // Used to Index to the next character.
	unsigned char Height;			 // Number of display lines for char height.
}Font_t;

Font_t Style[] =
{
	{&Times14Bold_bmp[0], &Times14Bold_Offset[0], 19},
//	{&Times10Bold032bmp[0], &Times10BoldOffset[0], 13},
	{&Times8Bold032bmp[0], &Times8BoldOffset[0], 11},
//-------------------------------------------------------//		
	{&Font6x8[0], &Times8BoldOffset[0], 8},				// NOTE: Dummy Offset.
	{&Font5x7[0], &Times8BoldOffset[0], 7},
	{&Font4x6[0], &Times8BoldOffset[0], 6},
	{&Font5HVW[0],&Times8BoldOffset[0], 5}
};


//
//-----------------------------------------------------------------------------


unsigned char LCD_Buffer[LCDPAGES][LCDCOLUMNS];
unsigned char PixelType;
uint8_t ZdsplScale;
uint8_t Bstat;

void CtrStrg(char *pString)
//-----------------------------------------------------------------------------
//	Using FONT6x8 font center the string in the open window.
//-----------------------------------------------------------------------------
{
	LCD_CtrStrg(pString, FONT6X8, C_MIDDLE, STD);
} 

void NewWindow(int x, int y, int h, int w, unsigned char border)
//-----------------------------------------------------------------------------
//	Open a new window and clear it.
//-----------------------------------------------------------------------------
{
	LCD_OpenWindow(x,y,h,w, border);
	LCD_ClearWindow(NOBORDER, STD);
}


void LCD_OpenWindow(int x, int y, int h, int w, unsigned char border)
//-----------------------------------------------------------------------------
//	Sets the window area on the desired LCD graphic page. All further LCD
//	writes will be referenced to this area.
//
//	NOTE:	Character position calculation uses Border to compute x,y location. 
//			SBORDER is drawn on the outer parameter. Double line border grows
//			within the defined window.
//-----------------------------------------------------------------------------
{
	int Lines;
	unsigned char Pattern;
	//
	//	If a box is to be drawn around the window, we must use the full page
	//	screen size to position the new window box.
	//
	if((border == SBORDER) || (border == NOBORDER))	Window.b = 1;
	else if(border == BORDER)								Window.b = 2;
	Window.h = h;
	Window.w = w;
	if(border)
	{
		Window.x = 0;      							     // Opens Window at x, y, page.
		Window.y = 0;

		switch(border)
		{
			case SBORDER:
			{
				Lines = 1;
				Pattern = STD;
				break;
			}
			case BORDER:
			{
				Lines = 2;
				Pattern = STD;
				break;
			}
			case DOT:
			{
				Lines = 2;
				Pattern = DOT;
				break;
			}
			default:
			{
				Lines = 2;
				Pattern = STD;
				break;
			}
		}
			
		LCD_DrawBox(x, y, h, w, Lines, Pattern);   // Draws a box in the open window.
	}
	
	Window.x = x;
	Window.y = y;
	Window.Xposition = BORDERPIXELS;
	Window.Yposition = BORDERPIXELS;
}


void LCD_ClearDspl(void)
//-------------------------------------------------------------------------
// Clears Entire Screen of "page". (Writes 0's)
//-------------------------------------------------------------------------
{
	int x,y;
	
	for(x = 0; x < LCDPAGES; x++)						// Byte Size of display
	{
		LCD_set_page_address(x);
		LCD_set_column_address(0);	
		for(y = 0; y < LCDCOLUMNS; y++)
		{
			LCD_Buffer[x][y] = 0;
			LCD_DataWrite(0x00);
		}
	}
}	


void LCD_ClearWindow(uint8_t border, uint8_t video)
//-------------------------------------------------------------------------
// Clears the active window. If Border is true, any previously drawn border
// is erased as well.
//
//	NOTE:	Standard "STD" clear, is a REV video pixel line.
//-------------------------------------------------------------------------
{
	uint16_t GraphMask[LCDPAGES], PagePtr, RowPtr, EndRow, PageData;
	uint16_t BorderWidth, ColumnPtr, EndColumn;
	
	if(border)			BorderWidth = 0;
	else					BorderWidth = BORDERPIXELS;
	
	//--------------------------------------------------------------------------
	//	Build a column mask of bits for the height of the GRAPH_W.
	//
	for(PagePtr = 0; PagePtr < LCDPAGES; PagePtr++)
	{
		GraphMask[PagePtr] = 0x00;
	}
	RowPtr = Window.y + BorderWidth;
	EndRow = Window.y + Window.h - BorderWidth;
	while(RowPtr < (EndRow))
	{
		PagePtr = RowPtr >> 3;									// Page to write.
		GraphMask[PagePtr] |= (1 << (RowPtr & 0x07));	// Bit Position to change.
		RowPtr++;
	}
	//
	//--------------------------------------------------------------------------
	ColumnPtr = Window.x + BorderWidth;
	EndColumn = Window.x + Window.w - BorderWidth;
	while(ColumnPtr < EndColumn)
	{
		for(PagePtr = 0; PagePtr < LCDPAGES; PagePtr++)
		{
			if(GraphMask[PagePtr])
			{
				LCD_set_column_address(ColumnPtr);
				LCD_set_page_address(PagePtr);
				if(video == STD)
				{
					PageData = ~GraphMask[PagePtr];
					LCD_Buffer[PagePtr][ColumnPtr] &= PageData;
				}
				else
				{
					PageData = GraphMask[PagePtr];
					LCD_Buffer[PagePtr][ColumnPtr] |= PageData;
					
				}
				LCD_DataWrite(LCD_Buffer[PagePtr][ColumnPtr]);
			}
		}
		ColumnPtr++;
	}
}	


void LCD_DrawBox(int x, int y, int h, int w, int lines, unsigned char pattern)
//-----------------------------------------------------------------------------
// Draw a double line box in the "open window" starting at xy, with specified
// height and width.
//
//    "Pattern" contains the pixel pattern to draw. and "lines" is the
//    number of lines used for the box border.
//-----------------------------------------------------------------------------
{
if(lines == 1)
   {
   //
  	// If there is only one line thickness, the square corners are filled.
  	//
   // Top Horiz
  	LCD_DrawLine(x+lines, y, lines, w -(2*lines), pattern);

   // Bottom Horiz
  	LCD_DrawLine(x+lines,(y+h-lines), lines, w-(2*lines), pattern);

   // Left Vert
  	LCD_DrawLine(x, (y+lines), h-(2*lines), lines, pattern);

  	// Right Vertical
  	LCD_DrawLine((x+w-lines), (y+lines), h-(2*lines), lines, pattern);
  	}
else
   {
   // Top Horizontal.
   //--------------------
   LCD_DrawLine((x+lines), y, lines, w-(2*lines), pattern);

   // Bottom Horizontal.
   //--------------------
   LCD_DrawLine((x+lines), (y+h-lines), lines, w -(2*lines), pattern);

   // Left Vertical.
   //--------------------
   LCD_DrawLine(x, y+(lines/2), h-lines, lines, pattern);

   // Right Vertical.
   //--------------------
   LCD_DrawLine(x+w -lines, y+(lines/2), h-lines, lines, pattern);
   }
}



void LCD_CtrStrg(char *pString, int FontType, int Position, int video)
//-------------------------------------------------------------------------
// Center the string pointed to by "pString" in the Opened Window.
//	The first byte for each character in the Font Table is the character
//	width.  Compute the maximum characters that can be placed in the open
//	window, move them to a temp string buffer then display 
//-------------------------------------------------------------------------
{
	uint8_t Border2x, Character;
	uint16_t CharWidth, StrgWidth, CharHeight, xStrgPosition, yStrgPosition;
	char *pStrg;
	unsigned char *pChar;
	unsigned int *pCharOffset;
	char StrgBuffer[100];				// 3pixel min char width TB10.
	char *pStrgBuffer;
	
	//	Copy the originating string into the local "StrgBuffer" until complete or
	//	while the pixel count is less than window width.
	//
	StrgWidth = 0;
	pStrg = pString;
	pStrgBuffer = &StrgBuffer[0];
	CharHeight = Style[FontType].Height;
	Border2x = Window.b * 2;			//  Top/Bottom Window Border Lines.
	
	//--------------------------------------------------------------------------
	//
	if((FontType == FONT4X6) || (FontType == FONT5X7) || (FontType == FONT6X8))
	{
		CharWidth = 6;
		if(FontType == FONT4X6)			CharWidth = 4;
		else if(FontType == FONT5X7)	CharWidth = 5 +1;  // 1 =Char spacing.
		do 
		{
			if((StrgWidth + CharWidth) < (Window.w - Border2x))
			{
				StrgWidth += CharWidth;
				*pStrgBuffer = *pStrg;			// Copy Char.
				pStrgBuffer++;
				pStrg++;
			}
			else
			{
				break;
			}
		}							
		while (*pStrg);
	}
	//
	//--------------------------------------------------------------------------
	//
	else if(FontType == FONTVWX5)
	{
		pCharOffset = 0;
		do 
		{
			// Fonts begin with "SPACE" character.
			Character = *pStrg;
			if(Character > '[')	Character = '?';
			Character -= 0x20;
			pChar = Style[FontType].pFont + (Character * 7);
			CharWidth = pgm_read_byte_near(pChar);	// First byte is Pixel Width.
			if((StrgWidth + (CharWidth)) < (Window.w - Border2x))
			{
				*pStrgBuffer = *pStrg;								// Copy Char.
				pStrgBuffer++;
				StrgWidth += (CharWidth + NTR_CHAR_SPACE);	// Char Width + Spacing.
				pStrg++;
			}
			else
			{
				break;
			}
		} while (*pStrg);
	}
	//
	//--------------------------------------------------------------------------
	else
	{
		do 
		{
			pCharOffset = Style[FontType].pOffset;		// Font Byte offset for character.
			pCharOffset += (*pStrg) -0x20;				// Fonts don't include CTRL chars.
	
			pChar = Style[FontType].pFont + pgm_read_word_near(pCharOffset);
			CharWidth = pgm_read_byte_near(pChar);		// First byte is Pixel Width.
			if((StrgWidth + (CharWidth + NTR_CHAR_SPACE)) < (Window.w - Border2x))
			{
				*pStrgBuffer = *pStrg;			// Copy Char.
				pStrgBuffer++;
				StrgWidth += (CharWidth + NTR_CHAR_SPACE);// Char Width + Spacing.
				pStrg++;
			}
			else
			{
				break;
			}
		}
		while(*pStrg);		
	}
	//
	//--------------------------------------------------------------------------
		
	*pStrgBuffer = 0x00;				// Terminate Copied String.
	
	// Calculate the xy plot position of the first character based on the "Position"
	// input.
	//
	switch(Position)
	{
		case T_MIDDLE:
		{
			yStrgPosition = 3;
			xStrgPosition = (Window.w - StrgWidth)/2;
			break;
		}
		case C_MIDDLE:
		{
			yStrgPosition = ((Window.h -Border2x)/2) -(CharHeight/2) + Window.b;
			xStrgPosition = (Window.w - StrgWidth)/2;
			break;
		}
		case B_MIDDLE:
		{
			yStrgPosition = Window.h - CharHeight -Border2x;
			xStrgPosition = (Window.w - StrgWidth)/2 ;
			break;
		}
		//----------------
		case T_LEFT:
		{
			yStrgPosition = 3;
			xStrgPosition = 0 + 3;
			break;
		}
		case C_LEFT:
		{
			yStrgPosition =  ((Window.h -Border2x)/2) -(CharHeight/2) + Window.b;
			xStrgPosition = 0 + 3;
			break;
		}
		case B_LEFT:
		{
			yStrgPosition = Window.h - CharHeight -Border2x;
			xStrgPosition =  0 + 3;
			break;
		}
		//----------------
		case T_RIGHT:
		{
			yStrgPosition = 3;
			xStrgPosition = Window.w - StrgWidth -Window.b;
			break;
		}
		case C_RIGHT:
		{
			yStrgPosition = ((Window.h -Border2x)/2) -(CharHeight/2) + Window.b;
			xStrgPosition = Window.w - StrgWidth -Window.b;
			break;
		}
		case B_RIGHT:
		{
			yStrgPosition = Window.h - CharHeight -Window.b;
			xStrgPosition = Window.w - StrgWidth -Window.b;
			break;
		}
	}
//	CharQty = strlen(&StrgBuffer[0]);
	LCD_PutString(&StrgBuffer[0], FontType, xStrgPosition, yStrgPosition, video);
}



void LCD_PutString(char *pString, int FontType, int x, int y, int video)
//------------------------------------------------------------------------------
//	Put the String in the open window and mirror changes in the LCD_Buffer[][]
//------------------------------------------------------------------------------
{
	uint16_t NextX, CharWidth, CharSpacing;

	if((FontType == FONT4X6) || (FontType == FONT6X8))
	//
	// Spacing embedded in Table.
	{
		CharSpacing = 0;
	}
	else
	{
		CharSpacing = NTR_CHAR_SPACE;
	}
	
	NextX = x;
	while(*pString)
	{
		CharWidth = LCD_PutCharacter(pString, FontType, NextX, y, video);
		NextX = CharWidth + NextX +CharSpacing;
		pString++;
	}
}

uint8_t LCD_PutCharacter(char *pString, int FontType, int x, int y, int video)
//------------------------------------------------------------------------------
//	Convert the bit map AsciiCharacter into a x/y format for the LCD.  The First
//	bit map byte contains the number of horizontal bits of a character row.
//	Following are concatenated column bit values for the first and remaining
//	lines/rows.
//------------------------------------------------------------------------------
//	The Font Tables consist of ASCII characters from 0x20 (space) through decimal
//	127.  The format:
//				First Byte:		Pixel width of the character
//				Second Byte:	First horizontal row of pixel.  
//				Third Byte:		Remaining first rows bits, or Second row bits
//
//	The bytes following the first byte is a series of pixels concatenated together
//	for each column and row of the character.  If the char width is more than 8
//	bits the following byte(s) is used to complete the row.  The next bit will
//	begin the pixel string for the following row.
//
//	The Offset table forms an entry point for each character. 
//------------------------------------------------------------------------------
//
//	Example:
//			First Byte:		0x06			// Number of pixels on first line/row.
//			Second Byte:	0x55			// D7-> D2 = pixels on line 1.
//												// D1-> D0 = first two pixels on second line.
//			Third Byte:						// D7-> D4 = remaining pixels on second line.
//
//	Character Pixels:
//			R0 R0 R0 R0 R0 R0 R1 R1, R1 R1 R1 R1 R2 R2 R2 R2, R2 R2 R3 R3..........
//
//	The LCD contains LCDCOLUMNS comprised of vertical PAGES, each page organized
//	as D0 -> D7 top to bottom.  The number of LCD horizontal pixels is it's 
//	width whereas the number of vertical pixels is expressed in 8 bit PAGES.

//	This function uses a vertical column buffer to translate serial bit patterns
//	of the font into proper "Y" position beginning with row "X" position.  Once
//	all the bits for a specific column have been moved "X" is incremented and "Y"
// buffer updated for next column.
//
//	NOTE:	On entry "x" and "y" positions are relative to the open Window.
//			Returns  Xposition following Char and cleared space for next char.
//	
//------------------------------------------------------------------------------
{
//	uint16_t CharColumnPtr, CharRowPtr, CharHeight, FirstBitPosition;
//	uint8_t LCD_ColumnPtr, LCD_RowPtr, PagePtr;
//	uint8_t LCD_Page[LCDPAGES], PageMask[LCDPAGES];

//	uint8_t ByteMask, ByteValue, Index, FirstPage, LastPage;

	uint16_t CharColumnPtr, CharRowPtr, CharHeight, FirstBitPosition;
	uint16_t LCD_ColumnPtr, LCD_RowPtr, PagePtr;
	uint16_t LCD_Page[LCDPAGES], PageMask[LCDPAGES];

	uint16_t ByteMask, ByteValue, Index, FirstPage, LastPage;
	
	uint8_t *pChar, CharWidth;
	unsigned int *pCharOffset, HexValue;
	
	CharHeight = Style[FontType].Height;		// Character Height, Width
	pChar = Style[FontType].pFont;

	if((FontType == FONT4X6) || (FontType == FONT6X8))
	{
		CharWidth = 6;
		if(FontType == FONT4X6) CharWidth = 4;
		// Find the first byte of the Character BitMap.
		//
		HexValue = ((*pString) -0x20) * ((CharWidth * CharHeight)/8);
		pChar += HexValue;
	}
	else if(FontType == FONT5X7)
	//
	//	Column BitMap, Fixed.
	{
		CharWidth = 5;						
		HexValue = ((*pString) -0x20) * CharWidth;
		pChar += HexValue;
//		CharWidth++;									// Add blank column between chars.
	}
	else if(FontType == FONTVWX5)
	{
		HexValue = *pString;
		if(HexValue > '^')	HexValue = '?';	// Lower Case ONLY..!!!.
		HexValue = (HexValue -0x20) * 7;			// 7 bytes for each char entry.
		pChar += HexValue;
		CharWidth = pgm_read_byte_near(pChar);;
		pChar++;											// First Column to Plot.		
	}
	else
	//
	//	Compressed Row BitMap.
	{
		pCharOffset = Style[FontType].pOffset;
		pCharOffset += (*pString) -0x20;
		HexValue = pgm_read_word_near(pCharOffset);
	
		pChar +=  HexValue;
		CharWidth = pgm_read_byte_near(pChar);
		pChar++;											// First byte is character bit map.
	}

	// The outside loop advances through each Char Horizontal Pixel position
	//	while the inside loop will copy Char Row Pixels to Vertical LCD buffer.
	//	This process converts horizontally sequenced pixels to vertical
	//	columns compatible with LCD Page writes.
	//
	LCD_ColumnPtr = Window.x + x;							// First LCD Column to write.
	for(CharColumnPtr = 0; CharColumnPtr < CharWidth; CharColumnPtr++)
	{
		for(Index = 0; Index < LCDPAGES; Index++)		// Clear LCD Vertical Buffer.
		{
			LCD_Page[Index] = 0;
		}
		LCD_RowPtr = Window.y +y;				// First LCD_Row to plot pixel.

		if((FontType == FONT5X7) || (FontType == FONTVWX5))
		//------------------------------------------------------------------
		//	ROM Font 5x7 table formated vertically 7 bits high, 5 bits wide 
		//	in 5 successive byte.
		//
		//	Move all bits in the column into the "LCD_Page" buffer.
		//	
		//	NOTE:  The final column will be cleared to allow space between chars.
		{
			CharHeight = 7;
			if(FontType == FONTVWX5)	CharHeight = 5;

			FirstBitPosition = (1 << (8 -CharHeight));
			CharRowPtr = 0;
			if(CharColumnPtr < CharWidth)
			{
				ByteValue = pgm_read_byte_near(pChar + CharColumnPtr);
				while(CharRowPtr < CharHeight)
				{
					ByteMask = 0;
					ByteMask = (FirstBitPosition << CharRowPtr);
					ByteMask &= ByteValue;

					if(ByteMask)
					//
					// Locate page and bit position within the page to set the pixel.
					{
						PagePtr = LCD_RowPtr >> 3;
						LCD_Page[PagePtr] |= (1 << (LCD_RowPtr & 0x07));
					}

					//	Advance to the next Vertical bit position.
					//
					CharRowPtr ++;							// Next Char Row Position.
					LCD_RowPtr++;							// Next LCD Row Position.
				}
			}
		}
		//
		//---------------------------------------------------------------------

		else
		//------------------------------------------------------------------------
		//	ROM Font Table Format is in compressed Horizontal "ROWS".  
		//	Move all Vertical Character bits to the LCD Page buffer.
		//
		{
			CharRowPtr = CharColumnPtr;			// First Bit in LCD_Column
			while(CharRowPtr < (CharHeight * CharWidth))
			{
				// If the bit value is "1", set the appropriate bit in the LCD page. 
				// Note: "CharRowPtr >> 3" will increment pChar on every 8th bit.
				//
				ByteValue = pgm_read_byte_near((pChar + (CharRowPtr >> 3)));

				ByteMask = 0;
				ByteMask = (0x80 >> (CharRowPtr & 0x07));	// Check if bit position is High.
				ByteMask &= ByteValue;

				if(ByteMask)
				//
				// Locate page and bit position within the page to set the pixel.
				{
					PagePtr = LCD_RowPtr >> 3;
					LCD_Page[PagePtr] |= (1 << (LCD_RowPtr & 0x07));
				}

				//	Advance to the next Vertical bit position.
				//
				CharRowPtr += CharWidth;					// Next Char Row Position.
				LCD_RowPtr++;									// Next LCD Row Position.
			}
		}
		//
		//------------------------------------------------------------------------
		//	All Character Row bits have been moved into the LCD Column buffer.
		//
		// Create a MASK for the new LCD Column data.  The MASK will Zero the
		//	appropriate bits in the LCD_Buffer[][] to allow new bits to overwrite
		//	previously saved data.
		//
		for(Index = 0; Index < LCDPAGES; Index++)
			PageMask[Index] = 0;
	
		LCD_RowPtr = Window.y + y;
		FirstPage = (Window.y + y) >> 3;
		LastPage = (Window.y+y + CharHeight) >> 3;

		while(LCD_RowPtr < (CharHeight + Window.y + y))
		{
			PagePtr = LCD_RowPtr >> 3;								// Page to write.
			PageMask[PagePtr] |= (1 << (LCD_RowPtr & 0x07));// Bit Position to change.
			LCD_RowPtr++;
		}

		// Using the PageMask insert the new values and then write the new page
		//	information to the column position on the display.
		//
		for(Index = FirstPage; Index <= LastPage; Index++)
		{
			LCD_Buffer[Index][LCD_ColumnPtr] &= ~PageMask[Index]; // Cut a hole...
			if(video == STD) 
			{
				LCD_Buffer[Index][LCD_ColumnPtr] |=  LCD_Page[Index];
			}
			else if(video == REV)
			{
				LCD_Buffer[Index][LCD_ColumnPtr] |= (PageMask[Index] & (~LCD_Page[Index]));
			}				
			LCD_set_column_address(LCD_ColumnPtr);
			LCD_set_page_address(Index);
			LCD_DataWrite(LCD_Buffer[Index][LCD_ColumnPtr]);
		}

		LCD_ColumnPtr++;		// Next LCD Column.
	}
	return(CharWidth);
}




void LCD_SetCursorPosition(unsigned int Position)
//-----------------------------------------------------------------------------
//	Move the cursor to the memory address "Position".  Wait for Cursor, then send
//	the "WRITE" command.
//
//	The screen horizontal line occupies 16 bytes with 64 vertical lines.
//-----------------------------------------------------------------------------
{
	uint8_t LineNumber;
	uint8_t ColumnNumber;
	
	LineNumber = Position/16;
	ColumnNumber = Position & 0x0f;
	
	LCD_CmdWrite(LCD_CMD_START_LINE_SET(LineNumber));
	LCD_set_column_address(ColumnNumber);
}

void LCD_DrawLine(int x, int y, int h, int w, uint8_t action)
//------------------------------------------------------------------------------
//	Draw a Straight line from "x"->"x+w" having a height of "h" and horizontal
//	bit pattern of "Pattern".
//
//	On entry:
//			x = column/horizontal beginning pixel position
//			y = row/line beginning pixel position
//			h = height in pixels
//			w = pixel width of the line
//			Pattern = horizontal pixel pattern to write.
//			Screen = Screen number currently being displayed.
//
//	The LCD display controller is based on LCDcolumns and LCDlines.  Each vertical
//	page consists of 8 pixels organized as D0->D7 top to bottom.
//
//	NOTE:	The x,y,h,w values are based on absolute screen position without regard
//			to an open window.  Therefore it is imperative these values are
//			adjusted by window position prior to entry.
//------------------------------------------------------------------------------
{
	uint16_t	RowPtr, PagePosition, Page;
	uint16_t	Column, Index;
	uint16_t	Vbuffer[LCDPAGES];
	

	// Loop until all Columns have been updated.
	//
	for (Column = x; Column < (x + w);  Column++)
	{
		for(Index = 0; Index < LCDPAGES; Index++)
		{
			Vbuffer[Index] = 0;				//	Clear the column buffer..
		}

		//	Write "1's" to each row beginning with row "y" for height of "h".
		//
		RowPtr = y;
		Index = h;
		while(Index)
		{
			Page = RowPtr >> 3;
			PagePosition = RowPtr & 0x07;
			//--------------------------------------------------------------------
			// "Vertical PAGE Bits" to change.
			//
			Vbuffer[Page] |= (1 << PagePosition);
			//
			//--------------------------------------------------------------------			
			RowPtr++;
			Index--;
		}

		//	Write each page (vertical column) that contains data.
		//
		for(Page = 0; Page < LCDPAGES; Page++)
		{
			if(LCD_Buffer[Page][Column] != Vbuffer[Page])
			{
				LCD_set_column_address(Column);
				LCD_set_page_address(Page);
				if(action == STD)			
				{
					LCD_Buffer[Page][Column] |= Vbuffer[Page];
					LCD_DataWrite(LCD_Buffer[Page][Column]);
				}
				else if(action == REV)
				{
					LCD_Buffer[Page][Column] &= ~Vbuffer[Page];
					LCD_DataWrite(LCD_Buffer[Page][Column]);
				}
			}
		}
		//
		//-----------------------------------------------------------------------
	}
}
		

void LCD_DiagonalLine(int Xstart, int Ystart, int Xend, int Yend,
																			 unsigned char Pattern)
//-----------------------------------------------------------------------------
//	Plot a diagonal line from "Xstart", "Ystart" to "Xend", "Yend".  Integer values
//	associated with the slope of the line are used rather than tan functions to 
//	speed plot times.
//
//	Pattern value is rotated on each plot group. If the LSBit is 1 the plot value
//	will fill the pixels.  A zero will clear the pixels.
//
//	Where "dx" = Xend - Xstart		Negative indicates 90 -> 180 degrees
//	  and "dy" = Yend - Ystart		
//
//	0 -> 45 	degrees:	slope = dx/dy  pixels per row before increment to next row
//	46-> 90 	degrees:	slope = dy/dx	pixels per column before inc to next column
//	91-> 135 degrees:	slope = dy/dx
//	136->180	degrees:	slope = dx/dy
//
//-----------------------------------------------------------------------------
{
	int dx, dy, Row, Column, Pixels, Ylimit;
	int r, dyt, Ystep, dxt, Xstep;
	unsigned char action, mask;
	
	
	Row = Ystart;			// Starting pixel location.
	Column = Xstart;
	mask = Pattern;
	Ylimit = Yend;			// No F2 Window limit imposed.
	//
	//	If Pattern indicates diagonal line equates to a NEEDLE function, Stop
	//	line before it reaches F2 window.
	//
	if(Pattern == NEEDLE_CLR)
	{
		Ylimit = Yend -9;
		mask = CLEAR;
	}
	if(Pattern == NEEDLE_FILL)
	{
		Ylimit = Yend -9;
		mask = FILL;
	}
	
	dx = Xend - Xstart;			// Delta x,	Total Columns to plot.
	dy = Yend - Ystart;			// Delta y,	Total Rows to plot.
										//	Slope = dx/dy;
	if((Xstart == Xend) && (Ystart != Yend))
	//--------------------------------------------------------------------------
	//	Vertical Line.
	//
	{
		for(Pixels = dy; Pixels; Pixels--)
		{
			if(Row < Ylimit)
			{
				action = mask & 0x01;			// action = Current LSBit of mask.
				mask = mask >> 1;					// setup for next Bit mask.
				mask |= (action << 7);			// rotate the previous bit to D7.
				LCD_PutPixel(Column, Row, action);
			}
			Row++;
		}
	}
	//
	//--------------------------------------------------------------------------
		
	
	else if ((Ystart == Yend) && (Xstart != Xend))
	//--------------------------------------------------------------------------
	//	Horizontal line.
	//
	{
		for(Pixels = dx; Pixels; Pixels--)
		{
			action = mask & 0x01;			// action = Current LSBit of mask.
			mask = mask >> 1;					// setup for next Bit mask.
			mask |= (action << 7);			// rotate the previous bit to D7.
			
			LCD_PutPixel(Column, Row, action);
			Column++;
		}
	}
	//
	//--------------------------------------------------------------------------

	else if((dy > 0) && (dx > 0) && (dy > dx))
	//--------------------------------------------------------------------------
	//	Angle is greater than 45 degrees.  "dx" is the columns to plot and "dy/dx"
	//	is the number of pixels on the same column.
	//
	{
		r = 0;
//		while(Row < Yend)
		while(Row < Ylimit)
		{
			Ystep = 0;
			dyt = r + dy;
			while(dyt > 0)
			{
				dyt -= dx;
				if(dyt >= 0)
					Ystep++;
			}
			if(dyt < 0)
				r = dyt +dx;
			else
				r = 0;
			
			// Ystep	is the number of row pixels for every Xpixel.
			//			
			for(Pixels = Ystep; Pixels; Pixels--)
			{
				if(Row < Ylimit)
				{
					action = mask & 0x01;			// action = Current LSBit of mask.
					mask = mask >> 1;					// setup for next Bit mask.
					mask |= (action << 7);			// rotate the previous bit to D7.
					LCD_PutPixel(Column, Row, action);
				}
				Row++;
			}
			Column++;
		}
	}
	//
	//--------------------------------------------------------------------------
		
	else if((dy > 0) && (dx > 0) && (dy <= dx))
	//--------------------------------------------------------------------------						
	// Less than, or equal to 45 degrees. "dy" is the rows to plot and "dx/dy"
	//	are the pixels on the same row.
	{
		r = 0;
		while(Row < Yend)
		{
			Xstep = 0;
			dxt = r + dx;
			while(dxt > 0)
			{
				dxt -= dy;
				if(dxt >= 0)
					Xstep++;
			}
			if(dxt < 0)
				r = dxt +dy;
			else
				r = 0;
			
			// Xstep	is the number of Column pixels for every Ypixel.
			//			
			for(Pixels = Xstep; Pixels; Pixels--)
			{
				if(Row < Ylimit)
				{
					action = mask & 0x01;			// action = Current LSBit of mask.
					mask = mask >> 1;					// setup for next Bit mask.
					mask |= (action << 7);			// rotate the previous bit to D7.
					LCD_PutPixel(Column, Row, action);
				}
				Column++;
			}
			Row++;
		}
	}
	//
	//--------------------------------------------------------------------------
		

	else if((dx < 0) && (fabs(dx) >= dy))	
	//--------------------------------------------------------------------------
	// 135 -> 180 degrees
	{
		r = 0;
		dx = fabs(dx);
		while(Row < Yend)
		{
			Xstep = 0;
			dxt = r + dx;
			while(dxt > 0)
			{
				dxt -= dy;
				if(dxt >= 0)
					Xstep++;
			}
			if(dxt < 0)
				r = dxt +dy;
			else
				r = 0;
			
			// Xstep	is the number of row pixels for every Ypixel.
			//			
			for(Pixels = Xstep; Pixels; Pixels--)
			{
				if(Row < Ylimit)
				{
					action = mask & 0x01;			// action = Current LSBit of mask.
					mask = mask >> 1;					// setup for next Bit mask.
					mask |= (action << 7);			// rotate the previous bit to D7.
					LCD_PutPixel(Column+1, Row, action);
				}
				Column--;	// Left (Y)
			}
			Row++;			// Down (X)
		}
	}
	//
	//--------------------------------------------------------------------------
	
	else
	//--------------------------------------------------------------------------
	// 90-> 135 degrees.  "dx" is the number of columns and "dx/dy" is the pixels
	//	to plot on each column.
	{
		r = 0;
		dx = fabs(dx);
		while(Row < Yend)
		{
			Ystep = 0;
			dyt = r + dy;
			while(dyt > 0)
			{
				dyt -= dx;
				if(dyt >= 0)
					Ystep++;
			}
			if(dyt < 0)
				r = dyt +dx;
			else
				r = 0;
			
			// Ystep	is the number of row pixels for every Xpixel.
			//			
			for(Pixels = Ystep; Pixels; Pixels--)
			{
				if(Row < Ylimit)
				{
					action = mask & 0x01;			// action = Current LSBit of mask.
					mask = mask >> 1;					// setup for next Bit mask.
					mask |= (action << 7);			// rotate the previous bit to D7.
					LCD_PutPixel(Column, Row, action);
				}
				Row++;	// Down (Y)
			}
			Column--;	// Left (X)
		}
	}
	//
	//--------------------------------------------------------------------------
}		


void LCD_PutPixel(int x, int y, int action)
//-----------------------------------------------------------------------------
//	The LCD is made up of LCDCOLUMNBYTES (bytes per row *8 = pixels) and LCDROWs.
//	Compute the byte that contains the pixel to change and modify the buffer BIT,
//	then move the LCD cursor to the correct byte position and update.
//
//	action= true if pixel is to be turned on.
//	x		= Xposition within the open window.
//	y	 	= Yposition within the open window.
//-----------------------------------------------------------------------------
{
	uint8_t  RowPosition, ColumnPosition, PagePosition;
	unsigned char Data;


	//	Locate the Column, Page and bit position for the pixel.
	//
	PagePosition = (y + Window.y) >> 3;
	RowPosition = (y+ Window.y) & 0x07;
	ColumnPosition = (x + Window.x);
	Data = (1 << RowPosition);
	LCD_set_column_address(ColumnPosition);
	LCD_set_page_address(PagePosition);

	if(PixelType == NOBUF)
	//
	//	Set the Bit on the LCD, but don't save it in the LCD buffer.
	{
		Data |= LCD_Buffer[PagePosition][ColumnPosition];
		LCD_DataWrite(Data);
	}
	else if(action)
	//
	//	Set the bit.....
	{
		LCD_Buffer[PagePosition][ColumnPosition] |= Data;
		LCD_DataWrite(LCD_Buffer[PagePosition][ColumnPosition]);
	}
	else
	{
		LCD_Buffer[PagePosition][ColumnPosition] &= ~Data;
		LCD_DataWrite(LCD_Buffer[PagePosition][ColumnPosition]);
	}
}




int StrgPixels(char *pString, int FontType)
//-------------------------------------------------------------------------
// Calculate the number of horizontal pixels the string will occupy.
//-------------------------------------------------------------------------
{
	char *pStrg;
	unsigned char *pFontPointer;
	unsigned char *pChar;
	unsigned int *pOffsetTbl;
	int StrgWidth;
	long Sindex;
	
	
	pStrg = pString;
	
	pFontPointer = Style[FontType].pFont;       // Base Addr of 1st character in Font array.
	pOffsetTbl = Style[FontType].pOffset;       // Offset Table array.
	
	StrgWidth = 0;
	while(*pStrg)
	{
		Sindex = (int)(*pStrg);						  // Char locator within Font Structure.
		{
			Sindex -= 0x20;
			pChar = (pFontPointer +
			(*(pOffsetTbl + Sindex)) );         // Character ptr for plotting.
			StrgWidth += (*pChar + NTR_CHAR_SPACE);   // First byte is Character Width.
		}
		pStrg++;
	}
	return(StrgWidth);
}


void SetupDCMeterWindows(void)
//-----------------------------------------------------------------------------
//	Initialize the windows associated with DC Meter readings. Reverse Video for
// T/R/G terminal with data windows	connected with solid lines.
//
//	On Return:	TR value window is open.
//-----------------------------------------------------------------------------
{
	NewWindow(DATA_W, SBORDER);				// Clear the DATA_W...
	LCD_OpenWindow(LTR_T_W, SBORDER);		//	Letter "T"
	LCD_ClearWindow(NOBORDER, REV);
	LCD_CtrStrg("T",FONT5X7, C_MIDDLE, REV);
		
	LCD_OpenWindow(LTR_R_W, SBORDER);		// letter "R"
	LCD_ClearWindow(NOBORDER, REV);
	LCD_CtrStrg("R",FONT5X7, C_MIDDLE, REV);

	LCD_OpenWindow(LTR_G_W, SBORDER);
	LCD_ClearWindow(NOBORDER, REV);
	LCD_CtrStrg("G",FONT5X7, C_MIDDLE, REV);
		
	// Draw solid connecting lines.  Terminal Windows will clear out an area
	//	for the corresponding values.
	//
	LCD_OpenWindow(DATA_W, SBORDER);
	LCD_DiagonalLine(LTR_T_WX + LTR_T_WW, 8, TR_WX, 8, SOLID);
	LCD_DiagonalLine(TR_WX + TR_WW, 8, LTR_R_WX, 8, SOLID);
	
	LCD_DiagonalLine(LTR_T_WX + LTR_T_WW, 12, LTR_G_WX, DATA_WH -12,
	SOLID);

	LCD_DiagonalLine(LTR_R_WX -2, 11, LTR_G_WX + LTR_G_WW -2,
	DATA_WH -12, SOLID);
		
	//	Value Windows.  These windows will accommodate LU6, FONT6X8 or FONT4X6.
	//
	NewWindow(RG_W, SBORDER);
	NewWindow(TG_W, SBORDER);
	NewWindow(TR_W, SBORDER);
}		

void DisplayFreqLevel(void)
{
	char WrkStrg[10];
	NewWindow(AF1_W, NOBORDER);
	LCD_CtrStrg(AxData[SYSdata.AxIndex].pMsg,FONTVWX5, C_LEFT, STD);
	strcpy(WrkStrg, "L: ");
	strcat(WrkStrg, LvlData[SYSdata.LevelIndex].pMsg);
	NewWindow(AF3_W, NOBORDER);
	LCD_CtrStrg(WrkStrg,FONTVWX5, C_RIGHT, STD);
}


void LossGraph(void)
//-----------------------------------------------------------------------------
//	Clear the display and load the graphic screen for Loss.
//
//	Returns with OpenWindow => "GraphArea"
//-----------------------------------------------------------------------------
{
	LCD_ClearDspl();
	LCD_OpenWindow(MODE_W, SBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("Estimated Loss",TB8, C_MIDDLE, STD);
	LCD_OpenWindow(GRAPH_W, NOBORDER);
}	




void Graph100Pixels(uint8_t *pPixel)
//------------------------------------------------------------------------------
//	Graph 100 yPixels.
//
//	NOTE:	Larger Pixel values appear closer to window bottom.
//------------------------------------------------------------------------------
{
	uint8_t IndexCnt = 0;				// Determines Horiz Graticule pixel spacing

	uint8_t xPosition, yPosition, yPrevious;
	uint8_t GraphMask[LCDPAGES], GraphGrat[LCDPAGES], PagePtr, RowPtr;
	uint8_t ClearIndex, OneShot, PageData;

	LCD_OpenWindow(GRAPH_W, NOBORDER);

	//--------------------------------------------------------------------------
	//	Build a column mask of bits for the height of the GRAPH_W.
	//
	for(PagePtr = 0; PagePtr < LCDPAGES; PagePtr++)
	{
		GraphMask[PagePtr] = 0x00;
		GraphGrat[PagePtr] = 0x00;
	}
	IndexCnt = 4;
	RowPtr = GRAPH_WY;
	while(RowPtr < (GRAPH_WY + GRAPH_WH))
	{
		PagePtr = RowPtr >> 3;										// Page to write.
		GraphMask[PagePtr] |= (1 << (RowPtr & 0x07));		// Bit Position to change.
		
		// Place a Vertical pixel graticule marker every 5th row.
		//
		IndexCnt++;
		if(IndexCnt == 8)
		{
			GraphGrat[PagePtr] |= (1 << (RowPtr & 0x07));
			IndexCnt = 0;
		}
		RowPtr++;
	}
	//
	//--------------------------------------------------------------------------

	xPosition = 0;
	while(xPosition < 100)
	{
		yPosition = *pPixel;

		//------------------------------------------------------------------------
		//	Before plotting the Pixel, Advance Clear and put in the GraphGraticule.
		//
		ClearIndex = OneShot = 0;
		do
		{
			for(PagePtr = 0; PagePtr < LCDPAGES; PagePtr++)
			{
				if(GraphMask[PagePtr] || (GraphGrat[PagePtr] && ((IndexCnt & 0x07) == 7)))
				{
					LCD_set_column_address(xPosition +ClearIndex +GRAPH_WX);
					LCD_set_page_address(PagePtr);
					PageData = ~GraphMask[PagePtr];
					LCD_Buffer[PagePtr][xPosition +ClearIndex +GRAPH_WX] &= PageData;
					if(((IndexCnt & 0x07) == 7) && (!OneShot))
					//
					// Horizontal Graticule pixel spacing
					{
						LCD_Buffer[PagePtr][xPosition +ClearIndex +GRAPH_WX] |=
						GraphGrat[PagePtr];
					}
					LCD_DataWrite(LCD_Buffer[PagePtr][xPosition +ClearIndex +GRAPH_WX]);
				}
			}
			if((ClearIndex + xPosition) < GRAPH_WW)	ClearIndex++;
			OneShot++;
			
		} while(((ClearIndex + xPosition) < GRAPH_WW) && (ClearIndex < 6));
		IndexCnt++;
		//
		//---------------------------------------------------------------------------
		// Plot the new point and draw a line between the two yPositions.
		//
		LCD_PutPixel(xPosition, yPosition, FILL);
		if(xPosition)
		{
			if(yPrevious > yPosition)
			{
				LCD_DiagonalLine(xPosition, yPosition, xPosition,
				yPrevious, FILL);
			}
			else
			{
				LCD_DiagonalLine(xPosition, yPrevious, xPosition,
				yPosition, FILL);
			}
		}
		xPosition++;
		pPixel++;
		yPrevious = yPosition;
	}
}




void NewModeMessage(char *pString)
//-----------------------------------------------------------------------------
//	Write "pString", then update LOWBAT and TALK windows.
//-----------------------------------------------------------------------------
{

	LCD_OpenWindow(MODE_W, SBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg(pString, FONT5X7, C_MIDDLE, STD);
	
	PreviousBstat = ~Bstat;					// Force update of Battery & Talk status.
	BIupdate();
}





void WriteFunctionWindow(char *pString, uint8_t WinNumber, uint8_t Video)
//------------------------------------------------------------------------------
// Open the Function Window, clear, and write new message.
// 
// NOTE:	Since this function uses Miniature Font 5xX, all characters must be
//			upper case.
//------------------------------------------------------------------------------
{
uint8_t Border;

	SwapWindow();
	if(Video == REV)		Border = NOBORDER;
	else						Border = SBORDER;

	switch(WinNumber)
	{
		case WF1:
		{
			LCD_OpenWindow(F1_W,Border);
			break;
		}
		case WF2:
		{
			LCD_OpenWindow(F2_W,Border);
			break;
		}
		case WF3:
		{
			LCD_OpenWindow(F3_W,Border);
			break;
		}
	}
	LCD_ClearWindow(NOBORDER, Video);
	LCD_CtrStrg(pString, FONTVWX5, C_MIDDLE, Video);
	SwapWindow();
}


void ClearFunctionWindow(uint8_t WinNumber, uint8_t Video)
//------------------------------------------------------------------------------
// Open the Function Window, clear, and write new message.
// 
// NOTE:	Since this function uses Miniature Font 5xX, all characters must be
//			upper case.
//------------------------------------------------------------------------------
{
	switch(WinNumber)
	{
		case WF1:
		{
			LCD_OpenWindow(F1_W,SBORDER);
			LCD_ClearWindow(SBORDER, Video);
			break;
		}
		case WF2:
		{
			LCD_OpenWindow(F2_W,SBORDER);
			LCD_ClearWindow(SBORDER, Video);
			break;
		}
		case WF3:
		{
			LCD_OpenWindow(F3_W,SBORDER);
			LCD_ClearWindow(SBORDER, Video);
			break;
		}
	}
}


void DeleteFunctionWindow(uint8_t WinNumber, uint8_t Video)
//------------------------------------------------------------------------------
// Open the Function Window, Clear and Delete, and RE-write DATA_W Border.
//------------------------------------------------------------------------------
{
	switch(WinNumber)
	{
		case WF1:
		{
			LCD_OpenWindow(F1_W,SBORDER);
			LCD_ClearWindow(SBORDER, Video);
			break;
		}
		case WF2:
		{
			LCD_OpenWindow(F2_W,SBORDER);
			LCD_ClearWindow(SBORDER, Video);
			break;
		}
		case WF3:
		{
			LCD_OpenWindow(F3_W,SBORDER);
			LCD_ClearWindow(SBORDER, Video);
			break;
		}
	}
	LCD_OpenWindow(DATA_W, SBORDER);
}



void PutRetestMessage(void)
//------------------------------------------------------------------------------
// Put the "RETEST" message on the F3 window position.
//------------------------------------------------------------------------------
{
	SwapWindow();			// Save active window.
	WriteFunctionWindow("RETEST", WF3, STD);
	SwapWindow();
}

void FunctionSetUpWindow(void)
//------------------------------------------------------------------------------
//	Put Function windows at F1/F2/F3 for Up/Set/Dwn after clearing the DataWindow.
//------------------------------------------------------------------------------
{
	WriteFunctionWindow("SET", WF1, STD);			// UP/DWN arrows to change value.
	WriteFunctionWindow("]", WF2, STD);
	WriteFunctionWindow("[", WF3, STD);
}


