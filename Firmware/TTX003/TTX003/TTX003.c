/*
 * TTX003.c
 *
 * Created: 10/14/2018 10:42:29 AM
 * Author : hwmathews
 */ 

#include	<stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>
#include <string.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include "Meter.h"
#include "DDS.h"
#include "Sound.h"
#include "sysclk.h"
#include "TTX003.h"
#include "lcd.h"
#include "tc.h"
#include "SnoopyComb.h"

//-----------------------------------------------------------------------------
// The below Sine Table is used to produce a sine wave of 16 steps, or 22.5
//	degrees per step.  Amplitude is controlled by taking a percentage of sine
//	table values moved into SRAM and loaded into the associated PWM CCA register
//	during SineWave generation.  The use of SRAM preloaded before use by the PWM
//	minimizes ISR execution time.
//
//	TCC1 counter updates the PWM 16 times per SineWave Cycle, therefore directly
//	controlling the frequency being generated.
//
//		TCC1 ISR interval = (1/(16 * Freq));
//
//	CLKper of 8MHz reduces system current.  TC pre-scaler is set for /1 which
//	yields 31.250KHz, therefore with Counter PERiod TOP 255 defines the
//	resolution.  CCA center is thus 128 allowing +/- 127 PWM set points
//	within the PERiod.
//
//		Given Desired Freq = 577hz, and TCC1 clocks @8MHz, the ISR counter
//		should update the PWM compare registers every 108.32uSec, or each
//		108.32uS/(1/8e6) = 866.56 counts of Timer1. (867 = 576.7Hz)
//
//-----------------------------------------------------------------------------
const int SineTable16[] = { 0, 783, 1447, 1891, 2047, 1891, 1447, 783,
0, -783, -1447, -1891, -2047, -1891, -1447, -783};

 
////////////////////////////////////////////////////////////////////////////////
// Below should be moved to ".h" to assist debugging which allows watch points
// regardless of module location of the breakpoint.
int BeepSineTable[16];
int ToneSineTable[16];
uint8_t Bstat;

//-----------------------------------------------------------------------------
// ISR key push values of 0-10 are used to index into the table to produce an
// unsigned integer equivalent to a function key equate (see TTS003.h).  This
// method allows for hardware changes while providing a mask for keys based on
// allowed key presses within a function.
//-----------------------------------------------------------------------------
uint16_t KeyXlate[] =
{
	K_CLR,
	K_SPR1,
	K_SPR2,
	K_SPR3,
	K_FREQ,							// 4
	K_LEVEL,
	K_CFG,
	K_SPR1,
	K_F1,								// 8
	K_F2,
	K_F3,								// Key Push Value = 10;
};

char *pSMPLX_1T	= "SMPLX 1T";
char *pSMPLX_3T	= "SMPLX 3T";
char *pTRIPLEX		= "TRI-PLEX";



int main(void)
{
	InitSysClk();
	PMIC.CTRL = PMIC_LVL_LOW | PMIC_LVL_MEDIUM |	PMIC_LVL_HIGH | PMIC_RREN_bm;
	sei();

	InitPorts();
	SysData.CorrectionFlag = ON;			// Use Meter Correction Constants.
	
	InitTCC0();									//	TCC0.CCA = 16x Speaker DAC ISR service
													// TCC0.CCB = 16x Xmit DAC ISR update
	GetSysData();								// Move EEPROM in local SRAM.
	InitTCD0();									// TCD0.OVFL = 1mSec system timer
													// TCD0.CCD = Backlight PWM
	InitTCF0();									// TCF0.CCA	= FX100 Clock.
	InitSPI_D();								// Serial LCD and Bal pot.
//	InitA2D();
	InitComb();
	InitDAC_B();
//	GetSysData();								// Move EEPROM in local SRAM.

	WarmStart();
	SetISRcount2(3000);
	while(ISRcountStatus2 == ACTIVE)
	{
		KPstat(HOTKEYS);
	}
	SystemIdle();
}


void WarmStart(void)
//-----------------------------------------------------------------------------
//	Entry point for ColdStart, First PowerUp, or Restart after PowerDown.
//
//	Indicate sign ON messages while doing a system calibration.
//-----------------------------------------------------------------------------
{
//	uint8_t Scale;
	
	FKey = 0;
	TestInfo.SysStatFlag = ENABLE;			// Check battery level in KP fetch.
	LoadSpkrLevel(OFF);

	sPWR_EN;											// Turn On System Power.
	LCD_init();										// Services InitSPI_C....
	InitDDS();
	UpDatePotentiometer(SysData.BALpot);

	LCD_OpenWindow(0,0,64,128,SBORDER);
	LCD_CtrStrg("AALogic",TB14,C_MIDDLE, STD);
	LCD_OpenWindow(0,32,31,128,NOBORDER);
	LCD_CtrStrg("www.AALogic.com",FONT6X8,C_MIDDLE, STD);
	SpkrAlert(PwrUpSound);

	#ifdef 	WDTENABLED
	wdt_enable(WDT_PER_4KCLK_gc);		// Reset on calls to GetKP.
	#endif
	DDSctl(DDS_SLEEP);						// Sleep the DDS.

}


void SystemIdle(void)
//------------------------------------------------------------------------------
//	After initialization, or completion of a function, the system will loop
//	here waiting for the next function command.  The following function commands
//	are supported within the D105 system:
//		Test()
//		AutoTest()
//		Scan()
//		Tone()
//		Talk()
//		Configure()
//		SPLFunction()
//------------------------------------------------------------------------------
{

	while(1)
	{
		//		ToneInfo.Mode = M_DISABLE;			// Silence, but allow Keypush beep.
		//		ReleaseAll();
		if(FKey == K_CLR)
		{
			FKey = 0;
			DelayMS(500);
			KPstat(HOTKEYS);
			if((FKey == K_CLR))
			{
				PowerDown();
			}
		}

		if(!(FKey & HOTKEYS))
		{
			IdleScreen();
			AllowedKeys = K_F1 | K_F2 | K_F3;
			Wait4Key(HOTKEYS | AllowedKeys);
		}
		//------------------------------------------------------------------------
		//	Clear existing window in the event we have returned due to a HOTKEY.
		//
		// No Action is allowed if operating on external battery.
		//
		if((FKey != K_CLR) && (Bstat != EXTPWR))
		{
			NewModeMessage(PRODUCTSTRING);
			LCD_OpenWindow(DATA_W, SBORDER);
			LCD_ClearWindow(NOBORDER, STD);

			switch(FKey)
			{
				case K_FREQ:
				case K_LEVEL:
				case K_CFG:
				{
					ServiceSetup();
					break;
				}
				//
				//	Freq, Level, and Cfg can be modified within each tone mode as well.
				case K_F1:
				{
					Tone();
					break;
				}
				case K_F2:
				{
					Probe();
					break;
				}
				case K_F3:
				{
					MeterFunction();
					break;
				}
			}
		}

		// Allow POWER OFF anytime.
		//
		else
		{
			DelayMS(500);
			FKey = 0;
			DelayMS(500);
			KPstat(HOTKEYS);
			if((FKey == K_CLR))
			{
				PowerDown();
			}
			FKey = K_CLR;
		}
	}
}



void IdleScreen(void)
//------------------------------------------------------------------------------
//	Initialize the display for startup.
//------------------------------------------------------------------------------
{
	uint8_t Video;

	Video = STD;

	FKey = 0;
	LCD_ClearDspl();
	NewModeMessage(PRODUCTSTRING);

	LCD_OpenWindow(DATA_W, SBORDER);
	LCD_CtrStrg("AALogic",TB14,C_MIDDLE, Video);

	WriteFunctionWindow("SEND", WF1, Video);
	WriteFunctionWindow("PROBE", WF2, Video);
	WriteFunctionWindow("METER", WF3, Video);
}


void MeterFunction(void)
//------------------------------------------------------------------------------
//	Vmeter or TDR.
//------------------------------------------------------------------------------
{
	
	WriteFunctionWindow("VOLTS", WF1, STD);
//	WriteFunctionWindow("OHMS", WF2, STD);
	DeleteFunctionWindow(WF2, STD);
	WriteFunctionWindow("TDR", WF3, STD);
//	AllowedKeys = K_CLR | K_F1 | K_F2 | K_F3;
	AllowedKeys = K_CLR | K_F1 | K_F3;
	Wait4Key(AllowedKeys);
	
	while(FKey != K_CLR)
	{
		switch(FKey)
		{
			case K_F1:
			{
				DoVDC();
				if(FKey == K_F1)
				{
					FKey = K_F2;		// Switch to Volts AC.
				}
				break;
			}
			case K_F2:
			{
//				Ohms();
				DoVAC();
				break;
			}
			case K_F3:
			{
				DoTDR();
				break;
			}
		}
	}
}


void DoVDC(void)
//------------------------------------------------------------------------------
// Check Vdc Tip-Ring, TG, and RG.
//------------------------------------------------------------------------------
{
	char WrkStrg[10];	
	
	volatile int16_t BitValue;
	volatile float Volts;
	
	uint16_t HoldOff;
	
	//	Route TR to meter section.
	//
	TestInfo.Function = F_METER;
	TestInfo.Mode = M_VDC;
	SetFunctionPath();
	NewModeMessage("DC VoltMeter");
	SetupDCMeterWindows();					// 3 Terminal Screen.
	WriteFunctionWindow("VAC", WF1, STD);

	HoldOff = 200;
	A2Dresult.Samples = SAMPLES_PER_CYCLE;
	Comb.FilterAction = CMD_BYPASS;					// 60Hz avg in VppDetector.
	TestInfo.SysStatFlag = ENABLE;
	
	AllowedKeys = K_CLR | K_F1;
	FKey = K_NONE;
	while(!(FKey & (K_CLR | K_F1)))
	{
		InitComb();
		//------------------------------------------------------------------------
		// First time slot is Tip to Ground measurement.
		//
		TestInfo.Terminal = TIP;
		GetVDC();
		//
		//------------------------------------------------------------------------
		//
		//	Second measurement is TR.
		//
		TestInfo.Terminal = TR;
		GetVDC();
		//
		//	Finally measure RG.
		//
		TestInfo.Terminal = RING;
		GetVDC();
		//
		//	Check battery level and User keypush.
		//
		KPstat(AllowedKeys);
	}
}

void GetVDC(void)
//------------------------------------------------------------------------------
//	Fetch the voltage on the active "TestInfo.Terminal" and display the results
//	in the 3 terminal window position.
//
//	On Entry Comb attributes have been set.
//------------------------------------------------------------------------------
{
	char WrkStrg[10];	
	
	volatile int16_t BitValue;
	volatile int32_t Accumulator;
	volatile float Volts;
	
	uint16_t HoldOff;

	HoldOff = 68;										// 17mS * 4
		
	SelectTerminal();
	SetADCDiffGain(ACDC, ADC_G1);
	SetISRcount2(HoldOff);
	while(ISRcountStatus2 == ACTIVE);
	BitValue = PeakDetector();						// Average over final 17mS.
	BitValue -= 2048;
	BitValue -= SysData.Voffset[TestInfo.Terminal];
	//
	//	Adjust the "BIT" value according to calibrated ratio xxxx/1024.
	//
	Accumulator = (int32_t)(BitValue) * SysData.VDCgain[TestInfo.Terminal];
	Accumulator /= 1024; 
	BitValue = (int16_t)(Accumulator);
	Volts = (ADCVREF *(float)(BitValue))/(DCTR_FEGAIN *2048);
	//
	//	If Volts is low and Calibration flag is not on, Zero the reading.
	if((fabs(Volts) < 2.1)  && (TestInfo.Cflag == OFF))
	{
		Volts = 0.0;
	}
	sprintf(WrkStrg, "%2.1f VDC" , Volts);
	PutMeter(&WrkStrg[0]);
}


void GetVAC(void)
//------------------------------------------------------------------------------
//	Fetch the voltage on the active "TestInfo.Terminal" and display the results
//	in the 3 terminal window position.
//
//	On Entry Comb attributes have been set.
//------------------------------------------------------------------------------
{
	char WrkStrg[10];
		
	volatile int16_t BitValue;
	volatile int32_t Accumulator;
	volatile float Volts;
		
	uint16_t HoldOff;

	HoldOff = 68;										// 17mS * 4
	SelectTerminal();

	SetADCDiffGain(ACDC, ADC_G1);
	SetISRcount2(HoldOff);
	while(ISRcountStatus2 == ACTIVE);
	BitValue = PeakDetector();
	BitValue = A2Dresult.Vpp;
	//
	//	Adjust the "BIT" value according to calibrated ratio xxxx/1024.
	//
	Accumulator = (int32_t)(BitValue) * SysData.VACgain[TestInfo.Terminal];
	Accumulator /= 1024;
	BitValue = (int16_t)(Accumulator);

	Volts = ((ADCVREF *(float)(BitValue))/(ACTR_FEGAIN * 4096));
	sprintf(WrkStrg, "%2.1f VDC" , Volts);
	PutMeter(&WrkStrg[0]);
			
}

void DoVAC(void)
//------------------------------------------------------------------------------
// Check Vac Tip-Ring, TG, and RG.
//------------------------------------------------------------------------------
{
	char WrkStrg[18];
	uint16_t VacBits;
	uint8_t RepeatQty =4;
	
	volatile float Volts, db, AvgVolts;
	
	//	Route TR to meter section.
	//
//	TestInfo.Function = F_METER;
//	TestInfo.Mode = M_VAC;
//	SetFunctionPath();

	SetupDCMeterWindows();					// 3 Terminal Screen.
	if(SysData.MeterType == NOISE)
	{
		NewModeMessage("db NoiseMeter");
		WriteFunctionWindow("VAC", WF3, STD);
	}
	else
	{
		NewModeMessage("AC VoltMeter");
		WriteFunctionWindow("DB", WF3, STD);
	}
	WriteFunctionWindow("VDC", WF1, STD);
	
	//	The HoldOff is the alloted time for Comb filter to settle after switching
	//	terminal input; however, the Comb setup is merely used to place voltage
	//	samples into a buffer for evaluation. 
	// 
	A2Dresult.Samples = SAMPLES_PER_CYCLE;
	InitComb();
	Comb.FilterAction = CMD_BYPASS;
	AllowedKeys = K_CLR | K_F1 | K_F3;
	FKey = K_NONE;
	
	TestInfo.SysStatFlag = DISABLE;
	//
	//	Route the line input to the FE amplifier.
	//
	TestInfo.Function = F_METER;
	TestInfo.Mode = M_VAC;
	SetFunctionPath();
	
	sTONE;									// Terminate the line with Tone Drivers.
	sE2S;										// Ground Reference for TG/RG measurements.
	
	while(!(FKey & (K_CLR | K_F1)))	// F1 exits for VDC...
	{
		//------------------------------------------------------------------------
		// First time slot is Tip to Ground measurement. The path uses ACV_T ADC
		//	input when ACDC is greater than half ADCVREF.
		//
		TestInfo.Terminal = TIP;
		SelectTerminal();

		AvgVolts = 0;
		for(Loop = 0; Loop < RepeatQty; Loop++)
		{
			Volts = AutoVAC(ACDC);
			if(Volts > ADCVREF/2)
			{
				SetADCDiffGain(ACV_T, ADC_G1);
				SetISRcount2(200);
				while(ISRcountStatus2 == ACTIVE);
				PeakDetector();
				VacBits = A2Dresult.Vpp;
				Volts = ((ADCVREF * (float)(VacBits))/(ACT_FEGAIN *2047));
			}
			AvgVolts += Volts;
		}
		Volts = AvgVolts/RepeatQty;
		Volts2db(Volts);
		KPstat(AllowedKeys);
		if(!FKey)
		{
			//
			//------------------------------------------------------------------------
			//
			// Second time slot is Ring to Ground measurement. The path uses ACV_R ADC
			//	input when ACDC is greater than half ADCVREF.
			//
			TestInfo.Terminal = RING;
			SelectTerminal();
		
			AvgVolts = 0;
			for(Loop = 0; Loop < RepeatQty; Loop++)
			{
				Volts = AutoVAC(ACDC);
				if(Volts > ADCVREF/2)
				{
					SetADCDiffGain(ACV_R, ADC_G1);
					SetISRcount2(54);
					while(ISRcountStatus2 == ACTIVE);
					VppDetector();
					VacBits = A2Dresult.Vpp;
					Volts = ((ADCVREF * (float)(VacBits))/(ACT_FEGAIN *2047));
				}
				AvgVolts += Volts;
			}
			Volts = AvgVolts/RepeatQty;
			Volts2db(Volts);
			KPstat(AllowedKeys);		
		}
		if(!FKey)
		{
			//
			//Tip-Ring measurement.
			//
			TestInfo.Terminal = TR;
			SelectTerminal();
			AvgVolts = 0;
			for(Loop = 0; Loop < RepeatQty; Loop++)
			{
				AvgVolts += AutoVAC(ACDC);
			}
			Volts = AvgVolts/RepeatQty;
			if(Volts > ADCVREF/2)
			//
			// If level above TR VAC limit, switch to VDC path for Vpp measurement.
			{
				sVDC;
				SetISRcount2(54);
				while(ISRcountStatus2 == ACTIVE);
				VppDetector();
				VacBits = A2Dresult.Vpp;
				Volts = ((ADCVREF * (float)(VacBits))/(DCTR_FEGAIN *2047));
			}
			sVAC;
			Volts2db(Volts);
		}
		KPstat(AllowedKeys);
		if(FKey == K_F3)
		{
			SetupDCMeterWindows();					// 3 Terminal Screen.
			switch(SysData.MeterType)
			{
				case ACMETER:
				{
					SysData.MeterType = NOISE;
					NewModeMessage("db NoiseMeter");
					WriteFunctionWindow("VAC", WF3, STD);
					break;
				}
				case NOISE:
				{
					SysData.MeterType = ACMETER;
					NewModeMessage("AC VoltMeter");
					WriteFunctionWindow("DB", WF3, STD);
					break;
				}
			}
			WriteFunctionWindow("VDC", WF1, STD);
			SetParity();
			FKey = K_NONE;
		}
	}
}


void Ohms(void)
//------------------------------------------------------------------------------
// OhmMeter
//
//	The TONE DC output will be applied to the unknown resistance.  The first
//	step will be to determine the series current by applying only the non-inverting
//	let of U15.  Current is computed as:
//					I = (Vapplied - Vmeasured)/ R84
//
//	The second step is to measure the differential between TR.
//
//	Due to hardware limitations only the resistance between Tip and Ring will be
//	measured.
//------------------------------------------------------------------------------
{
	volatile float V_TipRing, V_Tip, I_Rx, V_Drop, Rx;
	char WrkStrg[20];

	//	Route TR to meter section.
	//
	TestInfo.Function = F_TONE;			// Apply Tone V_source to line.
	TestInfo.Mode = M_VDC;
	SetFunctionPath();
	SetupDCMeterWindows();					// 3 Terminal Screen.
	NewModeMessage("OhmMeter");
	
	//	The HoldOff is the alloted time for Comb filter to settle after switching
	//	terminal input; however, the Comb setup is merely used to place voltage
	//	samples into a buffer for evaluation.
	//
	A2Dresult.Samples = SAMPLES_PER_CYCLE;
	InitComb();
	Comb.FilterAction = CMD_BYPASS;
	AllowedKeys = K_CLR | K_F1;
	FKey = K_NONE;
	while(!FKey)
	{
		rE2S;										// Uses +/- supplies.
		TestInfo.Terminal = TR;
		SelectTerminal();
		V_TipRing = AutoVDC(ACDC);
		
		TestInfo.Terminal = TIP;			// Tip Source V_drop.
		SelectTerminal();
		V_Tip = AutoVDC(ACDC);
		V_Drop = 13.0 - V_Tip;
		I_Rx = V_Drop/R35;
		
		Rx = V_TipRing/I_Rx;
		sprintf(WrkStrg, " %2.1e", Rx);
		PutMeter(WrkStrg);		
	}

}		





void ServiceSetup(void)
//------------------------------------------------------------------------------
// Service FREQ/LEVEL/CFG based on FKey value.
//------------------------------------------------------------------------------
{
	uint8_t Index;
	
	NewWindow(DATA_W, SBORDER);		// Clear the main screen.
	switch(FKey)
	{
		case K_FREQ:
		{
			Index = SelectItem(FREQSEL);
			if(FKey == K_SET)
			{
				SysData.AxIndex = Index;
				SysData.Frequency = AxData[Index].Fbase;
				SetParity();
			}
			break;
		}
		case K_LEVEL:
		{
			Index = SelectItem(LEVELSEL);
			if(FKey == K_SET)
			{
				SysData.LevelIndex = Index;
				SysData.ToneLevel = LvlData[Index].Fbase;
				LoadXmitLevel();
				SetParity();
			}
			break;
		}
		case K_CFG:
		{
			//Special entry into Calibrate by pressing CFG, then LEVEL within 750mS.
			//
			SetISRcount2(750);
			while(ISRcountStatus2 == ACTIVE)
			{
				KPstat(K_LEVEL);
			}
			if(FKey == K_LEVEL)
			{
				Calibrate();
			}
			else
			{
				Config();						// System Setup
			}
			break;
		}
	}
	if(FKey != K_CLR)		FKey = K_NONE;
}


void Wait4Key(uint16_t Mask)
//------------------------------------------------------------------------------
// Wait for a keypush.  Translate the binary keypush value to a bit assigned
// value.  If the value is contained within the mask return with value placed
// in the global register "FKey".
//
// Keypush value will be 1 to 16 relating to bit position d15 -> d0 as
// referenced in the keypush translation array "KeyXlate".
//-----------------------------------------------------------------------------
{
	uint16_t Function;

	FKey = 0;						// Zero the last keypress translation.
	KyReg = 0;
	while(1)
	{
		//--------------------------------------------------------------------------
		// While waiting for a keypress update the low battery indicator.
		//
		while(!KyReg)
		{
			#ifdef WDTENABLED
			// Reset the WatchDogTimer before timeout as setup in WarmStart.
			//
			wdt_reset();
			#endif

			if(TestInfo.SysStatFlag == ENABLE)
			{
				CheckBatteryVoltage();
			}
		}
		//
		//------------------------------------------------------------------------
		// A KeyPress has been detected, sound beeper if key is valid, then return
		// to the calling program.
		//
		RTC.CNT = 0;					// Restart the RTCCountdown till shutdown..
		if(FKey == K_CLR)				// System coming out of AutoShutdown...
		{
			KyReg = 0;
			return;
		}
		else
		{
			Function = GetKp(Mask);

			if(Function)
			{
				FKey = Function;
				return;
			}
		}
	}
}



uint16_t GetKp(uint16_t ValidKeys)
//-------------------------------------------------------------------------------
// Sound the Beeper and pickup the local keypush.  The Keypush will be convert
// an equivalent function key.  If key is invalid, return void.
//-------------------------------------------------------------------------------
{
	unsigned long FunctionKey;

	// Convert the binary key value to bit assigned key value.
	//
	FunctionKey = KyReg;
	FunctionKey &= ValidKeys;

	if(FunctionKey & ValidKeys)
	{
		AcknowledgeSound(KPsound);    // Valid keypush tone.
	}

	else
	{
		FunctionKey = 0;
		AcknowledgeSound(KPerror);    // InValid keypush tone.
	}

	KyReg = 0;
	return(FunctionKey);
}


void KPstat(uint16_t Mask)
//-----------------------------------------------------------------------------
//	Get the KeyPush if one is present.  Unlike "Wait4Key()" this function
// returns if the "Keys" regster is not active.  The "Mask" has the bit values
// that constitute a valid keypush.
//
// NOTE:	Do NOT call this function during critical timing functions.
//
// Uses Global "KyReg" for Return Value:
//       	KyReg   = any allowed keypush value (defined in "system.h").
//				KyReg = void, no active keys
//          KyReg   = void, non-allowed keypush as defined by "Mask".
//
// In addition to checking the "Keys" register check USB service flags to
// allow abort of nested functions such as "Wait4DTMFtrigger()".
//-----------------------------------------------------------------------------
{
	#ifdef WDTENABLED
	// Reset the WatchDogTimer before timeout as setup in WarmStart.
	//
	wdt_reset();
	#endif


	if(TestInfo.SysStatFlag == ENABLE)
	{
		CheckBatteryVoltage();
	}
	
	if(KyReg)
	{
		RTC.CNT = 0;					// Restart TimeOut RTC.
		FKey = GetKp(Mask);
	}
}


void BIupdate(void)
//-----------------------------------------------------------------------------
//	Update the battery indicator status windows without getting Battery Voltage.
//-----------------------------------------------------------------------------
{
	if(PreviousBstat != Bstat)
	{
		SwapWindow();

		if(Bstat == EXTPWR)
		//
		//	Erase battery indicators.
		{
			LCD_OpenWindow(BAT_W, SBORDER);
			LCD_ClearWindow(SBORDER, STD);
			LCD_OpenWindow(BBUTTON_W, SBORDER);
			LCD_ClearWindow(SBORDER, STD);
			LCD_OpenWindow(BATLVL1_W, NOBORDER);
			LCD_ClearWindow(SBORDER, STD);
			LCD_OpenWindow(BATLVL2_W, NOBORDER);
			LCD_ClearWindow(SBORDER, STD);
			LCD_OpenWindow(BATLVL3_W, NOBORDER);
		}
		else
		{
			LCD_OpenWindow(BAT_W, SBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			LCD_OpenWindow(BBUTTON_W, SBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			if((Bstat == BEMPTY) || (Bstat == BHALF) || (Bstat == BFULL))
			{
				LCD_OpenWindow(BATLVL1_W, BORDER);
				LCD_ClearWindow(SBORDER, REV);
			}
			if((Bstat == BHALF) || (Bstat == BFULL))
			{
				LCD_OpenWindow(BATLVL2_W, BORDER);
				LCD_ClearWindow(SBORDER, REV);
			}
			if(Bstat == BFULL)
			{
				LCD_OpenWindow(BATLVL3_W, BORDER);
				LCD_ClearWindow(SBORDER, REV);
			}
		}
		PreviousBstat = Bstat;
		SwapWindow();
	}
}


void SwapWindow(void)
//------------------------------------------------------------------------------
//	This function swaps the active window parameters with a fixed temp structure
//	working much like a PUSH/POP function.  Called once is a PUSH, second call
//	POPS, or restores the original window variables.
//------------------------------------------------------------------------------
{
	static uint8_t Stack = 0;

	if(!Stack)
	//	SAVE
	{
		SWindow.p = Window.p;
		SWindow.x = Window.x;
		SWindow.y = Window.y;
		SWindow.h = Window.h;
		SWindow.w = Window.w;
		SWindow.Xposition = Window.Xposition;
		SWindow.Yposition = Window.Yposition;
		Stack = 1;
	}

	else
	//	RESTORE
	{
		Window.p = SWindow.p;
		Window.x = SWindow.x;
		Window.y = SWindow.y;
		Window.h = SWindow.h;
		Window.w = SWindow.w;
		Window.p = SWindow.p;
		Window.Xposition = SWindow.Xposition;
		Window.Yposition = SWindow.Yposition;
		Stack = 0;
	}
}


void SpkrAlert(const uint16_t *pSoundArray)
//------------------------------------------------------------------------------
//	Enable speaker amplifier and make noise.
//
//	On Exit:	Disable speaker if Not required by calling program.
//------------------------------------------------------------------------------
{
	sSPKR_EN;									// Data sheet indicates 9mS

	InitBeepSequence(pSoundArray);
	while(ToneInfo.BeepStringFlag);		// Wait for the last beep to finish.
	if(ToneInfo.SpkrEnable)
	{
		sSPKR_EN;
	}
}

void InitBeepSequence(uint16_t *pSoundArray)
//-----------------------------------------------------------------------------
// A string of "beep" sounds are to be played.  Initialize the sequence.  At the
// end of each tone a low level interrupt is enabled to service the new timer
// values and advance the "pSound" pointer.  This process is used to allow normal
// program execution with minimum system impact.
//-----------------------------------------------------------------------------
{
	while(ToneInfo.BeepStringFlag);		// Wait for the last beep to finish.

	ToneInfo.pSound = pSoundArray;		// Advanced via low level isr.
	Beeper();
	TCC0.INTFLAGS = TC0_CCAIF_bm;			// Reset ISR flag.
	ToneInfo.BeepStringFlag = ON;			// Allow ISR 1ms to take control.
	TCC0.INTCTRLB |= TC_CCAINTLVL_HI_gc;
}


void Beeper(void)
//-----------------------------------------------------------------------------
//	A request has been made to sound the beeper with "freq" for "duration".
//
//	Frequency of the tone is determine by 16 CycleSteps of TCC1 clocked at 2MHz.
//
//	The "duration" time in milliseconds is controlled by system ISR TCC4_OVF_vect.
//	Upon completion of time in milliseconds the tone is turned off when the
//	SineIndex becomes ZERO.
//-----------------------------------------------------------------------------
{
	uint16_t GenFreq;

	GenFreq = *ToneInfo.pSound;
	ToneInfo.BeepDuration = *(ToneInfo.pSound+1);
	if(GenFreq == 0)
	//
	// Silent duration. Load Sine buffer with MID-SCALE, but keep servicing the
	//	DAC ISR.
	{
		LoadSpkrLevel(OFF);					//	Turn off tone.
		TCC0.INTCTRLB |= TC_CCAINTLVL_HI_gc;
	}
	else if((GenFreq > 100) && (GenFreq < 10000))
	{
		SetSineFreq(GenFreq);
		LoadSpkrLevel(ON);					//	Turn ON tone.
		TCC0.INTCTRLB |= TC_CCAINTLVL_HI_gc;
	}
	else
	{
		LoadSpkrLevel(OFF);	//	Turn off tone.
	}
}



void LoadSpkrLevel(uint8_t Status)
//-----------------------------------------------------------------------------
//	Using the basic 16 entry SineTable and User selected amplitude compute the
//	16 steps used during ISR service for the Speaker Tones.
//-----------------------------------------------------------------------------
{
	uint8_t Index;
	long Accumulator;

//	while(SpkrTone.SineIndex);
	for(Index = 0; Index < 16; Index++)
	{
		if(SpkrTone.Flag.Bit.Enabled)
		{
			if(Status == ON)
			{
				Accumulator = SineTable16[Index] *(long)(SysData.BeeperLevel);
				SpkrSineTable[Index] = (int16_t)(Accumulator/100) +SINE_MIDSCALE;
			}
			else
			{
				SpkrSineTable[Index] = SINE_MIDSCALE;
			}
		}
	}
}


void LoadXmitLevel(void)
//------------------------------------------------------------------------------
//	Using the Transmit Level Selection in SysData update the XmitSineTable[];
//------------------------------------------------------------------------------
{
	uint8_t Index;
	long Accumulator, ToneLevel;

	ToneLevel = (long)(LvlData[SysData.LevelIndex].Fbase);
	for(Index = 0; Index < 16; Index++)
	{
		Accumulator = SineTable16[Index] *ToneLevel;
		XmitSineTable[Index] = (int16_t)(Accumulator/100) +SINE_MIDSCALE;
	}
}



void SetSineFreq(uint16_t Freq)
//-----------------------------------------------------------------------------
//	On entry the TCD5 pre-scaler has been selected.  Compute the integer needed
//	to generate an ISR 16 times per SineCycle.
//
//	PER register sets the frequency while CCA and CCB is loaded with values below
//	PER so as to generate separate ISR's for each yet maintain timing of 16x.
//
//	NOTE: While OVFL can generate an ISR as well, only CCA and CCB are used to
//			service DAC outputs 0/1.
//
//			Interrupt levels must be set to enable ISR flags.
//-----------------------------------------------------------------------------
{
	volatile float CycleTime, ClockTime, PreScaler;
//	uint16_t Divisor;
	
	CycleTime = (1/(float)(Freq)) / 16.0;		// uSec per desired ISR
	ClockTime = 1/CLKper;							// Timer Input clock.
	PreScaler = GetTC0PreScaler(&TCC0);

	ClockTime *= PreScaler;
	TCC0.CNT = 0;
	TCC0.PERBUF = (uint16_t)(CycleTime/ClockTime);	// uSec whole number.
//	TCC0.PER = TCC0.PERBUF;
//	TCC0.CCABUF = TCC0.CCA = TCC0.PERBUF/4;
//	TCC0.CCBBUF = TCC0.CCB = TCC0.PERBUF/2;
//	TCC0.PER = TCC0.PERBUF;
	TCC0.CCABUF = TCC0.PERBUF/4;
	TCC0.CCBBUF = TCC0.PERBUF/2;
}


uint16_t GetTC0PreScaler(void *tc)
//-----------------------------------------------------------------------------
//	Get the latest loaded PreScale value for the counter "tc" type 0.
//-----------------------------------------------------------------------------
{
	uint16_t PreScaler, Divisor;
	
	Divisor = (((TC0_t *)tc)->CTRLA) & TC0_CLKSEL_gm;	// Timer Clock Divisor value.
	switch(Divisor)
	{
		case TC_CLKSEL_DIV1_gc:
		{
			PreScaler = 1;
			break;
		}
		case TC_CLKSEL_DIV2_gc:
		{
			PreScaler = 2;
			break;
		}
		case TC_CLKSEL_DIV4_gc:
		{
			PreScaler = 4;
			break;
		}
		case TC_CLKSEL_DIV8_gc:
		{
			PreScaler = 8;
			break;
		}
		case TC_CLKSEL_DIV64_gc:
		{
			PreScaler = 64;
			break;
		}
		case TC_CLKSEL_DIV256_gc:
		{
			PreScaler = 256;
			break;
		}
		case TC_CLKSEL_DIV1024_gc:
		{
			PreScaler = 1024;
			break;
		}
	}
	return(PreScaler);
}


void DelayMS(unsigned int DelayTime)
//-----------------------------------------------------------------------------
//	Using the variables updated by the 1mS ISR set the desired time and wait for
//	the variable to decrement to ZERO.
//
//	Reset the WDT every 16ms interval.
//-----------------------------------------------------------------------------
{
	int16_t TripPoint;

	ISRcount1 = DelayTime;
	TripPoint = DelayTime -16;
	ISRcountStatus1 = ACTIVE;
	while(ISRcountStatus1 == ACTIVE)
	{
		if(ISRcount1 <= TripPoint)				// Reset WDT every 16mSec
		{
			TripPoint -= 16;
			#ifdef WDTENABLED
			// Reset the WatchDogTimer before timeout as setup in WarmStart.
			//
			wdt_reset();
			#endif
		}
	}
}


void SoftDelay(void)
//------------------------------------------------------------------------------
//	Delay for LCD Read/Write
//------------------------------------------------------------------------------
{
	volatile uint16_t TimeLoop;
	
	for(TimeLoop = 10; TimeLoop; TimeLoop--);
}


void SetISRcount2(unsigned int Time)
//------------------------------------------------------------------------------
//	Load the MilliSecond Counter with "Time", then return to the calling program.
//	ISRcountStatus2 will "COMPLETE" once count reaches Zero.
//------------------------------------------------------------------------------
{
	ISRcount2 = Time;
	ISRcountStatus2 = ACTIVE;
}


void AcknowledgeSound(const uint16_t *pSound)
//------------------------------------------------------------------------------
//	Momentarily stop any tone transmission and send a local AcknowledgeSound.
//------------------------------------------------------------------------------
{
	SpkrAlert(pSound);
	SetSineFreq(SysData.Frequency);		// Exit with XmitTone Freq Active.
}



void SetParity(void)
//------------------------------------------------------------------------------
//	Compute the new Checksum for the changed CAL settings.
//------------------------------------------------------------------------------
{
	uint16_t Uwork;

	Uwork = SysData.BALpot;
	Uwork += SysData.BeeperLevel + SysData.ToneLevel +SysData.MonitorLevel;
	Uwork += SysData.VDCgain[0] +SysData.VDCgain[1] +SysData.VDCgain[2];
	Uwork += SysData.MeterType +CKSUMOFFSET;
	SysData.CheckSum = Uwork;
	eeprom_write_block(&SysData, EECAL, sizeof SysData);	// *scr, *dst, qty

	SpkrAlert(&OKSound[0][0]);
	SysData.CorrectionFlag = ON;			// Use Meter Correction Constants.
}




void GetSysData(void)
//------------------------------------------------------------------------------
//	Move EEPROM Calibration factors into the local SRAM SysData structure.
//------------------------------------------------------------------------------
{
	uint16_t CheckSum, DataWords;

	DataWords = sizeof SysData;
	eeprom_read_block(&SysData, EECAL, DataWords);		// *dst, *scr, qty

	CheckSum = SysData.BALpot;
	CheckSum += SysData.BeeperLevel +SysData.ToneLevel +SysData.MonitorLevel;
	CheckSum += SysData.VDCgain[0] +SysData.VDCgain[1] +SysData.VDCgain[2];
	CheckSum += SysData.MeterType + CKSUMOFFSET;
	if(SysData.CheckSum != CheckSum)
	//
	//	Load Default Settings.
	{
		SysData.VoP = 68;
		SysData.BeeperLevel = MIDBEEP;
		SysData.MonitorLevel = LOW_VOL;
		SysData.ToneLevel = MAXMETALLIC;
		for(Loop = 0; Loop < 3; Loop++ )		// Default unity Gain.
		{
			SysData.VDCgain[Loop] = 1024;
			SysData.VACgain[Loop] = 1024;
		}
		SysData.Voffset[0] = -10;				// TR Offset
		SysData.Voffset[1] = -47;				// Ring Offset.
		SysData.Voffset[2] = 28;				//	Tip Offset.
		
		SysData.BALpot = 117;
		SysData.BackLight = LCDBKLT_M;
		SysData.Contrast = LCDCONTRAST_M;
		SysData.Frequency = 577;
		SysData.LevelIndex = 1;					// Medium Level
		SysData.AxIndex = 1;						// Frq = 577Hz
		SysData.MeterType = NOISE;
		SetParity();
	}
}




void UpDatePotentiometer(uint16_t Balance)
//-----------------------------------------------------------------------------
//	Send the byte Qty placed in SPIbuffer to the potentiometer Port.  The first byte
//	will be loaded into the SPI xmit buffer, and interrupts enabled to complete
//	the total byte transmission.
//-----------------------------------------------------------------------------
{
	uint8_t ByteQty;
	
	ByteQty = 2;
	SPIinfo.Buffer[0] = 0x00;
	sBAL_CS;
	SPIinfo.Buffer[1] = (uint8_t)Balance;
	SendSPI(ByteQty);
	rBAL_CS;
}


//==============================================================================
//	System Configuration includes:
//			1-	Setting VoP
//==============================================================================
void Config(void)
//------------------------------------------------------------------------------
//	HOTKEY Entry K_CFG.		The following CFG items can be modified.
//		K_F1:	VoP
//------------------------------------------------------------------------------
{
	uint8_t Video;
	uint16_t LocalAllowedKeys;

	NewModeMessage("Config System");
	LCD_OpenWindow(DATA_W, SBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	Video = STD;
	NewWindow(DATA_W, SBORDER);
	WriteFunctionWindow("BCKLTE", WF1, Video);
	WriteFunctionWindow("CONTRST", WF2, Video);
	WriteFunctionWindow("VOLUME", WF3, Video);

	LocalAllowedKeys = K_F1 | K_F2 | K_F3;
	Wait4Key(HOTKEYS | LocalAllowedKeys);
	switch(FKey)
	{
		case K_F1:
		{
			SetBackLight();
			break;
		}
		case K_F2:
		{
			SetContrast();
			break;
		}
		case K_F3:
		{
			SetVolume();
			break;
		}
	}
}

void SetSound(void)
//------------------------------------------------------------------------------
//	Two options, set volume level or monitor on/off.
//------------------------------------------------------------------------------
{
	NewModeMessage("Spkr Volume");
	DeleteFunctionWindow(WF1, STD);
	WriteFunctionWindow("KEYPUSH", WF2, STD);
//	WriteFunctionWindow("PROBE", WF3, STD);
	DeleteFunctionWindow(WF3, STD);
	Wait4Key(K_F2 | K_CLR);
	switch(FKey)
	{
		case K_F2:
		{
			SetVolume();
			break;
		}
		case K_F3:
		{
			SetMonitor();
			break;
		}
	}

}

void SetMonitor(void)
//------------------------------------------------------------------------------
//	Allow user to turn ON or OFF received tone to Speaker.
//------------------------------------------------------------------------------
{
	char WrkStrg[20];
	uint16_t Temp, LocalAllowedKeys;

	NewModeMessage("Probe Volume");
	NewWindow(DATA_W, SBORDER);
	FunctionSetUpWindow();
	
	// Get the EEPROM value for LCD Volume.
	//
	// Get the EEPROM value for LCD Volume.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("VOLUME: ", TB8, C_RIGHT, STD);

	LCD_OpenWindow(CFG_D_W, NOBORDER);
	sprintf(&WrkStrg[0], "%d", SysData.MonitorLevel+1);
	LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);

	Temp = SysData.MonitorLevel;
	LocalAllowedKeys = K_UP | K_DOWN | K_SET;
	while(!(FKey & (K_CLR | K_SET)))
	{
		LoadSpkrLevel(ON);
		Wait4Key(HOTKEYS | LocalAllowedKeys);
		{
			switch(FKey)
			{
				case K_DOWN:
				{
					if(SysData.MonitorLevel > MONITOR_OFF)
					{
						SysData.MonitorLevel -= 1;
					}
					break;
				}
				case K_SET:
				{
					SetParity();
					break;
				}
				case K_UP:
				{
					if(SysData.MonitorLevel < MAX_VOL)
					{
						SysData.MonitorLevel += 1;
					}
					break;
				}
			}
			LCD_OpenWindow(CFG_D_W, NOBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			sprintf(&WrkStrg[0], "%d", SysData.MonitorLevel +1);
			LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);
		}
	}
	if(FKey == K_CLR)
	{
		SysData.MonitorLevel = Temp;
	}
}


void SetVolume(void)
//------------------------------------------------------------------------------
//	Change BeeperVolume.
//------------------------------------------------------------------------------
{
	char WrkStrg[20];
	uint16_t Temp, LocalAllowedKeys;

	NewModeMessage("KeyPush Volume");
	FunctionSetUpWindow();

	// Get the EEPROM value for LCD Volume.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("VOLUME: ", TB8, C_RIGHT, STD);

	LCD_OpenWindow(CFG_D_W, NOBORDER);
	sprintf(&WrkStrg[0], "%d", SysData.BeeperLevel/10);
	LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);

	FKey = 0;
	Temp = SysData.BeeperLevel;
	LocalAllowedKeys = K_UP | K_DOWN | K_SET;
	while(!(FKey & (K_CLR | K_SET)))
	{
		LoadSpkrLevel(ON);
		Wait4Key(HOTKEYS | LocalAllowedKeys);
		{
			switch(FKey)
			{
				case K_DOWN:
				{
					if(SysData.BeeperLevel > SPKR_LVL_L)
					{
						SysData.BeeperLevel -= 10;
					}
					break;
				}
				case K_SET:
				{
					SetParity();
					break;
				}
				case K_UP:
				{
					if(SysData.BeeperLevel < SPKR_LVL_H)
					{
						SysData.BeeperLevel += 10;
					}
					break;
				}
			}
			LCD_OpenWindow(CFG_D_W, NOBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			sprintf(&WrkStrg[0], "%d", SysData.BeeperLevel/10);
			LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);
		}
	}
	if(FKey == K_CLR)
	{
		SysData.BeeperLevel = Temp;
	}
}




void SetBackLight(void)
//------------------------------------------------------------------------------
//	 Allow the user to adjust the backlight, then save in SysData structure.
//------------------------------------------------------------------------------
{
	uint8_t Temp;
	char WrkStrg[20];
	uint16_t LocalAllowedKeys;

	NewModeMessage("BackLight");
	FunctionSetUpWindow();

	// Get the EEPROM value for LCD Backlight.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("BackLight: ", TB8, C_LEFT, STD);

	LCD_OpenWindow(CFG_D_W, NOBORDER);
	sprintf(&WrkStrg[0], "%d", SysData.BackLight/10);
	LCD_CtrStrg(&WrkStrg[0], TB8, C_MIDDLE, STD);

	FKey = 0;
	Temp = SysData.BackLight;
	LocalAllowedKeys = K_UP | K_DOWN | K_SET;
	while(!(FKey & (K_CLR | K_SET)))
	{
		Wait4Key(K_CLR | LocalAllowedKeys);
		{
			switch(FKey)
			{
				case K_DOWN:
				{
					if(SysData.BackLight > LCDBKLT_L)
					{
						SysData.BackLight -= 10;
					}
					break;
				}
				case K_UP:
				{
					if(SysData.BackLight < LCDBKLT_H)
					{
						SysData.BackLight +=10;
					}
					break;
				}
				case K_SET:
				{
					SetParity();
					break;
				}
			}
			BackLightDutyCycle(SysData.BackLight);
			LCD_OpenWindow(CFG_D_W, NOBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			sprintf(&WrkStrg[0], "%d", SysData.BackLight/10);
			LCD_CtrStrg(&WrkStrg[0], TB8, C_MIDDLE, STD);
		}
	}
	if(FKey == K_CLR)
	//	Exit without saving the new value.
	{
		SysData.BackLight = Temp;					// Original value.
		BackLightDutyCycle(SysData.BackLight);
	}
	
	
	FKey = K_NONE;		//	No further action.
}

void SetContrast(void)
//------------------------------------------------------------------------------
//	 Allow the user to adjust the Contrast, then save in SysData structure.
//------------------------------------------------------------------------------
{
	char WrkStrg[20];
	uint8_t Temp;

	NewModeMessage("Contrast");
	FunctionSetUpWindow();

	// Get the EEPROM value for LCD Contrast.
	//
	LCD_OpenWindow(CFG_T_W, NOBORDER);
	LCD_ClearWindow(NOBORDER, STD);
	LCD_CtrStrg("CNTRST: ", TB8, C_RIGHT, STD);

	LCD_OpenWindow(CFG_D_W, NOBORDER);
	sprintf(&WrkStrg[0], "%d", SysData.Contrast);
	LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);

	FKey = 0;
	Temp = SysData.Contrast;
	AllowedKeys = K_UP | K_DOWN | K_SET;
	while(!(FKey & (K_CLR | K_SET)))
	{
		Wait4Key(K_CLR | AllowedKeys);
		{
			switch(FKey)
			{
				case K_DOWN:
				{
					if(SysData.Contrast > LCDCONTRAST_L)	SysData.Contrast--;
					break;
				}
				case K_SET:
				{
					SetParity();
					break;
				}
				case K_UP:
				{
					if(SysData.Contrast < LCDCONTRAST_H)	SysData.Contrast++;
					break;
				}
			}
			LCD_CmdWrite(LCD_CMD_ELECTRONIC_VOLUME_MODE_SET);	//	Contrast CMD
			LCD_CmdWrite(SysData.Contrast);							// Contrast Value

			LCD_OpenWindow(CFG_D_W, NOBORDER);
			LCD_ClearWindow(NOBORDER, STD);
			sprintf(&WrkStrg[0], "%d", SysData.Contrast);
			LCD_CtrStrg(&WrkStrg[0], TB8, C_LEFT, STD);
		}
	}
	if(FKey == K_CLR)
	//	Exit without saving the new value.
	{
		SysData.Contrast = Temp;
		LCD_CmdWrite(LCD_CMD_ELECTRONIC_VOLUME_MODE_SET);	//	Contrast CMD
		LCD_CmdWrite(SysData.Contrast);							// Contrast Value
	}
}



struct SPI_information volatile SPIinfo;

void SendSPI(char ByteQty)
//-----------------------------------------------------------------------------
//	Send the byte Qty placed in SPIbuffer.  The first byte will be loaded into
//	the SPI xmit buffer, and interrupts enabled to complete the total byte
//	transmission.
//-----------------------------------------------------------------------------
{
	uint8_t Reg;
	
	while(SPIinfo.Status != COMPLETE);
	Reg = SPID_STATUS;						// Clear IF flag.
	Reg = SPID_DATA;
	SPIinfo.Status = ACTIVE;
	SPIinfo.ByteCnt = ByteQty;
	SPIinfo.XmitCnt = 1;
	SPID_INTCTRL = SPI_INTLVL_HI_gc;		// Enable Low Level Interrupts
	SPID_DATA = SPIinfo.Buffer[0];		// Send first byte.
	while(SPIinfo.Status == ACTIVE);		// Wait for transmission to be complete.
}

void SetSPI_Mode2(void)
//-----------------------------------------------------------------------------
//	Mode2, CLK idles HIGH, Data Sampled on Falling Edge.
//-----------------------------------------------------------------------------
{
	SPID_CTRL &= (~SPI_MODE_gm);
	SPID_CTRL |= (0x02 << SPI_MODE_gp);
}

void SetSPI_Mode3(void)
//-----------------------------------------------------------------------------
//	Mode3, CLK idles HIGH, Data Sampled on Rising Edge.
//-----------------------------------------------------------------------------

{
	SPID_INTCTRL &= ~SPI_INTLVL_gm;				// Disable Interrupts.
	SPID_CTRL &= (~SPI_MODE_gm);
	SPID_CTRL |= (0x03 << SPI_MODE_gp);			// Mode 3, Rising Edge CLock
}


uint16_t GetTCPreScaler(void *tc)
//-----------------------------------------------------------------------------
//	Get the latest loaded PreScale value for the counter "tc" type 0.
//-----------------------------------------------------------------------------
{
	uint16_t PreScaler, Divisor;
	
	Divisor = (((TC0_t *)tc)->CTRLA) & TC0_CLKSEL_gm;	// Timer Clock Divisor value.
	switch(Divisor)
	{
		case TC_CLKSEL_DIV1_gc:
		{
			PreScaler = 1;
			break;
		}
		case TC_CLKSEL_DIV2_gc:
		{
			PreScaler = 2;
			break;
		}
		case TC_CLKSEL_DIV4_gc:
		{
			PreScaler = 4;
			break;
		}
		case TC_CLKSEL_DIV8_gc:
		{
			PreScaler = 8;
			break;
		}
		case TC_CLKSEL_DIV64_gc:
		{
			PreScaler = 64;
			break;
		}
		case TC_CLKSEL_DIV256_gc:
		{
			PreScaler = 256;
			break;
		}
		case TC_CLKSEL_DIV1024_gc:
		{
			PreScaler = 1024;
			break;
		}
	}
	return(PreScaler);
}
