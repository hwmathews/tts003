//	File Name: isr.c
//
//	Content:
//
//		Interrupt service routines for TTS-003
//			- 1ms Timer
//
// 	Date:  7 Sept 2018
//
//
//	Copyright 2018 Allied Analogic, Inc
//	All rights reserved
//===========================================================================
#include "TTS003.h"
#include "SnoopyComb.h"
#include "Meter.h"
#include  "Sound.h"
#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

uint8_t PreviousKeys, DebounceQty, LedIndex, LEDserviceFlag, Keys, RepeatFlag;
volatile uint8_t ISRcountStatus1, ISRcountStatus2;
uint16_t RepeatTm, RepeatCnt, Repeat;
volatile uint16_t ISRcount1, ISRcount2;

uint8_t CycleState, RepeatFlag;
uint8_t KyColumn, KyRow, ColumnShift, RowShift, Temp;


ISR(TCC4_OVF_vect)
//------------------------------------------------------------------------------
//	TCC4 Timer Generates an Interrupt every 1ms performing the following:
//
//			- Keypush Service
//			- Interval Delay Timer Update
//			- Sync Beeper Enable, and Duration update
//
//	NOTE: Priority of this interrupt is set LOW to allow the 16x ISR to interrupt
//			when updating the SineWave generator.
//-----------------------------------------------------------------------------
{
	uint8_t DelayLoop;
	
	TCC4.INTFLAGS = TC4_OVFIF_bm;
	//--------------------------------------------------------------------------
	//	Service the interval ISRentry counters.  Status flags are used rather than
	//	register values in the event an ISR occurs during the fetch/test of the
	//	counter registers.
	//
	if(ISRcount1)
	{
		ISRcount1--;
	}
	else if(ISRcountStatus1 == ACTIVE)
	{
		ISRcountStatus1 = COMPLETE;
	}

	if(ISRcount2)
	{
		ISRcount2--;
	}
	else if(ISRcountStatus2 == ACTIVE)
	{
		ISRcountStatus2 = COMPLETE;
	}

	//--------------------------------------------------------------------------
	//	Check for active keys.  If same keys are active over debounce time place
	//	the value in "KyReg".
	//--------------------------------------------------------------------------
	// Keypush Rows D7, D6, D5, and Columns D4, D3, D2. Initially all column
	// outputs are at a low level while Row inputs are configured pull-ups. Once
	// a key is pressed the associated Row input is taken low.  During this ISR
	// each column is taken high until the Row input goes back high.
	//
	// Consecutive passes equal to KEYDEBOUNCE with the same value results in
	// a key closure.
	//
	KyRow = (~PORTD.IN & ROW_MASK_gm) >> ROW_MASK_bp;		// 3 LSBits..
	if(KyRow)
	//---------------------------------------------------------------------------
	//	Rotate through all columns until KyRow goes back high.
	{
		ToneInfo.SpkrEnable = 0;
		if(SpkrTone.Flag.Bit.Enabled)
		{
			ToneInfo.SpkrEnable = (1 << SPKR_EN_bp);
		}
		KyColumn = (1 << COLUMN_MASK_bp);		//	Start with first bit position.
		ColumnShift = RowShift = 0;
		do
		{
			PORTD.OUT = KyColumn | ToneInfo.SpkrEnable;
			ColumnShift++;
			KyColumn <<= 1;
			for(DelayLoop = 0; DelayLoop < 20; DelayLoop++);
		}
		while(((~PORTD.IN & ROW_MASK_gm) >> ROW_MASK_bp) && (KyColumn & COLUMN_MASK_gm));
		PORTD.OUT = ToneInfo.SpkrEnable;				// For Next ISR.

		while(KyRow)
		{
			RowShift++;
			KyRow >>= 1;
		}

		RowShift -= 1;							// xxxx RRRCCC
		ColumnShift -=1;
		ColumnShift |= (RowShift << 2);
		Keys = KeyXlate[ColumnShift];
		if(Keys == PreviousKeys)
		{
			DebounceQty++;
			if(DebounceQty > DEBI)
			{
				if(Repeat == OFF)
				{
					KyReg = Keys;				// Pass Debounced Key to service routine.
					Repeat = ON;				//	Pass here only once per KeyValue.
					RepeatTm = KEY_RPT_TM1;	// Wait period for re-loading KeyValue.
					RepeatCnt = 0;
				}
				if(RepeatCnt != RepeatTm)
				{
					RepeatCnt++;
				}
				else
				{
					KyReg = Keys;					// Log Repeated KeyPress, and wait for another.
					RepeatCnt = 0;
					RepeatFlag = ON;
					RepeatTm = KEY_RPT_TM2;		// Successive repeat Key press time.
				}
			}
		}
		else
		{
			DebounceQty = 0;					// Different Key, reset registers.
			Repeat = OFF;
			RepeatFlag = OFF;
			PreviousKeys = Keys;
		}
	}
	else
	{
		PreviousKeys = 0;
		Repeat = OFF;
		RepeatFlag = OFF;
		DebounceQty = 0;
	}
	//
	//---------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	//	Duration flags are loaded with multiple 1mSec quantities and decremented
	//	on each ISR pass.  Once they reach ZERO the active function will be
	//	flagged to load MID_SCALE values into the associated SineTable.
	//
	if(ToneInfo.BeepStringFlag)
	{
		if(ToneInfo.BeepDuration)
		//
		// Indicates BeepDuration has been loaded..
		{
			ToneInfo.BeepDuration--;
			if(ToneInfo.BeepDuration == 0)			// Sequence tone set.
			{
				ToneInfo.pSound += 2;
				if (*(ToneInfo.pSound+1))
					SpkrBeeper();					// New freq and duration setup.
				else
				{
					ToneInfo.BeepStringFlag = OFF;	// Does NOT turn off TONE..!!..
					rSPKR_EN;
				}
			}
		}
	}
	//---------------------------------------------------------------------------
	// Service Xmit Tone Flags.
	//
	if(XmitTone.Flag.Bit.Enabled == ON)
	{
		if(XmitTone.Flag.Bit.EoT == OFF)
		{
			if(XmitTone.ActiveCnt == XmitTone.ActiveTm) 
			{
				XmitTone.Flag.Bit.EoT = ON;
				rSPKR_EN;
			}
			else
			{
				XmitTone.ActiveCnt++;
			}
		}
		//------------------------------------------------------------------------
		if((XmitTone.Flag.Bit.EoT == ON) && (XmitTone.Flag.Bit.EoS == OFF))
		{
			if(XmitTone.IdleCnt == XmitTone.IdleTm)
			{ 
				XmitTone.Flag.Bit.EoS = ON;
				XmitTone.Flag.Bit.SoS = OFF;
			}
			else
			{
				XmitTone.IdleCnt++;
			}
		}
	}
	//
	//---------------------------------------------------------------------------

}

#ifdef TRANSDUCER 
//==============================================================================
//	Beep method using AT1224 Transducer.
ISR(TCD5_CCA_vect)
//------------------------------------------------------------------------------
//	Set the Beeper square wave pulse.
{
	TCD5.INTFLAGS = TC5_CCAIF_bm;
	sBEEP;
	ToneInfo.BeepState = (1 << SPKR_EN_bp);	// Key Press Beep status.
}

ISR(TCD5_OVF_vect)
//------------------------------------------------------------------------------
//	Reset the Beeper square wave pulse and disable future Beep service ISR's if
//	the system timer has elapsed (1mSec beep enable).
//------------------------------------------------------------------------------
{
	rBEEP;
	ToneInfo.BeepState = OFF;						// Keep State during KeyPress ISR.
	TCD5.INTFLAGS = TC5_OVFIF_bm;
	if(ToneInfo.BeepStringFlag == OFF)
	//	End of tone pattern duration.
	{
		TCD5.INTCTRLA = TC45_OVFINTLVL_OFF_gc;
		TCD5.INTCTRLB = TC45_CCAINTLVL_OFF_gc;
		
	}
}
//
//==============================================================================

#else

ISR(TCD5_CCA_vect)
//-----------------------------------------------------------------------------
//	This ISR when enabled will output SineTable value until active tone sequence
//	has turned off ToneInfo.BeepStringFlag.
//-----------------------------------------------------------------------------
{
	TCD5.INTFLAGS = TC5_CCAIF_bm;
	if((SpkrTone.SineIndex) || (ToneInfo.BeepStringFlag == ON)) 
	{
		DACA_CH1DATA = SpkrSineTable[SpkrTone.SineIndex];
		SpkrTone.SineIndex = (SpkrTone.SineIndex +1) & 0x0F;		// Advance to next step.
	} 
	else if((SpkrTone.SineIndex == 0) && (ToneInfo.BeepStringFlag == OFF)) 
	{
		TCD5.INTCTRLB &= ~TC45_CCAINTLVL_HI_gc;			// Turn OFF Future ISR's
		DACA_CH1DATA = SINE_MIDSCALE;
	}
	
}

#endif

ISR(TCD5_CCB_vect)
//-----------------------------------------------------------------------------
//	This ISR when enabled will output SineTable value until active tone length
//	is less than Sync.EoT for XmitTone.
//-----------------------------------------------------------------------------
{
//		sDEBUG;



	TCD5.INTFLAGS = TC5_CCBIF_bm;
	if((XmitTone.Flag.Bit.EoT == OFF) || (XmitTone.SineIndex))
	{
		DACA_CH0DATA = XmitSineTable[XmitTone.SineIndex];
		XmitTone.SineIndex = (XmitTone.SineIndex +1) & 0x0F;		// Advance to next step.
	} 
	if((XmitTone.SineIndex == 0) && (XmitTone.Flag.Bit.EoT == ON))
	{
		TCD5.INTCTRLB &= ~TC45_CCBINTLVL_HI_gc;
		DACA_CH0DATA = SINE_MIDSCALE;
	}

//	rDEBUG;	
}





ISR(SPIC_INT_vect)
//-----------------------------------------------------------------------------
//	Program execution here due to empty SPI xmit buffer.
//-----------------------------------------------------------------------------
{
	if(SPIinfo.XmitCnt != SPIinfo.ByteCnt)
	{
		SPIC_DATA = SPIinfo.Buffer[SPIinfo.XmitCnt];
		SPIinfo.XmitCnt++;
	}
	else
	{
		SPIinfo.Status = COMPLETE;
	}
}



ISR(PORTD_INT_vect)
//-----------------------------------------------------------------------------
// The POWER KeyPush change on Port A Caused this interrupt.
// Reset the interrupt flag and service depending on request.
//	
//	ISR Level: MEDIUM....
//-----------------------------------------------------------------------------
{
	PORTD.INTFLAGS = PORT_INT7IF_bm;			// Release Interrupt flag.

}

//==============================================================================
#define VRAIL 100							// +/- Binary Rails.
ISR(ADCA_CH0_vect)
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
{
	volatile int16_t DiffValue;
	volatile uint16_t Value, Value_Real;
	uint16_t *p = waveform + wave_idx;
	uint16_t w = *p;
	

	DiffValue = ADCA.CH0.RES;						// +/-2047....
	DiffValue += 2047;								// 1-> 4095
	Value = (uint16_t)(DiffValue & 0x0fff);
	
	Comb.ADC_in = Value;								// Used for SPI request.

	if(Value < 20 || Value > ADC_MAX-20) clipped = 1;

	if(data_available) overrun++;					// Main Loop clears data_available

	// There are 400 data points taken within a 60Hz cycle.  Summation of cycle
	//	data previous points within the cycle, current point in cycle, and previous
	// cycle points are used to lock onto the 60Hz multiple.  
	if(wave_idx >= CORR_DELTA && wave_idx < SAMPLES_PER_CYCLE-CORR_DELTA)
	{
		Value_Real  = Value >> (ADC_BITS-8);
		#define corr_accum(accum, delta) accum += Value_Real * (p[delta] >> 8);
		corr_accum(corr_mid, 0)						// Current sample point sum.
		corr_accum(corr_left, -CORR_DELTA)		// Same Cycle current sample point.
		corr_accum(corr_right, CORR_DELTA)		// Previous Cycle advanced by Delta.
	}

	w -= (w >> relax_bits);
	w += Value << (EXTRA_BITS-relax_bits);
	*p = w;
	//
	//---------------------------------------------------------------------------
	//	If received command was "BandPass" compute the current value to be sent
	//	to the DAC. 
	if(Comb.FilterAction == CMD_BANDPASS)
	{
		clean = (int16_t)(signal_in - signal_smoothed) << (1+ Comb.Gain);
		if(clean < -(2047 +VRAIL)) clean = -(2047 -VRAIL);
		if(clean > (2047 -VRAIL)) clean = (2047 -VRAIL);
		pwm_out = clean +2048;
		if(SYSdata.MonitorLevel != OFF)
		{
			// out value to the speaker.
			DACA_CH1DATA = pwm_out >> (HIGH_VOL - SYSdata.MonitorLevel);
		}
		A2Dbuffer[wave_idx] = pwm_out; 
	}
	//	For Vdc values averaging will be done over the 60Hz period outsid of this
	//	ISR.
	else
	{
		A2Dbuffer[wave_idx] = Value;				// Synchronous throughput for Vdc.
	}
	
	//
	//	Advance pointer to the next sample bin for data logging.
	//
	//------------------------------------------------------------------------
	wave_idx++;
	if(wave_idx == SAMPLES_PER_CYCLE)
	{
		wave_idx = 0;
		cycle_complete = 1;
		if(relax_bits < RELAX_BITS) relax_bits++;
	}
	
	signal_in = Value;
	signal_smoothed = (w >> EXTRA_BITS);	// "w" == previous CYCLE value.

	data_available = 1;
}


ISR(TCC5_OVF_vect)
//------------------------------------------------------------------------------
// Sampling Rate Adjustment
//------------------------------------------------------------------------------
{
/*	uint32_t val;
	
	static uint8_t TestFlag = 0;
	val = CalData.adctimer_rate + CalData.adctimer_carry;
	CalData.adctimer_carry = (uint16_t)val;
	CalData.PerCnt = val >> 16;
	//	TCC5.PERBUF = CalData.PerCnt;
	TCC5.PERBUF =1333;					// Ideal sampling time for 400 points at 60Hz.
	TCC5.INTFLAGS = TC5_OVFIF_bm;
	*/
}

