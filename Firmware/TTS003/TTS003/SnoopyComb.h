//-----------------------------------------------------------------------------
//
// File Name: SnoopyComb.h
//
// Content:
//
//    Date:  14 May 2018
//
//    Header file for the Snoopy Comb Filter using ATxmega16E.
//
// Copyright 2018 Allied Analogic, Inc
// All rights reserved
//
//=============================================================================
// $Log:$
//=============================================================================

#ifndef	SNOOPYCOMB_H
#define	SNOOPYCOMB_H

//#pragma once

#include <avr/io.h>
#include <stdio.h>

////////////////////// Configuration ////////////////////////////////////
#define F_CPU	32000000UL

// Base frequency of filter.  If you want something outside of the 50Hz-60Hz
// range you may need to adjust the ADC sample rate.
#define LINE_FREQ 60.0

// Controls the selectivity of the filter.  A higher number gives a more
// precise (narrower bandwidth) filter but causes a slower response to changes
// in the base waveform.  The maximum value is 6.
#define RELAX_BITS 1		// 6
// How many cycles to wait between adjustments of the sampling rate
#define CYCLES_PER_ADJUST 1
// Magnitude of adjustments to sampling rate
#define MAJOR_ADJUST_AMOUNT 400					//200
#define MINOR_ADJUST_AMOUNT 20					//10
// The spacing used by the autocorrelation function (used for locking on to the
// target frequency)
#define CORR_DELTA 2

/////////////////////////////////////////////////////////////////////////

// These constants determine the sampling rate and must be adjusted carefully
// in order to not exhaust CPU or RAM resources

//#define ADC_PRESCALAR_BITS (_BV(ADPS2) | _BV(ADPS0))		// Division 32
// this is limited by the amount of RAM available
#define SAMPLES_PER_CYCLE 400
#define INITIAL_ADCTIMER_RATE (uint32_t)(F_CPU / LINE_FREQ / SAMPLES_PER_CYCLE * 65536.0)
//volatile uint32_t adctimer_rate;
//volatile uint16_t adctimer_carry;

#define ADC_BITS 12									// ATxmega16E, 12 bit resolution.
#define ADC_MAX ((1 << ADC_BITS)-1)
#define ADC_ZERO (1 << (ADC_BITS-1))
#define EXTRA_BITS (16-ADC_BITS)
/*
#if RELAX_BITS < 0 || RELAX_BITS > EXTRA_BITS
#error RELAX_BITS out of range
#endif
*/
// location in EEPROM to save calibration data
#define EE_ADCTIMER_RATE	(void *)(0x0010)						//((void*)2)
//#define EECAL			(uint16_t *)(0x0000)
int16_t clean;

//------------------------------------------------------------------------------
// SPI 
#define ACK	0x06										// SPI Acknowledge

//------------------------------------------------------------------------------
//	Command Structure:
//			D7		D6			D5			D4		D3		D2			D1		D0
//------------------------------------------------------------------------------
//			1		RQ Pointer			FilterAction			  Gain
enum SPI_CombCommands
{
	COMB_COMMAND	= 0x80,
	COMB_GAINx1		= (0x01 << 0),
	COMB_GAINx2		= (0x02 << 0),
	//
	// Filter settings.
	//
	CMD_BANDPASS	= (0x00 << 2),
	CMD_BANDGAP		= (0x01 << 2),
	CMD_BYPASS		= (0x02 << 2),
	CMD_CAL			= (0x03 << 2),
	CMD_CLEAN		= (0x04 << 2),
	//
	// Data Request changes pointer so next byte group to send to MASTER.
	//
	COMB_DUMMY		= (0x00 << 5),
	COMB_PTR_DAC	= (0x01 << 5),
	COMB_PTR_ADC	= (0x02 << 5),
	COMB_PTR_STATUS= (0x03 << 5),
};
#define ACTION_MASK		0x1C

enum STATUS
{
	ADJ_MAJPOS = 0x01,
	ADJ_MAJNEG = 0x02,
	ADJ_MINPOS = 0x04,
	ADJ_MINNEG = 0x08,
	SIG_CLIPPED = 0x10,
};
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//	Global variables.
//------------------------------------------------------------------------------
// The averaged base waveform
uint16_t waveform[SAMPLES_PER_CYCLE];
// Index of current location in waveform[]
volatile uint16_t wave_idx;

// Autocorrelation accumulators
volatile uint32_t corr_mid;
volatile uint32_t corr_left, corr_right;

// Input and output signals
volatile uint16_t signal_in, signal_smoothed;
// Used for stats/oscilloscope
volatile uint8_t data_available, overrun;

// Some flags
volatile uint8_t cycle_complete;
volatile uint8_t clipped, SysStatus;

// The signal output
volatile uint16_t pwm_out;

// Current selectivity of the filter, ranges from 0 to RELAX_BITS
volatile uint8_t relax_bits;

//
//-----------------------------------------------------------------------------
// Use 16 MHz RC Clock Source.... PSA divide by 4... PSBC default divide by 1.
//
#define CONFIG_SYSCLK_SOURCE			CLK_SCLKSEL_RC32M_gc
#define CONFIG_SYSCLK_PSADIV			CLK_PSADIV_4_gc
#define CONFIG_SYSCLK_PSBCDIV			CLK_PSBCDIV_1_1_gc

//-----------------------------------------------------------------------------
//	General Operation Definitions.
//-----------------------------------------------------------------------------
//#define	ON					1
//#define	OFF				0

#define	HIGH				1
#define	LOW				0

#define	ACTIVE			1
#define	COMPLETE			0

//-----------------------------------------------------------------------------
//	A2D related Definitions.
//-----------------------------------------------------------------------------
#define	A2Dsamples			2
#define	PK_ELEMENTS			8
#define	CHANNEL_MASK 		0x0f
//
//-----------------------------------------------------------------------------
// ADC resolution/shift equates.
//-----------------------------------------------------------------------------
enum A2Dbitmode
{
	A2D_8R	= 0x04,
	A2D_12R	= 0x00,
	A2D_12L	= 0x06,
};
#define	UNSIGNEDMODE		~(1 << ADC_CONMODE_bp)
#define	SIGNEDMODE			(1 << ADC_CONMODE_bp)
#define	MUX_LINEMONITOR	( 0x04 << ADC_CH_MUXPOS_gp)	
#define  MUX_BATTMONITOR	( 0x01 << ADC_CH_MUXPOS_gp)

#define	STME16MS			200		// value * 83.3uSec =16.67mS
#define	STME8MS			100		// 8ms
#define	STME4MS			50			// 4ms
#define	STME2MS			25			// 25 samples at 83.375uSec/Sample


//------------------------------------------------------------------------------ 
// Global Structures
//------------------------------------------------------------------------------
//
enum SYS_POINTER
{
	DAC_PTR,							// Default SPI transmission.
	ADC_PTR,
	STATUS_PTR,
};

struct SYS_information
{
	uint16_t ADC_in;
	uint16_t DAC_out;
	uint8_t Gain;					// 0 = Bypass, 1,2,4,8 Gain values.
	uint8_t Status;
	uint8_t FilterAction;		// Action Request.
	uint8_t DataIndex;			// Contains pointer to response of SPI request.
	uint8_t SPIState;				// Void if MSN it to be sent next.
};
struct SYS_information volatile Comb;

#define CHECKSUMOFFSET	55		// Ensures sum of 0's will not pass parity test.
struct Cal_data
{
	uint32_t adctimer_rate;
	uint16_t adctimer_carry;
	uint16_t PerCnt;
	uint16_t Parity;
};
struct Cal_data CalData;
extern struct Cal_data CalData;

volatile uint32_t   adctimer_rate;
volatile uint16_t adctimer_carry;
//
//-----------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Prototypes:
//------------------------------------------------------------------------------
int main(void);

void InitPorts(void);
void InitADC_A(void);
void InitDAC_A0(void);
void InitTCC4(void);
void InitTCC5(void);
void InitSPI_C(void);
void InitSystem(void);

void Delay(int ISRticks);
//int ACDCvalue(uint16_t Samples);
int16_t GetADC_Channel(uint8_t Channel);
uint16_t GetSRAM_DataSum(void);


#endif

