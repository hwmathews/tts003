#include <avr/io.h>
/*

void spi_set_prescale(SPI_t *spi, uint32_t DataClock);
void spi_set_prescale(SPI_t *spi, uint32_t DataClock)
//------------------------------------------------------------------------------
//	Set the SPI clock PreScaler based on the desired data rate.
//
//	Clock Rate Choices:		CLKper/4		// 00
//											/8		// 00 x
//
//	Example:		Sysclock == 8MHz
//					DataClock = 2MHz
//					if(DataClock >= 2MHz)	Set DIV4..... if(DataClock > 4MHz) double.
//------------------------------------------------------------------------------
{
	uint32_t Sysclock;
	
	Sysclock = sysclk_get_per_hz();
	
	// Reset Prescaler values.
	//
	spi->CTRL &= ~(SPI_PRESCALER_gm < SPI_PRESCALER_gp);
	spi->CTRL &= ~(SPI_CLK2X_bm);
	
	if(DataClock >= (Sysclock/4))									//8MHz/4 = 2MHz
	{
		spi->CTRL |= SPI_PRESCALER_DIV4_gc;
		if(DataClock >= (Sysclock/2))								//8MHz/2 = 4MHz
		{
			spi->CTRL |= SPI_CLK2X_bm;
		}
	}
	//---------------------------------------------------------------------------
	else if(DataClock >= (Sysclock/16))							//8MHz/16 = 500KHz
	{
		spi->CTRL |= SPI_PRESCALER_DIV16_gc;
		if(DataClock >= (Sysclock/8))								//8MHz/8	 = 1MHz
		{
			spi->CTRL |= SPI_CLK2X_bm;
		}
	}
	//---------------------------------------------------------------------------
	else if(DataClock >= (Sysclock/64))							//8MHz/64 = 125KHz
	{
		spi->CTRL |= SPI_PRESCALER_DIV16_gc;
		if(DataClock >= (Sysclock/32))							//8MHz/32 = 250MHz
		{
			spi->CTRL |= SPI_CLK2X_bm;
		}
	}
	//---------------------------------------------------------------------------
	else if(DataClock >= (Sysclock/128))						//8MHz/128 = 62.5KHz
	{
		spi->CTRL |= SPI_PRESCALER_DIV16_gc;
		if(DataClock >= (Sysclock/64))							//8MHz/32 = 125MHz
		{
			spi->CTRL |= SPI_CLK2X_bm;
		}
	}
};
*/

void spi_set_prescale(SPI_t *spi, uint32_t DataClock);
void spi_set_prescale(SPI_t *spi, uint32_t DataClock)
//------------------------------------------------------------------------------
//	Set the SPI clock PreScaler based on the desired data rate.
//
//	Clock Rate Choices:		CLKper/4		// 00
//											/8		// 00 x
//
//	Example:		Sysclock == 8MHz
//					DataClock = 2MHz
//					if(DataClock >= 2MHz)	Set DIV4..... if(DataClock > 4MHz) double.
//------------------------------------------------------------------------------
{
	uint32_t Sysclock;
	
	Sysclock = sysclk_get_per_hz();
	
	// Reset Prescaler values.
	//
	spi->CTRL &= ~(SPI_PRESCALER_gm < SPI_PRESCALER_gp);
	spi->CTRL &= ~(SPI_CLK2X_bm);
	
	if(DataClock >= (Sysclock/4))									//8MHz/4 = 2MHz
	{
		spi->CTRL |= SPI_PRESCALER_DIV4_gc;
		if(DataClock >= (Sysclock/2))								//8MHz/2 = 4MHz
		{
			spi->CTRL |= SPI_CLK2X_bm;
		}
	}
	//---------------------------------------------------------------------------
	else if(DataClock >= (Sysclock/16))							//8MHz/16 = 500KHz
	{
		spi->CTRL |= SPI_PRESCALER_DIV16_gc;
		if(DataClock >= (Sysclock/8))								//8MHz/8	 = 1MHz
		{
			spi->CTRL |= SPI_CLK2X_bm;
		}
	}
	//---------------------------------------------------------------------------
	else if(DataClock >= (Sysclock/64))							//8MHz/64 = 125KHz
	{
		spi->CTRL |= SPI_PRESCALER_DIV16_gc;
		if(DataClock >= (Sysclock/32))							//8MHz/32 = 250MHz
		{
			spi->CTRL |= SPI_CLK2X_bm;
		}
	}
	//---------------------------------------------------------------------------
	else if(DataClock >= (Sysclock/128))						//8MHz/128 = 62.5KHz
	{
		spi->CTRL |= SPI_PRESCALER_DIV16_gc;
		if(DataClock >= (Sysclock/64))							//8MHz/32 = 125MHz
		{
			spi->CTRL |= SPI_CLK2X_bm;
		}
	}
};