//	File Name: isr.c
//
//	Content:
//
//		Interrupt service routines for TTS-003
//			- 1ms Timer
//
// 	Date:  7 Sept 2018
//
//
//	Copyright 2018 Allied Analogic, Inc
//	All rights reserved
//===========================================================================
#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "TTX003.h"
#include "Meter.h"
#include "Sound.h"
#include "SnoopyComb.h"
#include "tc.h"

uint8_t PreviousKeys, DebounceQty, LedIndex, LEDserviceFlag, Keys, RepeatFlag;
volatile uint8_t ISRcountStatus1, ISRcountStatus2;
uint16_t RepeatTm, RepeatCnt, Repeat;
volatile uint16_t ISRcount1, ISRcount2;

uint8_t CycleState, RepeatFlag;
uint8_t KyColumn, KyRow, ColumnShift, RowShift, Temp;


ISR(TCD0_OVF_vect)
//------------------------------------------------------------------------------
//	TCD0 Timer Generates an Interrupt every 1ms performing the following:
//
//			- Keypush Service
//			- Interval Delay Timer Update
//			- Sync Beeper Enable, and Duration update
//
//	NOTE: Priority of this interrupt is set LOW to allow the 16x ISR to interrupt
//			when updating the SineWave generator.
//-----------------------------------------------------------------------------
{
	uint8_t DelayLoop;

		
	//--------------------------------------------------------------------------
	//	Service the interval ISRentry counters.  Status flags are used rather than
	//	register values in the event an ISR occurs during the fetch/test of the
	//	counter registers.
	//
	if(ISRcount1)
	{
		ISRcount1--;
	}
	else if(ISRcountStatus1 == ACTIVE)
	{
		ISRcountStatus1 = COMPLETE;
	}

	if(ISRcount2)
	{
		ISRcount2--;
	}
	else if(ISRcountStatus2 == ACTIVE)
	{
		ISRcountStatus2 = COMPLETE;
	}

	//--------------------------------------------------------------------------
	//	Check for active keys.  If same keys are active over debounce time place
	//	the value in "KyReg".
	//--------------------------------------------------------------------------
	// Keypush Rows D2, D1, D0, and Columns D5, D4, D3. Initially all column
	// outputs are at a low level while Row inputs are configured pull-ups. Once
	// a key is pressed the associated Row input is taken low.  During this ISR
	// each column is taken high until the Row input goes back high.
	//
	// Consecutive passes equal to KEYDEBOUNCE with the same value results in
	// a key closure.
	//
	KyRow = (~PORTC.IN & ROW_MASK_gm) >> ROW_MASK_bp;		// 3 LSBits..
	if(KyRow)
	//---------------------------------------------------------------------------
	//	Rotate through all columns until KyRow goes back high.
	{
		ToneInfo.SpkrEnable = 0;
		if(SpkrTone.Flag.Bit.Enabled)
		{
			ToneInfo.SpkrEnable = (1 << SPKR_EN_bp);
		}
		KyColumn = (1 << COLUMN_MASK_bp);		//	Start with first bit position.
		ColumnShift = RowShift = 0;
		do
		{
			PORTC.OUT = KyColumn | ToneInfo.SpkrEnable;
			ColumnShift++;
			KyColumn <<= 1;
			for(DelayLoop = 0; DelayLoop < 20; DelayLoop++);
		}
		while(((~PORTC.IN & ROW_MASK_gm) >> ROW_MASK_bp) && (KyColumn & COLUMN_MASK_gm));
		PORTC.OUT = ToneInfo.SpkrEnable;				// For Next ISR.

		while(KyRow)
		{
			RowShift++;
			KyRow >>= 1;
		}

		RowShift -= 1;							// xxxx RRRCCC
		ColumnShift -=1;
		ColumnShift |= (RowShift << 2);
		Keys = KeyXlate[ColumnShift];
		if(Keys == PreviousKeys)
		{
			DebounceQty++;
			if(DebounceQty > DEBI)
			{
				if(Repeat == OFF)
				{
					KyReg = Keys;				// Pass Debounced Key to service routine.
					Repeat = ON;				//	Pass here only once per KeyValue.
					RepeatTm = KEY_RPT_TM1;	// Wait period for re-loading KeyValue.
					RepeatCnt = 0;
				}
				if(RepeatCnt != RepeatTm)
				{
					RepeatCnt++;
				}
				else
				{
					KyReg = Keys;					// Log Repeated KeyPress, and wait for another.
					RepeatCnt = 0;
					RepeatFlag = ON;
					RepeatTm = KEY_RPT_TM2;		// Successive repeat Key press time.
				}
			}
		}
		else
		{
			DebounceQty = 0;					// Different Key, reset registers.
			Repeat = OFF;
			RepeatFlag = OFF;
			PreviousKeys = Keys;
		}
	}
	else
	{
		PreviousKeys = 0;
		Repeat = OFF;
		RepeatFlag = OFF;
		DebounceQty = 0;
	}
	//
	//---------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	//	Duration flags are loaded with multiple 1mSec quantities and decremented
	//	on each ISR pass.  Once they reach ZERO the active function will be
	//	flagged to load MID_SCALE values into the associated SineTable.
	//
	if(ToneInfo.BeepStringFlag)
	{
		if(ToneInfo.BeepDuration)
		//
		// Indicates BeepDuration has been loaded..
		{
			ToneInfo.BeepDuration--;
			if(ToneInfo.BeepDuration == 0)				// Sequence tone set.
			{
				ToneInfo.pSound += 2;
				if (*(ToneInfo.pSound+1))
				{
					Beeper();								// New freq and duration setup.
				}
				else
				{
					ToneInfo.BeepStringFlag = OFF;	// Does NOT turn off TONE..!!..
					rSPKR_EN;
				}
			}
		}
	}
	//---------------------------------------------------------------------------
	// Service Xmit Tone Flags.
	//
	if(XmitTone.Flag.Bit.Enabled == ON)
	{
		if(XmitTone.Flag.Bit.EoT == OFF)
		{
			if(XmitTone.ActiveCnt == XmitTone.ActiveTm) 
			{
				XmitTone.Flag.Bit.EoT = ON;
//				rTP9;									// Transmit Timing Debug.
				rSPKR_EN;
			}
			else
			{
				XmitTone.ActiveCnt++;
			}
		}
		//------------------------------------------------------------------------
		if((XmitTone.Flag.Bit.EoT == ON) && (XmitTone.Flag.Bit.EoS == OFF))
		{
			if(XmitTone.IdleCnt == XmitTone.IdleTm)
			{ 
				XmitTone.Flag.Bit.EoS = ON;
				XmitTone.Flag.Bit.SoS = OFF;
			}
			else
			{
				XmitTone.IdleCnt++;
			}
		}
	}
	//
	//---------------------------------------------------------------------------
	
}

ISR(TCC0_CCA_vect)
//-----------------------------------------------------------------------------
//	This ISR when enabled will output SineTable value until active tone sequence
//	has turned off ToneInfo.BeepStringFlag.
//-----------------------------------------------------------------------------
{
	if((SpkrTone.SineIndex) || (ToneInfo.BeepStringFlag == ON)) 
	{
		DACB_CH0DATA = SpkrSineTable[SpkrTone.SineIndex];
		SpkrTone.SineIndex = (SpkrTone.SineIndex +1) & 0x0F;		// Advance to next step.
	} 
	else if((SpkrTone.SineIndex == 0) && (ToneInfo.BeepStringFlag == OFF)) 
	{
		TCC0.INTCTRLB &= ~TC_CCAINTLVL_HI_gc;			// Turn OFF Future ISR's
		DACB_CH0DATA = SINE_MIDSCALE;
	}
	
}

ISR(TCC0_CCB_vect)
//-----------------------------------------------------------------------------
//	This ISR when enabled will output SineTable value until active tone length
//	is less than Sync.EoT for XmitTone.
//-----------------------------------------------------------------------------
{
//	sTP9;
	if((XmitTone.Flag.Bit.EoT == OFF) || (XmitTone.SineIndex))
	{
		DACB_CH1DATA = XmitSineTable[XmitTone.SineIndex];
		XmitTone.SineIndex = (XmitTone.SineIndex +1) & 0x0F;		// Advance to next step.
	} 
	if((XmitTone.SineIndex == 0) && (XmitTone.Flag.Bit.EoT == ON))
	{
		TCC0.INTCTRLB &= ~TC_CCBINTLVL_HI_gc;
		DACB_CH1DATA = SINE_MIDSCALE;
	}
//	rTP9;
}





ISR(SPID_INT_vect)
//-----------------------------------------------------------------------------
//	Program execution here due to empty SPI xmit buffer.
//-----------------------------------------------------------------------------
{
	if(SPIinfo.XmitCnt != SPIinfo.ByteCnt)
	{
		SPID_DATA = SPIinfo.Buffer[SPIinfo.XmitCnt];
		SPIinfo.XmitCnt++;
	}
	else
	{
		SPIinfo.Status = COMPLETE;
	}
}




//==============================================================================
#define VRAIL 100							// +/- Binary Rails.
ISR(ADCA_CH0_vect)
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
{
	volatile int16_t DiffValue;
	volatile uint16_t Value, Value_Real;
	uint16_t *p = waveform + wave_idx;
	uint16_t w = *p;
	
//	sTP9;

	DiffValue = ADCA.CH0.RES;						// +/-2047....
	DiffValue += 2047;								// 1-> 4095
	Value = (uint16_t)(DiffValue & 0x0fff);
	
	Comb.ADC_in = Value;								// Used for SPI request.

	if(Value < 20 || Value > ADC_MAX-20) clipped = 1;

	if(data_available) overrun++;					// Main Loop clears data_available

	// There are 400 data points taken within a 60Hz cycle.  Summation of cycle
	//	data previous points within the cycle, current point in cycle, and previous
	// cycle points are used to lock onto the 60Hz multiple.  
	if(wave_idx >= CORR_DELTA && wave_idx < SAMPLES_PER_CYCLE-CORR_DELTA)
	{
		Value_Real  = Value >> (ADC_BITS-8);
		#define corr_accum(accum, delta) accum += Value_Real * (p[delta] >> 8);
		corr_accum(corr_mid, 0)						// Current sample point sum.
		corr_accum(corr_left, -CORR_DELTA)		// Same Cycle current sample point.
		corr_accum(corr_right, CORR_DELTA)		// Previous Cycle advanced by Delta.
	}

	w -= (w >> relax_bits);
	w += Value << (EXTRA_BITS-relax_bits);
	*p = w;
	//
	//---------------------------------------------------------------------------
	//	If received command was "BandPass" compute the current value to be sent
	//	to the DAC. 
	if(Comb.FilterAction == CMD_BANDPASS)
	{
		clean = (int16_t)(signal_in - signal_smoothed) << (1+ Comb.Gain);
		if(clean < -(2047 +VRAIL)) clean = -(2047 -VRAIL);
		if(clean > (2047 -VRAIL)) clean = (2047 -VRAIL);
		pwm_out = clean +2048;
		if((SysData.MonitorLevel != OFF) && (TestInfo.DAC_EnableFlag == ON))
		{
			// out value to the speaker.
			DACB_CH0DATA = pwm_out >> (MAX_VOL - SysData.MonitorLevel);
		}
		A2Dbuffer[wave_idx] = pwm_out; 
	}
	//	For Vdc values averaging will be done over the 60Hz period outsid of this
	//	ISR.
	else
	{
		A2Dbuffer[wave_idx] = Value;				// Synchronous throughput for Vdc.
	}
	
	//
	//	Advance pointer to the next sample bin for data logging.
	//
	//------------------------------------------------------------------------
	wave_idx++;
	if(wave_idx == SAMPLES_PER_CYCLE)
	{
		wave_idx = 0;
		cycle_complete = 1;
		if(relax_bits < RELAX_BITS) relax_bits++;
	}
	
	signal_in = Value;
	signal_smoothed = (w >> EXTRA_BITS);	// "w" == previous CYCLE value.

	data_available = 1;
//	rTP9;
}


ISR(TCC1_OVF_vect)
//------------------------------------------------------------------------------
// Sampling Rate Adjustment
//------------------------------------------------------------------------------
{
	static uint8_t TestBit;
	
	if(TestBit == 1)
	{
		TestBit = 0;
//		rTP9;
	}
	else
	{
		TestBit =1;
//		sTP9;
	}
/*	uint32_t val;
	
	static uint8_t TestFlag = 0;
	val = SysData.adctimer_rate + SysData.adctimer_carry;
	SysData.adctimer_carry = (uint16_t)val;
	SysData.PerCnt = val >> 16;
	//	TCC5.PERBUF = SysData.PerCnt;
	TCC5.PERBUF =1333;					// Ideal sampling time for 400 points at 60Hz.
	TCC5.INTFLAGS = TC5_OVFIF_bm;
	*/
}

//******************************************************************************
//	Special TDR ISR's below.
//
ISR(TCE0_OVF_vect)
//------------------------------------------------------------------------------
//	TCE0 is used to generate a series of square waves or pulses for collection of
//	TDR data.  CCA will rise at midpoint of "PER" cycle.  On the initial cycle
//	CCB is set to trip 2T after CCA, then increment on successive cycles.  On each
//	cycle the rising edge of CCB will trigger an A2D measurement.  A total of
//	100 measurements are made and placed in A2Dbuffer for later processing.
//
//	Upon completion of 100 measurements the A2Dbuffer pointer is advanced to
//	point to the next 100 data points while the first is being processed and
//	displayed.
//
//	NOTE: All interrupts except TCE0 should be turned off during data sampling.
//------------------------------------------------------------------------------
{

}


ISR(TCE0_CCB_vect)
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
{
	if(TCE0.CTRLFCLR & 0x01)						// Down cnt, disable
	{
		TCE0.CTRLB &= ~TC_CCAEN;					// Disable TDR Output pulse.
	}
	else
	{
		TCE0.CTRLB |= TC_CCAEN;						// Enable TDR Output pulse.
	}
}

ISR(ADCA_CH1_vect)
//------------------------------------------------------------------------------
// Conversion has completed.  Save the results in the A2D buffer.
//------------------------------------------------------------------------------
{
	int16_t Value;

	if(TestInfo.TestIndex == TDRSWEEP)
	//
	//	DO TDR....
	{
		Value = ADCA.CH1.RES;
		A2Dbuffer[A2Dresult.Samples + (TDRInfo.SampleQuantity * A2Dresult.Group)]= Value;
		A2Dresult.Samples++;

		if(A2Dresult.Samples == TDRInfo.SampleQuantity)
		{
			TDRInfo.StatFlag = COMPLETE;			// Signal All samples completed.
		}

		else
		{
			TCE1.CCBBUF +=TDRInfo.StepSize;
		}
	}
	//
	//---------------------------------------------------------------------------
	// Line Audio Tracking.
	else 	if(TestInfo.TestIndex == TRACK)
	{
		Value = ADCA.CH1.RES;							// Read LSB first, then MSB.
		Value = ((~Value + 2047) & 0xfff);
		DACB_CH0DATA = Value >> TestInfo.Scale;	// Update Speaker DAC.

		// Collect values in averaging buffer until complete.  Signal external
		// program when ready to average.
		//
		if(A2Dresult.Samples < TRACKSAMPLES)
		{
			A2Dbuffer[A2Dresult.Samples] = Value;
			A2Dresult.Samples++;
		}
	}

	else if(TestInfo.TestIndex == PROBE)
	{
		Value = ADCA.CH1.RES;							// Read LSB first, then MSB.
		Value = ((~Value + 2047) & 0xfff);
		if(TestInfo.DAC_EnableFlag == ENABLE)
		{
			DACB_CH0DATA = Value >> TestInfo.Scale;	// Update Speaker DAC.
		}

		// Collect values in averaging buffer until complete.  Signal external
		// program when ready to average.
		//
		if(A2Dresult.Samples < TRACKSAMPLES)
		{
			A2Dbuffer[A2Dresult.Samples] = Value;
			A2Dresult.Samples++;
		}
	}
}
//
//******************************************************************************

ISR(TCD0_ERR_vect)
{
	static uint8_t TestBit;
	
	if(TestBit == 1)
	{
		TestBit = 0;
		rTP9;
	}
	else
	{
		TestBit =1;
		sTP9;
	}

}


ISR(PORTC_INT0_vect)
//-----------------------------------------------------------------------------
// The POWER KeyPush change on Port A Caused this interrupt.
// Reset the interrupt flag and service depending on request.
//	
//	ISR Level: MEDIUM....
//-----------------------------------------------------------------------------
{
	PORTC.INTFLAGS = PORT_INT0IF_bm;			// Release Interrupt flag.

}

