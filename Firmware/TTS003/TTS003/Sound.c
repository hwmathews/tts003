#include "Sound.h"

//=============================================================================
// Sound arrays consist of tone in hertz and duration in milliseconds, terminated
// with double "0".
//-----------------------------------------------------------------------------
/*
const uint16_t PwrUpSound[9][2] = {
	{1400, 100},
	{0,200},
	{1800, 100},
	{0,200},
	{2200, 100},
	{0,200},
	{2600, 100},
	{0,200},
	{0, 0}
};
*/
const uint16_t PwrUpSound[9][2] = {
	{1400, 100},
	{0,20},
	{1800, 100},
	{0,20},
	{2200, 100},
	{0,20},
	{2600, 100},
	{0,200},
	{0, 0}
};


const uint16_t PwrDwnSound[9][2] = {
	{1300, 100},
	{0,200},
	{1100, 100},
	{0,20},
	{900, 100},
	{0,20},
	{700, 100},
	{0,20},
	{0, 0}
};


const uint16_t TalkBeepSound[7][2] = {
	{1000, 100},
	{0,20},
	{1000, 100},
	{0,20},
	{1000, 100},
	{0,200},
	{0, 0}
};
/*
const uint16_t Acknowledge[3][2] = {
	{800, 100},
	{0,200},
	{0,0},
};
*/

const uint16_t ToneBuzzSound[9][2] ={
	{1600, dit},
	{0,cwspace},
	{1600, dah},
	{0,200},
	{0, 0}
};

const uint16_t OKSound[9][2] ={
	{800, dit},
	{0,cwspace},
	{800, dah},
	{0,cwspace},
	{800, dit},
	{0,200},
	{0, 0}
};


const uint16_t CalibrateSound[7][2] ={
	{800, dit},
	{0,cwspace},
	{800, dit},
	{0,cwspace},
	{800, dit},
	{0,200},
	{0, 0}
};
/*
const uint16_t ToneLevelAdjSound[9][2] ={
	{800, dit},
	{0,cwspace},
	{800, dah},
	{0,cwspace},
	{800, dit},
	{0,200},
	{800, dit},
	{0,200},
	{0, 0}
};


const uint16_t BeeperAdjSound[7][2] ={
	{800, dit},
	{0,cwspace},
	{800, dah},
	{0,cwspace},
	{800, dit},
	{0,200},
	{0, 0}
};
*/
const uint16_t KPsound[3][2] ={
	{2200, dit},
	{0,200},
	{0, 0}
};
const uint16_t KPerror[3][2] ={
	{400, dit},
	{0,200},
	{0, 0}
};
