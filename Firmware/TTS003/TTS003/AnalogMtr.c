
//----------------------------------------------------------------------------
//	File Name: AnalogMtr.c
//
//	Content:
//
//		Analog Meter LCD display functions for NHD-C12864LZ-FSW-FSW-FBW-3v3 using
//		ST7565R display controller driver.
//
// 	Date:  20 Oct 2014
//
//	Copyright 2014 Allied AnaLogic, Inc
//	All rights reserved
//
//===========================================================================

#include "lcd.h"
#include "string.h"
#include "TTS003.h"
#include "math.h"
#include "Meter.h"
#include <avr/pgmspace.h>
#

struct AnalogMeter Mtr60 =
{
	64,64,				// Circle Bottom xyPosition (DataWindow Reference)
	60,					// Circle Radius
	52,					// xPosition @ 30 degrees (R *cos30).
	42,					// R * Cos45.
};
struct AnalogMeter Mtr54 =
{
	64,64,				// Circle Bottom xyPosition (DataWindow Reference)
	52,					// Circle Radius
	45,					// xPosition @ 30 degrees (R *cos30).
	37,					// R * Cos45.
};
//
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//	Analog Meter Face TickMark Strings for Balance Meter.
//-----------------------------------------------------------------------------
const char F0v[] = "90";
const char F1v[] = "92";
const char F2v[] = ".";
const char F3v[] = "96";
const char F4v[] = ".";
const char F5v[] = "100";

//	Digit placement offset adjustment from computed for meter face.
//
char *BalMsg[] =
{
	{ 	&F0v[0]},
	{ 	&F1v[0]},
	{ 	&F2v[0]},
	{ 	&F3v[0]},
	{ 	&F4v[0]},
	{ 	&F5v[0]},
	{ 	&F4v[0]},
	{ 	&F3v[0]},
	{ 	&F2v[0]},
	{ 	&F1v[0]},
	{ 	&F0v[0]},
};



uint8_t	PreviousPercent;



void LinearAnalogFace(struct AnalogMeter *Type)
//-----------------------------------------------------------------------------
//	Draw the analog meter face with 11 linear tick marks (volts).  "pStrings[]"
//	points to an array of string pointers for values of each tick.
//-----------------------------------------------------------------------------
{
	int LineWidth, LineHeight, Tindex, Xpixel, Ypixel, Voffset;
	char NumStrg[4];
	uint8_t CharWidth, CharHeight, Pass;
	

	//--------------------------------------------------------------------------
	//	Diagonal lines will be drawn from top to bottom with x,y coordinates.
	//
	LineWidth = 2;									// Width of Vertical tick mark.
	LineHeight = 2;								// Height of Vertical tick mark.
	CharHeight = 10;								// Based on TB14
//	CharWidth = 12;
	CharWidth = 20;
	Voffset = 2;
	
	Pass = 0;
	for(Tindex = 0; Tindex <= 100; Tindex += 10)
	{
		Xpixel = LocateMeter_X(Tindex, Type);
		Ypixel = LocateMeter_Y(Xpixel, Type);		//	Returns with corrected Vert Offset.
		
		Xpixel += Type->x;						// Horizontal Position Offset.
		if(!(Pass & 0x01))
		{
			// Open Special Window for meter face letters.
			//
			Window.x = (uint16_t)(Xpixel - (CharWidth/2));
			Window.w = (CharWidth);
			Window.y = Ypixel -Voffset;
			Window.h = (CharHeight) -Voffset;
			Window.b = SBORDER;
			LCD_ClearWindow(NOBORDER, STD);
//			sprintf(&NumStrg[0], "%d", Tindex/10);			// 0- 10V
//			sprintf(&NumStrg[0], "%d", Tindex/2);			// 0-50V
			sprintf(&NumStrg[0], "%d", Tindex/1);			// 0 -100v
			LCD_CtrStrg(NumStrg, FONTVWX5, C_MIDDLE, STD);
		}
		else
		{
			LCD_DrawLine(Xpixel - (LineWidth/2), Ypixel, LineHeight, (LineWidth/2), STD);
		}
		Pass +=1;
	}
}




void AnalogNeedle(uint8_t Percent, struct AnalogMeter *Type)
//------------------------------------------------------------------------------
// Using parameters in the structure "Type" locate and plot the radial for
//	the "Percent" of full deflection.
//
//	Prior to plotting the needle position the Previous radial must be erased.
//
//	"Type" structure contains an "x" value for sin30 and sin45 in order to locate
//	a corresponding "Y" coordinate.
//
//------------------------------------------------------------------------------
{
	uint16_t xPixel, yPixel;
	
	LCD_OpenWindow(FULL_W, NOBORDER);
	if((PreviousPercent != Percent) &&
	((PreviousPercent >=0) || (PreviousPercent <= 100)) )
	{
		xPixel = LocateMeter_X(PreviousPercent, Type);	// Locate linear "X".
		yPixel = LocateMeter_Y(xPixel, Type);			// With "X" now find "Y".
//		LCD_DiagonalLine(xPixel+ Type->x, Type->x, yPixel, Type->y, SOLID);
		LCD_DiagonalLine(xPixel +Type->x, yPixel, Type->x, Type->y, NEEDLE_CLR);
	}
	if(PreviousPercent != Percent)
	// Using the same process, locate the X,Y for the new percentage.
	//
	{
		xPixel = LocateMeter_X(Percent, Type);		// Returns +/- "X" value.
		yPixel = LocateMeter_Y(xPixel, Type);
//		LCD_DiagonalLine(xPixel +Type->x, Type->x, yPixel, Type->y,	SOLID);
		LCD_DiagonalLine(xPixel +Type->x, yPixel, Type->x, Type->y, NEEDLE_FILL);
	}
	PreviousPercent = Percent;
}


void BalanceFace(struct AnalogMeter *Type)
//-----------------------------------------------------------------------------
//	Draw the analog meter face with 11 linear tick marks (volts).  "pStrings[]"
//	points to an array of string pointers for values of each tick.
//-----------------------------------------------------------------------------
{
	int LineWidth, LineHeight, Tindex, Xpixel, Ypixel, Voffset;
	char NumStrg[4];
	uint8_t CharWidth, CharHeight, Pass;
	
	//--------------------------------------------------------------------------
	//	Diagonal lines will be drawn from top to bottom with x,y coordinates.
	//
	LineWidth = 2;									// Width of Vertical tick mark.
	LineHeight = 2;								// Height of Vertical tick mark.
	CharHeight = 10;								// Based on TB14
	CharWidth = 20;
	Voffset = 4;
	
	Pass = 0;
	for(Tindex = 0; Tindex <= 100; Tindex += 10)
	{
		Xpixel = LocateMeter_X(Tindex, Type);
		Ypixel = LocateMeter_Y(Xpixel, Type);		//	Returns with corrected Vert Offset.
		
		Xpixel += Type->x;						// Horizontal Position Offset.
		// Open Special Window for meter face letters.
		//
		Window.x = (uint16_t)(Xpixel - (CharWidth/2));
		Window.w = (CharWidth);
		Window.y = Ypixel -Voffset;
		Window.h = (CharHeight) -Voffset;
		Window.b = SBORDER;
//		LCD_ClearWindow(NOBORDER, STD);
		LCD_CtrStrg(BalMsg[Tindex/10], FONTVWX5, C_MIDDLE, STD);
	}
}


void BalanceNeedle(uint8_t Percent, struct AnalogMeter *Type)
//------------------------------------------------------------------------------
// Using parameters in the structure "Type" locate and plot the radial for
//	the "Percent" of full deflection.
//
//	Prior to plotting the needle position the Previous radial must be erased.
//
//	"Type" structure contains an "x" value for sin30 and sin45 in order to locate
//	a corresponding "Y" coordinate.
//
//------------------------------------------------------------------------------
{
	int16_t xPixel, yPixel, Color;

	if((PreviousPercent != Percent) &&
	((PreviousPercent >=0) || (PreviousPercent <= 100)) )
	{
		xPixel = LocateMeter_X(PreviousPercent, Type);	// Locate linear "X".
		yPixel = LocateMeter_Y(xPixel, Type);			// With "X" now find "Y".
		LCD_DiagonalLine(xPixel+ Type->x, yPixel, Type->x, Type->y, NEEDLE_CLR);
	}
	if(PreviousPercent != Percent)
	// Using the same process, locate the X,Y for the new percentage.
	//
	{
		if(Percent >= 50)		Color = RED;
		else                 Color = GREEN;
		xPixel = LocateMeter_X(Percent, Type);		// Returns +/- "X" value.
		yPixel = LocateMeter_Y(xPixel, Type);
		LCD_DiagonalLine(xPixel +Type->x, yPixel, Type->x, Type->y, NEEDLE_FILL);
	}
	PreviousPercent = Percent;
}

int LocateMeter_X(uint8_t Percent, struct AnalogMeter *Mtr)
//------------------------------------------------------------------------------
//	Using the Percent range of +/- R*cos30 locate the "X" coordinate along the
//	circle radius based on parameters found in "Mtr".
//------------------------------------------------------------------------------
{
	int Xa;
	
	if(Percent <= 50)
	{
		Xa = (-Mtr->x30) + ((Percent * Mtr->x30 *2)/100);		// -139 -> 0
	}
	else
	{
		Xa = ((Percent -50) * Mtr->x30 * 2)/100;					// 0 -> 139
	}
	return(Xa);
}


int LocateMeter_Y(int Xa, struct AnalogMeter *Type)
//------------------------------------------------------------------------------
//	Locate Meter "Y" graph point based on known "X" linear position.  On entry Xa
// is the +/- horizontal position equating to meter position.  The range of Xa
// is +/- R * cos30.
//
//							 |.y
//                    |...y
//							 |....y
//					-x.....0.....+x			// Meter Position.
//
//	Find the corresponding "Y" position to draw a radius or graphic symbol.
//------------------------------------------------------------------------------
{
	int16_t Yb;
	
	if(Xa >= 0)						// Q1,  positive slope, 0 -> 90 degrees.
	{
		Yb = Ypoint(Xa, Type);
		//		LCD_PutPixel( Xa +Xcenter, Ybottom-Yb, ORANGE);
	}
	
	else								// Q2, negative slope.
	{
		Yb = Ypoint(abs(Xa), Type);
		//		LCD_PutPixel( Xa +Xcenter, Ybottom-Yb, GREEN);
	}
	return(Type->y -Yb);
}



int16_t Ypoint(int Xa, struct AnalogMeter *Mtr)
//------------------------------------------------------------------------------
//	On entry "Xa" has the linear "X" coordinate for a circle from which to plot a
// diagonal line, and the structure "MTR" contains parameter information as
//	follows:
//				Radius
//				30 degree point
//				45 degree point
//				x, y display offset
//	Using the Bresenham's algorithm via Wikipedi rotate the "x" position from
//	0 or 90 degree point until x == Xa, then return with "Yb".
//
//	NOTE:	There are two sections involved, 0 -> 45 degrees, and 90 -> 45.  The
//			sign of Xa will determine if the result is in Quadrant 0 or 1.
//
//			Xa range:  0-> Mtr.x30		// R * cos30 for 120 degree meter movement.
//------------------------------------------------------------------------------
{
	int16_t x, y, xChange, yChange, RadiusError, Radius;
	
	Radius = Mtr->r;
	
	if(Xa >= Mtr->a45)
	//
	//	Q0, Sector 0.  Plots from 0-> 45 degrees.
	{
		// Plots from 0->315 degrees  with Xa == 0 -> 113
		//
		x = Radius;								// Begin at 0 degrees
		y = 0;
		xChange = 1 - (Radius << 1);		//  1 -2*R
		yChange = 0;
		RadiusError = 0;

		while( (x >= y) && (x != Xa))
		{
			//			LCD_PutPixel(160 +x, 214 -y,GREEN);	// Inverts "Y" to plot starting at yPos.
			
			y++;
			RadiusError += yChange;
			yChange += 2;
			if (((RadiusError << 1) + xChange) > 0)
			{
				x--;
				RadiusError += xChange;
				xChange += 2;
			}
		}
	}
	//
	//---------------------------------------------------------------------------
	else if(Xa < Mtr->a45)
	{
		// Plot 90 -> 45 with Xa == 0 -> 113.
		//
		y = Radius;								// Begin at 0 degrees
		x = 0;
		yChange = 1 - (Radius << 1);		//  1 -2*R
		xChange = 0;
		RadiusError = 0;

		while( (y >= x) && (x != Xa))
		{
			//			LCD_PutPixel(x +160, 214-y,ORANGE);
			
			x++;
			RadiusError += xChange;
			xChange += 2;
			if (((RadiusError << 1) + yChange) > 0)
			{
				y--;
				RadiusError += yChange;
				yChange += 2;
			}
		}
	}
	return(y);
}

