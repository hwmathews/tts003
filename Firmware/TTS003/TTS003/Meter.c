//------------------------------------------------------------------------------
//
// File Name: Meter.c
//
//	Date:  25 Dec 2012
//
// Content:
//
//
// Copyright 2012 Allied Analogic, Inc
// All rights reserved
//
//=============================================================================
// $Log:$
//=============================================================================
//-----------------------------------------------------------------------------
// Header file Includes
//-----------------------------------------------------------------------------
#include	<stdio.h>
#include	<avr/io.h>
#include "TTS003.h"
#include "Meter.h"
#include "lcd.h"
#include "math.h"
#include "string.h"
#include <avr/wdt.h>

enum ADCgain{
	ADC_GAINx1
	};



void CheckBatteryVoltage(void)
//------------------------------------------------------------------------------
//	Check the System Battery Voltage.  If voltage within usable limits update
//	battery display on a condition change (Bstat != PreviousBstat).
//------------------------------------------------------------------------------
{
	GetBatteryVoltage();
	if(Bstat == BSHUTDOWN)
	{
		PowerDown();
	}
	else
	{
		BIupdate();
	}
}

 
void GetBatteryVoltage(void)
//-----------------------------------------------------------------------------
//	Check the system battery voltage.  Return with battery status flag.
//	Returned Status:
//					HIGH				// 3 Bars
//					MED				// 2 Bars
//					LOW				// 1 Bar
//					Critical			// Operates, but accuracy not guaranteed. 
//					ShutDown			// System turns Off.....
//					
//		Vbat divisor:		R79	49.9K
//								R81	12.1K
//		NOTE:	Max voltage = 95% of Vref (1.0V)
//-----------------------------------------------------------------------------
{
	uint8_t Channel, ADC_CtrlB, EventInfo, RefInfo;
	volatile uint16_t BitValue;
	float Vbat;

	ADC_CtrlB = ADCA.CTRLB;					// Don't disturb CONVMODE (signed/unsigned bit)
	EventInfo = ADCA.EVCTRL;				// Setup for Program control sampling.
	RefInfo = ADCA.REFCTRL;
	
	ADCA.EVCTRL = 0;							// Clear event driven operationo for ADC.
	ADCA.REFCTRL &= ~ADC_REFSEL_gm;		// Vrev Internal 1 Volt.
	ADCA.REFCTRL |= ADC_REFSEL_INT1V_gc;
	Channel = 0;
	SetADCSingleChannel(Channel, ADC_CH_MUXPOS_PIN0_gc);

	BitValue = ADCBitValue(Channel, STME4MS);
	Vbat = ((1.0 *BitValue)/(4095)) * ((R5+R3)/R5);
	//
	// Divide by 2 when using 2S battery.
	Vbat /= 2;
	//
	//	Put the ADC control data back to the way it was on entry.
	ADCA.CTRLB = ADC_CtrlB;
	ADCA.REFCTRL = RefInfo;	
	ADCA.EVCTRL = EventInfo;

	if(Vbat > 3.8)
	{
		Bstat = BFULL;
	}
	else if(Vbat > 3.6)
	{
		Bstat = BHALF;
	}
	else if(Vbat > 3.4)
	{
		Bstat = BEMPTY;
	}
	else if(Vbat > 3.2)							// System Uncal'd if Vbat < 3.3
	{
		Bstat = BCRITICAL;
	}
	else
	{
		Bstat = BSHUTDOWN;
	}
}

	
//==============================================================================
// Below code from STX-101

/*
uint16_t GetSingleEndedBitValue (uint8_t POSmux)
//------------------------------------------------------------------------------
//	Get Single Ended BitValue on Desired MUXPOS pin.
//
//	NOTE:  On return the ADC has been setup with differential reference settings.
//------------------------------------------------------------------------------
{
	uint8_t Channel, ADC_CtrlB;
	uint16_t BitValue;
	
	Channel = 0;
	ADC_CtrlB = ADCA.CTRLB;						// Don't disturb CONVMODE (signed/unsigned bit)
	ADCA.REFCTRL &= ~ADC_REFSEL_gm;			// Vrev Internal 1 Volt.
	ADCA.REFCTRL |= ADC_REFSEL_INT1V_gc;
	SetADCSingleChannel(Channel, POSmux);

	BitValue = ADCBitValue(Channel, STME4MS);
	ADCA.CTRLB = ADC_CtrlB;
	ADCA.REFCTRL &= ~ADC_REFSEL_gm;			// PortA0 Vref to 1.65v.
	ADCA.REFCTRL |= ADC_REFSEL_AREFD_gc;	// Back to normal Vref....
	
	return BitValue;
}
*/

/*
void SetADCSingleChannel(uint8_t ADC_channel, uint8_t POSmux)
//-----------------------------------------------------------------------------
//	Setup ADCA channel number for SingleEnded mode 12bit, unity gain, on the
//	requested CPU PIN number.
//
//			CONVMODE		== 0	Unsigned			// Vbat measurement.
//			CONVMODE		==	1	Signed			// NOT USED.
//			INPUTMODE	== 01	Single Ended
//
//			MUXPOS:		ADC0 -> ADC15
//-----------------------------------------------------------------------------
{
	ADC_CH_t *pChannel;

	// Clear Free Run, Set UNsigned Mode, 12bit Right justified.
	//
	ADCA.CTRLB = 0x00;

	// Assign a pointer to ADCA ch0 and advance to the "ADC_channel"
	//
	pChannel = &(ADCA.CH0.CTRL);
	pChannel += ADC_channel;			// Channel 0->4.

	//	Update the operating Mode and select the pin for measurement.
	//
	(*pChannel).CTRL	= 0x00;			// Clear Single/Diff Bits.
	(*pChannel).CTRL	= ADC_CH_INPUTMODE_SINGLEENDED_gc;
	
	(*pChannel).MUXCTRL	&= ~ADC_CH_MUXPOS_gm;		// Positive Channel.
	(*pChannel).MUXCTRL	|= POSmux;
}
*/

void SetADCDiffGain(uint8_t NEGmux, uint8_t Gain)
//-----------------------------------------------------------------------------
//	Set the A2D differential on ADC1 with gain == ADC_CH_GAIN_nX_gc.
//
//				CONVMODE		== 1									// Signed
//				INPUTMODE	== 11									// Differential With Gain
//				Gain "n"    == x1, x2,x4,x8,x16,x32,x64	// Gain Values
//
//				MUXPOS:	PIN1 (ADC1 Signal input)			// Options:	ADC0 -> 7
//				MUXNEG:	PIN7 (ADC7 Vcc/2 = Vref)			//				ADC4 -> 7
//-----------------------------------------------------------------------------
{
	//--------------------------------------------------------------------------
	// Setup for Differential mode, Unity Internal CPU GAIN.
	//
	ADCA.CTRLB = 0x00;										//	Clear Free Run..
	ADCA.CTRLB |= (1 << ADC_CONMODE_bp);				// Signed Mode.
	
	ADCA.CH0.CTRL &= ~ADC_CH_INPUTMODE_gm;				// Clear Single/Diff Bits.
	ADCA.CH0.CTRL |= ADC_CH_INPUTMODE_DIFFWGAINH_gc;// 4MSBits for MUX NEG
	ADCA.CH0.CTRL &= ~ADC_CH_GAIN_gm;					// Clear Previous Gain.
	ADCA.CH0.CTRL |= (Gain << ADC_CH_GAIN_gp);		// Gain Group Position.
	
	ADCA.CH0.MUXCTRL = 0;
//	ADCA.CH0.MUXCTRL &= ~ADC_CH_MUXPOS_gm;				// Negative Channel.
	ADCA.CH0.MUXCTRL |= ADC_CH_MUXPOS_PIN8_gc;
	
//	ADCA.CH0.MUXCTRL &= ~ADC_CH_MUXNEG_gm;				// Positive Channel.
	ADCA.CH0.MUXCTRL |= NEGmux;							// ADC8
	//
	//--------------------------------------------------------------------------
}


int ADCBitValue(uint8_t Channel, uint8_t Samples)
//------------------------------------------------------------------------------
//  Get the DC Bit Value on the ADC Channel for the interval Samples where
//  Samples = # of 1mSec Ticks to Average.
//
//	NOTE: On entry SetADCSingleChannel() must have initialized ADCA channel number.
//			Xmega ADC used in UNsigned Mode has a 200bit offset (See XmegaAU ADC
//			data sheet for explanation)
//------------------------------------------------------------------------------
{
	uint16_t SampleIndex;
	long Accumulator;

	ADC_CH_t volatile *pChannel;

	// Assign a pointer to ADCA ch0 and advance to the "ADC_channel"
	//
	pChannel = &(ADCA.CH0.CTRL);
	pChannel += Channel;										// Channel 0->4.


	Accumulator = 0;
	ADCA.CTRLA |= (1 << ADC_ENABLE_bp);					// Enable A2D.
	ADCA.CTRLA |= (1 << ADC_FLUSH_bp);
	ADCA.CTRLA &= ~(1<< ADC_FLUSH_bp);
	for(SampleIndex = 0; SampleIndex < Samples; SampleIndex++)
	{
		(*pChannel).INTFLAGS |= (1 << ADC_CH_IF_bp);		// Clear Interrupt Flag.
		(*pChannel).CTRL |= ADC_CH_START_bm;					// Start Conversion.
		DelayMS(1);
//		while(!((*pChannel).INTFLAGS & ADC_CH_IF_bm));	// Wait for conversion to Complete.
		Accumulator += (*pChannel).RES;
	}

	Accumulator /= Samples;
	Accumulator -= 200;								// Internal Offset in UNsigned mode
	ADCA.CTRLA &= ~(1 << ADC_ENABLE_bp);		// Disable A2D.
	return((int16_t)Accumulator);
}

int16_t PeakDetector(void)
//------------------------------------------------------------------------------
//	Sampling Process Completed with values placed in global A2Dbuffer. Determine
//	the Highest and lowest values in the sample array.  
//	
//	Upon completion A2Dresult structure will contain an averaged High/Low value,
//	and Vpp.
//
//	Returned value is average integer value over ADC sample interval.
//------------------------------------------------------------------------------
{
	int16_t  HIndex, LIndex, Index, Value;
	uint16_t SampleIndex, LowestHigh, HighestLow;


	A2Dresult.Accumulator = 0;
	A2Dresult.LowPeak = 0;
	A2Dresult.HighPeak = 0;
	HIndex = LIndex = 0;

	//	Find the Average Value to insert into the Peak Detector Buffers.
	//
	for(SampleIndex = 0; SampleIndex < A2Dresult.Samples; SampleIndex++)
	{
		A2Dresult.Accumulator += A2Dbuffer[SampleIndex];
	}
	Value = (uint16_t)(A2Dresult.Accumulator/A2Dresult.Samples);


	for(Index = 0; Index < PK_ELEMENTS; Index++)	// Clear High Peak Array buffer.
	{
		HighArray[Index] = Value;
		LowArray[Index] = Value;
	}
	//---------------------------------------------------------------------------
	//	Find the highest and lowest value within the Sample Buffer.
	//
	for(SampleIndex = 0; SampleIndex < A2Dresult.Samples; SampleIndex++)
	{
		LowestHigh = 4095;
		for(Index = 0; Index < PK_ELEMENTS; Index++)
		{
			if(HighArray[Index] < LowestHigh)
			{
				HIndex = Index;
				LowestHigh = HighArray[Index];
			}
		}
		// Point to the Highest LowValueArray element.
		//
		HighestLow = 0;
		for(Index = 0; Index < PK_ELEMENTS; Index++)
		{
			if(LowArray[Index] > HighestLow)
			{
				LIndex = Index;
				HighestLow = LowArray[Index];
			}
		}
		//
		// If the value is lower than the lowest, put the value in the low array,
		// else if value is higher than the highest, put it in the high array.
		//
		if(A2Dbuffer[SampleIndex] > HighArray[HIndex])
		{
			HighArray[HIndex] = A2Dbuffer[SampleIndex];
		}
		else if (A2Dbuffer[SampleIndex] < LowArray[LIndex])
		{
			LowArray[LIndex] = A2Dbuffer[SampleIndex];
		}
		
	}
	//--------------------------------------------------------------------------
	// Average the high peak and low peak elements.  The difference is the Peak
	// to Peak AC value.
	//
	for(Index = 0; Index < PK_ELEMENTS; Index++)
	A2Dresult.HighPeak += HighArray[Index];
	for(Index = 0; Index < PK_ELEMENTS; Index++)
	A2Dresult.LowPeak += LowArray[Index];

	A2Dresult.HighPeak /= PK_ELEMENTS;
	A2Dresult.LowPeak /= PK_ELEMENTS;
	A2Dresult.Vpp = (A2Dresult.HighPeak) -	(A2Dresult.LowPeak);
	return (Value);
}

int16_t VppDetector(void)
//------------------------------------------------------------------------------
//	Rapidly compute the Vpp bit value of A2Dbuffer[] Array.
//
//	NOTE:	The values in A2Dbuffer are "unsigned 0->4096".
//			Return with averaged value over sampled interval (Vdc), and
//			A2Dresult.Vpp containing averaged Vpp.
//------------------------------------------------------------------------------
{
	//	uint16_t Hprevious[PK_ELEMENTS], Lprevious[PK_ELEMENTS];
	long LowAccumulator, HighAccumulator;
	unsigned long AvgAccumulator;
	uint16_t SampleIndex, PK_index, Value;

	for(PK_index = 0; PK_index < PK_ELEMENTS; PK_index++)
	{
		HighArray[PK_index] = 2048;
		LowArray[PK_index] = 2048;
	}
	
	PK_index = 0;
	AvgAccumulator = 0;
	while(PK_index < PK_ELEMENTS)
	{
		for(SampleIndex = 0; SampleIndex < A2Dresult.Samples; SampleIndex++)
		{
			if(PK_index == 0)
			{
				AvgAccumulator += A2Dbuffer[SampleIndex];
			}
			if(A2Dbuffer[SampleIndex] > HighArray[PK_index])
			{
				HighArray[PK_index] = A2Dbuffer[SampleIndex];
				A2Dbuffer[SampleIndex] = 2048;
			}
			else if(A2Dbuffer[SampleIndex] < LowArray[PK_index])
			{
				LowArray[PK_index] = A2Dbuffer[SampleIndex];
				A2Dbuffer[SampleIndex] = 2048;
			}
		}
		PK_index++;
	}
	
	LowAccumulator = 0;
	HighAccumulator = 0;
	for(PK_index = 0; PK_index < PK_ELEMENTS; PK_index++)
	{
		LowAccumulator += LowArray[PK_index];
		HighAccumulator += HighArray[PK_index];
	}
	A2Dresult.Vpp = (HighAccumulator - LowAccumulator)/PK_ELEMENTS;
	AvgAccumulator /= A2Dresult.Samples;
	return((int16_t)(AvgAccumulator));
}
//
//==============================================================================





void SetADCSingleChannel(uint8_t ADC_channel, uint8_t POSmux)
//-----------------------------------------------------------------------------
//	Setup ADCA channel number for SingleEnded mode 12bit, unity gain, on the
//	requested CPU PIN number.
//	
//			CONVMODE		== 0	Unsigned			// Vbat measurement.
//			CONVMODE		==	1	Signed			// NOT USED.
//			INPUTMODE	== 01	Single Ended
//			
//			MUXPOS:		ADC0 -> ADC15
//-----------------------------------------------------------------------------
{
	ADC_CH_t *pChannel;

	// Clear Free Run, Set UNsigned Mode, 12bit Right justified.
	//
	ADCA.CTRLB = 0x00;

	// Assign a pointer to ADCA ch0 and advance to the "ADC_channel"
	//
	pChannel = &(ADCA.CH0.CTRL);
	pChannel += ADC_channel;			// Channel 0->4.

	//	Update the operating Mode and select the pin for measurement.
	//
	(*pChannel).CTRL	= 0x00;			// Clear Single/Diff Bits.
 	(*pChannel).CTRL	= ADC_CH_INPUTMODE_SINGLEENDED_gc;
	
	(*pChannel).MUXCTRL	&= ~ADC_CH_MUXPOS_gm;		// Positive Channel.
	(*pChannel).MUXCTRL	|= POSmux;
}



